function mss_plot_transect(STA,CTD,MIX)
% plot a transect of MSS data

close all;

%% specify plotting parameters

PlotTitle = 'T6';

ZMax      = -5;                  % vertical plotting area
ZMin      = -80;
Dz        = 0.2;                 % vertical plotting resolutionA
TextPos   = [0.05 0.2];           % position of annotation text labels in normalized axes coordinates
TextColor = [0.0 0.0 0.0];       % color of annotation text labels

LatNIOZ   = 54.9455;

%SigLevels = [1:0.5:20];
SigLevels = [5.7:0.05:6.2];

XAxis     = 'Lat (deg)';          % possible: Lat (deg), Lon (deg), Yearday                                 
%XAxis     = 'Yearday';

%% create plotting arrays


Z     = [ZMin:Dz:ZMax];           % z-levels for interpolation
ZBot  = - [STA.H];

NProf = length(STA);
NZ    = length(Z);

Lat     = [STA.LAT];
Lon     = [STA.LON];
DateStr = {STA.date};
DateNum = datenum(DateStr);
DateVec = datevec(DateNum);
Year    = DateVec(1,1);
Yearday = DateNum - datenum(Year,1,1,0,0,0);

ZBotNIOZ = interp1(Lat,ZBot,LatNIOZ);
ZTopNIOZ = ZBotNIOZ + 30;


if isfield(CTD,'O2')
    isO2 = true;
else
    isO2 = false;
end

if isfield(CTD,'Turb')
    isTurb = true;
else
    isTurb = false;
end

%% pre-allocate arrays

T     = NaN(NZ,NProf);
S     = NaN(NZ,NProf);
Sig   = NaN(NZ,NProf);
N2    = NaN(NZ,NProf);
Eps   = NaN(NZ,NProf);
Krho  = NaN(NZ,NProf);

if isO2
    o2    = NaN(NZ,NProf);
end

if isTurb
    Turb  = NaN(NZ,NProf);
end

%% fill-in data and interpolate on fixed levels


% data with CTD resolution
for i=1:NProf
   
   % depth 
   ZTmp   = -CTD(i).P;
   Ind    = ~isnan(ZTmp);
   ZTmp   = ZTmp(Ind);
   
 
   % temperature
   Var    = CTD(i).T(Ind);
   T(:,i) = interp1(ZTmp,Var,Z);
   
   % salinity
   Var    = CTD(i).S(Ind);
   S(:,i) = interp1(ZTmp,Var,Z);   
 
   % density
   Var    = CTD(i).SIGTH(Ind);
   Sig(:,i) = interp1(ZTmp,Var,Z);
   
   % buoyancy frequency
   Var    = CTD(i).N2(Ind);
   N2(:,i) = interp1(ZTmp,Var,Z);  
      
   if isO2
       % O2
       Var    = CTD(i).O2(Ind);
       O2(:,i) = interp1(ZTmp,Var,Z);
   end

   
end

% data with turbulence resolution
for i=1:NProf
    
   % depth
   ZTmp   = -MIX(i).P;
   Ind    = ~isnan(ZTmp);
   ZTmp   = ZTmp(Ind);
   
   % dissipation rate
   Var    = MIX(i).eps(Ind);
   Eps(:,i) = log10(interp1(ZTmp,Var,Z));
   
   % diffusivity
   Var       = MIX(i).Krho(Ind);
   Krho(:,i) = log10(interp1(ZTmp,Var,Z));


end

keyboard


%Save stratification
% For T3 : 
% $$$ t0   = datenum(2010,3,5,10,10,0);
% $$$ tf   = datenum(2010,3,5,10,20,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1C_T3.mat N2ave EPSave Z Zbottom
% For T4 : 
% $$$ t0   = datenum(2010,3,5,16,45,0);
% $$$ tf   = datenum(2010,3,5,16,55,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1C_T4.mat N2ave EPSave Z Zbottom
% For T5 : 
% $$$ t0   = datenum(2010,3,6,20,47,0);
% $$$ tf   = datenum(2010,3,6,20,57,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1C_T5.mat N2ave EPSave Z Zbottom

% For T7 (T1B)
% $$$ t0   = datenum(2010,3,7,15,09,0);
% $$$ tf   = datenum(2010,3,7,15,19,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1B_T7.mat N2ave EPSave Z Zbottom

% For T8 (T1B)
% $$$ t0   = datenum(2010,3,7,21,34,0);
% $$$ tf   = datenum(2010,3,7,21,44,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1B_T8.mat N2ave EPSave Z Zbottom

% For T9 (T1B)
% $$$ t0   = datenum(2010,3,8,4,28,0);
% $$$ tf   = datenum(2010,3,8,4,38,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1B_T9.mat N2ave EPSave Z Zbottom



%% T-S relationship
% For T3 : 
t0   = datenum(2010,3,5,10,0,0);
tf   = datenum(2010,3,5,10,30,0); 
n = datenum(DateStr);
I = find(n>=t0 & n<=tf);

TVec = [];
sigVec = []; 
for i = 1:length(I)
    TVec = [TVec; CTD(I(i)).T];
    sigVec = [sigVec; CTD(I(i)).SIGT];
end

plot(TVec, sigVec, '.k')

% For T4 : 
% $$$ t0   = datenum(2010,3,5,16,45,0);
% $$$ tf   = datenum(2010,3,5,16,55,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1C_T4.mat N2ave EPSave Z Zbottom
% For T5 : 
% $$$ t0   = datenum(2010,3,6,20,47,0);
% $$$ tf   = datenum(2010,3,6,20,57,0); 
% $$$ n = datenum(DateStr);
% $$$ I = find(n>=t0 & n<=tf);
% $$$ N2ave = nanmean(N2(:,I),2);
% $$$ EPSave = nanmean(Eps(:,I),2);
% $$$ Zbottom = nanmean(ZBot(I));
% $$$ save N2_T1C_T5.mat N2ave EPSave Z Zbottom


% find appropriate x-axis
switch XAxis
    case 'Lon (deg)'
        XVal = Lon;
    case 'Lat (deg)'
        XVal = Lat;
    case 'Yearday'
        XVal = Yearday;
    otherwise
        error('Unkown type for XAxis.');
end


%% basic axes layout for all plots
AxesXRange = 0.8;
AxesYRange = 0.85;
AxesLeft   = 0.1;
AxesBottom = 0.1;
AxesYSpace = 0.05;


%% plot temperature transect

figure('Unit','normalized','Position',[0.2, 0.2, 0.55, 0.7])


% calculate axis for upper plot
AxesHeight = (AxesYRange - AxesYSpace)/2;           
AxisY      = AxesBottom + AxesHeight + AxesYSpace;  

ax = axes('Position',[AxesLeft AxisY AxesXRange AxesHeight ]);

hold on;

% plot temperature
colormap(ax,jet)
contour(XVal,Z,T,'Fill','on','LineColor',[0 0 0],'LineStyle','none','LevelStep',0.01);
colorbar;
%caxis([2 9])
caxis([1.3 1.8])

% add density contours
hold on;
[c,h]=contour(XVal,Z,Sig,SigLevels,'k','LineWidth',1);

% add bottom line
line(XVal,ZBot,'Color','k','LineWidth',2); 

% add mooring positions
line([LatNIOZ LatNIOZ],[ZBotNIOZ ZTopNIOZ]);

% add cast markers
line(XVal,ones(size(XVal))*ZMax,'Marker','d','LineStyle','None', ... 
    'MarkerSize',6,'MarkerFaceColor','k','MarkerEdgeColor','k');


% % add station numbers
for kn=1:length(XVal)
    %text(XVal(kn),ZMax,STA(kn).fname(end-8:end-4),'Rotation',90)
    text(XVal(kn),ZMax,STA(kn).fname(end-4:end),'Rotation',90)    
end

% set axis and labels
set(gca,'FontSize',14,'YLim',[ZMin ZMax],'Layer','Top');
set(gca,'XTickLabel',{});
ylabel('z (m)');

text(TextPos(1),TextPos(2),[PlotTitle ': Temperature (\circ C)'],'FontSize',14,'Units','normalized','Color',TextColor);

box on;
hold off;


%% plot dissipation rate transect

AxisY      = AxesBottom;

ax = axes('Position',[AxesLeft AxisY AxesXRange AxesHeight ]);


% plot dissipation rate
colormap(ax,flipud(hot));
contour(XVal,Z,Eps,'Fill','on','LineColor',[0 0 0],'LineStyle','none','LevelStep',0.1);
colorbar;
caxis([-9.5 -6])


% add density contours
hold on;
[c,h]=contour(XVal,Z,Sig,SigLevels,'w','LineWidth',1);

% add bottom line
line(XVal,ZBot,'Color','k','LineWidth',2); 

% add mooring positions
line([LatNIOZ LatNIOZ],[ZBotNIOZ ZTopNIOZ]);

%add cast markers
line(XVal,ones(size(XVal))*ZMax,'Marker','d','LineStyle','None', ... 
    'MarkerSize',6,'MarkerFaceColor','k','MarkerEdgeColor','k'); 


% set axes and labels
set(gca,'FontSize',14,'YLim',[ZMin ZMax],'Layer','Top');

xlabel(XAxis);
ylabel('z (m)');
text(TextPos(1),TextPos(2),[PlotTitle ': log_{10}[\epsilon (W/kg)]'],'FontSize',14,'Units','normalized','Color',TextColor);

box on;
hold off;


figure(10)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 20 17])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.08; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
ADCP = load('../VMADCP/velo_at_T1C_T3.mat')

[Y, I] = min(abs(LatNIOZ-XVal));
subplot(141)
plot(T(:,I), Z, 'k', 'lineWidth', 2)
hold on
plot(sqrt(real(ADCP.vm.currMean).^2 + imag(ADCP.vm.currMean).^2)*50, ADCP.vm.hab-(ADCP.vm.bottomMean-1.2), 'lineWidth', 2)
plot(sqrt(real(ADCP.f.currMean).^2 + imag(ADCP.f.currMean).^2)*50, ADCP.f.hab-(ADCP.f.bottomMean-1.5),'r', 'lineWidth', 2)
set(gca, 'xgrid', 'on', 'ygrid', 'on')
ylabel('Depth (m)', 'fontSize', 12, 'fontWeight', 'bold')
xlabel('T (^{\circ}C)', 'fontSize', 12, 'fontWeight', 'bold')
ylim([-65 0])
xlim([0 10])
set(gca, 'fontSize', 12)
adjust_space

% VM_shear
U = real(ADCP.vm.currMean);
V = imag(ADCP.vm.currMean);
dz = 2;
dudz = gradient(U)./dz;
dvdz = gradient(V)./dz;
S2_vm = dudz.^2 + dvdz.^2;

% F_adcp shear
U = real(ADCP.f.currMean);
V = imag(ADCP.f.currMean);
II = ~isnan(U);
U(II(end)) = NaN;
V(II(end)) = NaN;
dz = 0.5;
dudz = gradient(U)./dz;
dvdz = gradient(V)./dz;
S2 = dudz.^2 + dvdz.^2;

subplot(142)
semilogx(N2(:,I), Z, 'k', 'lineWidth', 2)
hold on
semilogx(S2, ADCP.f.hab-(ADCP.f.bottomMean-1.5), 'r', 'lineWidth', 2)
semilogx(S2_vm, ADCP.vm.hab-(ADCP.vm.bottomMean-1.2), 'b', 'lineWidth', 2)
set(gca, 'xtick', [1e-8 1e-5 1e-1])
set(gca, 'xgrid', 'on', 'ygrid', 'on')
set(gca, 'yticklabel', [])
xlabel('N^2 (s^{-2})', 'fontSize', 12, 'fontWeight', 'bold')
xlim([9e-9 2e-1])
ylim([-65 0])
set(gca, 'fontSize', 12)
adjust_space

subplot(143)
plot(Eps(:,I), Z, 'k', 'lineWidth', 2)
set(gca, 'xgrid', 'on', 'ygrid', 'on')
set(gca, 'yticklabel', [])
xlabel('log_{10} (\epsilon (W kg^{-1}))', 'fontSize', 12, 'fontWeight', 'bold')
ylim([-65 0])
xlim([-11 -6])
set(gca, 'fontSize', 12)
adjust_space

subplot(144)
plot(Krho(:,I), Z, 'k', 'lineWidth', 2)
set(gca, 'xgrid', 'on', 'ygrid', 'on')
set(gca, 'yticklabel', [])
xlabel('log_{10} (K (m^2 s^{-1}))', 'fontSize', 12, 'fontWeight', 'bold')
xlim([-7 -1])
ylim([-65 0])
set(gca, 'fontSize', 12)
adjust_space

% $$$ subplot(155)
% $$$ FO2 = 10.^(Krho(:,I)).*gradient(O2(:,I))./abs(diff(Z(1:2))); 
% $$$ semilogx(FO2, Z, 'k', 'lineWidth', 2)

set(gcf, 'renderer', 'painters')
print('-depsc2', './TNEK_subplot.eps')  


keyboard


%% plot additional parameters (if available)

figure('Unit','normalized','Position',[0.25, 0.15, 0.55, 0.7])


nSub = 2; % maximum 2 additional parameters at the moment

% calculate axes height
AxesHeight = (AxesYRange - (nSub-1)*AxesYSpace)/nSub;

% cacluate axis position
AxisY      = AxesBottom + (nSub-1)*(AxesHeight + AxesYSpace);

axes('Position',[AxesLeft AxisY AxesXRange AxesHeight ])

hold on;

if isO2
    % plot oxygen
    contour(XVal,Z,O2,'Fill','on','LineColor',[0 0 0],'LineStyle','none','LevelStep',1);
    colorbar;
    caxis([25 45])

else
    % plot salinity
    contour(XVal,Z,S,'Fill','on','LineColor',[0 0 0],'LineStyle','none','LevelStep',0.01);
    colorbar;
    %caxis([5 17])
    caxis([7.3 7.5])    

end

    
% add density contours
hold on;
[c,h]=contour(XVal,Z,Sig,SigLevels,'k','LineWidth',1);

% add bottom line
line(XVal,ZBot,'Color','k','LineWidth',2);

% add mooring positions
line([LatNIOZ LatNIOZ],[ZBotNIOZ ZTopNIOZ]);

%add cast markers
line(XVal,ones(size(XVal))*ZMax,'Marker','d','LineStyle','None', ...
    'MarkerSize',6,'MarkerFaceColor','k','MarkerEdgeColor','k');

% add station numbers
for kn=1:length(XVal)
    %text(XVal(kn),ZMax,STA(kn).fname(end-8:end-4),'Rotation',90)
    text(XVal(kn),ZMax,STA(kn).fname(end-4:end),'Rotation',90)    
end

% set axis and labels
set(gca,'FontSize',14,'YLim',[ZMin ZMax],'Layer','Top');
set(gca,'XTickLabel',{})

ylabel('z (m)');

if isO2
  text(TextPos(1),TextPos(2),[PlotTitle ': Saturation (%)'],'FontSize',14,'Units','normalized','Color',TextColor);
else
  text(TextPos(1),TextPos(2),[PlotTitle ': Salinity (g/kg)'],'FontSize',14,'Units','normalized','Color',TextColor);
end

box on;
hold off;




%% plot N2 transect

% calculate axis position
AxisY      = AxesBottom + (nSub-2)*(AxesHeight + AxesYSpace);

ax = axes('Position',[AxesLeft AxisY AxesXRange AxesHeight ]);

hold on;



% plot N2
colormap(ax,flipud(hot));
contour(XVal,Z,log10(abs(N2)),'Fill','on','LineColor',[0 0 0],'LineStyle','none','LevelStep',0.02);
colorbar;
%caxis([-3 -0.5])
caxis([-6 -4])


% add density contours
hold on;
%[c,h]=contour(XVal,Z,Sig,SigLevels,'k','LineWidth',1);

% add bottom line
line(XVal,ZBot,'Color','k','LineWidth',2);

% add density contours
hold on;
%[c,h]=contour(XVal,Z,Sig,SigLevels,'k','LineWidth',1);


%add cast markers
line(XVal,ones(size(XVal))*ZMax,'Marker','d','LineStyle','None', ...
    'MarkerSize',6,'MarkerFaceColor','k','MarkerEdgeColor','k');

% set axis and labels
set(gca,'FontSize',14,'YLim',[ZMin ZMax],'Layer','Top');

xlabel(XAxis)
ylabel('z (m)');

text(TextPos(1),TextPos(2),[PlotTitle ': log_{10}[N^2 (s^{-2})]'],'FontSize',14,'Units','normalized','Color',TextColor);


box on;
hold off;

