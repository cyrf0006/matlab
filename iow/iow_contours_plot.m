%% Limit the domain
I = find(timeVec>=t0 & timeVec<=tf);
J = find(zVec>=z0 & zVec<=zf);
maxZ = max(zVec);

myTime = timeVec(I);
myZ = zVec(J);
myT = Tbin(J,I);


% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 20;%cm
paperheight = 18;%cm
FS = 14;


%% plot
disp('plotting...')
h = figure(1);
clf
contourf(myTime,abs(myZ-maxZ),myT, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,abs(myZ-maxZ),myT, V2, 'k')
datetick
ylabel('hab(m)', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
datetick
xlim([t0 tf])
xlabel(XLABEL, 'fontSize', FS, 'fontWeight', 'bold')
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 

adjust_space
pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.1;
cbPos(4) = cbPos(4)-3*.05;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)
datetick('x', 7, 'keeplimits')
if exist('CAXIS')
    caxis(CAXIS);
end

%ylim(maxZ-[z0 zf])



