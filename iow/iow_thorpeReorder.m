clear all
close all

%% Few parameters
tempFile = 'Titp_T1C_p1std_1secAve.mat';
nSkip = 0;
g = 9.81;

% $$$ % T5
% $$$ TSrelFile = '/home/cyrf0006/research/NIOZ/IOW/MSS_DATA/temp_rho_relFile_iow_T5.mat';
% $$$ t0   = datenum(2010,3,6,20,22,0);
% $$$ tf   = datenum(2010,3,6,20,50,0); 
% $$$ z0 = 20;
% $$$ zf = 28;
% $$$ smoothingWindow = 10; % minutes (for S2, N2 smoothing)

% T4
TSrelFile = '/home/cyrf0006/research/NIOZ/IOW/MSS_DATA/temp_rho_relFile_iow_T4.mat';
t0   = datenum(2010,3,6,20,22,0);
tf   = datenum(2010,3,6,20,50,0); 
z0 = 20;
zf = 28;
smoothingWindow = 10; % minutes (for S2, N2 smoothing)


% $$$ timeWindowMinutes = 60;
% $$$ windowSize = timeWindowMinutes/1440; % in days
% $$$ g = 9.81;
% $$$ dz = .6;
% $$$ totalDepth = 919;
% $$$ V = 5:.05:10;
% $$$ myRi = []; myGa = [];


%% Load temperature
load(tempFile);


%% Prepare data
% make sure matrix is flip correctly
[zVec, I] = sort(zVec);
T = Tbin(I,:);

% reduce number of pts according to nskip
if nSkip ~=0
    T = T(:,1:nSkip:end);
    timeSensor = timeSensor(1:nSkip:end);
end
I1 = find(timeVec>=t0 & timeVec<=tf);
I2 = find(zVec>=z0 & zVec<=zf);
T1 = T(I2,I1);
timeVec = timeVec(I1);
zVec = zVec(I2);
dz = abs(diff(zVec(1:2)));

%% From T to sigma
% to potential temperature
pt = gsw_pt0_from_t(T1*0+35,T1,zVec);

% to density
load(TSrelFile);
rho1 = polyval(p, T1)+1000;


%% Reorder vertical profiles
disp('Reordering profiles')
windowRho = rho1;
windowTime = timeVec;
H = size(windowRho,1);

% N2 calculation
[windowRhoSort, I] = sort(windowRho,1);
[dRx, dRz] = gradient(windowRhoSort);
N2bg = g./windowRhoSort.*dRz./dz;

APEFVec = []; 
N2Vec = []; 
S2Vec = []; 
LtVec = []; 
LovtVec = [];
JbVec = []; JbVec1 = []; JbVec2 = []; JbVec_eddy = [];
epsVec = []; epsVec1 = []; epsVec2 = [];
epsMat = nan(length(zVec), length(windowTime));
maxdVec = [];
for j = 1:length(windowTime)
    rhoVec_raw =  windowRho(:,j);
    [rhoVec_sort, I] = sort(rhoVec_raw);
    d = zVec - zVec(I);
    EP1 = sum(rhoVec_raw.*(max(zVec)-zVec)); 
    EP2 = sum(rhoVec_sort.*(max(zVec)-zVec));
    rho0 = nanmean(windowRho(:,j));
    maxdVec = [maxdVec max(d)];
    
    % local stratification
    N2local = g./rho0.*gradient(rhoVec_sort)./gradient(zVec);
% $$$     EP1 = sum(rhoVec_raw.*(max(zVec)-zVec').*sqrt(N2local)); 
% $$$     EP2 = sum(rhoVec_sort.*(max(zVec)-zVec').*sqrt(N2local));    
    N2Vec = [N2Vec nanmean(N2local)];   
    APEFVec = [APEFVec g./H/rho0.*(EP1-EP2)];
    JbVec = [JbVec g./H/rho0.*(EP1-EP2).*nanmean(sqrt(N2local))];
% $$$     JbVec = [JbVec g./H/rho0.*(EP1-EP2)];
    JbVec_eddy = [JbVec_eddy g./H/rho0.*(EP1-EP2)./(rms(d).^(2/3).*(.64*rms(d).^2.*nanmean(sqrt(N2local)).^3).^(-1/3))];

    epsVec = [epsVec .64*rms(d).^2.*nanmean(sqrt(N2local)).^3];

    %local shear
    %S2Vec = [S2Vec nanmean(S2bg(:,j))];
    
    % background stratification
    I = find(windowTime>(windowTime(j)-smoothingWindow/1440/2) & windowTime<(windowTime(j)+smoothingWindow/1440/2));
    N2 = N2bg(:,I);
    N2 = N2(:);
    N = sqrt(nanmean(N2));
% $$$     S2 = S2bg(:,I);
% $$$     S2 = S2(:);
% $$$     S = sqrt(nanmean(S2));
    JbVec1 = [JbVec1 g./H/rho0.*(EP1-EP2).*N];
    epsVec1 = [epsVec1 .64*rms(d).^2.*N.^3]; 
    
    LtVec = [LtVec rms(d)];
    LovtVec = [LovtVec max(d)];
    
    %JbVec2 = [JbVec2 g./H/rho0.*(EP1-EP2).*S];
    epsVec2 = [epsVec2 .64*rms(d).^2.*N.^3]; 
    
    epsMat(:,j) = .64*d.^2.*N.^3;    
end
disp('  -> done!')
 

semilogy(timeVec, epsVec1)
hold on
semilogy(timeVec, epsVec, 'r')


%% Compute Re
% $$$ Re = hst_Re('./roc12.mat',zVec, maxdVec, timeVec);
% $$$ Re = nanmean(Re);
% $$$ 

keyboard

%% Few plots.
% With ghamma timeseries
%APEF_vs_Thorpe_plot3
%APEF_vs_Thorpe_plot4

% No gamma timeseries
APEF_vs_Thorpe_plot5
%APEF_vs_Thorpe_plot6


%end
keyboard



%% Mixing efficiency distribution
GamVec = JbVec./epsVec;
I = find(GamVec>1);
GamVec(I) = 1;
dclass = .05;
classes = dclass/2:dclass:1-dclass/2;
GamClass = nan(size(classes));
for i = 1:length(classes)
    I = find(GamVec>classes(i)-dclass/2 & GamVec<=classes(i)+dclass/2);
    GamClass(i) = length(I)./length(GamVec);
end 

figure(2)
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 15 12])
bar(classes, GamClass, 'k')
xlabel('\Gamma')
ylabel('f')
hold on
med = median(GamVec);
plot([med med], [0 max(GamClass)], '--k')
text(med+dclass/2, max(GamClass)-dclass/5, sprintf('med = %3.2f',med))
text(med+dclass/2, max(GamClass)-dclass/2.5, sprintf('n = %d',length(GamVec)))
xlim([0 1])
ylim([0 ceil(max(GamClass)*100)/100])
set(gca, 'fontSize', 10)
set(gcf, 'renderer', 'painters')
print('-dpng', 'GammaHistogram.png')
print('-depsc2','GammaHistogram.eps')



keyboard
dt = 40/60/24;
timeVec = round(timeSensor(1)*24)/24:dt/2:timeSensor(end);
GammaVec = nan(size(timeVec));

for i = 1:length(timeVec)   
    I = find(timeSensor>=timeVec(i)-dt/2 & timeSensor<timeVec(i)+dt/2);
    GammaVec(i) = nanmean(JbVec(I))./nanmean(epsVec(I));
end

figure
plot(timeVec, GammaVec, 'k', 'linewidth', 2)
ylabel('\Gamma')
datetick


%% R3 comments on N2 vs Gamma:
figure
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 15 12])
loglog(APEFVec./LtVec.^2, 1./N2Vec, '.k')
hold on
I = find(JbVec./epsVec>.25);
loglog(APEFVec(I)./LtVec(I).^2, 1./N2Vec(I), '.m')
ylabel('N^{-2}', 'fontSize', 12, 'fontWeight', 'bold')
xlabel('APEF*Lt^{-2}','fontSize', 12, 'fontWeight', 'bold')
xlim([5e-8 1e-5])
ylim([4e4 4e6])  
legend('\gamma<0.25', '\gamma>0.25', 'fontWeight', 'bold')
set(gca, 'fontSize', 12)
print('-dpng', '-r300', 'Gamma_vs_N2.png')