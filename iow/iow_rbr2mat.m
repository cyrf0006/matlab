function iow_rbr2mat(configFile)

% usage ex:
% iow_rbr2mat('RBR_T1A.config')
    
%% Open and read config file
fid = fopen(configFile);
tline = fgetl(fid);
while ischar(tline)
  eval(tline);
  tline = fgetl(fid);
end
fclose(fid);    

%% load list file
fid = fopen(listRBR);
C = textscan(fid, '%s', 'delimiter', '\n');
files = char(C{1});
noFiles = size(files, 1); 

if noFiles ~= length(habRBR)
    disp('Number of files does not match depth vector size (quit)')
    return
end


%% Loop on files
TMat = [];

for i = 1:noFiles
    
    fname = files(i, :);
    I = find(fname==' '); %remove white space  
    fname(I) = [];
    disp(fname);
    
    fid = fopen(fname);
 
    C = textscan(fid, '%s', 'delimiter', '\n');
    fileText = char(C{1});
    
    fileText(1:19,:) = [];
    dateText = fileText(:,1:19);
    fileText(:,1:19) = [];
    TVec = str2num(fileText);
    
    dtRBR = diff(abs(datenum(dateText(1:2,:))));
    % this is MUCH! faster, but time vector not always good size
    timeSensor = datenum(dateText(1,:)):dtRBR:datenum(dateText(end,:));
    if length(timeSensor) > length(TVec)
        timeSensor = timeSensor(1:length(TVec));
    elseif length(timeSensor) < length(TVec)
        TVec = TVec(1:length(timeSensor));
    end
    
    for itime = 1:length(timeVec)
        I = find(timeSensor>=timeVec(itime)-dt/2 & timeSensor<=timeVec(itime)+dt/2);
        Titp(itime) = nanmean(TVec(I));
    end
    
     TMat = [TMat; Titp];
end

%% Quick plot of the result    
figure(1)
clf
imagesc(timeVec, habRBR, TMat); shading flat
datetick
ylabel('mab')


habVec = habRBR;
zVec = totalDepth - habVec;
save(outFile, 'TMat', 'timeVec', 'zVec', 'habVec')




