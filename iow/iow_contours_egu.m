clear all
close all
myColor1 = [0.0, 0.55, 0.55];
myColor2 = [1.0, 0.0, 0.5];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ------------- S1 - All ------------ %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load Titp_S1_p4std_10minAve.mat
t0 = min(timeVec);
tf = max(timeVec);
z0 = min(zVec);
zf = max(zVec);
V1 = 0:.1:10;
V2 = 0:1:10;
XLABEL = 'Feb./March 2010';


box1 = [datenum(2010, 2, 28, 22, 0, 0), datenum(2010, 2, 28, 23, 0, 0), 2.5,7.5];
box2= [datenum(2010, 2, 28, 22, 0, 0), datenum(2010, 2, 28, 23, 0, 0), 25,30];
box3 = [datenum(2010, 3, 1, 12, 15, 0), datenum(2010, 3, 1, 12, 50, 0), 22,28];
box4 = [datenum(2010, 3, 2, 3, 0, 0), datenum(2010, 3, 2, 9, 0, 0), 0,30];

I = find(timeVec>=t0 & timeVec<=tf);
J = find(zVec>=z0 & zVec<=zf);
maxZ = max(zVec);

myTime = timeVec(I);
myZ = zVec(J);
myT = Tbin(J,I);


% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 20;%cm
paperheight = 16;%cm
FS = 14;


%% plot
disp('plotting...')
h = figure(1);
clf
contourf(myTime,abs(myZ-maxZ),myT, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,abs(myZ-maxZ),myT, V2, 'k')
datetick
ylabel('hab(m)', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
datetick
xlim([t0 tf])
xlabel(XLABEL, 'fontSize', FS, 'fontWeight', 'bold')
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 

adjust_space
pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.1;
cbPos(4) = cbPos(4)-3*.05;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)
datetick('x', 7, 'keeplimits')
if exist('CAXIS')
    caxis(CAXIS);
end

line([box1(1) box1(2) box1(2) box1(1) box1(1)], [box1(3) box1(3) box1(4) box1(4) box1(3)], 'color', myColor1, 'lineWidth', 3)
line([box2(1) box2(2) box2(2) box2(1) box2(1)], [box2(3) box2(3) box2(4) box2(4) box2(3)], 'color', myColor1, 'lineWidth', 3)
line([box3(1) box3(2) box3(2) box3(1) box3(1)], [box3(3) box3(3) box3(4) box3(4) box3(3)], 'color', myColor1, 'lineWidth', 3)
line([box4(1) box4(2) box4(2) box4(1) box4(1)], [box4(3) box4(3) box4(4) box4(4) box4(3)], 'color', myColor1, 'lineWidth', 3)
text(box1(1)-60/1440, box1(3), '1b.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(box2(1)-60/1440, box2(3), '1a.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(box3(1)-60/1440, box3(3), '2.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(box4(1)-60/1440, box4(3), '3.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(datenum(2010, 3, 1, 12, 0, 0),15,'$\rightarrow$' , 'interpreter','latex','fontSize', 25, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'left', 'verticalAlignment', 'bottom')

text(datenum(2010, 3, 1, 12, 0, 0),15,'Deepening caused','fontSize', 14, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(datenum(2010, 3, 1, 12, 0, 0),15,'by a storm','fontSize', 14, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'top')

set(gcf, 'renderer', 'painters')
print(h, '-depsc', 'iow_contours_S1_egu.eps')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---------- T1B - all -------------- %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load Titp_T1B_p2std_5minAve.mat
t0 = min(timeVec);
tf = max(timeVec);
z0 = min(zVec);
zf = max(zVec);
V1 = 0:.1:10;
V2 = 0:1:10;
XLABEL = '7-8 March 2010';

box1= [datenum(2010, 3, 7, 23, 0, 0), datenum(2010, 3, 8, 1, 30, 0), 0,2];
box2= [datenum(2010, 3, 7, 19, 0, 0), datenum(2010, 3, 7, 21, 0, 0), 6,11];


I = find(timeVec>=t0 & timeVec<=tf);
J = find(zVec>=z0 & zVec<=zf);
maxZ = max(zVec);

myTime = timeVec(I);
myZ = zVec(J);
myT = Tbin(J,I);

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 20;%cm
paperheight = 16;%cm
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 paperwidth paperheight])
FS = 14;


%% plot
disp('plotting...')
h = figure(2);
clf
contourf(myTime,abs(myZ-maxZ),myT, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,abs(myZ-maxZ),myT, V2, 'k')
datetick
ylabel('hab(m)', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
datetick
xlim([t0 tf])
xlabel(XLABEL, 'fontSize', FS, 'fontWeight', 'bold')
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 

adjust_space
pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.1;
cbPos(4) = cbPos(4)-3*.05;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)
datetick('x', 15, 'keeplimits')
if exist('CAXIS')
    caxis(CAXIS);
end

line([box1(1) box1(2) box1(2) box1(1) box1(1)], [box1(3) box1(3) box1(4) box1(4) box1(3)], 'color', myColor2, 'lineWidth', 3)
line([box2(1) box2(2) box2(2) box2(1) box2(1)], [box2(3) box2(3) box2(4) box2(4) box2(3)], 'color', myColor2, 'lineWidth', 3)
text(box1(1)-20/1440, box1(3), '5.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'bottom')
text(box2(1)-20/1440, box2(4), '4.', 'fontSize', 18, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'right', 'verticalAlignment', 'middle')

set(gcf, 'renderer', 'painters')
print(h, '-depsc', 'iow_contours_T1B_egu.eps')
