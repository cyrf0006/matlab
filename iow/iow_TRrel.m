clear all 
close all

%% Info to be edited
% T4
load T1_4.mat
TLIM = [2 9];
t0   = datenum(2010,3,5,16,40,0);
tf   = datenum(2010,3,5,17,10,0); 
dataFile = 'temp_rho_relFile_iow_T4.mat';
figureFile = 'temp_rho_iow_T4.eps';
% $$$ % T5
% $$$ load T1_5.mat
% $$$ TLIM = [3 9];
% $$$ t0   = datenum(2010,3,6,20,47,0);
% $$$ tf   = datenum(2010,3,6,20,57,0); 
% $$$ dataFile = 'temp_rho_relFile_iow_T5.mat';
% $$$ figureFile = 'temp_rho_iow_T5.eps';

%% Dealing with MSS info
Lat     = [STA.LAT];
Lon     = [STA.LON];
DateStr = {STA.date};
n = datenum(DateStr);
I = find(n>=t0 & n<=tf);

%% get data
TVec = [];
RVec = []; 
for i = 1:length(I)
% $$$     temp = CTD(I(i)).T;
% $$$     sal = CTD(I(i)).S;
% $$$     pres = CTD(I(i)).P;
% $$$     
% $$$     [SA, ~] = gsw_SA_from_SP(sal,pres,STA(I(i)).LON,STA(I(i)).LAT);
% $$$     CT = gsw_CT_from_t(SA,temp,pres);
% $$$     % [N2_single, p_mid] = gsw_Nsquared(SA_single(:,i),CT_single(:,i),p_new,LAT(i));
% $$$     sig0 = gsw_sigma0(SA,CT);
% $$$ 
% $$$     TVec = [TVec; CT];
% $$$     RVec = [RVec; sig0];    
    TVec = [TVec; CTD(I(i)).T];
    RVec = [RVec; CTD(I(i)).SIGTH];
end
% Clean T,S
I = find(TVec>=TLIM(1) & TVec <= TLIM(2));
TVec = TVec(I);
RVec = RVec(I);


%% Fit computation
[Y, I] = sort(TVec);
YY  = RVec(I);
p = polyfit(TVec(I), RVec(I), 7);
Tfit = Y;
Rfit = polyval(p, Tfit);


%% Figure
figure(2)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 16])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.08 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.04; % very right of figure
tops = 0.05; % top of figure
bots = 0.18; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

subplot(121)

plot(RVec, TVec, '.k')
hold on
plot(Rfit, Tfit, 'color', [1 1 1]*.5, 'lineWidth', 2) 

%Rfit = Y*p(1) + p(2);
R = corrcoef(Rfit, YY);
%title(sprintf('[%2.1f-%2.1f]degC / R=%3.2f', TLIM(1), TLIM(2), R(2)))
set(gca, 'tickdir', 'out')
ylabel('\theta_0(^{\circ}C)', 'fontSize', 14, 'fontWeight', 'bold')
XLAB = xlabel('\sigma_{0} (kg m^{-3})', 'fontSize', 14, 'fontWeight', 'bold');
ylim(TLIM)
xlim([6 12])
text(11.5, 2.5, 'a', 'fontSize', 16, 'fontWeight', 'bold')
%text(31.95, 8.75, '\sigma_{fit} = a_3T^3 + a_2T^2 + a_1T^1 + a_0', 'fontSize', 9)
adjust_space
XLAB_POS = get(XLAB, 'pos');
XLAB_POS(2) = 1.5;
set(XLAB, 'pos', XLAB_POS);


subplot(122)
plot(YY-Rfit, Y, '.k')
hold on
plot([0 0], [min(Y) max(Y)], 'color', [.5 .5 .5], 'lineWidth', 2)
%xlabel('$\rm \sigma_{1000}-\,~\tilde{\sigma}_{1000}~(kg~m^{-3})$', 'interpreter', 'latex')
XLAB = xlabel('\sigma_{0{obs}}-{\sigma}_{0_{fit}} (kg m^{-3})', 'fontSize', 14, 'fontWeight', 'bold');
set(gca, 'tickdir', 'out')
set(gca, 'yticklabel', [])
ylim(TLIM)
xlim([-.55 .55])
text(.45, 2.5, 'b', 'fontSize', 16, 'fontWeight', 'bold')
adjust_space
XLAB_POS = get(XLAB, 'pos');
XLAB_POS(2) = 1.5;
set(XLAB, 'pos', XLAB_POS);

set(gcf, 'renderer', 'painters')
print('-depsc2', figureFile)
save(dataFile, 'p')
disp('temp_rho_relFile_3rd.mat saved!')



