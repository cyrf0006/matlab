clear all;
close all;

%% global parameters
global PAR

PAR.verbose = 3;
PAR.flag_UP=5;   %Flag the upper XX m of the shear-derived data (not CTD- microCT)
PAR.CalibYEAR='header';
PAR.CalibCorrSensors=[];
PAR.CalibCorrSensors='P';
PAR.CalibCorr=[-0.2 1 0 0 0 0];
PAR.p_range=[1 200];        %Pressure range to process the data
PAR.dpMIX=0.5;
PAR.dpCTD=0.1;
PAR.dpBOT=0.5;
PAR.BOTrange=20;  %range (m) for bottom binning
PAR.Fs=1024;
PAR.NFFT=1024;
PAR.lineav=4; %will average to 256Hz
PAR.eps_method='INT'; %choose between 'INT' or 'FIT'
PAR.lagCTD = 0.3;
PAR.FP07Offset = 0.0;  % mounting distance between FP07 and CTD sensors
%PAR.GD_NTCHP=0.15; %won't need if you don't have pre-emphasized NTC channel
PAR.GD_NTCHP=0.23; %won't need if you don't have pre-emphasized NTC channel
PAR.rho=1018;
PAR.figopt='n'; %'y' or 'n'
%PAR.SP_sens=[3.25e-4 4.81e-4]; % sensors 17 and 18
PAR.SP_sens=[3.90e-4 4.05e-4]; % sensors 32 and 33 (MSS038)

Profiler = mss_get_profiler('mss38');
%Profiler = mss_get_profiler('mss45');

GPS.LatStr     = 'Latitude';
GPS.LonStr     = 'Longitude';
GPS.TimeStr    = 'GPS-Time';
GPS.Format     = 'decimal';

%% compose file names

%fpath = '/home/lars/software/instruments/iow_mss/';
fpath = '/shic/cruises/2010_al351/deployment/mss038/rawdata/';
prefix = 'A351';
ext    = 'MRD';

fnames={};
%for ifile=828:845; 
%for ifile=828:845; 
for ifile=846:863; 
        file=[fpath prefix num2str(ifile,'%04.0f') '.' ext]; 
        fnames=cat(2,fnames,file);
end


%% loop over all files

for i=1:length(fnames)
        fname=char(fnames(i));
        disp(['Processing ' fname]);
       	[STA(i),DATA(i),CTD(i),MIX(i),BOT(i)] =  mss_doitall(fname,Profiler,GPS,'falling');
end

%% Plot the data
mss_plot_CTD(STA,CTD)
mss_plot_MIX(STA,MIX)
