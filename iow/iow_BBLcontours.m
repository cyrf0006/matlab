clear all 
close all


%% T1C -  T3
load Titp_T1C_p4std_1secAve.mat
t0 = datenum(2010,3,5,9,45,0);
tf = datenum(2010,3,5,10,45,0); 
z0 = 2;
zf = 15;
fadcpFile = 'IOWdata/ADCP/FADCP/T3_mooringVec.mat';
mssFile = 'MSS_DATA/N2_T1C_T3.mat';
XLIM_S2 = [1e-6 2e-1];
XLIM_u  = [0 .15];
YLIM = [2 7];


%% T1C -  T4
% $$$ load Titp_T1C_p4std_1secAve.mat
% $$$ t0   = datenum(2010,3,5,17,30,0);
% $$$ tf   = datenum(2010,3,5,18,30,0); 
% $$$ t0   = datenum(2010,3,5,16,20,0);
% $$$ tf   = datenum(2010,3,5,17,20,0); 
% $$$ z0 = 2;
% $$$ zf = 15;
% $$$ fadcpFile = 'IOWdata/ADCP/FADCP/T4_mooringVec.mat';
% $$$ mssFile = 'MSS_DATA/N2_T1C_T4.mat';
% $$$ XLIM_S2 = [1e-5 2e-1];
% $$$ XLIM_u  = [-.1 .1];
% $$$ YLIM = [2 7];

%% T1C -  T5
% $$$ load Titp_T1C_p4std_1secAve.mat
% $$$ t0   = datenum(2010,3,6,21,45,0);
% $$$ tf   = datenum(2010,3,6,22,45,0); 
% $$$ t0   = datenum(2010,3,6,20,22,0);
% $$$ tf   = datenum(2010,3,6,21,22,0); 
% $$$ z0 = 2;
% $$$ zf = 20;
% $$$ fadcpFile = 'IOWdata/ADCP/FADCP/T5_mooringVec.mat';
% $$$ mssFile = 'MSS_DATA/N2_T1C_T5.mat';
% $$$ XLIM_S2 = [2e-4 3e-2];
% $$$ XLIM_u  = [-.2 .1];
% $$$ YLIM = [2 15];


%% T1B -  T7
% $$$ load Titp_T1B_p2std_1secAve.mat
% $$$ t0   = datenum(2010,3,7,18,0,0);
% $$$ tf   = datenum(2010,3,7,19,0,0); 
% $$$ z0 = 2;
% $$$ zf = 19;
% $$$ fadcpFile = 'IOWdata/ADCP/FADCP/T6_mooringVec.mat';
% $$$ mssFile = 'MSS_DATA/N2_T1B_T7.mat';
% $$$ XLIM_S2 = [2e-4 3e-2];
% $$$ XLIM_u  = [-.2 .1];
% $$$ YLIM = [2 19];

%% T1B -  T8
% $$$ load Titp_T1B_p2std_1secAve.mat
% $$$ t0   = datenum(2010,3,7,21,09,0);
% $$$ tf   = datenum(2010,3,7,22,09,0); 
% $$$ z0 = 2;
% $$$ zf = 19;
% $$$ fadcpFile = 'IOWdata/ADCP/FADCP/T6_mooringVec.mat';
% $$$ mssFile = 'MSS_DATA/N2_T1B_T8.mat';
% $$$ XLIM_S2 = [2e-4 3e-2];
% $$$ XLIM_u  = [-.2 .1];
% $$$ YLIM = [2 19];

%% T1B -  T9
% $$$ load Titp_T1B_p2std_1secAve.mat
% $$$ t0   = datenum(2010,3,8,4,03,0);
% $$$ tf   = datenum(2010,3,8,5,03,0); 
% $$$ z0 = 2;
% $$$ zf = 19;
% $$$ fadcpFile = 'IOWdata/ADCP/FADCP/T6_mooringVec.mat';
% $$$ mssFile = 'none'%MSS_DATA/N2_T1B_T7.mat';
% $$$ XLIM_S2 = [2e-4 3e-2];
% $$$ XLIM_u  = [-.2 .1];
% $$$ YLIM = [2 19];

%timeVec = timeSensor;
V1 = 0:.1:10;
V2 = 0:.5:9;
XLABEL = 'March 2010';

%% Limit the domain
I = find(timeVec>=t0 & timeVec<=tf);
%I = find(timeSensor>=t0 & timeSensor<=tf);
J = find(habVec>=z0 & habVec<=zf);

%myTime = timeSensor(I);
myTime = timeVec(I);
myZ = habVec(J);
%myT = T(J,I);
myT = Tbin(J,I);
clear T Titp


I = find(isnan(nansum(myT,2))==1);
myT(I,:) = [];
myZ(I) = [];

% $$$ %% Get ADCP data
% $$$ ADCP = load('./VMADCP/velo_at_T1C_T3.mat')
% $$$ % VM_shear
% $$$ U = real(ADCP.vm.currMean);
% $$$ V = imag(ADCP.vm.currMean);
% $$$ dz = 2;
% $$$ dudz = gradient(U)./dz;
% $$$ dvdz = gradient(V)./dz;
% $$$ S2_vm = dudz.^2 + dvdz.^2;
% $$$ 
% $$$ % F_adcp shear
% $$$ U = real(ADCP.f.currMean);
% $$$ V = imag(ADCP.f.currMean);
% $$$ II = ~isnan(U);
% $$$ U(II(end)) = NaN;
% $$$ V(II(end)) = NaN;
% $$$ dz = 0.5;
% $$$ dudz = gradient(U)./dz;
% $$$ dvdz = gradient(V)./dz;
% $$$ S2 = dudz.^2 + dvdz.^2;

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 20;%cm
paperheight = 18;%cm
FS = 14;

% $$$ dt = 15/60/24;
% $$$ timeBin = round(timeVec(1)/dt)*dt:dt:timeVec(end);
% $$$ [S2, habAdcp] = iow_getShear('~/research/NIOZ/IOW/IOWdata/adcp/IOWp02.mat', [3, 6, 27, 29, 57:60], timeBin);
% $$$ 


%% plot
% $$$ disp('plotting...')
% $$$ figure(1);
% $$$ clf
% $$$ subplot(1,5,1)
% $$$ plot(nanmean(myT,2), abs(myZ-maxZ), 'k', 'linewidth', 2)
% $$$ ylim([min(abs(myZ-maxZ)) max(abs(myZ-maxZ))]);
% $$$ 
% $$$ subplot(1,5,2)
% $$$ semilogx(nanmean(S2,2), habAdcp, 'k', 'linewidth', 2)
% $$$ ylim([0 10])
% $$$ ylim([min(abs(myZ-maxZ)) max(abs(myZ-maxZ))]);
% $$$ 
% $$$ subplot(1,5,[3 5])
% $$$ contourf(myTime,abs(myZ-maxZ),myT, V1, 'linestyle', 'none')
% $$$ datetick
% $$$ xlim([min(myTime) max(myTime)])
% $$$ ylim([min(abs(myZ-maxZ)) max(abs(myZ-maxZ))]);


% $$$ figure(2);
% $$$ clf
% $$$ subplot(1,4,1)
% $$$ plot(nanmean(myT,2), abs(myZ-maxZ), 'k', 'linewidth', 2)
% $$$ ylim([min(abs(myZ-maxZ)) max(abs(myZ-maxZ))]);
% $$$ ylabel('mab', 'fontSize', 14, 'fontWeight', 'bold')
% $$$ xlabel('T (^{\circ}C)', 'fontSize', 14, 'fontWeight', 'bold')
% $$$ ylim([2.35 4])
% $$$ set(gca, 'fontSize', 14)
% $$$ 
% $$$ subplot(1,4,[2 4])
% $$$ contourf(myTime,abs(myZ-maxZ),myT, V1, 'linestyle', 'none')
% $$$ hold on
% $$$ contour(myTime,abs(myZ-maxZ),myT, [2:2:8], 'k')
% $$$ datetick
% $$$ xlim([min(myTime) max(myTime)])
% $$$ ylim([min(abs(myZ-maxZ)) max(abs(myZ-maxZ))]);
% $$$ ylim([2.35 4])
% $$$ cb = colorbar;
% $$$ ylabel(cb, 'T (^{\circ}C)', 'fontSize', 14, 'fontWeight', 'bold')
% $$$ set(gca, 'fontSize', 14)


figure(3)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 25 16])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 7; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.09; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

subplot(1,7,1)
plot(nanmean(myT,2), myZ, 'k', 'linewidth', 2)
ylabel('mab', 'fontSize', 14, 'fontWeight', 'bold')
xlabel('T (^{\circ}C)', 'fontSize', 14, 'fontWeight', 'bold')
ylim(YLIM)
set(gca, 'fontSize', 14)
set(gca, 'ygrid', 'on')
adjust_space

subplot(1,7,2)
% $$$ plot(sqrt(real(ADCP.vm.currMean).^2 + imag(ADCP.vm.currMean).^2), (ADCP.vm.hab-1.2), 'lineWidth', 2)
% $$$ hold on
% $$$ plot(sqrt(real(ADCP.f.currMean).^2 + imag(ADCP.f.currMean).^2), (ADCP.f.hab-1.5),'r', 'lineWidth', 2)
% $$$ 

fid = fopen(fadcpFile);
if fid == -1
    disp([fadcpFile ' not found!'])
    uVec = NaN;
    vVec = NaN;
    habeVec = NaN;
else
    
    load(fadcpFile)
    hab_dz = 1;
    habVecBin = 1:hab_dz:15;
    uVecBin = nan(size(habVecBin));
    for i = 1:length(habVecBin)
        I = find(habVec>= habVecBin(i)-hab_dz/2 & habVec<= habVecBin(i)+hab_dz/2);
        uVecBin(i) = nanmean(uVec(I));
        vVecBin(i) = nanmean(vVec(I));
    end
    habVec = habVecBin;
    uVec = uVecBin;
    vVec = vVecBin;
end

plot(uVec, habVec-1.5, 'k', 'lineWidth', 2)
hold on
plot(vVec, habVec-1.5,'r', 'lineWidth', 2)
xlabel('U,V (ms^{-1})', 'fontSize', 14, 'fontWeight', 'bold')
ylim(YLIM)
xlim(XLIM_u)
set(gca, 'fontSize', 14)
set(gca, 'yticklabel', [])
set(gca, 'xgrid', 'on')
set(gca, 'ygrid', 'on')
adjust_space

subplot(1,7,3)
dz = abs(diff(habVec(1:2)));
if fid == -1
    S2ave = NaN;
else
    S2ave = (gradient(uVec)./dz).^2 + (gradient(vVec)./dz).^2;
end
% add N2
load(mssFile);
I = find(~isnan(N2ave));
Z = abs(Z(I));
N2ave = N2ave(I);
EPSave = EPSave(I);
semilogx(N2ave, Z(1)-Z, 'r', 'lineWidth', 2)
hold on
semilogx(S2ave, habVec-1.5,'k', 'lineWidth', 2 )

xlabel('S^2, N^2 (s^{-2})', 'fontSize', 14, 'fontWeight', 'bold')
ylim(YLIM)
xlim(XLIM_S2)
set(gca, 'xtick', [1e-5 1e-3 1e-1])
set(gca, 'fontSize', 14)
set(gca, 'yticklabel', [])
set(gca, 'xgrid', 'on')
set(gca, 'ygrid', 'on')
adjust_space

subplot(1,7,4)
plot(EPSave, Z(1)-Z, 'k', 'lineWidth', 2)
xlabel('log[\epsilon (W kg^{-1})]', 'fontSize', 14, 'fontWeight', 'bold')
ylim(YLIM)
xlim([-9.5 -6.5])
set(gca, 'yticklabel', [])
set(gca, 'fontSize', 14)
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')
adjust_space

subplot(1,7,[5 7])
contourf(myTime,myZ,myT, V1, 'linestyle', 'none')
hold on
contour(myTime,myZ,myT, [1:1:9], 'k')
%contour(myTime,abs(myZ-maxZ),myT, [1.7 5:1:8], 'k', 'linewidth',2)
datetick
xlim([min(myTime) max(myTime)])
ylim(YLIM)
cb = colorbar;
caxis([0 9])
ti = ylabel(cb, 'T (^{\circ}C)', 'fontSize', 14, 'fontWeight', 'bold');
set(gca, 'fontSize', 14)
set(gca, 'yticklabel', [])
set(gca, 'ygrid', 'on')

adjust_space
pos1 = get(gca, 'pos');

adjust_space
adjust_space;
pos2 = get(gca, 'pos');
pos3 = [pos1(1), pos1(2), pos2(1)+pos2(3)-pos1(1) pos1(4)];
set(gca, 'pos', pos3);

pause(.5)
cbPos = get(cb, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.1;
cbPos(4) = cbPos(4)-3*.05;
set(cb, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)

print(gcf, '-dpng', '-r300', 'BBL_figure-to-be-renamed.png')
set(gcf, 'renderer', 'painters')
print(gcf, '-depsc', 'BBL_figure-to-be-renamed.eps')




%% Lag coorelation
C1 = contourc(myTime,myZ,myT, [5 5]);
C2 = contourc(myTime,myZ,myT, [6 6]);
C3 = contourc(myTime,myZ,myT, [7 7]);
C4 = contourc(myTime,myZ,myT, [8 8]);
C5 = contourc(myTime,myZ,myT, [1.7 1.7]);

% C1
C = C1;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 5);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec1 = timeVec;
vals1 = vals-nanmean(vals);

% C2
C = C2;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 6);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec2 = timeVec;
vals2 = vals-nanmean(vals);

% C3
C = C3;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 7);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec3 = timeVec;
vals3 = vals-nanmean(vals);

% C4
C = C4;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 8);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec4 = timeVec;
vals4 = vals-nanmean(vals);

% C5
C = C5;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 1.7);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec5 = timeVec;
vals5 = vals-nanmean(vals);

[c1,lags1] = xcov(vals1, vals2);
[c2,lags2] = xcov(vals1, vals3);
[c3,lags3] = xcov(vals1, vals4);
[c4,lags4] = xcov(vals1, vals5);


figure(2)
clf
plot(lags1, c1, 'k')
hold on
plot(lags2, c2, 'r')
plot(lags3, c3, 'm')
plot(lags4, c4, 'b')
YLIM = get(gca, 'ylim');
plot([0 0], YLIM, '--k')

legend('5-6^{\circ}C', '5-7^{\circ}C', '5-8^{\circ}C')
xlabel('lag (s)')
ylabel('xcorr')



%% Temperature correlation
[Y, I1] = min(abs(myZ-5));
[Y, I2] = min(abs(myZ-3));
[Y, I3] = min(abs(myZ-7));

vals1 = myT(I1,:);
vals1 = vals1-nanmean(vals1);
vals2 = myT(I2,:);
vals2 = vals2-nanmean(vals2);
vals3 = myT(I3,:);
vals3 = vals3-nanmean(vals3);

[c1,lags1] = xcov(vals1, vals1);
[c2,lags2] = xcov(vals1, vals2);
[c3,lags3] = xcov(vals1, vals3);

figure(4)
clf
plot(lags1, c1, 'k')
hold on
plot(lags2, c2, 'r')
plot(lags3, c3, 'm')
YLIM = get(gca, 'ylim');
plot([0 0], YLIM, '--k')

legend('5-5 hab', '5-3 hab', '5-7 hab')
xlabel('lag (s)')
ylabel('xcorr')

%% Here you can run iow_contours_spectra

