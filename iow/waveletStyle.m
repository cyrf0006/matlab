clear all 
close all

load Titp_T1C_p1std_1secAve.mat


%% Param
dtWindow = 60/1440;
timeBin = timeVec(1)+dtWindow/2:dtWindow/2:timeVec(end);

% $$$ %% T contour
% $$$ % C1
% $$$ C1 = contourc(timeVec, habVec, Tbin, [5 5]);
% $$$ C = C1;
% $$$ timeVec = C(1,:);
% $$$ vals = C(2,:);
% $$$ I = find(timeVec == 5);
% $$$ vals(I) = [];
% $$$ timeVec(I) = [];
% $$$ vals(end) = [];
% $$$ timeVec(end) = [];
% $$$ [timeVec, I] = sort(timeVec);
% $$$ vals = vals(I);
% $$$ timeVec1 = timeVec;
% $$$ vals1 = vals;

%% temp at certain depth
[Y, I] = min(abs(habVec - 5));
vals1 = Tbin(I,:);


psMat = [];
fMat = [];
for i = 1:length(timeBin)

    I = find(timeVec>=timeBin(i)-dtWindow/2 & timeVec<=timeBin(i)+dtWindow/2);
    T = vals1(I);
    Td = detrend(T);
    
    fs = 1/dtWindow/86400; %Hz

    nx = length(Td); % Hanning
    timeWindow = 60*60; %sec. window
    na = length(Td)./(timeWindow*fs);
    w = hanning(floor(nx/na));

    [ps, f] = pwelch(Td, [], [], [], fs); 
    psMat = [psMat; ps'];
    fMat = [fMat; f'];
end

figure(1)
clf
pcolor(timeBin, f, log10(psMat)'); shading interp
