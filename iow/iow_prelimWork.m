clear all
close all

%% Few parameters
hdfFile = 'BOR1001_S1_raw.h5';
hdfFile = 'BOR1001_S1_p4std.h5';
% $$$ TSrelFile = 'temp_rho_relFile.mat';
% $$$ TSrelFile = 'temp_rho_relFile_3rd.mat';
nSkip = 0;
% $$$ smoothingWindow = 20; % minutes (for S2, N2 smoothing)
% $$$ timeWindowMinutes = 60;
% $$$ windowSize = timeWindowMinutes/1440; % in days
% $$$ g = 9.81;
% $$$ dz = .6;
% $$$ totalDepth = 919;
% $$$ V = 5:.05:10;
% $$$ myRi = []; myGa = [];


%% read whole HDF file<
disp('loading data...')
T = hdf5read(hdfFile, '/data/temperature');
T = T';
n = hdf5read(hdfFile, '/dims/time'); % raw sensor time
zVec = abs(hdf5read(hdfFile, '/dims/depth'));
t0_str = hdf5read(hdfFile, '/', 'StartDate');
%samplingInterval = hdf5read(hdfFile, '/', 'SamplingInterval');
t0 = datenum(t0_str.Data, 'dd-mm-yyyy HH:MM:SS');
t0 = datenum(2010, 2, 28, 11, 02, 0); % by-pass, problem...
samplingIntervals = hdf5read(hdfFile, '/', 'SamplingValue'); % Vector_in_secs
samplingInterval = 0.2;
timeSensor = [t0:samplingInterval/86400:t0+samplingInterval/86400*(length(n)-1)]';
%clear n
disp('  -> done!')
%samplingIntervals = hdf5read(hdfFile, '/', 'SamplingValues'); % Vector_in_secs

%% Prepare data
% make sure matrix is flip correctly
[zVec, I] = sort(zVec);
T = T(I,:);

% reduce number of pts according to nskip
if nSkip ~=0
    T = T(:,1:nSkip:end);
    timeSensor = timeSensor(1:nSkip:end);
end
I = find(T<-10);
T(I) = NaN;


%%%% Interpolate and save data %%%%

disp('Vectical inpterpolation for missing sensors...')
Titp = nan(size(T));
for i = 1:length(n)
    TVec = T(:,i);
    II = find(~isnan(TVec) & TVec~=0);
    Titp(:,i) = interp1(zVec(II), TVec(II), zVec);
end
disp('  -> done!')
save Titp_S1_p4std.mat T Titp n zVec



figure(1)
clf
imagesc(timeSensor,flipud(zVec),Titp)
datetick
ylabel('mab')
set(gca, 'ydir', 'normal')





%%%% Spectral analysis %%%%%
[Y, I] = min(abs(zVec-27));
T27 = T(I,:);
[Y, I] = min(abs(zVec-10));
T10 = T(I,:);
I = find(isnan(T10)); T10(I) = 0;
I = find(isnan(T27)); T27(I) = 0;

T10d = detrend(T10);
T27d = detrend(T27);

freq = 5;
nx = max(size(T27)); % Hanning
na = 250;
w = hanning(floor(nx/na));

[psT_10m, fT_10m] = pwelch(T10d, w, 0, [], freq*60); 
[psT_27m, fT_27m] = pwelch(T27d, w, 0, [], freq*60); 


figure(2)
clf
loglog(fT_10m, psT_10m, 'k', 'linewidth', 2)  
hold on
loglog(fT_27m, psT_27m, 'm', 'linewidth', 2)  


%%%% Loop to diplay slices %%%%
dtSlice = .5/24;
nSlice = round((timeSensor(end)-timeSensor(1))/dtSlice)*2;
lengthSlice = (dtSlice*86400/samplingInterval);
V = [0:.5:7 7.5:.2:15];


iow_loop
