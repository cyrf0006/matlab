% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 2; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
    
Tdata = load(tempFile);
figure(3)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 20 15])

subplot(211)
imagesc(timeBin, habAdcp, Ebin)
hold on
contour(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, 10, 'linecolor', 'k')
set(gca, 'ydir', 'normal')
caxis([-.2 .2])
ylim([0 60])
datetick('x')
set(gca, 'xticklabel', [])
xlim([timeBin(1) timeBin(end)])
ylabel('mab', 'fontSize', 12, 'fontWeight', 'bold')
colorbar
set(gca, 'fontSize', 12)
adjust_space

subplot(212)
imagesc(timeBin, habAdcp, Nbin)
hold on
contour(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, 10, 'linecolor', 'k')
set(gca, 'ydir', 'normal')
caxis([-.2 .2])
ylim([0 60])
datetick('x')
xlim([timeBin(1) timeBin(end)])
ylabel('mab', 'fontSize', 12, 'fontWeight', 'bold')
colorbar
set(gca, 'fontSize', 12)
adjust_space

print('-dpng', 'velocity_temp_toberenamed.png')
