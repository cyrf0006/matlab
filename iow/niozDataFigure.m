clear all
close all

%% some infos
load Titp_T1C_p2std_10minAve.mat%Titp_T1C_p1std_1secAve.mat
t0 = datenum(2010, 3, 4, 15, 0, 0);
tf = datenum(2010, 3, 7, 7, 45, 0);
% $$$ load Titp_T1B_p2std_5minAve.mat
% $$$ t0 = datenum(2010, 3, 7, 11, 30, 0);
% $$$ tf = datenum(2010, 3, 8, 8, 45, 0);
z0 = 0;
zf = 20;
V1 = 0:.1:10;
V2 = 0:.2:9;
V3 = 0:1:9;
XLABEL = 'March 2010';
XLIM = [t0 tf];
YLIM = [z0 zf];

% box 1
t00 = datenum(2010,3,5,16,20,0);
tff = datenum(2010,3,5,17,20,0); 
box1_x = [t00 tff tff t00 t00];
box1_y = [2 2 7 7 2];

% box 2
t00 = datenum(2010,3,6,20,22,0);
tff = datenum(2010,3,6,21,22,0); 
box2_x = [t00 tff tff t00 t00];
box2_y = [2 2 15 15 2];

% box 3
t00 = datenum(2010,3,5,9,45,0);
tff = datenum(2010,3,5,10,45,0); 
box3_x = [t00 tff tff t00 t00];
box3_y = [2 2 7 7 2];
BOX_COLOR = [1 0 0];

%% Limit the domain
I = find(timeVec>=t0 & timeVec<=tf);
J = find(habVec>=z0 & habVec<=zf);

myTime = timeVec(I);
myZ = habVec(J);
myT = Tbin(J,I);


%% Clean ADPC data
adcpFile = '~/research/NIOZ/IOW/IOWdata/adcp/IOWp02.mat';
Iremove = [3, 27, 29, 57:60]; %top
mabADCP = 1.58;
totalDepth = 70; % <--- approximative, not of any use
orientation = 'up';
binSize = 60; %min
    
% Load and get the relevant variables
load(adcpFile);
N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';
timeVec = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
if strcmp(orientation, 'up')    
    habAdcp = mabADCP + (SerBins-SerBins(1)) + RDIBin1Mid;
    zVec = totalDepth - habAdcp;
else
    zVec = mabADCP + (SerBins-SerBins(1)) + RDIBin1Mid;
    habAdcp = totalDepth - zVec;
end

% Cleaning
I = find(E==-32.7680); % bad counts
E(I) = NaN;
I = find(N==-32.7680);
N(I) = NaN;
I = find(W==-32.7680);
W(I) = NaN;

E(Iremove,:) = NaN;
N(Iremove,:) = NaN;
W(Iremove,:) = NaN;

% TimeAverage
dt = binSize/60/24;
timeBin = round(timeVec(1)/dt)*dt:dt:timeVec(end);
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));
for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ebin(:,i) = nanmean(E(:,I), 2);
    Nbin(:,i) = nanmean(N(:,I), 2);
    Wbin(:,i) = nanmean(W(:,I), 2);
end

% Vertical itp
for i = 1:size(Ebin, 2)
    % remove NANs
    uvec = Ebin(:,i);
    I = find(isnan(uvec)==1);
    xitp = 1:length(uvec);
    x = xitp;
    uvec(I) = [];
    x(I) = [];
    if ~isempty(uvec)
         Ebin(:, i) = interp1(x, uvec, xitp);  
    end   
        
    vvec = Nbin(:,i);
    I = find(isnan(vvec)==1);
    xitp = 1:length(vvec);
    x = xitp;
    vvec(I) = [];
    x(I) = [];
    if ~isempty(vvec)
        Nbin(:, i) = interp1(x, vvec, xitp);
    end
    
    wvec = Wbin(:,i);
    I = find(isnan(wvec)==1);
    xitp = 1:length(wvec);
    x = xitp;
    wvec(I) = [];
    x(I) = [];
    if ~isempty(wvec)
        Wbin(:, i) = interp1(x, wvec, xitp);
    end    
    
end
[Ualong, Ucross] = rotate_vecd(Ebin, Nbin, 62.37);


%% Plotting...
disp('plotting...')
h = figure(1);
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 20])
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% Figure dimension
paperwidth = 20;%cm
paperheight = 18;%cm
FS = 14;

% S1
subplot(311)
contourf(myTime,myZ,myT, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,myZ,myT, V2, 'k')
datetick
ylabel('mab', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
set(gca, 'xticklabel', [])
xlim(XLIM)
ylim(YLIM)
%xlabel(XLABEL, 'fontSize', FS, 'fontWeight', 'bold')
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 
adjust_space
% plot boxes
plot(box1_x, box1_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box2_x, box2_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box3_x, box3_y, 'color', BOX_COLOR, 'linewidth', 2)

pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.05;
cbPos(4) = cbPos(4)-3*.025;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)

% S2
subplot(312)
contourf(timeBin, habAdcp, Ualong, 10, 'linestyle', 'none')
hold on
contour(myTime,myZ,myT, V3, 'k')
set(gca, 'ydir', 'normal')
caxis([-.2 .2])
ylim([0 60])
datetick('x')
xlim(XLIM)
ylim(YLIM)
set(gca, 'xticklabel', [])
xlim([timeBin(1) timeBin(end)])
ylabel('mab', 'fontSize', FS, 'fontWeight', 'bold')
c = colorbar;
caxis([-.15 .15])
set(gca, 'fontSize', 12)
ti = ylabel(c, 'u (ms^{-1})', 'FontSize', FS, 'fontWeight', 'bold'); 
adjust_space
% add boxes
plot(box1_x, box1_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box2_x, box2_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box3_x, box3_y, 'color', BOX_COLOR, 'linewidth', 2)

pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.05;
cbPos(4) = cbPos(4)-3*.025;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [4 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)


% S3
subplot(313)
contourf(timeBin, habAdcp, Ucross, 10, 'linestyle', 'none')
hold on
contour(myTime,myZ,myT, V3, 'k')
set(gca, 'ydir', 'normal')
caxis([-.2 .2])
ylim([0 60])
xlim(XLIM)
ylim(YLIM)
datetick('x')
xlim([timeBin(1) timeBin(end)])
ylabel('mab', 'fontSize', FS, 'fontWeight', 'bold')
xlabel('March 2010', 'fontSize', FS, 'fontWeight', 'bold')
c = colorbar;
caxis([-.15 .15])
set(gca, 'fontSize', 12)
ti = ylabel(c, 'v (ms^{-1})', 'FontSize', FS, 'fontWeight', 'bold'); 
% add boxes
plot(box1_x, box1_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box2_x, box2_y, 'color', BOX_COLOR, 'linewidth', 2)
plot(box3_x, box3_y, 'color', BOX_COLOR, 'linewidth', 2)
adjust_space

pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.05;
cbPos(4) = cbPos(4)-3*.025;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [4 CLIM(1)-.05*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)

disp('saving figure...')
print(h, '-r300','niozT1c.png')
set(gcf, 'renderer', 'painters')
print(h, '-depsc', 'niozT1c.eps')

keyboard

iow_contours_spectra
set(gcf, 'renderer', 'painters')
print('-depsc', 'iow_spectra_toberename.eps')
