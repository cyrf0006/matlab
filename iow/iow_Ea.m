function iow_Ea(adcpFile, zAdcp, orientation, binSize, cutOff, depthRange, varargin)

% usage ex:
% iow_Ea('IOWp02.mat', 70, 'up', 1, 30, [55 65])
% iow_Ea('IOWp01.mat', 80, 'up', 1, 30, [60 70], [40 50])


%% Deal with varargin
if isempty(varargin)
    xtraPlot = 0;
else
    xtraPlot = 1;
    depthRange2 = varargin{1};
end

    
%% Load and get the relevant variables
load(adcpFile);
N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';
timeVec = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
if strcmp(orientation, 'up')    
    zVec = zAdcp - (SerBins+RDIBin1Mid);
else
    zVec = zAdcp + (SerBins+RDIBin1Mid);
end


%% Cleaning
I = find(E==-32.7680); % bad counts
E(I) = NaN;
I = find(N==-32.7680);
N(I) = NaN;
I = find(W==-32.7680);
W(I) = NaN;


%% Vertical reduction
I = find(zVec>=depthRange(1) & zVec<=depthRange(2));
E = E(I,:);
N = N(I,:);
W = W(I,:);
zVec = zVec(I);


%% TimeAverage
dt = binSize/60/24;
timeBin = round(timeVec(1)/dt)*dt:dt:timeVec(end);
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));

for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ebin(:,i) = nanmean(E(:,I), 2);
    Nbin(:,i) = nanmean(N(:,I), 2);
    Wbin(:,i) = nanmean(W(:,I), 2);
end


%% High-pass the timeserie
Uiw = nan(size(Ebin));
Viw = nan(size(Nbin));
Ubc = nan(size(Ebin));
Vbc = nan(size(Nbin));

% remove barotropic
Ubt = nanmean(Ebin,1);
Vbt = nanmean(Nbin,1);
for i = 1:size(Ebin,1)
    Ubc(i,:) = Ebin(i,:) - Ubt;
    Vbc(i,:) = Nbin(i,:) - Vbt;
end

% Apply the Filter
dt = (timeVec(2)-timeVec(1));
fs = 1/(dt*86400); %high pass
freq = 1/(cutOff*60); %Hz
Wn = freq/(fs/2);
[b,a] = butter(6, Wn, 'high');
[b,a] = cheby1(6, 0.5,  Wn, 'high');
for i = 1:size(Ubc, 1)
    
    % remove NaNs
    vec = Ubc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    if length(vec)<2
        continue
    end
    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Uiw(i, I) = filtfilt(b, a, vec(I));
    end
    
    vec = Vbc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Viw(i, I) = filtfilt(b, a, vec(I));
    end
end

%% Energy calculation
% $$$ %% Vertical reduction
% $$$ I = find(zVec>=depthRange(1) & zVec<=depthRange(2));
% $$$ E = E(I,:);
% $$$ N = N(I,:);
% $$$ W = W(I,:);
% $$$ zVec = zVec(I);

dz = abs(zVec(2)-zVec(1));   
rho_0 = 1015;    
% vertical
Ea = rho_0*dz*nansum((Wbin), 1).^2; % in J/m^2 

% horizontal
KeH = rho_0*dz*(nansum((Uiw), 1).^2 + nansum((Viw), 1).^2); % in J/m^2 
KeH_all = rho_0*dz*(nansum((Ebin), 1).^2 + nansum((Nbin), 1).^2); % in J/m^2 
A = gaussfiltfilt(KeH, 300, 100);                       


figure(1)
clf
semilogy(timeBin, Ea, 'k')
datetick
xlim([min(timeBin) max(timeBin)])
ylim([1e-1 1e2])
ylabel('Ea')

figure(2)
clf
plot(timeBin, KeH*100, 'r')
hold on
plot(timeBin, KeH_all, 'k')
plot(timeBin, A*1000, 'm', 'linewidth', 2)
datetick
xlim([min(timeBin) max(timeBin)])
ylabel('Ke_H')
%if xtraPlot
    

keyboard

% $$$ 
% $$$ figure(7)
% $$$ clf
% $$$ set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 16 18])
% $$$ % *********************** Adjust_space.m ************************ %
% $$$ % Fields required by the function adjust_space.m. Please fill every
% $$$ % of the following and call "adjust_space" in the script whenever
% $$$ % you want. Do not touch four last fields
% $$$ ncol = 2; % no. subplot column
% $$$ nrow = 3; % no. subplot row
% $$$ dx = 0.09 ; % horiz. space between subplots
% $$$ dy = 0.09; % vert. space between subplots
% $$$ lefs = 0.1; % very left of figure
% $$$ rigs = 0.02; % very right of figure
% $$$ tops = 0.05; % top of figure
% $$$ bots = 0.1; % bottom of figure
% $$$ figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
% $$$ figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
% $$$ count_col = 1;
% $$$ count_row = 1;
% $$$ % *************************************************************** %clf
% $$$ 
% $$$ 
% $$$ % identify sub-timeseries (24-h)
% $$$ IHigh = find(timeVec >= datenum(2011, 09, 22, 11, 0, 0) & timeVec <= ...
% $$$              datenum(2011, 09, 22, 21, 0, 0));
% $$$ ILow = find(timeVec >= datenum(2011, 10, 1, 14, 0, 0) & timeVec <= ...
% $$$             datenum(2011, 10, 02, 0, 0, 0));
% $$$ 
% $$$ 
% $$$ timeLow = timeVec(ILow);
% $$$ EaLow = Ea(ILow);
% $$$ S2_5mLow = S2_5m(ILow);
% $$$ timeHigh = timeVec(IHigh);
% $$$ EaHigh = Ea(IHigh);
% $$$ S2_5mHigh = S2_5m(IHigh);
% $$$ 
% $$$ xLow = [timeLow(1) timeLow(end) timeLow(end) timeLow(1) timeLow(1)];
% $$$ xHigh = [timeHigh(1) timeHigh(end) timeHigh(end) timeHigh(1) timeHigh(1)];
% $$$ y = [1e-5 1e-5 1 1 1e-5];
% $$$ 
% $$$ 
% $$$ 
% $$$ % thresholds for Ea and S2
% $$$ thresh = 1e-3;
% $$$ thresh = mean(Ea) + 5*std(Ea);
% $$$ 
% $$$ m = nanmean(log(S2_5m));
% $$$ s2 = nanvar(log(S2_5m));
% $$$ M = exp(m+s2/2);
% $$$ VAR = exp(2*m+s2).*(exp(s2)-1);
% $$$ threshS2 = M+5*sqrt(VAR);
% $$$ % $$$ threshS2 = 1e-2;
% $$$ % $$$ threshS2 = nanmean(S2_5m)*10;
% $$$ 
% $$$ LETTER_Y= 4.5e-1; 
% $$$ GRAY_LEV = .5;
% $$$ 
% $$$ % sub1
% $$$ sub1 = subplot(3,2,[1 2]);
% $$$ semilogy(timeVec, S2_5m, 'k')   
% $$$ hold on
% $$$ patch(xLow, y, [1 1 1]*.8)
% $$$ patch(xHigh, y, [1 1 1]*.8)
% $$$ semilogy(timeVec, S2_5m, 'k') 
% $$$ semilogy(timeVec, Ea, 'color', [1 1 1]*GRAY_LEV)
% $$$ plot([timeVec(1) timeVec(end)], [thresh thresh], 'color', [1 1 1]*GRAY_LEV, 'linestyle', '--')
% $$$ plot([timeVec(1) timeVec(end)], [threshS2 threshS2], '--k')
% $$$ hold off
% $$$ ylim([1e-5 1])
% $$$ datetick('x', 7)
% $$$ xlim([timeVec(1) timeVec(end)])
% $$$ xlabel('Sept/Oct 2011')
% $$$ ylabel('E_a, S^2 (Kg s^{-2}, s^{-2})')
% $$$ set(gca, 'ytick', [1e-4 1e-3 1e-2 1e-1])
% $$$ set(gca, 'ygrid', 'on')
% $$$ text(timeVec(end)-0.5, LETTER_Y, 'a', 'fontsize', 11, 'fontweight', 'bold')
% $$$ adjust_space
% $$$ pos1 = get(sub1, 'pos');
% $$$ 
% $$$ adjust_space
% $$$ pos2 = get(sub1, 'pos');
% $$$ offset = pos2(1)-pos1(1)-pos1(3);
% $$$ pos1(3) = 2*pos1(3)+offset;
% $$$ set(sub1, 'pos', pos1)
% $$$ 
% $$$ % sub2
% $$$ sub2 = subplot(323);
% $$$ semilogy(timeHigh, S2_5mHigh, 'k') 
% $$$ hold on
% $$$ semilogy(timeHigh, EaHigh,  'color', [1 1 1]*GRAY_LEV)
% $$$ plot([timeHigh(1) timeHigh(end)], [thresh thresh], 'color', [1 1 1]*GRAY_LEV, 'linestyle', '--')
% $$$ plot([timeHigh(1) timeHigh(end)], [threshS2 threshS2], '--k')
% $$$ out1 = plot([timeHigh(1) datenum(2011,09,22,13,20,0)], [1 100], 'k');
% $$$ set(out1, 'clipping', 'off')
% $$$ out2 = plot([datenum(2011,09,22,14,0,0) timeHigh(end)], [100 1], 'k');
% $$$ set(out2, 'clipping', 'off')
% $$$ 
% $$$ hold off
% $$$ ylim([1e-5 1])
% $$$ %datetick
% $$$ xlim([timeHigh(1) timeHigh(end)])
% $$$ xlabel(datestr(timeHigh(1), 1))
% $$$ ylabel('E_a, S^2 (kg s^{-2}, s^{-2})')
% $$$ set(gca, 'ytick', [1e-4 1e-3 1e-2 1e-1])
% $$$ set(gca, 'ygrid', 'on')
% $$$ set(gca, 'xtick', datenum(2011, 09, 22, 12:4:20, 0, 0))
% $$$ set(gca, 'xticklabel', datestr(datenum(2011, 09, 22, 12:4:20, 0, 0),15))
% $$$ text(timeHigh(end)-0.03, LETTER_Y, 'b', 'fontsize', 11, 'fontweight', 'bold')
% $$$ adjust_space
% $$$ 
% $$$ % sub3
% $$$ sub3 = subplot(3,2,4);
% $$$ semilogy(timeLow, S2_5mLow, 'k') 
% $$$ hold on
% $$$ semilogy(timeLow, EaLow,   'color', [1 1 1]*GRAY_LEV)
% $$$ plot([timeLow(1) timeLow(end)], [thresh thresh], 'color', [1 1 1]*GRAY_LEV, 'linestyle', '--')
% $$$ plot([timeLow(1) timeLow(end)], [threshS2 threshS2], '--k')
% $$$ out1 = plot([timeLow(1) datenum(2011, 10, 1, 13, 10, 0)], [1 100], 'k');
% $$$ set(out1, 'clipping', 'off')
% $$$ out2 = plot([datenum(2011, 10, 1, 13 ,40,0) timeLow(end)], [100 1], 'k');
% $$$ set(out2, 'clipping', 'off')
% $$$ 
% $$$ hold off
% $$$ ylim([1e-5 1])
% $$$ %datetick
% $$$ xlim([timeLow(1) timeLow(end)])
% $$$ xlabel(datestr(timeLow(1), 1))
% $$$ set(gca, 'ytick', [1e-4 1e-3 1e-2 1e-1])
% $$$ set(gca, 'yticklabel', [])
% $$$ set(gca, 'ygrid', 'on')
% $$$ set(gca, 'xtick', datenum(2011, 10, 01, 15:4:23, 0, 0))
% $$$ set(gca, 'xticklabel', datestr(datenum(2011, 10, 01, 15:4:23, 0, 0), 15))
% $$$ text(timeLow(end)-0.03, LETTER_Y, 'c', 'fontsize', 11, 'fontweight', 'bold')
% $$$ adjust_space
% $$$ 
% $$$ 
% $$$ % sub4
% $$$ sub4 = subplot(3,2,5);
% $$$ 
% $$$ I = find(Ea>=thresh);
% $$$ J = find(S2_5m(I)>=threshS2);
% $$$ 
% $$$ semilogy(Ea(I), S2_5m(I), '.k')
% $$$ hold on
% $$$ plot([min(Ea(I)) max(Ea(I))],[M M], 'k')
% $$$ plot([min(Ea(I)) max(Ea(I))],[threshS2 threshS2], '--k')
% $$$ hold off
% $$$ xlim([min(Ea(I)) max(Ea(I))])
% $$$ ylim([5e-4 1])
% $$$ ylabel('S^2 (s^{-2})')
% $$$ xlabel('E_a (kg s^{-2})')
% $$$ set(gca, 'ytick', [1e-3 1e-2 1e-1])
% $$$ set(gca, 'ygrid', 'on')
% $$$ set(gca, 'yminorgrid', 'off')
% $$$ 
% $$$ disp(sprintf('%d out of %d IWs enhance shear (%d percent)', length(J), length(I), round(length(J)/length(I)*100)));
% $$$ text(7.2e-3, LETTER_Y, 'd', 'fontsize', 11, 'fontweight', 'bold')
% $$$ adjust_space
% $$$ 
% $$$ 
% $$$ % sub5
% $$$ sub5 = subplot(3,2,6);
% $$$ % $$$ plot(t_bin, Ea_bin, 'k')
% $$$ % $$$ xlabel('time to hightide (h)')
% $$$ % $$$ ylabel('E_a (J m^{-2})')
% $$$ % $$$ xlim([-6.5 6.5])
% $$$ 
% $$$ clear relativeNo;
% $$$ I = find(Ea>=thresh);
% $$$ 
% $$$ dclass = 2;
% $$$ classEdge = -7:dclass:7;
% $$$ for j = 1:length(classEdge)-1
% $$$     J = find(time2(I)>=classEdge(j) & time2(I)<classEdge(j+1));
% $$$     relativeNo(j) = length(J)/length(I);
% $$$ end
% $$$ 
% $$$ bar(classEdge(1:end-1)+dclass/2, relativeNo, 'facecolor', [.2 .2 .2]);
% $$$ xlim([-7 7])
% $$$ ylim([0 .35])
% $$$ ylabel('rel. freq.')
% $$$ xlabel('time to hightide (h)')
% $$$ 
% $$$ II = find(S2_5m(I)>=threshS2);
% $$$ clear relativeNo;
% $$$ 
% $$$ for j = 1:length(classEdge)-1
% $$$     J = find(time2(I(II))>=classEdge(j) & time2(I(II))<classEdge(j+1));
% $$$     relativeNo(j) = length(J)/length(I);
% $$$ end
% $$$ hold on
% $$$ bar(classEdge(1:end-1)+dclass/2, relativeNo, 'facecolor', [1 1 1]*GRAY_LEV);
% $$$ hold off
% $$$ text(6.35, .32, 'e', 'fontsize', 11, 'fontweight', 'bold')
% $$$ adjust_space
% $$$ 
% $$$ 
% $$$ print(gcf, '-dpng', 'Ea_S2_subplot.png')
% $$$ set(gcf, 'renderer', 'painters')
% $$$ print(gcf, '-depsc2', 'Ea_S2_subplot.eps')
