clear all
close all

%% T1B - 4 (2min waves)
load Titp_T1B_p2std_1sAve.mat
t0 = datenum(2010, 3, 7, 19, 0, 0);
tf = datenum(2010, 3, 7, 21, 0, 0);
z0 = 19;
zf = 24;
V1 = 0:.01:10;
V2 = 0:.5:10;
XLABEL = '7 March 2010';

%% Limit the domain
I = find(timeVec>=t0 & timeVec<=tf);
J = find(zVec>=z0 & zVec<=zf);
maxZ = max(zVec);
myTime = timeVec(I);
myZ = zVec(J);
myT1 = Tbin(J,I);


%% ------------------------- Spectra: --------------------------- %

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 15;%cm
paperheight = 12;%cm
FS = 14;
FS1 = 12;

% low-pass timeserie
dt = myTime(2)-myTime(1);
fs = 1/dt/86400; %Hz

nx = max(size(myT1)); % Hanning
timeWindow = 120*60; %sec. window
na = length(myTime)./(timeWindow*fs);
w = hanning(floor(nx/na));

psMat1 = [];
for i = 1:size(myT1,1);
    Td = myT1(i,:);
    [ps, f] = pwelch(Td, w, 0, [], fs); 
    psMat1 = [psMat1; ps'];
end

figure(3)
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 paperwidth paperheight])
F1 = 1/600;
F2 = 1/300;
F3 = 1/180;
F4 = 1/60;
F5 = 1/1020;
F6 = 1/5;
F7 = 1/30;
F8 = 1/180;
F9 = 1/(3*60);
F10 = 1/2.2;
F11 = 1/(7.5*60);
F12 = 1/100;


clf
loglog(f, nanmean(psMat1,1), 'k', 'lineWidth', 2)
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')
hold on
YLIM = get(gca, 'ylim');
%plot([F1 F1], YLIM, '--k'); 
%plot([F2 F2], YLIM, '--k');
%plot([F3 F3], YLIM, '--k');
%plot([F4 F4], YLIM, '--k');
plot([F5 F5], YLIM, '--k', 'lineWidth', 1.5);
%plot([F6 F6], YLIM, '--k');
%plot([F7 F7], YLIM, '--k');
%plot([F8 F8], YLIM, '--k');
%plot([F9 F9], [YLIM(1) 1e5], '--k', 'lineWidth', 1.5);
plot([F10 F10], [YLIM(1) 1e-2], '--k', 'lineWidth', 1.5);
plot([F11 F11], [YLIM(1) 1e4], '--k', 'lineWidth', 1.5);
plot([F12 F12], [YLIM(1) 1e2], '--k', 'lineWidth', 1.5);
xlabel('f (Hz)', 'fontSize', FS, 'fontWeight', 'bold')
ylabel('PSD', 'fontSize', FS, 'fontWeight', 'bold')
ylim(YLIM);
xlim([3e-4 1]);



%text(F1, YLIM(2), '10min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F2, YLIM(2), '5min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F3, YLIM(2), '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F4, YLIM(2), '1min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F5, YLIM(2), '17min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
%text(F6, YLIM(2), '5sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F7, YLIM(2), '30sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F8, YLIM(2), '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F9, 1e5, '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'left', 'fontSize', FS1, 'fontWeight', 'bold')
text(F10, 5e-2, '2.2s', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
text(F10-.05, 1e-2, '(surf. waves)', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
text(F11, 1e4, '7.5min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
text(F12, 1e2, '100s', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'left', 'fontSize', FS1, 'fontWeight', 'bold')
set(gca, 'fontSize', FS1)

set(gcf, 'renderer', 'painters')
print('-depsc', 'iow_spectra_2min.eps')
