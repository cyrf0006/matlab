clear all
close all

ctd1 = load('~/research/NIOZ/IOW/ctd/0001_01_bav_buo.cnv');
ctd2 = load('~/research/NIOZ/IOW/ctd/0002F01_bav_buo.cnv');

[Y, I] = max(ctd1(:,8));
I1 = 1:I;
[Y, I] = max(ctd2(:,8));
I2 = 1:I;

zVec1 = ctd1(I1,8);
zVec2 = ctd1(I2,8);
N1 = sqrt(ctd1(I1,15));
N2 = sqrt(ctd2(I2,15));
period1 = 2*pi./N1;
% $$$ I = find(zVec1<40);
% $$$ period1(I) = NaN;
period2 = 2*pi./N2;
% $$$ I = find(zVec2<40);
% $$$ period2(I) = NaN;

sig1 = nanmean([ctd1(I1,12),ctd1(I1,13)],2); 
sig2 = nanmean([ctd2(I2,12),ctd2(I2,13)],2); 
T1 = nanmean([ctd1(I1,2),ctd1(I1,3)],2); 
T2 = nanmean([ctd2(I2,2),ctd2(I2,3)],2); 
S1 = nanmean([ctd1(I1,6),ctd1(I1,7)],2); 
S2 = nanmean([ctd2(I2,6),ctd2(I2,7)],2); 


SA1 = gsw_SA_from_SP(S1,zVec1,16,55);
SA2 = gsw_SA_from_SP(S2,zVec2,16,55);
CT1 = gsw_CT_from_t(SA1,T1,zVec1);
CT2 = gsw_CT_from_t(SA2,T2,zVec2);

sigma01 = gsw_sigma0(SA1,CT1);
sigma02 = gsw_sigma0(SA2,CT2);



%% plots %%

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.02 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
paperwidth = 20;%cm
paperheight = 16;%cm
FS = 14;
FS1 = 12;

myColor1 = [0.0, 0.55, 0.55];
myColor2 = [1.0, 0.0, 0.5];

figure(1)
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 paperwidth paperheight])

subplot(141)
plot(T2, max(zVec2)-zVec2, 'color', myColor2, 'lineWidth', 2)
hold on
plot(T1, max(zVec1)-zVec1, 'color', myColor1, 'lineWidth', 2) 
set(gca, 'ydir', 'normal')
xlabel('\Theta_0 (^{\circ}C)', 'fontSize', FS, 'fontWeight', 'bold')
ylabel('hab (m)', 'fontSize', FS, 'fontWeight', 'bold')
ylim([0 86])
set(gca, 'fontSize', FS1)
text(5, 30, 'S1', 'color', myColor1, 'fontSize', FS, 'fontWeight', 'bold')
text(1, 5, 'T1B', 'color', myColor2, 'fontSize', FS, 'fontWeight', 'bold')
adjust_space

subplot(142)
plot(S2, max(zVec2)-zVec2, 'color', myColor2, 'lineWidth', 2)
hold on
plot(S1, max(zVec1)-zVec1, 'color', myColor1, 'lineWidth', 2) 
set(gca, 'ydir', 'normal')
set(gca, 'yticklabel', [])
xlabel('S_A', 'fontSize', FS, 'fontWeight', 'bold')
xlim([7 17.5])
ylim([0 86])
set(gca, 'xtick', 8:4:16)
set(gca, 'fontSize', FS1)
adjust_space

% $$$ subplot(143)
% $$$ plot(sig2, max(zVec2)-zVec2, 'color', myColor2, 'lineWidth', 2)
% $$$ hold on
% $$$ plot(sig1, max(zVec1)-zVec1, 'color', myColor1, 'lineWidth', 2) 
% $$$ set(gca, 'ydir', 'normal')
% $$$ set(gca, 'yticklabel', [])
% $$$ xlabel('\sigma_0', 'fontSize', FS, 'fontWeight', 'bold')
% $$$ xlim([5.5 13.5])
% $$$ ylim([0 86])
% $$$ set(gca, 'fontSize', FS1)
% $$$ adjust_space

subplot(143)
plot(N2, max(zVec2)-zVec2, 'color', myColor2, 'lineWidth', 2)
hold on
plot(N1, max(zVec1)-zVec1, 'color', myColor1, 'lineWidth', 2) 
set(gca, 'ydir', 'normal')
set(gca, 'yticklabel', [])
xlabel('N (s^{-1})', 'fontSize', FS, 'fontWeight', 'bold')
ylim([0 86])
set(gca, 'fontSize', FS1)
adjust_space

subplot(144)
plot(period2, max(zVec2)-zVec2, 'color', myColor2, 'lineWidth', 2)
hold on
plot(period1, max(zVec1)-zVec1, 'color', myColor1, 'lineWidth', 2) 
plot([100 100],[0 60], 'lineStyle', '--' ,'color',[.2 .2 .2], 'lineWidth', 2)
plot([61 61],[0 70], 'lineStyle', '--' ,'color',[.2 .2 .2], 'lineWidth', 2)
plot([240 240],[0 50], 'lineStyle', '--' ,'color',[.2 .2 .2], 'lineWidth', 2)
plot([720 720],[0 80], 'lineStyle', '--' ,'color',[.2 .2 .2], 'lineWidth', 2)
set(gca, 'ydir', 'normal')
set(gca, 'yticklabel', [])
xlabel('2\pi / N (s)', 'fontSize', FS, 'fontWeight', 'bold')
%xlim([0 1000])
xlim([6e1 1e3])
ylim([0 86])
set(gca, 'xscale', 'log')
set(gca, 'fontSize', FS1)
text(100, 62, '100sec', 'fontSize', FS, 'fontWeight', 'bold','color',[.2 .2 .2])
text(61, 72, '1min', 'fontSize', FS, 'fontWeight', 'bold','color',[.2 .2 .2])
text(240, 52, '4min', 'fontSize', FS, 'fontWeight', 'bold','color',[.2 .2 .2])
text(720, 82, '12min', 'fontSize', FS, 'fontWeight', 'bold','color',[.2 .2 .2], 'horizontalAlignment', 'right')
adjust_space


%save figure
print('-dpng', '-r300', 'BOR10_ctd.png')
set(gcf, 'renderer', 'painters'); % vectorial figure
print('-depsc', 'BOR10_ctd.eps')

