clear all
close all

adcpFile = 'IOWp02.mat';
mabADCP = 1.58;
totalDepth = 70; % <--- approximative, not of any use
orientation = 'up';

binSize = 15; %min

% Whole deploy.
t1 = datenum(2010, 3, 4, 15, 0, 0);
t2 = datenum(2010, 3, 7, 7, 45, 0);
% T2
% $$$ t1   = datenum(2010,3,5,01,25,0);
% $$$ t2   = datenum(2010,3,5,07,21,0);
% $$$ %T3
% $$$ t1   = datenum(2010,3,5,08,45,0);
% $$$ t2   = datenum(2010,3,5,13,46,0);
% $$$ %T6
% $$$ t1   = datenum(2010,3,7,01,38,0);
% $$$ t2   = datenum(2010,3,7,07,10,0);



%% Load and get the relevant variables
load(adcpFile);
N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';
timeVec = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
if strcmp(orientation, 'up')    
    habAdcp = mabADCP + SerBins + RDIBin1Mid;
    zVec = totalDepth - habAdcp;
else
    zVec = mabADCP + SerBins + RDIBin1Mid;
    habAdcp = totalDepth - zVec;
end

%% Cleaning
I = find(E==-32.7680); % bad counts
E(I) = NaN;
I = find(N==-32.7680);
N(I) = NaN;
I = find(W==-32.7680);
W(I) = NaN;

Iremove = [27, 57:60]; %top
% $$$ Iremove = [1:7 27 57:60];
E(Iremove,:) = NaN;
N(Iremove,:) = NaN;
W(Iremove,:) = NaN;

%% TimeAverage
dt = binSize/60/24;
timeBin = t1:dt:t2;
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));
for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ebin(:,i) = nanmean(E(:,I), 2);
    Nbin(:,i) = nanmean(N(:,I), 2);
    Wbin(:,i) = nanmean(W(:,I), 2);
end

%% Vertical itp
for i = 1:size(Ebin, 2)
    % remove NANs
    uvec = Ebin(:,i);
    I = find(isnan(uvec)==1);
    xitp = 1:length(uvec);
    x = xitp;
    uvec(I) = [];
    x(I) = [];
    if ~isempty(uvec)
         Ebin(:, i) = interp1(x, uvec, xitp);  
    end   
        
    vvec = Nbin(:,i);
    I = find(isnan(vvec)==1);
    xitp = 1:length(vvec);
    x = xitp;
    vvec(I) = [];
    x(I) = [];
    if ~isempty(vvec)
        Nbin(:, i) = interp1(x, vvec, xitp);
    end
    
    wvec = Wbin(:,i);
    I = find(isnan(wvec)==1);
    xitp = 1:length(wvec);
    x = xitp;
    wvec(I) = [];
    x(I) = [];
    if ~isempty(wvec)
        Wbin(:, i) = interp1(x, wvec, xitp);
    end    
    
end


%% Best way to compute shear
dz = zVec(2) - zVec(1);
S = [];
for i = 1:length(timeBin)
    duz = gradient(Ebin(:,i));
    dvz = gradient(Nbin(:,i));
    dudz = duz./dz;
    dvdz = dvz./dz;
    shear = (dudz).^2 + (dvdz).^2;
    S = [S sqrt(shear)];
end


figure(1)
clf
%imagesc(Veclog10(S))
contourf(timeBin, habAdcp, log10(S), 10, 'lineStyle', 'none');
hold on
cb = colorbar;
caxis([-4 -1])
T = load('~/research/NIOZ/IOW/Titp_T1C_p2std_10minAve.mat');
contour(T.timeVec, flipud(T.zVec), T.Tbin, 10, 'color', 'k');
ylim([0 60])
datetick
xlim([min(timeBin) max(timeBin)])
ylabel('mab')
ylabel(cb, 'log10(S)')

figure(2)
clf
plot(nanmean(S,2), zVec)
