clear all
close all

%% S1 - top/bottom
load Titp_S1_p4std_1sAve.mat
t0 = datenum(2010, 2, 28, 22, 0, 0);
tf = datenum(2010, 2, 28, 23, 0, 0);
z20 = 22.5;
z2f = 27.5;
z10 = 0;
z1f = 5;
V1 = 0:.01:10;
V2 = 8:.1:10;
V3 = 0:1:10;
XLABEL = '28 Feb. 2010';


%% Limit the domain
I = find(timeVec>=t0 & timeVec<=tf);
J1 = find(zVec>=z10 & zVec<=z1f);
J2 = find(zVec>=z20 & zVec<=z2f);

maxZ = max(zVec);

myTime = timeVec(I);
myZ1 = zVec(J1);
myT1 = Tbin(J1,I);
myZ2 = zVec(J2);
myT2 = Tbin(J2,I);


% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 2; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 15;%cm
paperheight = 20;%cm
FS = 14;
FS1 = 12;


%% plot
disp('plotting...')
h = figure(1);
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 paperwidth paperheight])

subplot(211)
contourf(myTime,abs(myZ1-maxZ),myT1, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,abs(myZ1-maxZ),myT1, V3, 'k')
datetick
ylabel('hab (m)', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
xlim([t0 tf])
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 
adjust_space
pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.05;
cbPos(4) = cbPos(4)-3*.025;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.025*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)
datetick('x', 15, 'keeplimits')
set(gca, 'xgrid', 'on')
set(gca, 'xticklabel', [])
text(datenum(2010, 2, 28, 22, 01, 0), 25.5, 'a) top of mooring', 'FontSize', FS, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'left')

subplot(212)
contourf(myTime,abs(myZ2-maxZ),myT2, V1, 'linestyle', 'none')
hold on
c = colorbar;
contour(myTime,abs(myZ2-maxZ),myT2, V2, 'k')
datetick
ylabel('hab (m)', 'fontSize', FS, 'fontWeight', 'bold')
set(gca, 'ydir', 'normal')
set(gca, 'fontSize', FS);
datetick
xlim([t0 tf])
xlabel(XLABEL, 'fontSize', FS, 'fontWeight', 'bold')
ti = ylabel(c, 'T (^{\circ}C)', 'FontSize', FS, 'fontWeight', 'bold'); 
adjust_space
pause(.5)
cbPos = get(c, 'pos');
cbPos(1) = cbPos(1)-.01;
cbPos(3) = cbPos(3)/2;
cbPos(2) = cbPos(2)+.05;
cbPos(4) = cbPos(4)-3*.025;
set(c, 'pos', cbPos)
ti_pos = get(ti, 'pos');
CLIM = get(gca, 'clim');
set(ti, 'rotation', 0)
ti_pos = [3 CLIM(1)-.025*(CLIM(2)-CLIM(1)) 1];
set(ti, 'pos', ti_pos)
datetick('x', 15, 'keeplimits')
set(gca, 'xgrid', 'on')
text(datenum(2010, 2, 28, 22, 01, 0), 3, 'b) bottom of mooring', 'FontSize', FS, 'fontWeight', 'bold', 'color', [1 1 1], 'horizontalAlignment', 'left')

 disp('saving figure...')
print(h, '-r300','iow_S1_topBBL_compa.png')
set(gcf, 'renderer', 'painters')
print(h, '-depsc', 'iow_S1_topBBL_compa.eps')



%% ------------------------- Spectra: --------------------------- %

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% plotting info
%figure dimension
paperwidth = 15;%cm
paperheight = 12;%cm
FS = 14;

% low-pass timeserie
dt = myTime(2)-myTime(1);
fs = 1/dt/86400; %Hz

nx = max(size(myT1)); % Hanning
timeWindow = 60*60; %sec. window
na = length(myTime)./(timeWindow*fs);
w = hanning(floor(nx/na));

psMat1 = [];
psMat2 = [];
for i = 1:size(myT1,1);
    Td = myT1(i,:);
    [ps, f] = pwelch(Td, w, 0, [], fs); 
    psMat1 = [psMat1; ps'];
end
for i = 1:size(myT2,1);    
    Td = myT2(i,:);
    [ps, f] = pwelch(Td, w, 0, [], fs); 
    psMat2 = [psMat2; ps'];    
end

figure(3)
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 paperwidth paperheight])
F1 = 1/600;
F2 = 1/300;
F3 = 1/180;
F4 = 1/60;
F5 = 1/1200;
F6 = 1/5;
F7 = 1/30;
F8 = 1/180;
F9 = 1/(3.8*60);
F10 = 1/2.5;
F11 = 1/(5.5*60);
F12 = 1/100;


clf
loglog(f, nanmean(psMat1,1), 'k', 'lineWidth', 2)
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')
hold on
loglog(f, nanmean(psMat2,1), 'color', [.5 .5 .5], 'lineWidth', 2)
legend('top', 'bottom', 'location', 'southWest')
YLIM = get(gca, 'ylim');
%plot([F1 F1], YLIM, '--k'); 
%plot([F2 F2], YLIM, '--k');
%plot([F3 F3], YLIM, '--k');
%plot([F4 F4], YLIM, '--k');
plot([F5 F5], YLIM, '--k', 'lineWidth', 1.5);
%plot([F6 F6], YLIM, '--k');
%plot([F7 F7], YLIM, '--k');
%plot([F8 F8], YLIM, '--k');
plot([F9 F9], [YLIM(1) 1e5], '--k', 'lineWidth', 1.5);
plot([F10 F10], [YLIM(1) 1e-2], '--k', 'lineWidth', 1.5);
plot([F11 F11], [YLIM(1) 1e4], '--k', 'lineWidth', 1.5);
plot([F12 F12], [YLIM(1) 1e2], '--k', 'lineWidth', 1.5);
xlim([4e-4 1])
xlabel('f (Hz)', 'fontSize', FS, 'fontWeight', 'bold')
ylabel('PSD', 'fontSize', FS, 'fontWeight', 'bold')
ylim(YLIM);
xlim([4e-4 1]);



%text(F1, YLIM(2), '10min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F2, YLIM(2), '5min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F3, YLIM(2), '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F4, YLIM(2), '1min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F5, YLIM(2), '20min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
%text(F6, YLIM(2), '5sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F7, YLIM(2), '30sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
%text(F8, YLIM(2), '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F9, 1e5, '3.8min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'left', 'fontSize', FS1, 'fontWeight', 'bold')
text(F10, 5e-2, '2.5s', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
text(F10, 1e-2, '(surf. waves)', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center', 'fontSize', FS1, 'fontWeight', 'bold')
text(F11, 1e4, '5.5min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'right', 'fontSize', FS1, 'fontWeight', 'bold')
text(F12, 1e2, '100s', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'left', 'fontSize', FS1, 'fontWeight', 'bold')
set(gca, 'fontSize', FS1)

set(gcf, 'renderer', 'painters')
print('-depsc', 'iow_spectra_compa.eps')
