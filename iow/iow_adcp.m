function iow_adcp(adcpFile, zAdcp, orientation)

%function iow_adcp(adcpFile, zAdcp, orientation)
%
% usage ex:
%   iow_adcp('IOW10T1C.mat', 70, 'up')
%   iow_adcp('IOWp02.mat', 70, 'up')

    
%% Load and get the relevant variables
load(adcpFile);

N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';


%% Cleaning
I = find(E==-32.7680); % bad counts
E(I) = NaN;
I = find(N==-32.7680);
N(I) = NaN;
I = find(W==-32.7680);
W(I) = NaN;

% $$$ Iremove = [1:3 34 48:60];
% $$$ Iremove = [1:7 27 57:60];
% $$$ E(Iremove,:) = NaN;
% $$$ N(Iremove,:) = NaN;
% $$$ W(Iremove,:) = NaN;

timeVec = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
if strcmp(orientation, 'up')    
    zVec = zAdcp - SerBins;
else
    zVec = zAdcp + SerBins;
end


%% TimeAverage
dt = 5/60/24;
timeBin = round(timeVec(1)/dt)*dt:dt:timeVec(end);
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ebin(:,i) = nanmean(E(:,I), 2);
    Nbin(:,i) = nanmean(N(:,I), 2);
end


% Filtering
filterTime = 60; % in minutes 
dt = (timeBin(2)-timeBin(1));
% Low pass 
fs = 1/(dt*86400);
freq_low = 1/(filterTime*60); %Hz
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);

E_filt = nan(size(Ebin));
N_filt = nan(size(Nbin));

for i = 1:size(Ebin, 1)
    % remove NANs
    uvec = Ebin(i,:);
    I = find(isnan(uvec)==1);
    xitp = 1:length(uvec);
    x = xitp;
    uvec(I) = [];
    x(I) = [];
    if ~isempty(uvec)
        uvec = interp1(x, uvec, xitp);  
        I = find(~isnan(uvec)==1);
        E_filt(i, I) = filtfilt(b, a, uvec(I));
    end   
        
    vvec = Nbin(i,:);
    I = find(isnan(vvec)==1);
    xitp = 1:length(vvec);
    x = xitp;
    vvec(I) = [];
    x(I) = [];
    if ~isempty(vvec)
        vvec = interp1(x, vvec, xitp);    
        I = find(~isnan(vvec)==1);
        N_filt(i, I) = filtfilt(b, a, vvec(I));
    end
end


du = diff(E_filt,1,1);
dv = diff(N_filt,1,1);
dz = zVec(2) - zVec(1);

S2Mat = (du/dz).^2 + (dv/dz).^2;
zS2 = zVec(1:end-1)+dz/2;









% Compute shear...
dz = zVec(2)-zVec(1);
du = diff(Ebin, 1);
dv = diff(Nbin, 1);
zVec_S = zVec(1:end-1)+dz/2;

S2 = (du./dz).^2; + (dv./dz).^2;



keyboard


% $$$ % Filter timeserie
% $$$ dt = diff(abs(timeAdcp(1:2))); %day
% $$$ fs = 1/dt; % cpd
% $$$ freq_low = 1; %cpd
% $$$ Wn_low = freq_low/(fs/2);
% $$$ [b,a] = butter(4, Wn_low);
% $$$ 
% $$$ for i = 1:length()
% $$$ 
% $$$ Ufilt = filtfilt(b, a, E);
% $$$ Vfilt = filtfilt(b,a,vVec);


keyboard



% isolate good timeserie (remove if depth <10)
I = abs(zAdcp - nanmean(zAdcp))>10;
zAdcp(I) = [];
timeAdcp(I) = [];
N(:,I) = [];
E(:,I) = [];
W(:,I) = [];

clear Ser* An*

% eliminate data below bottom
I = find(abs(E)>10);
E(I) = NaN;
I = sum(isnan(E),2);
II = find(I>mean(I));
Iremove = II(1):length(I);
E(Iremove,:) = [];
N(Iremove,:) = [];
W(Iremove,:) = [];
zAdcp(Iremove) = [];


% roation may be not necessary
[U,V] = rotate_vecd(E,N,-30);
uVec = nanmean(U,1);
vVec = nanmean(V,1);
wVec = nanmean(W,1);

% Filter timeserie
dt = diff(abs(timeAdcp(1:2))); %day
fs = 1/dt; % cpd
freq_low = 1; %cpd
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
Ufilt = filtfilt(b, a, uVec);
Vfilt = filtfilt(b,a,vVec);


%keyboard

Vitp = interp1(timeAdcp, Vfilt, timeVec);

% find max velocities
timeMax = [];
timeMin = [];
for i = 2:length(timeVec)-1
    if Vitp(i-1)<Vitp(i) & Vitp(i+1)<Vitp(i)
        timeMax = [timeMax timeVec(i)];        
    end
    
    if Vitp(i-1)>Vitp(i) & Vitp(i+1)>Vitp(i)
        timeMin = [timeMin timeVec(i)];        
    end
end

% find closest maximum vel
time2maxV = nan(size(timeVec));
for i = 1:length(timeVec)
    [Y,I] = min(abs(timeVec(i)-timeMax));
    time2maxV(i) = timeVec(i)-timeMax(I);
end
