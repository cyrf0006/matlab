function [S2, habAdcp] = iow_getShear(adcpFile, Iremove, timeBin)

% usage ex: 
%   [S2, habAdcp] = iow_getShear('~/research/NIOZ/IOW/IOWdata/adcp/IOWp02.mat', [3, 6, 27, 29, 57:60], 15)    

% $$$ adcpFile = '~/research/NIOZ/IOW/IOWdata/adcp/IOWp02.mat';
% $$$ Iremove = [3, 6, 27, 29, 57:60]; %top

% $$$ 
% $$$ adcpFile = '~/research/NIOZ/IOW/IOWdata/adcp/IOWp03.mat';
% $$$ Iremove = [3, 27, 29, 57:60]; %top


mabADCP = 1.58;
totalDepth = 70; % <--- approximative, not of any use
orientation = 'up';

%binSize = 15; %min
    
%% Load and get the relevant variables
load(adcpFile);
N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';
timeVec = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
if strcmp(orientation, 'up')    
    habAdcp = mabADCP + (SerBins-SerBins(1)) + RDIBin1Mid;
    zVec = totalDepth - habAdcp;
else
    zVec = mabADCP + (SerBins-SerBins(1)) + RDIBin1Mid;
    habAdcp = totalDepth - zVec;
end


%% Cleaning
I = find(E==-32.7680); % bad counts
E(I) = NaN;
I = find(N==-32.7680);
N(I) = NaN;
I = find(W==-32.7680);
W(I) = NaN;

%keyboard
E(Iremove,:) = NaN;
N(Iremove,:) = NaN;
W(Iremove,:) = NaN;


%% TimeAverage
dt = abs(diff(timeBin(1:2)));
% $$$ timeBin = round(timeVec(1)/dt)*dt:dt:timeVec(end);
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));
for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ebin(:,i) = nanmean(E(:,I), 2);
    Nbin(:,i) = nanmean(N(:,I), 2);
    Wbin(:,i) = nanmean(W(:,I), 2);
end

%% Vertical itp
for i = 1:size(Ebin, 2)
    % remove NANs
    uvec = Ebin(:,i);
    I = find(isnan(uvec)==1);
    xitp = 1:length(uvec);
    x = xitp;
    uvec(I) = [];
    x(I) = [];
    if ~isempty(uvec)
         Ebin(:, i) = interp1(x, uvec, xitp);  
    end   
        
    vvec = Nbin(:,i);
    I = find(isnan(vvec)==1);
    xitp = 1:length(vvec);
    x = xitp;
    vvec(I) = [];
    x(I) = [];
    if ~isempty(vvec)
        Nbin(:, i) = interp1(x, vvec, xitp);
    end
    
    wvec = Wbin(:,i);
    I = find(isnan(wvec)==1);
    xitp = 1:length(wvec);
    x = xitp;
    wvec(I) = [];
    x(I) = [];
    if ~isempty(wvec)
        Wbin(:, i) = interp1(x, wvec, xitp);
    end    
    
end


dz = abs(diff(habAdcp(1:2)));

[Fx du] = gradient(Ebin);
[Fx dv] = gradient(Nbin);

S2 = (du./dz).^2 + (dv./dz).^2;
