clear all
close all

tempFile = 'Titp_T1C_p2std_1secAve.mat';
Tdata = load(tempFile);


% Apply the Filter ...
%  ... on velocities
cutOff = 30;
dt = (Tdata.timeVec(2) - Tdata.timeVec(1));
fs = 1/(dt*86400); %high pass
freq = 1/(cutOff*60); %Hz
Wn = freq/(fs/2);
[b,a] = butter(6, Wn, 'high');
%[b,a] = cheby1(6, 0.5,  Wn, 'high');
Tfilt = nan(size(Tdata.Tbin));
for i = 1:size(Tdata.Tbin, 1)
    
    % remove NaNs
    vec = Tdata.Tbin(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    if length(vec)<2
        continue
    end    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Tfilt(i, I) = filtfilt(b, a, vec(I));
    end
    
end

C1 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [2.5 2.5]);
C2 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [5.5 5.5]);
C3 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [8.5 8.5]);

% $$$ C1 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tfilt, [2.5 2.5]);
% $$$ C2 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tfilt, [5.5 5.5]);
% $$$ C3 = contourc(Tdata.timeVec, flipud(Tdata.zVec+.1), Tfilt, [8.5 8.5]);

% C1
C = C1;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 2.5);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec1 = timeVec;
vals1 = vals;

% C2
C = C2;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 5.5);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec2 = timeVec;
vals2 = vals;

% C3
C = C3;
timeVec = C(1,:);
vals = C(2,:);
I = find(timeVec == 8.5);
vals(I) = [];
timeVec(I) = [];
vals(end) = [];
timeVec(end) = [];
[timeVec, I] = sort(timeVec);
vals = vals(I);
timeVec3 = timeVec;
vals3 = vals;

figure(1)
clf
plot(timeVec1, vals1, 'k')
hold on
plot(timeVec2, vals2, 'r')
plot(timeVec3, vals3, 'm')


X1 = datenum(2010, 3, 6, 0, 0, 0);
X2 = datenum(2010, 3, 6, 1, 0, 0);

I1  = find(timeVec1>=X1 & timeVec1<=X2);
I2  = find(timeVec2>=X1 & timeVec2<=X2);
I3  = find(timeVec3>=X1 & timeVec3<=X2);
[c1,lags1] = xcov(vals1(I1), vals2(I1));
[c2,lags2] = xcov(vals2(I2), vals3(I2));
[c3,lags3] = xcov(vals1(I3), vals3(I3));


% $$$ [c1,lags1] = xcov(vals1, vals2, 100000);
% $$$ [c2,lags2] = xcov(vals2, vals3, 100000);
% $$$ [c3,lags3] = xcov(vals1, vals3, 100000);


figure(2)
clf
plot(lags1/60, c1, 'k')
hold on
plot(lags2/60, c2, 'r')
plot(lags3/60, c3, 'm')
YLIM = get(gca, 'ylim');
plot([0 0], YLIM, '--k')

legend('2.5-5.5^{\circ}C', '5.5-8.5^{\circ}C', '2.5-8.5^{\circ}C')
xlabel('lag (min)')
ylabel('xcorr')