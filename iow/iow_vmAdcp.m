load adcp_005.mat


t0 = datenum(2010, 3, 6, 0, 0, 0);
tf = datenum(2010, 3, 6, 12, 0, 0);

% pass X
% $$$ t0 = datenum(2010, 3, 6, 4, 30, 0);
% $$$ tf = datenum(2010, 3, 6, 5, 35, 0);

% pass X
t0 = datenum(2010, 3, 6, 6, 5, 0);
tf = datenum(2010, 3, 6, 7, 20, 0);

% pass X
% $$$ t0 = datenum(2010, 3, 6, 7, 50, 0);
% $$$ tf = datenum(2010, 3, 6, 10, 10, 0);



I = find(ADCP.mtime>=t0 & ADCP.mtime<=tf);
J = find(ADCP.config.ranges>=5  &ADCP.config.ranges<=72);

timeVec = ADCP.mtime(I);
zVec = ADCP.config.ranges(J);
E = ADCP.east_vel(J,I);
N = ADCP.north_vel(J,I);
W = ADCP.vert_vel(J,I);
Eb = ADCP.bt_vel(1,I);
Nb = ADCP.bt_vel(2,I);
% $$$ imagesc(timeVec, zVec, U)
% $$$ caxis([-1 1])


dt = 120/86400;
timeBin = timeVec(1)+dt/2:dt:timeVec(end);
Ebin = nan(length(zVec), length(timeBin));
Nbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));
botE = nan(1,length(timeBin));
botN = nan(1,length(timeBin));

for i = 1:length(timeBin)
    
    I = find(timeVec>=(timeBin(i)-dt/2) &  timeVec<=(timeBin(i)+dt/2));
    
    Ebin(:,i) = nanmean(E(:,I),2);
    Nbin(:,i) = nanmean(N(:,I),2);
    Wbin(:,i) = nanmean(W(:,I),2);
    botE(i) - nanmean(Eb(I));
    botN(i) - nanmean(Nb(I));
    
end

[dux duz] = gradient(Ebin);
[dvx dvz] = gradient(Nbin);

EbinCor = nan(size(Ebin));
NbinCor = nan(size(Nbin));
for i = 1:length(zVec)
    EbinCor(i,:) = Ebin(i,:) - botE;
    NbinCor(i,:) = Nbin(i,:) - botN;
end

dz = abs(zVec(2)-zVec(1));
S2 = (duz./dz).^2 + (dvz./dz).^2;

figure(1)
clf
contourf(timeBin, zVec, log10(S2), 20, 'lineStyle', 'none')
set(gca, 'ydir', 'reverse')
hold on
contour(timeBin, zVec, Nbin, [0:.1:1], 'k')
contour(timeBin, zVec, Nbin, [-1:.1:0], '--k')
ylim([0 80])
c = colorbar;
caxis([-6 -2])
ylabel('Depth (m)')
xlabel('6 October 2010')
ylabel(c, 'S^2 (s^{-2})')
datetick