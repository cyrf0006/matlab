% 1 Dec. 2015, Rostock.

clear all
file = 'Titp_S1_p4std_10minAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

clear all
file = 'Titp_S1_p4std_1sAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_S1_p4std_5minAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_S1_p4std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1B_p2std_1sAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1B_p2std_5minAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1B_p2std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1B_p4std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p1std_1secAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p1std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p2std_10minAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p2std_1minAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p2std_1secAve.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p2std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p4std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')

file = 'Titp_T1C_p5std.mat';
disp(file)
load(file)
habVec = flipud([.1:.2:30]');
Tbin(144,:) = [];
save(file, 'timeVec', 'habVec', 'Tbin');
disp('saved!')
