clear all
close all

% $$$ iow_rbr2mat('RBR_TSC1.config')
% $$$ iow_cats2mat('MC_TSC1.config')

MC = load('MC_TSC1_10min.mat');
RBR = load('RBR_TSC1_10min.mat');
timeVec = RBR.timeVec;

disp('Substitute density values to RBR measurements:')
RBR.sigMat = nan(size(RBR.TMat));
count = 0;
for i = 1:length(RBR.timeVec)
    
    if mod(i, 100) == 0
        disp(sprintf(' %d / %d', i, length(RBR.timeVec)))
    end

    for j = 1:length(RBR.zVec);

        I = find(MC.zVec<RBR.zVec(j));
        if isempty(I)
            count = count + 1;
            continue
        else
            Imin = I(end);
        end
        
        I = find(MC.zVec>RBR.zVec(j));
        if isempty(I)
            count = count + 1;
            continue
        else
            Imax = I(1);
        end
        
        zHR = MC.zVec(Imin):.01:MC.zVec(Imax);
        TMC_itp = interp1(MC.zVec([Imin Imax]), MC.TMat([Imin Imax],i), zHR);
        sigMC_itp = interp1(MC.zVec([Imin Imax]), MC.sigMat([Imin Imax],i), zHR);

        if RBR.TMat(j,i) < min(TMC_itp) | RBR.TMat(j,i) > max(TMC_itp)
            RBR.sigMat(j,i) = NaN; % impossible to guess: density inversion!
            count = count + 1;
        else
            [Y, I] = min(abs(TMC_itp - RBR.TMat(j,i)));
            RBR.sigMat(j,i) = sigMC_itp(I);
        end    
        
        
    end
% $$$     TMC_itp = interp1(MC.zVec, MC.TMat(:,i), zHR);
% $$$     sigMC_itp = interp1(MC.zVec, MC.sigMat(:,i), zHR);
% $$$     
% $$$     for j = 1:length(RBR.zVec);
% $$$         if RBR.TMat(j,i) < min(TMC_itp) | RBR.TMat(j,i) > max(TMC_itp)
% $$$             RBR.sigMat(j,i) = NaN; % inversion, impossible to guess...
% $$$             count = count + 1;
% $$$         else
% $$$             [Y, I] = min(abs(TMC_itp - RBR.TMat(j,i)));
% $$$             RBR.sigMat(j,i) = sigMC_itp(I);
% $$$         end        
% $$$     end 
end
disp(sprintf('  -> impossible to substitute %d out of %d measurements', count, length(RBR.timeVec)*length(RBR.zVec)))



%% Put RBR and MC together
sigMat = [MC.sigMat; RBR.sigMat];
zVec = [MC.zVec RBR.zVec];
[zVec, I] = sort(zVec);
sigMat = sigMat(I,:);


%% Vertical ITP
for i = 1:length(timeVec)
    vec = sigMat(:,i);
    I = find(~isnan(vec));
    if length(I)>2
        sigMat(:,i) = interp1(zVec(I), vec(I), zVec);
    end
end

figure(1)
clf
contourf(timeVec, zVec, sigMat, 100, 'linestyle', 'none')
hold on
contour(timeVec, zVec, sigMat, 10, 'k')
set(gca, 'ydir', 'reverse')
datetick
xlim([min(timeVec) max(timeVec)])