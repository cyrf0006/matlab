clear all
close all

load Titp_T1C_p1std_1secAve.mat
t0 = datenum(2010, 3, 4, 15, 0, 0);
tf = datenum(2010, 3, 7, 7, 45, 0);
z0 = 20;
zf = 30;

I = find(zVec>=z0 & zVec<=zf);
J = find(timeVec>=t0 & timeVec<=tf);
myTime = timeVec(J);
myT = Tbin(I,J);


% low-pass timeserie
dt = myTime(2)-myTime(1);
fs = 1/dt/86400; %Hz

nx = max(size(myT)); % Hanning
timeWindow = 20*60; %sec. window
na = length(myTime)./(timeWindow*fs);
w = hanning(floor(nx/na));

psMat = [];
for i = 1:size(myT,1);
    Td = myT(i,:);
    %Td = detrend(myT(i,:));
    [ps, f] = pwelch(Td, w, 0, [], fs); 
    psMat = [psMat; ps'];
end


figure(3)
F1 = 1/600;
F2 = 1/300;
F3 = 1/180;
F4 = 1/60;
F5 = 1/1200;
F6 = 1/5;
F7 = 1/30;


clf
loglog(f, nanmean(psMat,1), 'lineWidth', 2)
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')
hold on
YLIM = get(gca, 'ylim');
plot([F1 F1], YLIM, '--k')
plot([F2 F2], YLIM, '--k')
plot([F3 F3], YLIM, '--k')
plot([F4 F4], YLIM, '--k')
plot([F5 F5], YLIM, '--k')
plot([F6 F6], YLIM, '--k')
plot([F7 F7], YLIM, '--k')

text(F1, YLIM(2), '10min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F2, YLIM(2), '5min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F3, YLIM(2), '3min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F4, YLIM(2), '1min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F5, YLIM(2), '20min', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F6, YLIM(2), '5sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')
text(F7, YLIM(2), '30sec', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'center')


