clear all
close all


% ----  parameter to adjust: ------- %
%% S1 - All
% $$$ load Titp_S1_p4std_10minAve.mat
% $$$ t0 = min(timeVec);
% $$$ tf = max(timeVec);
% $$$ z0 = min(zVec);
% $$$ zf = max(zVec);
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = 'Feb./March 2010';

%% S1 - 1 (near bottom) - doubleD?
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 2, 28, 22, 0, 0);
% $$$ tf = datenum(2010, 2, 28, 23, 0, 0);
% $$$ z0 = 22.5;
% $$$ zf = 27.5;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 9:.1:10;
% $$$ XLABEL = '28 Feb. 2010';

%% S1 - 2 (near mooring top) 
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 2, 28, 22, 0, 0);
% $$$ tf = datenum(2010, 2, 28, 23, 0, 0);
% $$$ z0 = 0;
% $$$ zf = 5;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '28 Feb. 2010';

%% S1 - 3 (compressed pycno.) 
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 1, 10, 0, 0);
% $$$ tf = datenum(2010, 3, 1, 11, 0, 0);
% $$$ z0 = 3;
% $$$ zf = 7;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '1 March 2010';

%% S1 - 4 (soliton)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 1, 12, 15, 0);
% $$$ tf = datenum(2010, 3, 1, 12, 50, 0);
% $$$ z0 = 2;
% $$$ zf = 8;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '1 March 2010';

%% S1 - 5 (isopyc. spread)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 1, 12, 0, 0);
% $$$ tf = datenum(2010, 3, 1, 15, 0, 0);
% $$$ z0 = 2;
% $$$ zf = 8;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '1 March 2010';

%% S1 - 6 (un-official)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 1, 13, 0, 0);
% $$$ tf = datenum(2010, 3, 1, 17, 0, 0);
% $$$ z0 = 2;
% $$$ zf = 15;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '1 March 2010';

%% S1 - 7 (whole WC internalwaves)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 2, 3, 0, 0);
% $$$ tf = datenum(2010, 3, 2, 9, 0, 0);
% $$$ z0 = 0;
% $$$ zf = 30;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '2 March 2010';

%% S1 - 8 (whole WC / non-linear)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 2, 8, 30, 0);
% $$$ tf = datenum(2010, 3, 2, 9, 30, 0);
% $$$ z0 = 0;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '2 March 2010';

%% S1 - 9 (Fast IWS, 3-5 min.)
% $$$ load Titp_S1_p4std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 3, 23, 0, 0);
% $$$ tf = datenum(2010, 3, 4, 2, 0, 0);
% $$$ z0 = 0;
% $$$ zf = 10;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '4 March 2010';



%% T1B - all
% $$$ load Titp_T1B_p2std_5minAve.mat
% $$$ t0 = min(timeVec);
% $$$ tf = max(timeVec);
% $$$ z0 = min(zVec);
% $$$ zf = max(zVec);
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '7-8 March 2010';

%% T1B - 1 (near bottom)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 8, 1, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 2, 0, 0);
% $$$ z0 = 22.5;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '8 March 2010';


%% T1B - 2 (45sec waves)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 8, 3, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 9, 0, 0);
% $$$ z0 = 10;
% $$$ zf = 20;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:10;
% $$$ XLABEL = '8 March 2010';

%% T1B - 3 (45sec waves, ZOOM)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 8, 6, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 7, 0, 0);
% $$$ z0 = 10;
% $$$ zf = 20;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.2:10;
% $$$ XLABEL = '8 March 2010';

%% T1B - 4 (2min waves)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 7, 19, 0, 0);
% $$$ tf = datenum(2010, 3, 7, 21, 0, 0);
% $$$ z0 = 19;
% $$$ zf = 24;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.5:10;
% $$$ XLABEL = '7 March 2010';

% $$$ %% T1B - 5 (BBL-2mab, whole timeserie)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = min(timeVec);
% $$$ tf = max(timeVec);
% $$$ z0 = 28;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.01:10;
% $$$ XLABEL = '7-8 March 2010';

%% T1B - 5 (BBL-2mab, whole timeserie)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = min(timeVec);
% $$$ tf = max(timeVec);
% $$$ z0 = 28;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.01:10;
% $$$ XLABEL = '7-8 March 2010';


%% T1B - 6 (BBL - IW break? LARGE)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 7, 23, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 1, 0, 0);
% $$$ z0 = 20;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.1:10;
% $$$ XLABEL = '7-8 March 2010';

%% T1B - 7 (BBL - IW break? ZOOM)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 7, 23, 30, 0);
% $$$ tf = datenum(2010, 3, 8, 0, 30, 0);
% $$$ z0 = 25;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = [8:.1:9 9:.02:9.3];
% $$$ XLABEL = '7-8 March 2010';
% $$$ CAXIS = [8.3 9.3];

%% T1B - 8 (BBL - IW break? ZOOM2)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 7, 23, 30, 0);
% $$$ tf = datenum(2010, 3, 8, 0, 30, 0);
% $$$ z0 = 28;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = [8:.1:9 9:.02:9.3];
% $$$ XLABEL = '7-8 March 2010';
% $$$ CAXIS = [8.3 9.3];

%% T1B - 9 (BBL - IW break? ZOOM3)
% $$$ load Titp_T1B_p2std_1sAve.mat
% $$$ t0 = datenum(2010, 3, 7, 23, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 1, 30, 0);
% $$$ z0 = 28;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = [8:.1:9 9:.02:9.3];
% $$$ XLABEL = '7-8 March 2010';


% $$$ %% T1C - 1 (Whole)
% $$$ load Titp_T1C_p2std_10minAve.mat
% $$$ t0 = datenum(2010, 3, 4, 15, 0, 0);
% $$$ tf = datenum(2010, 3, 7, 7, 45, 0);
% $$$ z0 = 0;
% $$$ zf = 30;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:1:9;
% $$$ XLABEL = 'March 2010';

%% T1C - 1 (Whole)
load Titp_T1C_p2std_10minAve.mat%Titp_T1C_p1std_1secAve.mat
t0 = datenum(2010, 3, 4, 15, 0, 0);
tf = datenum(2010, 3, 7, 7, 45, 0);
z0 = 10;
zf = 30;
V1 = 0:.1:10;
V2 = 0:.2:9;
XLABEL = 'March 2010';

% $$$ %% T1C - 1 (1h)
% $$$ load Titp_T1C_p1std_1secAve.mat
% $$$ t0 = datenum(2010, 3, 5, 8, 30, 0);
% $$$ tf = datenum(2010, 3, 5, 9, 30, 0);
% $$$ z0 = 25;
% $$$ zf = 26;
% $$$ V1 = 0:.01:10;
% $$$ V2 = 0:.2:9;
% $$$ XLABEL = 'March 2010';


% $$$ %% T1C -  T2
% $$$ t0   = datenum(2010,3,5,01,25,0);
% $$$ tf   = datenum(2010,3,5,07,21,0); 
% $$$ load Titp_T1C_p1std_1secAve.mat
% $$$ z0 = 25;
% $$$ zf = 27;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:9;
% $$$ XLABEL = 'March 2010';

% $$$ %% T1C -  T3
% $$$ t0   = datenum(2010,3,5,08,45,0);
% $$$ tf   = datenum(2010,3,5,13,46,0); 
% $$$ load Titp_T1C_p1std_1secAve.mat
% $$$ z0 = 24;
% $$$ zf = 26;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:9;
% $$$ XLABEL = 'March 2010';

% $$$ %% T1C -  T6
% $$$ t0   = datenum(2010,3,7,01,38,0);
% $$$ tf   = datenum(2010,3,7,07,10,0);
% $$$ load Titp_T1C_p1std_1secAve.mat
% $$$ z0 = 22;
% $$$ zf = 28;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:1:9;
% $$$ XLABEL = 'March 2010';


%% 2Hz test
% $$$ load Titp_T1B_p2std.mat
% $$$ Tbin = Titp;
% $$$ timeVec = timeSensor;
% $$$ t0 = datenum(2010, 3, 8, 1, 0, 0);
% $$$ tf = datenum(2010, 3, 8, 2, 0, 0);
% $$$ z0 = 22.5;
% $$$ zf = 26;
% $$$ V1 = 0:.1:10;
% $$$ V2 = 0:2:10;
% $$$ XLABEL = '8 March 2010';




% ---------------------------------- %
    


%% now plot using another script:

iow_contours_plot
iow_contours_addAdcp



disp('saving figure...')
print(h, '-r300','iow_contours_toberename.png')
set(gcf, 'renderer', 'painters')
print(h, '-depsc', 'iow_contours_toberename.eps')


iow_contours_spectra
set(gcf, 'renderer', 'painters')
print('-depsc', 'iow_spectra_toberename.eps')
