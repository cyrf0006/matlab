clear all
close all

% if needed, use ow_h52mat.m, otherwise:
load Titp_S1_p4std.mat; 

% Few params:
sensorDepth = 15;

%% Find closest sensor
I = find(nansum(~isnan(T),2)==0);
T(I,:) = [];
zVec(I) = [];

%%%% Spectral analysis %%%%%
[Y, I] = min(abs(zVec-sensorDepth));
TVec = Titp(I,:);
Td = detrend(TVec);

%% sub-timeseries
I = find(timeSensor>=datenum(2010,2,28,12,0,0) & timeSensor<=datenum(2010,3,1,12,0,0));
Td1 = detrend(TVec(I));
I = find(timeSensor>=datenum(2010,3,2,0,0,0) & timeSensor<=datenum(2010,3,3,0,0,0));
Td2 = detrend(TVec(I));
I = find(timeSensor>=datenum(2010,3,3,12,0,0) & timeSensor<=datenum(2010,3,4,12,0,0));
Td3 = detrend(TVec(I));


% low-pass timeserie
fs = 5; %Hz
freq_low = 0.1;
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
Tfilt = filtfilt(b, a, Td);

nx = max(size(Td)); % Hanning
na = 50;

na = 100;
w = hanning(floor(nx/na));

[ps, f] = pwelch(Td, w, 0, [], fs); 
[psf, ff] = pwelch(Tfilt, w, 0, [], fs); 
[ps1, f1] = pwelch(Td1, w, 0, [], fs); 
[ps2, f2] = pwelch(Td2, w, 0, [], fs); 
[ps3, f3] = pwelch(Td3, w, 0, [], fs); 



figure(2)
clf
%loglog(f, ps, 'k', 'linewidth', 2)  
%hold on
%loglog(ff, psf, 'm', 'linewidth', 2)  
loglog(f1, ps1, 'r', 'linewidth', 2)  
hold on
loglog(f2, ps2, 'm', 'linewidth', 2)  
loglog(f3, ps3, 'g', 'linewidth', 2)  
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')
