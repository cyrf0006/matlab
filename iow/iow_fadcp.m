clear all
close all

% Transect 3
% $$$ load AL351_FADCP_001_val.mat
% $$$ t0 = datenum(2010,3,5,11, 30, 0);
% $$$ tf = datenum(2010,3,5,12, 0, 0);
% $$$ t0 = datenum(2010,3,5,10,0,0);
% $$$ tf = datenum(2010,3,5,10,30,0); 
% $$$ outputFile = 'T3_mooringVec.mat';

% Transect 4
% $$$ load AL351_FADCP_002_val.mat
% $$$ t0 = datenum(2010,3,5,17, 45, 0);
% $$$ tf = datenum(2010,3,5,18, 15, 0);
% $$$ t0 = datenum(2010,3,5,16, 35, 0);
% $$$ tf = datenum(2010,3,5,18, 05, 0);
% $$$ outputFile = 'T4_mooringVec.mat'

% $$$ % Transcet 5
load AL351_FADCP_004_val.mat
t0 = datenum(2010,3,6,22, 0, 0);
tf = datenum(2010,3,6,22, 30, 0);
t0   = datenum(2010,3,6,20,37,0);
tf   = datenum(2010,3,6,21,7,0); 
outputFile = 'T5_mooringVec.mat'


timeVec = adcpavg.nfix;
zVec = adcpavg.depth;
U = real(adcpavg.curr)';
V = imag(adcpavg.curr)';
timeVec(end) = [];
U(:,end) = [];
V(:,end) = [];

I = find(timeVec >= t0 & timeVec <= tf);


figure(1)
clf
subplot(211)
imagesc(timeVec, zVec, U)
caxis([-.3 .3])
set(gca, 'xticklabel', []);

subplot(212)
imagesc(timeVec, zVec, V)
caxis([-.3 .3])
datetick
set(gca, 'ygrid', 'on')

figure(2)
plot(nanmean(U(:,I),2), zVec)

uVec = nanmean(U(:,I),2);
vVec = nanmean(V(:,I),2);
habVec = nanmean(adcpavg.bottom(I)) - zVec;
I = find(habVec<0);
uVec(I) = [];
vVec(I) = [];
habVec(I) = [];
zVec(I) = [];



save(outputFile, 'uVec', 'vVec', 'zVec', 'habVec')
