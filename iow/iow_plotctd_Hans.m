%cd C:\projects\IOW\ctd
load /media/Seagate1TB/NIOZ/IOW/ctd/0001_01_bav_buo.cnv;
ctd1=X0001_01_bav_buo;
load /media/Seagate1TB/NIOZ/IOW/ctd/0002F01_bav_buo.cnv;
ctd2=X0002F01_bav_buo;

ff=find(ctd1(:,8)==max(ctd1(:,8)));
gg=find(ctd2(:,8)==max(ctd2(:,8)));
figure(1)
clf
ira=[0,500,-90,0];
subplot(1,2,1)
plot(2*pi./ctd1(1:ff(1),15).^0.5,-ctd1(1:ff(1),8))
axis(ira)
subplot(1,2,2)
plot(2*pi./ctd2(1:gg(1),15).^0.5,-ctd2(1:gg(1),8))
axis(ira)
pause

figure(2)
clf
ira=[5.5,13.5,-90,0];
subplot(1,2,1)
plot(ctd1(1:ff(1),12),-ctd1(1:ff(1),8),ctd1(1:ff(1),13),-ctd1(1:ff(1),8))
axis(ira)
subplot(1,2,2)
plot(ctd2(1:gg(1),12),-ctd2(1:gg(1),8),ctd2(1:gg(1),13),-ctd2(1:gg(1),8))
axis(ira)
pause

figure(3)
clf
ira=[0,100,-90,0];
subplot(1,2,1)
plot(ctd1(1:ff(1),9),-ctd1(1:ff(1),8),ctd1(1:ff(1),10),-ctd1(1:ff(1),8))
axis(ira)
subplot(1,2,2)
plot(ctd2(1:gg(1),9),-ctd2(1:gg(1),8),ctd2(1:gg(1),10),-ctd2(1:gg(1),8))
axis(ira)
pause

figure(4)
clf
ira=[-0.15 0.05,-90,0];
subplot(1,2,1)
plot(ctd1(1:ff(1),11),-ctd1(1:ff(1),8))
axis(ira)
subplot(1,2,2)
plot(ctd2(1:gg(1),11),-ctd2(1:gg(1),8))
axis(ira)
pause




% 
%Figure 3 overview some background data
%
is1=[5.8,6.8,-1530,-1380];
is2=[35.18,35.4,-1530,-1380];
is3=[27.65,27.85,-1530,-1380];
is4=[0,60,-1530,-1380];
ef=7.292115e-5;
figure(1)
clf
py1=[-1520,-1500,-1480,-1460,-1440,-1420,-1400];
py0=[' ',' ',' ',' ',' ',' ',' '];
p1=[.1,.55,.38,.35];
axes('position',p1)
hn=plot(ctd(:,2),-ctd(:,9),'k');
set(hn,'LineWidth',1.2)
axis(is1)
set(gca,'Fontsize',10)
set(gca,'YTick',py1)
set(gca,'YTickLabel',py1)
set(gca,'XAxisLocation','Top')
ylabel('z (m)')
xlabel('T (^oC)')
lk=text(-1400,5.9,'a');
set(lk,'Fontsize',13)

p1=[.52,.55,.38,.35];
axes('position',p1)
hn=plot(ctd(:,7),-ctd(:,9),'k');
set(hn,'LineWidth',1.2)
axis(is2)
set(gca,'Fontsize',10)
set(gca,'YTick',py1)
set(gca,'YTickLabel',py0)
set(gca,'XAxisLocation','Top')
set(gca,'YAxisLocation','Right')
xlabel('Sal (psu)')
lk=text(-1400,35.23,'b');
set(lk,'Fontsize',13)

p1=[.1,.17,.38,.35];
axes('position',p1)
hn=plot(ctd(:,8),-ctd(:,9),'k');
set(hn,'LineWidth',1.2)
axis(is3)
set(gca,'Fontsize',10)
set(gca,'YTick',py1)
set(gca,'YTickLabel',py0)
set(gca,'XAxisLocation','Bottom')
set(gca,'YAxisLocation','Left')
xlabel('\sigma_\theta (kg m^-^3)')
lk=text(-1400,27.7,'c');
set(lk,'Fontsize',13)

lk=text(27.65,-1580,'Figure 2');
set(lk,'Fontsize',16)

p1=[.52,.17,.38,.35];
axes('position',p1)
hn=plot((ctd(:,10).^0.5)/ef,-ctd(:,9),'k');
set(hn,'LineWidth',0.3)
hold on
hm=plot((ctd10(:,10).^0.5)/ef,-ctd10(:,9),'k--');
set(hm,'LineWidth',1.3)
hold off
axis(is4)
set(gca,'Fontsize',10)
set(gca,'YTick',py1)
set(gca,'YTickLabel',py1)
set(gca,'XAxisLocation','Bottom')
set(gca,'YAxisLocation','Right')
ylabel('z (m)')
xlabel('N/f')
lk=text(-1400,5,'d');
set(lk,'Fontsize',13)

orient portrait
print -dpsc ftf3SpecF2

