function iow_adcp2mat(inFile, outFile, dtSec, timeRange, zRange)
 
% usage ex (from anywhere):
% iow_adcp2mat('~/research/NIOZ/IOW/IOWdata/adcp/AL315_TSC1-1_ADCP_10s_val.mat','ADCP_T1D_1min.mat',60, [datenum(2010,3,4,14,0,0) datenum(2010,3,7,9,30,0)], [20 57])



load(inFile)

U = real(adcpavg.curr);
V = imag(adcpavg.curr);
W = adcpavg.vu;
U = U';
V = V';
W = W';
zVec = adcpavg.depth;
I = find(zVec >= zRange(1) & zVec<= zRange(2));
U = U(I,:);
V = V(I,:);
W = W(I,:);
zVec = zVec(I);

timeVec = adcpavg.rtc;

dt = dtSec/86400;
timeBin = timeRange(1):dt:timeRange(2);

Ubin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));
Wbin = nan(length(zVec), length(timeBin));

for i = 1:length(timeBin)
    I = find(timeVec>=timeBin(i)-dt/2 & timeVec<=timeBin(i)+dt/2);
    Ubin(:,i) = nanmean(U(:,I),2);
    Vbin(:,i) = nanmean(V(:,I),2);
    Wbin(:,i) = nanmean(W(:,I),2);
end

save(outFile, 'Ubin', 'Vbin', 'Wbin', 'timeBin', 'zVec')
