function iow_langmuir(timeVec, zVec, U, V, W, cutOff)


% cutOff: filter time in minutes
XLIM = [datenum(2010,3,5) datenum(2010,3,7,8,0,0)];

    
Uiw = nan(size(U));
Viw = nan(size(V));
Ubc = nan(size(U));
Vbc = nan(size(V));
Wiw = nan(size(V));

% remove barotropic
Ubt = nanmean(U,1);
Vbt = nanmean(V,1);
for i = 1:size(U,1)
    Ubc(i,:) = U(i,:) - Ubt;
    Vbc(i,:) = V(i,:) - Vbt;
end

% Apply the Filter ...
%  ... on velocities
dt = (timeVec(2)-timeVec(1));
fs = 1/(dt*86400); %high pass
freq = 1/(cutOff*60); %Hz
Wn = freq/(fs/2);
[b,a] = butter(6, Wn, 'high');
%[b,a] = cheby1(6, 0.5,  Wn, 'high');
for i = 1:size(Ubc, 1)
    
    % remove NaNs
    vec = Ubc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    if length(vec)<2
        continue
    end
    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Uiw(i, I) = filtfilt(b, a, vec(I));
    end
    
    vec = Vbc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Viw(i, I) = filtfilt(b, a, vec(I));
    end
    
    vec = W(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];    
    if length(vec)<2
        continue
    end    
    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Wiw(i, I) = filtfilt(b, a, vec(I));
    end
end

%% subinertial
Usub = U - Uiw;
Vsub = V - Viw;

%% plot
disp('plotting...')
% -------- Plotting ------------ %
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 4; % no. subplot row
dx = 0.01 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

Tdata = load('~/research/NIOZ/IOW/Titp_T1C_p2std_10minAve.mat');


figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 16 15])   
load ~/research/NIOZ/IOW/IOWdata/AL351_DDL_wind.mat
timeVecWind = DDL.time.data + datenum(2010,1,1);
wind = DDL.wind.speed.data;

subplot(4, 1, 1)
plot(timeVecWind, wind, 'k')
set(gca, 'xticklabel', [])
set(gca, 'xgrid', 'on')
ylabel('Wind (m s^{-1})')
xlim(XLIM)
ylim([0 20])
adjust_space

subplot(4, 1, 2)
contourf(timeVec, zVec, Uiw, 100, 'linestyle', 'none')
hold on
contour(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [0:.1:3 4:10], 'linecolor', 'k')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'normal')
set(gca, 'xgrid', 'on')
ylabel('mab')
cb = colorbar;
ylabel(cb, 'Uiw')
caxis([-.04 .04])
xlim(XLIM)
ylim([0 70])
adjust_space

subplot(4, 1, 3)
contourf(timeVec, zVec, Viw, 100, 'linestyle', 'none')
hold on
%contour(T.timeVec, T.zVec+39.67, T.Tbin, [0:1:10], 'color', 'k', 'linewidth', 2);
contour(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [0:.1:3 4:10], 'linecolor', 'k')
datetick('x', 15)
set(gca, 'ydir', 'normal')
set(gca, 'xticklabel', [])
set(gca, 'xgrid', 'on')
ylabel('mab')
cb = colorbar;
ylabel(cb, 'Viw')
caxis([-.04 .04])
xlim(XLIM)
ylim([0 70])
adjust_space

subplot(4, 1, 4)
contourf(timeVec, zVec, Wiw, 100, 'linestyle', 'none')
hold on
%contour(T.timeVec, T.zVec+39.67, T.Tbin, [0:1:10], 'color', 'k', 'linewidth', 2);
contour(Tdata.timeVec, flipud(Tdata.zVec+.1), Tdata.Tbin, [1:.1:1.5 4:10], 'linecolor', 'k')
datetick('x', 7)
set(gca, 'ydir', 'normal')
set(gca, 'xgrid', 'on')
ylabel('mab')
xlabel('March 2010')
xlim(XLIM)
ylim([0 70])
cb = colorbar;
ylabel(cb, 'Wiw')
caxis([-.01 .01])
adjust_space

keyboard


I = find(timeVec>=datenum(2010,3,6,9,0,0) & timeVec<=datenum(2010,3,6,15,0,0));
timeVec = timeVec(I);
Uiw = Uiw(:,I);
Viw = Viw(:,I);
Wiw = Wiw(:,I);


figure(2)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 16 15])   
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.01 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

subplot(3, 1, 1)
contourf(timeVec, zVec, Uiw, 100, 'linestyle', 'none')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'reverse')
ylabel('Depth (m)')
c= colorbar;
ti = ylabel(c,'Uiw', 'FontSize', 10);
caxis([-.05 .05])
adjust_space

subplot(3, 1, 2)
contourf(timeVec, zVec, Viw, 100, 'linestyle', 'none')
set(gca, 'ydir', 'reverse')
c = colorbar;
ti = ylabel(c,'Viw', 'FontSize', 10);
caxis([-.05 .05])
set(gca, 'xticklabel', [])
ylabel('Depth (m)')
xlim([timeVec(1) timeVec(end)])
adjust_space

subplot(3, 1, 3)
contourf(timeVec, zVec, Wiw, 100, 'linestyle', 'none')
set(gca, 'ydir', 'reverse')
c = colorbar;
ti = ylabel(c,'Viw', 'FontSize', 10);
caxis([-.02 .02])
datetick('x', 15)
ylabel('Depth (m)')
xlabel('6 March 2010')
xlim([timeVec(1) timeVec(end)])
adjust_space