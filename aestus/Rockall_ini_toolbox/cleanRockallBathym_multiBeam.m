clear all
close all

load('RockallBathym_CTD.dat')

dv = RockallBathym_CTD(:,3);
zv = RockallBathym_CTD(:,4);


% Remove top and bottom 
z = zv([1:3950]);
x = dv([1:3950]);
x0 = 40;

I = find(diff(z)==0);
z(I) = [];
x(I) = [];
x = x-x0;

% $$$ [z, I] = unique(z);
% $$$ x = x(I)-x0;

xVec = -250:.05:500;
zVec = interp1(x,z,xVec);


% clip top
[Y, I] = max(zVec);
zVec(1:I-1) = Y;

% clip bottom
[Y, I] = min(zVec);
zVec(I+1:end) = Y;

% smooth
% $$$ I = find(~isnan(zVec));
% $$$ zsmooth = loess(xVec(I),zVec(I),xVec(I),.01,2);
% $$$ zVecSmooth = runmean(zVec, 20);
zVecSmooth = zVec; % thus raw (by-passed)

plot(xVec, zVecSmooth, 'k')



%% density casts
load /home/cyrf0006/research/NIOZ/RockallBank/CTDinfo.mat
dz = 10;
zCTD = 0:dz:2500;
rhoCTD = nan(size(zCTD));
for i = 1:length(zCTD)
    I = find(ZVec>=zCTD(i)-dz & ZVec<zCTD(i)+dz);
    rhoCTD(i) = nanmean(RVec(I));
end


myX = (xVec)*1000;
I = find(myX>=0);
xplus = myX(I);
I = find(myX<0);
xminus = myX(I);

figure(1)
clf

% S1
s1 = subplot(3,3,[1 2]);
eta0 = 2;
a = 2.5e4;

AetaPlus = eta0.*exp(-xplus/a);
plot(xplus,  AetaPlus)
hold on
AetaMinus = eta0.*exp(xminus/a);
plot(xminus,  AetaMinus)
set(gca, 'xticklabel', [])
ylabel('\eta (m)')
xlim([-250000 500000])

% S2
s2 = subplot(3,3,[4 5 7 8]);
plot(myX/1000, zVecSmooth, 'k')

global Z S
Z = myX;
S = zVecSmooth;
a0 = [-1400 -1068 -58000 2000];
asol = fminsearch ( 'fitFun11', a0);
topoFit = asol(1) + asol(2)*tanh((myX+asol(3))/asol(4));

hold on
%plot(myX/1000, -1068*tanh((myX-58000)/20000)-1400, 'r')
plot(myX/1000, topoFit, 'r')
plot(dv-x0, zv, '--k')
xlabel('x (km)')
ylabel('Depth (m)')
xlim([-250 500])
legend('smooth topo', 'fit topo', 'real topo')



% S3
s3 = subplot(3,3,[6 9]);
plot(rhoCTD, zCTD)
set(gca, 'ydir', 'reverse')
ylim([0 2500])
set(gca, 'yticklabel', [])
xlabel('\sigma_1 (kg m^{-3})')

global Z S
Z = zCTD;
S = rhoCTD;
I = find(~isnan(S));
S = S(I);
Z = Z(I);


format long
% 1st fit
a0 = [30 .001 -1 10 -100];
asol = fminsearch ( 'fitFun1', a0);
S1 = asol(1) + asol(2).*zCTD + asol(3).*tanh((zCTD-asol(4))./asol(5));
S0 = a0(1) + a0(2).*Z + a0(3).*tanh((Z-a0(4))./a0(5));

hold on
plot(S1, zCTD, 'r--')

print('-dpng', '-r300', 'RockallTide_setup.png')

dlmwrite('SmoothRockalBathymWithCWC_CTD.dat', [myX' zVecSmooth'] ,'delimiter',' ','precision',12) 