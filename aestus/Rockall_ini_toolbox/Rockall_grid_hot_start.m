%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the Boundary Mixing case.
%

clear;

% Save the grid into text file in a format appropriate for the model
%root = '/Volumes/Macintosh HD 2/Research/Windex/shear_convection/aestusRuns/acrossChannel/dpdy/';

root_start = '../Runs/run2f/output/';
root = '../Runs/run3/';
 
% Load initial condition
n_ini = 1153;
load([root_start,'grid.mat']);
sig_ini = getfield([root_start,'sig'],n_ini);
u_ini   = getfield([root_start,'u'],n_ini);
v_ini   = getfield([root_start,'v'],n_ini);
w_ini   = getfield([root_start,'w'],n_ini);
h_ini   = getfield([root_start,'h'],n_ini);

sig_ini = putNaN(sig_ini,flag_s);

%% GRID


% Run 3
xmin = -500000000;
xmax =  500000000;
zmin = 0.0;
zmax = 2500;
dxMin = 10;
dxMax = 5000000;
dzMin = 2.0;
dzMax = 50.0;
x1 =  -14400;
x2 =  -12400;
z1 = 800.0; 
z2 = 900;

chi0 = makegrid(xmin,xmax,dxMin,dxMax,x1,x2,0.1,true);
%pause
zeta0 = makegrid(zmin,zmax,dzMin,dzMax,z1,z2,0.1,true);
%pause

%% Number of grid points
imax = length(chi0);
kmax = length(zeta0);


%% Width (B) and depth (H) 
B0(1:imax,1:kmax) = 1.0;

a  = [-1408.18  -1068.70  0.0 23413.12];
H0 = a(1) + a(2)*tanh((chi0 + a(3))/a(4));
H0 = abs(H0);
H0(1)     = 0.0;
H0(2)     = 0.0;
H0(end-1) = 0.0;
H0(end)   = 0.0;


S(1:imax,1:kmax)  = -999999;
T(1:imax,1:kmax)  = -999999;


%% Initial conditions


sig = interp2(chi',z',sig_ini',chi0,zeta0');
sig = sig';
j = 0;
for i = 1:imax
    for k = 1:kmax
        j = j + 1;
        SIG(j)   = sig(i,k);
        CHI0(j)  = chi0(i); 
        ZETA0(j) = zeta0(k); 
    end
end
ii = isfinite(SIG);
sig = griddata(CHI0(ii),ZETA0(ii),SIG(ii),chi0,zeta0');

u   = interp2(x',z,u_ini',chi0,zeta0');
v   = interp2(x',z,v_ini',chi0,zeta0');
w   = interp2(chi',zeta,w_ini',chi0,zeta0');
hh  = interp1(chi,h_ini,chi0);

sig = sig';
u   = u';
v   = v';
w   = w';
hh  = hh';

sig(:,1) = sig(:,2);
u(:,1)   = u(:,2);
v(:,1)   = v(:,2);
w(:,1)   = 0.0;

ii = find(isnan(sig) == 1);
for i = 1:imax
    for k = 2:kmax
        if isnan(sig(i,k)) == 1
            sig(i,k) = sig(i,k-1);
        end
    end
end
sig(1,:) = sig(3,:);
sig(2,:) = sig(3,:);
%sig(ii) = nanmax(sig(:));

ii = find(isnan(u) == 1);
u(ii) = 0.0;

ii = find(isnan(v) == 1);
v(ii) = 0.0;

ii = find(isnan(w) == 1);
w(ii) = 0.0;


%% Figures for verification
figure
plot(chi0,H0);
set(gca,'ydir','reverse');
title('H(x)');

figure
pcolor(chi0,zeta0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('C(x,z)');

figure
pcolor(chi0,zeta0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

%% Save grid
 
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',chi0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',zeta0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field
fid = fopen([root,'field.ini'],'w');
for i=1:imax
  for k=1:kmax
    %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),hh(i));
  end
end
fclose(fid);