%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the Rockall case (cold startr).
%
%% RUN3 %%


clear all;
close all;

% Save the grid into text file in a format appropriate for the model
root = './';


%% GRID SPECIFICATIONS
H1 = 339.48;
H2 = 2476.88;
L = 6e4;

% Run CWC
xmin  = -500000000;
xmax  =  500000000;
zmin  = 0.0;
zmax  = 2500;
dxMin = 500;
dxMax = 5000000;
dzMin = 5;
dzMax = 50.0;
z1    = H1; 
z2    = H2;
x1    =  -L/2;
x2    =  L/2;

chi0  = makegrid(xmin,xmax,dxMin,dxMax,x1,x2,0.1,true);
zeta0 = makegrid(zmin,zmax,dzMin,dzMax,z1,z2,0.1,true);

%% Number of grid points
imax = length(chi0);
kmax = length(zeta0);

%% Width (B) 
% Constant width throughout
B0(1:imax,1:kmax) = 1.0;

%% Depth (H)
% tanhish profile
a  = [-1408.18  -1068.70  0.0 23413.12];
H0 = a(1) + a(2)*tanh((chi0 + a(3))/a(4));
H0 = abs(H0);

% $$$ % ideal topography (regular slope)
% $$$ alpha = (H2-H1)./L;
% $$$ beta = (H2+H1)./2;
% $$$ H0 = alpha.*chi0 + beta;
% $$$ [Y, I] = min(abs(chi0-(-L/2)));
% $$$ H0(1:I) = H1;
% $$$ [Y, I] = min(abs(chi0-(L/2)));
% $$$ H0(I:end) = H2;

% Close both ends
H0(1)     = 0.0;
H0(2)     = 0.0;
H0(end-1) = 0.0;
H0(end)   = 0.0;

% $$$ CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
% $$$ xCWC = CWC_bathy(:,1)+200;%-60000000; % in m
% $$$ zCWC = abs(CWC_bathy(:,2));


%% Density and O2 profiles
%Constant density
sig0 = 31.6;
sigH = 32.5;
sigLin = interp1([zeta0(1) zeta0(end)], [sig0 sigH], zeta0);


%% Initial conditions

 for i = 1:imax
   for k = 1:kmax

     u(i,k) = 0.0;
     v(i,k) = 0.0;
     w(i,k) = 0.0;          

     % When the model is used with only density, set T and S to crazy 
     % values such that if those numbers are manipulated it will cause an 
     % obvious problem in the model.
     S(i,k) = -999999;
     T(i,k) = -999999;
 
     % The density backgorund profile
% $$$      b =  [31.25   0.00062596  -0.20034 95.684 -54.3126];
% $$$      sig(i,k) = b(1) + b(2).*zeta0(k) + b(3).*tanh((zeta0(k)-b(4))./b(5));
     
     sig(i,k) = sigLin(k); % <---------- sigma_1 CWC now at sig.
                           %     C1(i,k) = O2Itp3(k); % <---------- O2 CWC now at C1.

% $$$      if i>=I1 & i<=I2
% $$$          C2(i,k) = 1; % <---------- binary tracer
% $$$      else
% $$$          C2(i,k) = 0;
% $$$      end


   end 
   
   % The initial water level
   h(i) = 0.0;
 
 end

 
%% Figures for verification
figure
plot(chi0,H0);
set(gca,'ydir','reverse');
title('H(x)');

figure
pcolor(chi0,zeta0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('sig(x,z)');

figure
pcolor(chi0,zeta0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');
% $$$ 
% $$$ figure
% $$$ pcolor(chi0,zeta0,C1');
% $$$ set(gca,'ydir','reverse');
% $$$ shading('interp');
% $$$ colorbar;
% $$$ title('C1(x,z)');
% $$$ 
% $$$ figure
% $$$ pcolor(chi0,zeta0,C2');
% $$$ set(gca,'ydir','reverse');
% $$$ shading('interp');
% $$$ colorbar;
% $$$ title('C2(x,z)');
%% Save grid
 
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',chi0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',zeta0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field
fid = fopen([root,'field.ini'],'w');
for i=1:imax
  for k=1:kmax

      %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));

    % If one (or more) tracers C add them at the end
    %    fprintf(fid,['%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n'],i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i),C1(i,k));
    fprintf(fid,['%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n'],i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));
    %    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    
  end
end
fclose(fid);
