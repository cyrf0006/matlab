function [x,dx] = makegrid(xMin,xMax,dxMin,dxMax,x1,x2,slope,graph);
% MAKEGRID Make a stretched vector for the numerical model aestus
%
%   Input
%       xMin:          The minimum value of the vector
%       xMax:          The maximum value of the vector
%       dxMin:         The shorter grid size required
%       dxMax:         The larger  grid size required
%       x1:            The position of the first transition
%       x2:            The position of the second transition
%       stretchFactor: Maximum relative change (generally 0.1)
%       graph:         graph = true makes graphs for inspection
%
%   Output
%       x:  The grid
%       dx: The grid resolution
%
%   Example:
%
%       Example 1: Makes a grid from -50 km to 100 km with low resolution
%                  of 1 km and high resolution of 0.1 km between 50 - 80 km.
%         [x,dx] = makegrid(-50,100,0.1,1,50,80,0.1);
%
%       Example 2: Makes a uniform grid of constant low resolution 1 km
%          [x,dx] = makegrid(-50,100,1,1,-Inf,Inf,0.1);
%
%       Example 3: Only one transition at 50 km from 1 km to 0.1 km.
%          [x,dx] = makegrid(-50,100,0.1,1,50,Inf,0.1);

if dxMax < dxMin
    display('  ');
    display('  dxMax muxt be greater than dxMin in function makegrid.');
    display('  ');
    x = NaN; dx = NaN;
    return
end
if xMax < xMin
    display('  ');
    display('  xMax muxt be greater than xMin in function makegrid.');
    display('  ');
    x = NaN; dx = NaN;
    return
end
if x2 < x1
    display('  ');
    display('x2 muxt be greater than x1 in function makegrid.');
    display('  ');
    x = NaN; dx = NaN;
    return
end

% Determine the transition points
xt1 = x1 - (dxMax - dxMin)/slope;
xt2 = x2 + (dxMax - dxMin)/slope;

x(1)  = xMin;
i = 1;
while x(i) < xMax
    
    i = i+1;
    
    if x(i-1) < xt1
        dx(i-1) = dxMax;
    elseif x(i-1) >= xt1 & x(i-1) < x1
        dx(i-1) = -slope*(x(i-1) - xt1) + dxMax;
    elseif x(i-1) >= x1 & x(i-1) <= x2
        dx(i-1) = dxMin;
    elseif x(i-1) > x2 & x(i-1) < xt2
        dx(i-1) = slope*(x(i-1) - xt2) + dxMax;
    elseif x(i-1) > xt2
        dx(i-1) = dxMax;
    end
    
    x(i) = x(i-1) + dx(i-1);
    
end
dx(end+1) = dx(end);


if x(end) ~= xMax
    display(' ');
    display('  WARNING: x(end) is not equal to xMax in makegrid');
    display(['  xMax = ', num2str(xMax)]);
    display(['  x(end) = ', num2str(x(end))]);
    display(' ');
end


% Graphs
if graph
    subplot(3,1,1);
    plot(x,x);
    xlabel('x');
    ylabel('x');
    
    subplot(3,1,2);
    plot(x,dx);
    xlabel('x');
    ylabel('dx');
    
    subplot(3,1,3);
    plot(x,gradient(dx)./dx);
    xlabel('x');
    ylabel('gradient(dx)/dx');
    
end
