clear all
close all

load('RockallBathym_CTD_2.dat')
load('RockallBathym_CWC_2.dat')

dv = RockallBathym_CTD_2(:,3);
zv = RockallBathym_CTD_2(:,4);
zv2 = RockallBathym_CWC_2(:,4);
dv2 = RockallBathym_CWC_2(:,3);
% $$$ dv3 = RockallBathym_CTD(:,3);
% $$$ zv3 = RockallBathym_CTD(:,4);


figure(1)
clf
plot(dv, zv, 'b')
hold on
plot(dv2, zv2, 'm')


%% Smooth and merge Low / High res. bathym
I = find(~isnan(zv)); 
zvSmooth = loess(dv(I),zv(I),dv(I),.1,1);
dvSmooth = dv(I);
I = find(~isnan(zv2)); 
zv2Smooth = loess(dv2(I),zv2(I),dv2(I),.01,1);
dv2Smooth = dv2(I);

x1 = 37.45;
x2 = 39.4815;
I= find(dv2Smooth>=x1 & dv2Smooth <=x2);
zv2Smooth = zv2Smooth(I);
dv2Smooth = dv2Smooth(I);

[Y, I1] = min(abs(dvSmooth-x1));
[Y, I2] = min(abs(dvSmooth-x2));

xMerge = [dvSmooth(1:I1); dv2Smooth; dvSmooth(I2:end)];
zMerge = [zvSmooth(1:I1); zv2Smooth; zvSmooth(I2:end)];
plot(xMerge, zMerge, 'r')


% Save file for o2paper <---------------------
%save bathy_multi_and_gebco.mat dv zv

%% Clip top and bottom, extend domain...
% Remove top and bottom 
z = zMerge([1:4278]);
x = xMerge([1:4278]);
x0 = 38.65;

I = find(diff(z)==0);
z(I) = [];
x(I) = [];
x = x-x0;

xVec = -250:.05:500;
zVec = interp1(x,z,xVec);

% clip top
[Y, I] = max(zVec);
zVec(1:I-1) = Y;

% clip bottom
[Y, I] = min(zVec);
zVec(I+1:end) = Y;

zVecSmooth = zVec;
plot(xVec, zVecSmooth, 'k')
disp('done!')

% Save file for o2paper <---------------------
save bathy_multi_CWC2.mat xVec zVecSmooth

%% density casts
load /home/cyrf0006/research/NIOZ/RockallBank/CTDinfo.mat
dz = 10;
zCTD = 0:dz:2500;
rhoCTD = nan(size(zCTD));
for i = 1:length(zCTD)
    I = find(ZVec>=zCTD(i)-dz & ZVec<zCTD(i)+dz);
    rhoCTD(i) = nanmean(RVec(I));
end




% $$$ %% idealized topography
% $$$ g = 9.8; omega = 1e-4;
% $$$ h0 = 2500; %m
% $$$ zVec = zVec; %m
% $$$ h = zVecSmooth;
% $$$ q = h./h0;
% $$$ epsilon = 4*omega^2*g/h0;
% $$$ 
% $$$ z = nan(size(xVec));
% $$$ dx = xVec(2)-xVec(1);
% $$$ for i = 1:length(xVec)
% $$$     z(i) = sum(1./q(1:i)).*dx;
% $$$ end
% $$$ lambda = 0.9;
% $$$ q = 1+lambda.*tanh(z);
% $$$ x = z+lambda.*log(cosh(z));


myX = (xVec)*1000;
I = find(myX>=0);
xplus = myX(I);
I = find(myX<0);
xminus = myX(I);

figure(1)
clf

% S1
s1 = subplot(3,3,[1 2]);
eta0 = 2;
a = 2.5e4;

AetaPlus = eta0.*exp(-xplus/a);
plot(xplus,  AetaPlus)
hold on
AetaMinus = eta0.*exp(xminus/a);
plot(xminus,  AetaMinus)
set(gca, 'xticklabel', [])
ylabel('\eta (m)')
xlim([-250000 500000])

% S2
s2 = subplot(3,3,[4 5 7 8]);
plot(myX/1000, zVecSmooth, 'k')

global Z S
Z = myX;
S = zVecSmooth;
a0 = [-1400 -1068 -58000 2000];
asol = fminsearch ( 'fitFun11', a0);
topoFit = asol(1) + asol(2)*tanh((myX+asol(3))/asol(4));

hold on
%plot(myX/1000, -1068*tanh((myX-58000)/20000)-1400, 'r')
plot(myX/1000, topoFit, 'r')
plot(dv-x0, zv, '--k')
xlabel('x (km)')
ylabel('Depth (m)')
xlim([-250 500])
legend('smooth topo', 'fit topo', 'real topo')



% S3
s3 = subplot(3,3,[6 9]);
plot(rhoCTD, zCTD)
set(gca, 'ydir', 'reverse')
ylim([0 2500])
set(gca, 'yticklabel', [])
xlabel('\sigma_1 (kg m^{-3})')

global Z S
Z = zCTD;
S = rhoCTD;
I = find(~isnan(S));
S = S(I);
Z = Z(I);


format long
% 1st fit
a0 = [30 .001 -1 10 -100];
asol = fminsearch ( 'fitFun1', a0);
S1 = asol(1) + asol(2).*zCTD + asol(3).*tanh((zCTD-asol(4))./asol(5));
S0 = a0(1) + a0(2).*Z + a0(3).*tanh((Z-a0(4))./a0(5));

hold on
plot(S1, zCTD, 'r--')

print('-dpng', '-r300', 'RockallTide_setup_2.png')

dlmwrite('SmoothRockalBathymWithCWC_CWC_2.dat', [myX' zVecSmooth'] ,'delimiter',' ','precision',12) 