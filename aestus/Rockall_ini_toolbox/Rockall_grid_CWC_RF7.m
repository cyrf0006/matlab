%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the Rockall case (cold startr).
%
%% RUN3 %%


clear all;
close all;

% Save the grid into text file in a format appropriate for the model
root = './';
 

%% GRID SPECIFICATIONS

% Run CWC
xmin  = -500000000;
xmax  =  500000000;
zmin  = 0.0;
zmax  = 2500;
dxMin = 100;
dxMax = 5000000;
dzMin = 2.5;
dzMax = 50.0;
z1    = 345.0; 
z2    = 1200;
x1    =  -41000;
x2    =  10000;

chi0  = makegrid(xmin,xmax,dxMin,dxMax,x1,x2,0.1,true);
zeta0 = makegrid(zmin,zmax,dzMin,dzMax,z1,z2,0.1,true);

%% Number of grid points
imax = length(chi0);
kmax = length(zeta0);

%% Width (B) 
% Constant width throughout
B0(1:imax,1:kmax) = 1.0;

%% Depth (H)
% tanhish profile
% $$$ a  = [-1408.18  -1068.70  0.0 23413.12];
% $$$ H0 = a(1) + a(2)*tanh((chi0 + a(3))/a(4));
% $$$ H0 = abs(H0);

% real (smooth) topo
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
xCWC = CWC_bathy(:,1)+200;%-60000000; % in m
zCWC = abs(CWC_bathy(:,2));

Hitp = interp1([min(chi0); xCWC; max(chi0)], [zCWC(1); zCWC; zCWC(end)], chi0);
H0 = Hitp;

% Close both ends
H0(1)     = 0.0;
H0(2)     = 0.0;
H0(end-1) = 0.0;
H0(end)   = 0.0;


%% Density and O2 profiles
% ----> From argo
load('/home/cyrf0006/research/argo/oxyArgo/o2_meanProfile_rockallthough.mat')
sig_ave(1) = [];
O2_ave(1) = [];
zVec(1) = [];
I = find(isnan(sig_ave));
zVec(I) = [];
sig_ave(I) = [];
O2_ave(I) = [];

sigItp = interp1([0; zVec; max(zeta0)], [31.581; sig_ave; 32.5], zeta0);
O2Itp = interp1([0; zVec; max(zeta0)], [O2_ave(1); O2_ave; O2_ave(end)], zeta0);

% ----> From deep CTD
deepCTD = load('/home/cyrf0006/research/NIOZ/RockallBank/deepCTD/deepCast.mat');
O2Deep = [deepCTD.O2(3); deepCTD.O2(3:end); 250.1; 250.1];
zVecDeep = [0; deepCTD.P(3:end); 2000; 3000];

[SA, in_ocean] = gsw_SA_from_SP(deepCTD.SP,deepCTD.P,-15.8044,55.4324);
CT = gsw_CT_from_t(SA,deepCTD.T90,deepCTD.P);
sig1 = gsw_sigma1(SA,CT);
sig1Deep = [31; sig1(3:end); 32.5];
zVecDeep_sig = [0; deepCTD.P(3:end); 2600];

% home-made gaussian filter <---- dangerous because smoothing an un-even Vector
Psmooth = 0:min(diff(zeta0)):max(zeta0)+min(diff(zeta0));
O2smooth = interp1(zVecDeep, O2Deep, Psmooth); % interp to zeta0
sigsmooth = interp1(zVecDeep_sig, sig1Deep, Psmooth); % interp to zeta0                                        
%O2smooth = interp1(zVecDeep, O2Deep, zeta0); % interp to zeta0
%sigsmooth = interp1(zVecDeep_sig, sig1Deep, zeta0);
O2filt = gaussfiltfilt(O2smooth, 14, 75);
sigfilt = gaussfiltfilt(sigsmooth, 14,75);
% $$$ O2Itp2 = O2filt;
% $$$ sigItp2 = sigfilt;
O2Itp2 = interp1(Psmooth, O2filt, zeta0);
sigItp2 = interp1(Psmooth, sigfilt, zeta0);


% ----> From 24h transect
cwcCTD = load('/home/cyrf0006/research/NIOZ/RockallBank/CTD_rockall2012.mat');
I = find(zeta0>=1250);
O2CWC = [cwcCTD.o2Vec(3); cwcCTD.o2Vec(3:end); O2Itp2(I)'; O2Itp2(end)];
sig1CWC = [cwcCTD.sig1Vec(3); cwcCTD.sig1Vec(3:end); sigItp2(I)'; sigItp2(end)];
zVecCWC = [0; cwcCTD.zVec(3:end)'; zeta0(I)'; max(Psmooth)];
% $$$ O2smooth = interp1(zVecCWC, O2CWC, zeta0);
% $$$ sigsmooth = interp1(zVecCWC, sig1CWC, zeta0);
O2smooth = interp1(zVecCWC, O2CWC, Psmooth);
sigsmooth = interp1(zVecCWC, sig1CWC, Psmooth);

O2filt = gaussfiltfilt(O2smooth, 10, 75);
sigfilt = gaussfiltfilt(sigsmooth, 10, 75);

% $$$ O2Itp3 = O2filt;
% $$$ sigItp3 = sigfilt;
O2Itp3 = interp1(Psmooth, O2filt, zeta0);
sigItp3 = interp1(Psmooth, sigfilt, zeta0);


% ---> passive tracer for exchange with interior
[Y, I1] = min(abs(chi0-(-1000)));
[Y, I2] = min(abs(chi0-3000));



%% to check density difference
figure(15)
clf
plot(sigItp, zeta0)
hold on
plot(sigItp2, zeta0, 'm')
plot(sig1, deepCTD.P,'g')  
plot(sigItp3, zeta0, 'r')
plot(cwcCTD.sig1Vec, cwcCTD.zVec, 'k')  
set(gca, 'ydir', 'reverse')
legend('Argo', 'ThroughSmooth','ThroughRaw', 'CWCsmooth', 'CWCraw', 'location', 'southWest')
hold off

%% to check O2 difference
figure(16)
clf
plot(O2Itp, zeta0)
hold on
plot(O2Itp2, zeta0, 'm')
plot(deepCTD.O2(3:end), deepCTD.P(3:end), 'g')  
plot(O2Itp3, zeta0, 'r')
plot(cwcCTD.o2Vec, cwcCTD.zVec, 'k')  
legend('Argo', 'ThroughSmooth','ThroughRaw', 'CWCsmooth', 'CWCraw', 'location', 'southWest')
set(gca, 'ydir', 'reverse')
hold off


%% Initial conditions

 for i = 1:imax
   for k = 1:kmax

     u(i,k) = 0.0;
     v(i,k) = 0.0;
     w(i,k) = 0.0;          

     % When the model is used with only density, set T and S to crazy 
     % values such that if those numbers are manipulated it will cause an 
     % obvious problem in the model.
     S(i,k) = -999999;
     T(i,k) = -999999;
 
     % The density backgorund profile
% $$$      b =  [31.25   0.00062596  -0.20034 95.684 -54.3126];
% $$$      sig(i,k) = b(1) + b(2).*zeta0(k) + b(3).*tanh((zeta0(k)-b(4))./b(5));
     
     sig(i,k) = sigItp3(k); % <---------- sigma_1 CWC now at sig.
     C1(i,k) = O2Itp3(k); % <---------- O2 CWC now at C1.

     if i>=I1 & i<=I2
         C2(i,k) = 1; % <---------- binary tracer
     else
         C2(i,k) = 0;
     end


   end 
   
   % The initial water level
   h(i) = 0.0;
 
 end

 
%% Figures for verification
figure
plot(chi0,H0);
set(gca,'ydir','reverse');
title('H(x)');

figure
pcolor(chi0,zeta0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('sig(x,z)');

figure
pcolor(chi0,zeta0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

figure
pcolor(chi0,zeta0,C1');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('C1(x,z)');

figure
pcolor(chi0,zeta0,C2');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('C2(x,z)');
%% Save grid
 
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',chi0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',zeta0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field
fid = fopen([root,'field.ini'],'w');
for i=1:imax
  for k=1:kmax

      %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));

    % If one (or more) tracers C add them at the end
    %    fprintf(fid,['%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n'],i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i),C1(i,k));
    fprintf(fid,['%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n'],i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i),C1(i,k),C2(i,k));
    %    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    
  end
end
fclose(fid);
