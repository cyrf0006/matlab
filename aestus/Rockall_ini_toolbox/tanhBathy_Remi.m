clear all
close all

load('RockallBathym.dat')

dv = RockallBathym(:,3);
zv = RockallBathym(:,4);


% Remove only top and bottom (keep inner part)
z = zv([2030:3920]);
x = dv([2030:3920]);
x0 = 94.5;

xVec = -250:.05:500;
zVec = interp1((x-x0),z,xVec);

% clip top
[Y, I] = max(zVec);
zVec(1:I-1) = Y;

% clip bottom
[Y, I] = min(zVec);
zVec(I+1:end) = Y;

% smooth (maybe not needed)
zVecSmooth = runmean(zVec, 20);

myX = (xVec)*1000;
I = find(myX>=0);
xplus = myX(I);
I = find(myX<0);
xminus = myX(I);


figure(1)
clf
plot(myX/1000, zVecSmooth, 'k')

global Z S
Z = myX;
S = zVecSmooth;
a0 = [-1400 -1068 -58000 2000];
asol = fminsearch ( 'fitFun11', a0);
topoFit = asol(1) + asol(2)*tanh((myX+asol(3))/asol(4));

hold on
%plot(myX/1000, -1068*tanh((myX-58000)/20000)-1400, 'r')
plot(myX/1000, topoFit, 'r')
plot(dv-x0, zv, '--k')
xlabel('x (km)')
ylabel('Depth (m)')
xlim([-250 500])
legend('smooth topo', 'fit topo', 'real topo')
