%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the model aestus (Bourgault and Kelley, 2004).
%
% The current setting is meant for simulating the Turbidity Current 
% test case.
%
% Author: Daniel Bourgault, Fall 2008.

%% PARAMETERS
% Geometry 
H = 0.4;       % Tank depth
B = 0.2;       % Tank width 
L = 2.0;       % Tank length
L_lock = 0.2;  % Length of the locked initial sediment part

S1i = 5;      % Initial sediment concentration in the lock
S2i = 2;      % Initial sediment concentration in the lock

% Grid size
dx = 0.01; 
dz = 0.01; 


%% Make the x grid. 
x0 = [0:dx:L];
z0 = [0:dz:H];

%% Number of grid points
imax=length(x0);
kmax=length(z0);

%% Depth and width
H0(1:imax) = H;
H0(1:2)=0.0; H0(imax-1:imax)=0.0; % Tank is closed at both ends.
B0(1:imax,1:kmax)=B;

%% Initial condition for rho, u, w and h, T, S and C
u(1:imax,1:kmax)=0.0;
v(1:imax,1:kmax)=0.0;
w(1:imax,1:kmax)=0.0;
h(1:imax)=0.0;
sig(1:imax,1:kmax)=0.0;
T(1:imax,1:kmax)=-99999;
S(1:imax,1:kmax)=-99999;

%% Initial sediment concentration
S1(1:imax,1:kmax) = 0.0;
S2(1:imax,1:kmax) = 0.0;
for i = 1:imax
  if x0(i) <= L_lock
    for k = 1:kmax
      S1(i,k) = S1i;
      S2(i,k) = S2i;
    end
  end
end
%% Figures for verification
figure
pcolor(x0,z0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('\rho(x,z)');

figure
pcolor(x0,z0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

figure
pcolor(x0,z0,S1');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('S1');

figure
pcolor(x0,z0,S2');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('S2');


%% Save grid
% Save the grid into text file in a format appropriate for the model
root = '../../TestCases/TurbidityCurrent/';
 
fid1 = fopen([root,'x0.grid'],'w');
fid2 = fopen([root,'z0.grid'],'w');
fid3 = fopen([root,'H0.grid'],'w');
fid4 = fopen([root,'B0.grid'],'w');
  
 for i=1:imax
   fprintf(fid1,'%12.8f\n',x0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',z0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field
filename1=[root,'field.ini']; % Filename for the initial condition.
filename2=[root,'sed.ini'];   % Filename for the sediment.

fid1 = fopen(filename1,'w');
fid2 = fopen(filename2,'w');

for i=1:imax
  for k=1:kmax
    %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    fprintf(fid1,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));
    fprintf(fid2,'%6i %6i %12.8f %12.8f\n',i,k,S1(i,k),S2(i,k));
  end
end
fclose(fid1);
fclose(fid2);
