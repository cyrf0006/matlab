%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the lock exchange flow test case.
%
% Inspired from:
% Fringer et. al. (2006) An unstructured-grid, finite-volume, nonhydrostatic,
%   parallel coastal ocean simulator, Ocean Modelling, 14, 139-173.

%% PARAMETERS
% Geometry 
Hmax = 300;       % Tube depth
B    = 1.0;       % Tube width 
L    = 50000;     % Tube length

% Grid size
dx = 400.0;
dz = 5.0;

% Salinity structure
dh = 10.0;
zc = 20.0;
S1 = 20.0;
S2 = 35.0;

x0 = [-L/2+(dx/2)-2*dx:dx:L/2-(dx/2)+2*dx];
z0 = [0:dz:Hmax+2*dz];

%% Number of grid points
imax=length(x0);
kmax=length(z0);

%% Depth and width
H0 = -4.8d-7*x0.^2+300;
%H0(1:imax) = 300;
ii = find(H0 < 3*dz);
H0(ii) = 3*dz;
H0(1:2) = 0.0; 
H0(imax-1:imax) = 0.0;
B0(1:imax,1:kmax) = B;

%% Initial conditions

 for i=1:imax
   for k = 1:kmax

     % S(z) exponential function
     S(i,k) = 35.*exp(-10./(z0(k)+30));
     
     u(i,k) = 0.0;
     v(i,k) = 0.0;
     w(i,k) = 0.0;          

     % T(z) 5th order fit to data
     a1 = -0.000000000078309;
     a2 = 0.000000076845478;
     a3 = -0.000028887612060;
     a4 = 0.005077796184885;
     a5 = -0.381721371883246;
     a6 = 10.368071802373141;
     
     T(i,k) = a1*z0(k)^5 + a2*z0(k)^4 + a3*z0(k)^3 + a4*z0(k)^2 + a5*z0(k) + a6;
     
     % Cold intermediate layer
     %T_CIL = 0.0;
     %T0 = 4.0;
     %if z0(k) >= 50.0 & z0(k) <= 150.0 
     %  T(i,k) = T_CIL;
     %else
     %  T(i,k) = T0;
     %end
     
     C(i,k) = z0(k);
     
   end 
   
   h(i) = 0.0;
 
 end

% We don't simulate T and S only sigma. Set these to any unphysical values
% such that it will cause obvious errors if they get manipulated by
% mistake by the code.
sig(1:imax,1:kmax)=-99999;

%% Figures for verification
figure
plot(x0,H0);
set(gca,'ydir','reverse');
title('H(x)');

figure
pcolor(x0,z0,S');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('S(x,z)');

figure
pcolor(x0,z0,T');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('T(x,z)');

figure
pcolor(x0,z0,C');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('C(x,z)');

figure
pcolor(x0,z0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

%% Save grid
% Save the grid into text file in a format appropriate for the model
 root = '../../TestCases/BndMixing/';
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',x0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',z0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field
fid = fopen([root,'field.ini'],'w');
for i=1:imax
  for k=1:kmax
    %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i),C(i,k));
  end
end
fclose(fid);