%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the model aestus (Bourgault and Kelley, 2004).
%
% The current setting is meant for simulating Thorpe (1968) 
% laboratory experiment (his Figure 5). See also Bourgault 
% and Kelley (2004). The parameters are slightly different than 
% BK04.
%
% Author: Daniel Bourgault, Spring 2008.

%% PARAMETERS
% Geometry 
H=0.03;             % Tube depth
B=0.1;              % Tube width 
Ltotal=1.83;        % Tube length
L_high_res=0.5;     % The region of high resolution near the center of the tube. 

% Grid size
dxmin=1.d-3;        % Note, this is higher resolution than used in BK04
dxmax=1.d-2;
dz=6.d-4;           % Slightly higher res. than BK04. 
z1=3.d-3;           % Depth of the first cell below the free surface. 
                    % Must be greater than the meximum expected movement of
                    % the free surface. 

% Density structure
zi = H/2;
dh=1.5d-3;
rho0 = 1.d3;
drho = 15.6;

% Stability
Ri=0.025;   % BK04 Ri = 0.025. This gave a growth rate similar to Thorpe's
            % observations. However, Thorpe (1968) estimated his Ri to be
            % Ri = 0.09 (p. 701, last sentence).

% Other parameters
g=9.81;
noise_level=1.d-5;    % Amplitude (m/s) of the white noise added to the horizontal velocity 
fac_stretch=1.1;      % A factor for smoothly stretching the grid from
                      % high to low resolution regions. I normally use 1.1. 

%% Make the x grid. 
% First, the positive section is made (xr for x-right)
% and then the negative section (xl for x-left). Both are then combined
% to produce the vector x0 which is the final x-grid to be used by the
% model.

% Make xr
i=1;
xr(i)=0;
while xr(i) <= Ltotal/2
  if xr(i) <= L_high_res/2;
    dx=dxmin;
  else
    dx=fac_stretch*(xr(i)-xr(i-1));
    if dx > dxmax
      dx = dxmax;
    end
  end
  i=i+1;
  xr(i)=xr(i-1)+dx;
end

% Make xl
i=1;
xl(i)=-dxmin;
while xl(i) >= -Ltotal/2
  if xl(i) >= -L_high_res/2;
    dx=dxmin;
  else
    dx=-fac_stretch*(xl(i)-xl(i-1));
    if dx > dxmax
      dx = dxmax;
    end
  end
  i=i+1;
  xl(i)=xl(i-1)-dx;
end

% Combine xl and xr into x0 (xl has to be flipped first).
xl=fliplr(xl);
x0=cat(1,xl',xr');
x0=x0';

%% Make the z grid (this is really the zeta grid).
z0=[0 z1,z1+dz:dz:H+2*dz];

%% Number of grid points
imax=length(x0);
kmax=length(z0);

%% Depth and width
H0(1:imax) = H;
H0(1:2)=0.0; H0(imax-1:imax)=0.0; % Tube is close at both ends.

B0(1:imax,1:kmax)=B;

%% Initial condition for rho, u, w and h, T, S and C

 for i=1:imax
   for k = 1:kmax
     arg=(z0(k)-zi)/dh;
     rho(i,k) = rho0 + (drho/2)*(1+tanh(arg));
     
     % The function below for u comes from solving analytically
     % for u(z) the expression for the Richardson number, i.e.
     %      Ri = ((g/rho) drho/dz ) / (du/dz)^2
     % given the tanh profile above for rho(z) and by fixing Ri.
     % This is different than the method used by Smyth et. al. (2005).
     % Note that Thorpe (1968 and 1971) derived an analytical function for u(z)
     % from first principles. Different than this approach here.
     
     u(i,k) = -sqrt(2)*dh*atan(tanh(arg/2))*cosh(arg)*sqrt((g*drho*sech(arg)^2)/(Ri*dh*rho0));
          
     % Add a random noise to u(z) to help trigger the instabilities
     r = -1 + 2.*rand(1,1);
     u(i,k)=u(i,k)+r*noise_level;
     
     v(i,k)=0.0;
     
   end  
 end

 % aestus deals with sigma not rho. Make the conversion.
 sig=rho-1000;

w(1:imax,1:kmax)=0.0;
h(1:imax)=0.0;

% Calculate Ri to double check
 Ri(1:imax,1:kmax)=0.0;
 for i=1:imax
   for k=2:kmax-1
     dz=z0(k+1)-z0(k-1);
     S2=((u(i,k+1)-u(i,k-1))/dz)^2;
     N2=(g/rho0)*(rho(i,k+1)-rho(i,k-1))/dz;
     Ri(i,k)=N2/S2;
   end
 end

% We don't simulate T and S only sigma. Set these to any unphysical values
% such that it will cause obvious errors if they get manipulated by
% mistake by the code.
T(1:imax,1:kmax)=-99999;
S(1:imax,1:kmax)=-99999;


%% Figures for verification
figure
pcolor(x0,z0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('\rho(x,z)');

figure
pcolor(x0,z0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

figure
pcolor(x0,z0,tanh(Ri'));
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('Ri (x,z)');
caxis([0 1]);

%% Save grid
% Save the grid into text file in a format appropriate for the model
 root = '../../TestCases/Thorpe1968/';
 
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',x0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',z0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);


%% Save ini field

filename=[root,'field.ini']; % Filename for the initial condition.

fid = fopen(filename,'w');
for i=1:imax
  for k=1:kmax
    %fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),w(i,k),h(i),C(i,k));
    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));
  end
end
fclose(fid);
