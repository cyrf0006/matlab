%% Grid and initial conditions for aestus.x
% This script is for preparing the grid files and
% the file containing the initial conditions for 
% the lock exchange flow test case.
%
% Inspired from:
% Fringer et. al. (2006) An unstructured-grid, finite-volume, nonhydrostatic,
%   parallel coastal ocean simulator, Ocean Modelling, 14, 139-173.

%% PARAMETERS
% Geometry 
H = 0.1;             % Tube depth
B = 0.1;              % Tube width 
L = 0.8;        % Tube length

% Grid size
dx = 0.002;
dz = 0.001;


% Density structure
l = 0.01;
rho1 = 1000.0;
rho2 = 1001.02;


x0 = [-L/2+(dx/2)-2*dx:dx:L/2-(dx/2)+2*dx]
z0 = [0:dz:H+2*dz];

%% Number of grid points
imax=length(x0);
kmax=length(z0);

%% Depth and width
H0(1:imax) = H;
H0(1:2) = 0.0; 
H0(imax-1:imax) = 0.0;
B0(1:imax,1:kmax) = B;

%% Initial condition for rho, u, w and h, T, S and C

 for i=1:imax
   for k = 1:kmax
       
     rho(i,k) = rho1 +(1./2)*(rho2-rho1)*(1+tanh(x0(i)/l));
     u(i,k) = 0.0;
     v(i,k) = 0.0;
     w(i,k) = 0.0;          
     
   end 
   h(i) = 0.0;
 end

 % aestus deals with sigma not rho. Make the conversion.
 sig = rho-1000;

% We don't simulate T and S only sigma. Set these to any unphysical values
% such that it will cause obvious errors if they get manipulated by
% mistake by the code.
T(1:imax,1:kmax)=-99999;
S(1:imax,1:kmax)=-99999;

%% Figures for verification
figure
pcolor(x0,z0,sig');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('\rho(x,z)');

figure
pcolor(x0,z0,u');
set(gca,'ydir','reverse');
shading('interp');
colorbar;
title('u(x,z)');

%% Save grid
% Save the grid into text file in a format appropriate for the model
 root = '../../TestCases/LockExchange/';
 
 fid1 = fopen([root,'x0.grid'],'w');
 fid2 = fopen([root,'z0.grid'],'w');
 fid3 = fopen([root,'H0.grid'],'w');
 fid4 = fopen([root,'B0.grid'],'w');
 
 for i=1:imax
   fprintf(fid1,'%12.8f\n',x0(i));
   fprintf(fid3,'%12.8f\n',H0(i));
 end
 
 for k=1:kmax
   fprintf(fid2,'%12.8f\n',z0(k));
 end
 
 for i=1:imax
   for k=1:kmax
     fprintf(fid4,'%6i %6i %12.8f\n',i,k,B0(i,k));
   end
 end
 
 fclose(fid1);
 fclose(fid2);
 fclose(fid3);
 fclose(fid4);



%% Save ini field
filename=[root,'field.ini']; % Filename for the initial condition.

fid = fopen(filename,'w');
for i=1:imax
  for k=1:kmax
    fprintf(fid,'%6i %6i %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',i,k,sig(i,k),T(i,k),S(i,k),u(i,k),v(i,k),w(i,k),h(i));
  end
end
fclose(fid);