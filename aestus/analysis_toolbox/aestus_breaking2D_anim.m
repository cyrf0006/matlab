clear all
close all


%% some info on the grid
load time.out;
load grid.mat

% snapshot info 
% $$$ XLIM = [-4e4 2e4];
% $$$ ZLIM = [0 1600];

XLIM = [-2e4 0]; %---R7
ZLIM = [500 1300];

XLIM = [-1.5e4 .5e-4]; %--R5
ZLIM = [600 1200];

XLIM = [-1e4 -1.5e-4]; %--R5
ZLIM = [900 1150];


% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [450 550];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [510 545];
% Get data from this frame
timeVec = time(:,3);

size(timeVec)

frame0 = datenum(2000,1,1,0,0,0);
framef = datenum(2000,1,6,0,0,0); 

[Y, I0] = min(abs(timeVec-frame0));
[Y, If] = min(abs(timeVec-framef));
frameNos = I0:2:If;


for iFrame = 1:length(frameNos)
disp(sprintf('Frame %d / %d', iFrame, length(frameNos)))
frameTime = timeVec(frameNos(iFrame));

sig = getfield('sig',frameNos(iFrame));
sig = putNaN(sig,flag_s);
sig = sig';
u = getfield('u',frameNos(iFrame));
u = putNaN(u,flag_u);
u = u';    
v = getfield('v',frameNos(iFrame));
v = putNaN(v,flag_u);
v = v';   
w = getfield('w',frameNos(iFrame));
w = putNaN(w,flag_s);
w = w'; 

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
u = u(J,I);
sig = sig(JJ,I);
w = w(JJ,II);


xquiverDec = 10;
zquiverDec = 15;
figure(1)
clf
set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 18 14])
pcolor(xVec, zetaVec, sig)
shading flat  
set(gca, 'ydir', 'reverse')
hold on
contour(xVec, zetaVec, sig, 50, 'color', 'k')

Z = zVec(1:zquiverDec:end);
X = xVec(1:xquiverDec:end);
U = u(1:zquiverDec:end, 1:xquiverDec:end);
W = -w(1:zquiverDec:end, 1:xquiverDec:end);

I = find(Z>=ZLIM(1) & Z<=ZLIM(end));
J = find(X>=XLIM(1) & X<=XLIM(end));
Z = Z(I);
X = X(J);
U = U(I,J);
W = W(I,J);


[kmax imax] = size(U);
vecColor= [1 1 1];
unit = false;
scale = .5;

for i = 1:imax-1
    for k = 1:kmax-1
        arrow7_no1000(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
    end
end
colorbar
caxis([31.8 32.25])
title(datestr(frameTime, 0))
xlabel('x (km)')
ylabel('z (m)')

% $$$ keyboard
% $$$ XTICKLABEL = get(gca, 'xTickLabel');
% $$$ XTICKLABEL = str2num(XTICKLABEL)./1000;
% $$$ set(gca, 'xTickLabel', XTICKLABEL);

outFile = sprintf('frame%0.4d', iFrame);
print(gcf, '-dpng', '-r150', outFile)

end
