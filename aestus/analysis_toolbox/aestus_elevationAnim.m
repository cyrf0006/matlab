clear all
close all

% $$$ VIZ: La commande pour visualizer rapidement un champ. Exemple: 
% $$$ 
% $$$ viz('u',12);
% $$$ 
% $$$ où 12 est le frame de output.  viz('u',1) donne la solution initiale. 
% $$$ 
% $$$ GETFIELD: La commande pour extraire les données (elle est utilisée dans VIZ). Exemple:
% $$$ 
% $$$ sig = getfield('sig',1100);
% $$$ h = getfield('h',1100);  % Pour l'élévation de la surface
% $$$ 
% $$$ PUTNAN: Pour mettre des NaN sous la topographie. Exemple
% $$$ 
% $$$ sig = putNaN(sig,flag_s);
% $$$ u    = putNaN(u,flag_u);
% $$$ v    = putNaN(v,flag_u);
% $$$ w    = putNaN(w,flag_s);
% $$$ 
% $$$ La grille est déjà en Matlab dans le fichier grid.mat
% $$$ x: Les points de grille de u et sigma
% $$$ chi: les point de grille de w
% $$$ z: les u
% $$$ zeta: les sigma et w
% $$$ 
% $$$ flag_s: le flag des scalaires (mais aussi de w)
% $$$ flag_u: le flag de u et v

%% some info on the grid
load time.out;
load grid.mat
mooringDepth = 919;
noSteps = size(time,1);
I = find(zeta>=794 & zeta <= 919); % mooring depth range
Iu = find(z>=794 & z <= 919); % mooring depth range
[Y, xI] = min(abs(H0-mooringDepth));
XLIM = [datenum(2000,1,5,19.5,0,0) datenum(2000,1,6,19.5,0,0)];

zVec = zeta(I);
zuVec = z(Iu);
sigMat = nan(length(I), noSteps);
uMat = nan(length(Iu), noSteps);
vMat = nan(length(Iu), noSteps);
timeVec = time(:,3);
etaVec = [];

disp('Extract model results...')
for i = 1:400
    if mod(i, 100) == 0;
        disp(sprintf('   timestep %d/%d', i, noSteps))
    end    
    eta = getfield('h',i);
    
    sig = getfield('sig',i);
    sig = putNaN(sig,flag_s);
    sig = sig';
    u = getfield('v',i);
    u = putNaN(u,flag_u);
    u = u'; 
    
    figure(1)
    clf
    plot(x/1000, Hu/-1000-1, 'k', 'linewidth', 3)
    hold on
    plot(x/1000, eta, 'k', 'linewidth', 2)
    pcolor(x/1000, z/-1000-1, u)
    shading interp
    caxis([-.12 .12])
    contour(chi/1000, z/-1000-1, sig, 'k')
    ylim([-3.6 2.5]) 
    xlim([-1000 1000])
% $$$     ylim([-3.6 -1]) 
% $$$     xlim([-50 100])   
    outFile = sprintf('elevationFrame%0.4d', i);
    xlabel('x (km)')
    ylabel('\eta (m)')
    print(gcf, '-dpng', '-r150', outFile)    
end
disp('done!')





keyboard

