clear all
close all
% run in /media/Seagate2TB/Aestus_outputs/output_runF9/

figure(1)
clf
set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.14; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%% some info on the grid
load time.out;
load grid.mat

timeVec = time(:,3);
timeVec(end) = [];

sigVec = nan(size(timeVec));
for i = 1:length(timeVec)
    if mod(i,100) == 0
        disp(sprintf('%d / %d', i, length(timeVec)));
    end
    sig = getfield('sig',frameNos(i));
    sig = putNaN(sig,flag_s);
    sig = sig';
    
    [Y,J] = min(abs(x-2.7*1000));
    [Y,I] = min(abs(z-900));

    sigVec(i) = sig(I,J);
end


%% spectral Al.
% low-pass timeserie
dt = timeVec(2)-timeVec(1);
fs = 1/dt/86400; %Hz

nx = max(size(timeVec)); % Hanning
timeWindow = 86400*5; %sec. window
na = length(timeVec)./(timeWindow*fs);
w = hanning(floor(nx/na));


% $$$ psMat = [];
% $$$ for i = 1:size(myT,1);
% $$$         Td = myT(i,:);
% $$$     %Td = detrend(myT(i,:));
% $$$     [ps, f] = pwelch(Td, w, 0, [], fs); 
% $$$     psMat = [psMat; ps'];
% $$$ end
Td = detrend(sigVec);
[ps, f] = pwelch(Td, w, 0, [], fs); 
[ps, f] = pwelch(sigVec, w, 0, [], fs); 

figure(2)
clf
loglog(f, ps)
set(gca, 'ygrid', 'on')
set(gca, 'xgrid', 'on')

