clear all
close all
% run in /media/Seagate2TB/Aestus_outputs/output_runF9/

figure(1)
clf
set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.14; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

letterID = ['a', 'b', 'c', 'd', 'e', 'f'];

%% some info on the grid
load time.out;
load grid.mat

% snapshot info 
% $$$ XLIM = [-4e4 2e4];
% $$$ ZLIM = [0 1600];
XLIM = [-1.5e4 2.5e4];  %<---- GRL MS
ZLIM = [0 1600];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [450 550];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [510 545];
% Get data from this frame

XLIM = [2e3 5e3];  %<---- ZOOM mooring
ZLIM = [750 1000];

timeVec = time(:,3);

frameNos = 1:length(timeVec)-1;

load ~/git/matlab/colormaps/oxygen.mat

isub = 1;
for iFrame = [120:2:130]

    frameTime = timeVec(frameNos(iFrame));

    sig = getfield('sig',frameNos(iFrame));
    sig = putNaN(sig,flag_s);
    sig = sig';
    c1 = getfield('C01',frameNos(iFrame));
    c1 = putNaN(c1,flag_s);
    c1 = c1';    
    u = getfield('u',frameNos(iFrame));
    u = putNaN(u,flag_u);
    u = u';   
    w = getfield('w',frameNos(iFrame));
    w = putNaN(w,flag_s);
    w = w'; 

    % reduce the grid
    I = find(x>=XLIM(1) & x<=XLIM(2));
    J = find(z>=ZLIM(1) & z<=ZLIM(2));
    II = find(chi>=XLIM(1) & chi<=XLIM(2));
    JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
    xVec = x(I);
    zVec = z(J);
    chiVec = chi(II);
    zetaVec = zeta(JJ);
    c1 = c1(JJ,I);
    sig = sig(JJ,I);
    w = w(JJ,II);
    u = u(JJ,II);

    V1 = [175:1:235];
    V2 = [31:.01:32.5];

    %% Subplot(i)
    s = subplot(3,2,isub);
    contourf(xVec/1000, zetaVec, c1, V1, 'linestyle', 'none')
    shading flat  
    set(gca, 'ydir', 'reverse')
    hold on
    contour(xVec/1000, zetaVec, sig, V2, 'color', 'k')
    colormap(oxygen)

    plot([2.7 2.75 2.75 2.7 2.7], [920 920 810 810 920], 'r');
    
    %% vectors
    xquiverDec = 2;
    zquiverDec = 5;
    Z = zVec(1:zquiverDec:end);
    X = xVec(1:xquiverDec:end)./1000;
    U = u(1:zquiverDec:end, 1:xquiverDec:end);
    W = -w(1:zquiverDec:end, 1:xquiverDec:end);

    [kmax imax] = size(U);
    vecColor= [1 1 1];
    unit = false;
    scale = 1.3;

    for i = 1:imax-1
        for k = 1:kmax
            arrow7(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
        end
    end
    caxis([206 215])
    xlim([XLIM]/1000)
    ylim([ZLIM])

   
    if mod(isub,2) == 0
        set(gca, 'yticklabel', [])
    end

    if isub < 5
        set(gca, 'xticklabel', [])
    end
    
    
    if isub == 6
        cb = colorbar;
        adjust_space
        cbPos = get(cb, 'pos');
        figPos = get(gca, 'pos');
        cbPos(1) = figPos(1)+figPos(3)+.01;
        cbPos(2) = cbPos(2)+.01;
        cbPos(4) = cbPos(4)-2*.01;
        cbPos(3) = cbPos(3)*.7;
        set(cb, 'pos', cbPos)
        ti = ylabel(cb,'[O_2] (\mumol kg^{-1})', 'FontSize', 10, 'fontweight', 'bold');
        tiPos = get(ti, 'pos');
        set(ti, 'rotation', 90)
        %        tiPos = [-1 -4.2 1];
        set(ti, 'pos', tiPos)
    else
        adjust_space
    end

    if isub == 5 | isub == 6
        xlabel('x (km)', 'fontWeight', 'bold')
    end

    if isub == 3
        ylabel('z (m)', 'fontWeight', 'bold')
    end
    text(2.2, 980, letterID(isub), 'fontWeight', 'bold', 'fontSize', 14)        
    isub = isub+1;
end

set(gcf, 'renderer', 'painters')
print(gcf, '-depsc', 'breaking_subplots_toberename.eps')