clear all
close all


%% some info on the grid
load time.out;
load grid.mat

% snapshot info 
% $$$ XLIM = [-4e4 2e4];
% $$$ ZLIM = [0 1600];
XLIM = [-1.5e4 2.5e4];  %<---- GRL MS
ZLIM = [0 1600];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [450 550];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [510 545];
% Get data from this frame

XLIM = [2e3 5e3];  %<---- ZOOM mooring
ZLIM = [750 1000];

timeVec = time(:,3);


frameNos = 1:length(timeVec)-1;

% $$$ frame0 = datenum(2000,1,6,0,0,0);
% $$$ framef = datenum(2000,1,7,0,0,0); 
% $$$ 
% $$$ [Y, I0] = min(abs(timeVec-frame0));
% $$$ [Y, If] = min(abs(timeVec-framef));
% $$$ frameNos = I0:If;

load ~/git/matlab/colormaps/oxygen.mat


for iFrame = 1:length(frameNos)
disp(sprintf('Frame %d / %d', iFrame, length(frameNos)))
frameTime = timeVec(frameNos(iFrame));

sig = getfield('sig',frameNos(iFrame));
sig = putNaN(sig,flag_s);
sig = sig';
c1 = getfield('C01',frameNos(iFrame));
c1 = putNaN(c1,flag_s);
c1 = c1';    
u = getfield('u',frameNos(iFrame));
u = putNaN(u,flag_u);
u = u';   
w = getfield('w',frameNos(iFrame));
w = putNaN(w,flag_s);
w = w'; 

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
c1 = c1(JJ,I);
sig = sig(JJ,I);
w = w(JJ,II);
u = u(JJ,II);


quiverDec = 3;
V1 = [175:1:235];
V2 = [31:.1:32.5];
V2 = [31:.01:32.5];
figure(1)
clf
set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 18 14])
%pcolor(xVec, zetaVec, c1)
contourf(xVec/1000, zetaVec, c1, V1, 'linestyle', 'none')
shading flat  
set(gca, 'ydir', 'reverse')
hold on
contour(xVec/1000, zetaVec, sig, V2, 'color', 'k')
colormap(oxygen)

%% vectors
xquiverDec = 2;
zquiverDec = 5;
Z = zVec(1:zquiverDec:end);
X = xVec(1:xquiverDec:end)./1000;
U = u(1:zquiverDec:end, 1:xquiverDec:end);
W = -w(1:zquiverDec:end, 1:xquiverDec:end);

% $$$ I = find(Z>=ZLIM(1) & Z<=ZLIM(end));
% $$$ J = find(X>=XLIM(1) & X<=XLIM(end));
% $$$ Z = Z(I);
% $$$ X = X(J);
% $$$ U = U(I,J);
% $$$ W = W(I,J);


[kmax imax] = size(U);
vecColor= [1 1 1];
unit = false;
scale = 1;

for i = 1:imax-1
    for k = 1:kmax
        arrow7(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
    end
end


c = colorbar;
caxis([200 250])
caxis([206 215])
title(datestr(frameTime, 0))
xlim([XLIM]/1000)
ylim([ZLIM])
xlabel('x (km)', 'fontweight', 'bold')
ylabel('z (m)', 'fontweight', 'bold')
ti = ylabel(c,'[O_2] (\mumol kg^{-1})', 'fontweight', 'bold');


% $$$ keyboard
% $$$ XTICKLABEL = get(gca, 'xTickLabel');
% $$$ XTICKLABEL = str2num(XTICKLABEL)./1000;
% $$$ set(gca, 'xTickLabel', XTICKLABEL);

outFile = sprintf('wframe%0.4d', iFrame);
print(gcf, '-dpng', '-r150', outFile)

end
