clear all
close all


%% Model data
load time.out;
load grid.mat

timeVec = time(:,3);
timeVec(end-5:end) = [];
%timeVec(end-81:end) = [];
timeVec = timeVec + datenum(2000,1,1);

mooringDepth = 919;
[Y, xI] = min(abs(H0-mooringDepth));
[Y, yI] = min(abs(z-(mooringDepth-20)));
Iz = find(z>=820 & z<=900);

frame0 = datenum(2000,1,1,0,0,0);
framef = datenum(2000,1,10,0,0,0); 

[Y, I0] = min(abs(timeVec-frame0));
[Y, If] = min(abs(timeVec-framef));
frameNos = I0:If;
timeVec = timeVec(frameNos);

uVec = [];
vVec = [];
utestVec = [];
vtestVec = [];

for iFrame = 1:length(frameNos)
    disp(sprintf('Frame %d / %d', iFrame, length(frameNos)))
    frameTime = timeVec(frameNos(iFrame));

    u = aes_getfield('u',frameNos(iFrame));
    u = aes_putNaN(u,flag_u);
    u = u';   
    v = aes_getfield('v',frameNos(iFrame));
    v = aes_putNaN(v,flag_u);
    v = v';   
    
    %    uVec = [uVec; u(yI, xI)];
    %vVec = [vVec; v(yI, xI)];
    uVec = [uVec; nanmean(u(Iz, xI))];
    vVec = [vVec; nanmean(v(Iz, xI))];
    
    utestVec = [utestVec; u(1, xI)];
    vtestVec = [vtestVec; v(1, xI)];
end
timeVec = timeVec+datenum(2012,10,9, -11,0,0)-timeVec(1);


%% ADCP data
[U, E, N, W] = hst_velocity2hst('~/research/NIOZ/RockallBank/roc12.mat', z, timeVec) ;
[U, V] = rotate_vecd(E,N,+62);

% $$$ vvVec = V(yI,:);
% $$$ uuVec = U(yI,:);
vvVec = nanmean(V(Iz,:));
uuVec = nanmean(U(Iz,:));


% Filter timeserie
dt = diff(abs(timeVec(1:2))); %day
fs = 1/dt; % cpd
freq_low = 1; %cpd
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
Ufilt = filtfilt(b, a, uVec);
Vfilt = filtfilt(b,a,vVec);

I = find(~isnan(uuVec));
UUfilt = nan(size(timeVec));
VVfilt = nan(size(timeVec));
UUfilt(I) = filtfilt(b, a, uuVec(I));
VVfilt(I) = filtfilt(b,a,vvVec(I));

figure(1)
clf
plot(timeVec, VVfilt-nanmean(VVfilt), 'k', 'linewidth', 2) % Vadcp is Umodel
hold on
plot(timeVec, UUfilt-nanmean(UUfilt), 'r', 'linewidth', 2)
plot(timeVec, Ufilt-nanmean(Ufilt), '--k', 'linewidth', 2)
plot(timeVec, Vfilt-nanmean(Vfilt), '--r', 'linewidth', 2)
legend('Uadcp','Vadcp', 'Umodel', 'Vmodel')
datetick('x')
ylabel('u,v (m s^{-1})')

figure(2)
clf
plot(VVfilt-nanmean(VVfilt), UUfilt-nanmean(UUfilt), 'k')
xlim([-.15 .15])
ylim([-.15 .15])
hold on
plot(Ufilt-nanmean(Ufilt), Vfilt-nanmean(Vfilt), 'r') 
legend('ADCP', 'model')

nanstd([VVfilt Vfilt])
nanstd([UUfilt Ufilt])


figure(3)
clf
plot(timeVec, uuVec, 'b', 'linewidth', 2)
hold on
plot(timeVec, uVec, 'r', 'linewidth', 2) 
legend('ADCP', 'model')
datetick('x', 7)
xlabel('October 2012', 'fontsize',14, 'fontWeight', 'bold')
ylabel('u (m s^{-1})', 'fontSize',14, 'fontWeight', 'bold')
set(gca, 'fontSize', 12, 'fontWeight', 'bold')  

figure(4)
clf
plot(timeVec, vvVec, 'b', 'linewidth', 2)
hold on
plot(timeVec, vVec, 'r', 'linewidth', 2) 
legend('ADCP', 'model')
datetick('x', 7)
xlabel('October 2012', 'fontsize',14, 'fontWeight', 'bold')
ylabel('v (m s^{-1})', 'fontSize',14, 'fontWeight', 'bold')
set(gca, 'fontSize', 12, 'fontWeight', 'bold')  

figure(5)
clf
plot(timeVec, uuVec, 'b', 'linewidth', 2)
hold on
plot(timeVec, vvVec, 'r', 'linewidth', 2)
plot(timeVec, uVec, 'b', 'linewidth', 1) 
plot(timeVec, vVec, 'r', 'linewidth', 1) 
legend('Uadcp','Vadcp', 'Umodel', 'Vmodel')
datetick('x', 7)
xlabel('October 2012', 'fontsize',14, 'fontWeight', 'bold')
ylabel('u,v (m s^{-1})', 'fontSize',14, 'fontWeight', 'bold')
set(gca, 'fontSize', 12, 'fontWeight', 'bold')  

nanvar([vvVec' vVec])
nanvar([uuVec' uVec])
