clear all
close all


%% some info on the grid
load time.out;
load grid.mat

% snapshot info 

XLIM = [-8*10^4 8*10^4]
ZLIM = [0 2600]
XLIM = [-9500 -8000]
ZLIM = [850 1100]

% Get data from this frame
timeVec = time(:,3);



frame0 = datenum(2000,1,1,0,0,0);
framef = datenum(2000,1,10,23,45,0); 

[Y, I0] = min(abs(timeVec-frame0));
[Y, If] = min(abs(timeVec-framef));
frameNos = I0:If;


for iFrame = 1:length(frameNos)
disp(sprintf('Frame %d / %d', iFrame, length(frameNos)))
frameTime = timeVec(frameNos(iFrame));


u = getfield('u',frameNos(iFrame));
u = putNaN(u,flag_u);
u = u';  

v = getfield('v',frameNos(iFrame));
v = putNaN(v,flag_u);
v = v'; 

w = getfield('w',frameNos(iFrame));
w = putNaN(w,flag_s);
w = w'; 

sig = getfield('sig',frameNos(iFrame));
sig = putNaN(sig,flag_s);
sig = sig';

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
u = u(J,I);
v = v(J,I);
w = w(JJ,II);
sig = sig(JJ,II);


% quiverDec = 5;
% figure(1)
% 
% clf
% set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 18 14])
% pcolor(xVec, zVec, u)
% shading flat
% set(gca, 'ydir', 'reverse')
% hold on
% contour(xVec, zVec, u, -0.25:0.1:0.45, 'color', 'k')
% 
% 
% colorbar
% caxis([-0.25 0.45])
% title(datestr(frameTime, 0))
% xlabel('x (m)')
% ylabel('z (m)')
% 
% outFile = sprintf('u%0.4d', iFrame);
% print(gcf, '-dpng', '-r150', outFile)
% 
% figure(2)
% 
% clf
% set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 18 14])
% pcolor(xVec, zVec, v)
% shading flat
% set(gca, 'ydir', 'reverse')
% hold on
% contour(xVec, zVec, v, 30, 'color', 'k')
% 
% 
% colorbar
% %caxis([-0.4 0.55])
% title(datestr(frameTime, 0))
% xlabel('x (m)')
% ylabel('z (m)')
% 
% outFile = sprintf('v%0.4d', iFrame);
% print(gcf, '-dpng', '-r150', outFile)

figure(3)

clf
set(gcf,'PaperUnits','centimeters', 'paperposition', [0 0 18 14])
%pcolor(chiVec, zetaVec, w)
pcolor(chiVec, zetaVec,sig)
shading flat
set(gca, 'ydir', 'reverse')
hold on
%contour(chiVec, zetaVec, w, 30, 'linestyle', 'none')
contour(chiVec, zetaVec, sig,31.8:0.001:32.2,'color','k')

colorbar
%caxis([-15*10^(-5) 15*10^(-5)])
%caxis([-3.2*10^(-3) 3.2*10^(-3)])
caxis([32 32.2])
title(datestr(frameTime, 0))
xlabel('x (m)')
ylabel('z (m)')

outFile = sprintf('w%0.4d', iFrame);
print(gcf, '-dpng', '-r150', outFile)

end
