function var = aes_getfield(field,frame);
%AES_GETFIELD - This function reads output fields from the output
%               unformatted binary files created by the model aestus,
%               version 2016 and up.
%               To work properly, this file needs to read the file
%               'parameters.in' also produced by aestus. This is the file
%               that contains the grid dimension. 
%
% Syntax:  var = getfield(field,frame)
%
% Inputs:
%    field - A string of the required field which could be any of the .out
%            files produced by aestus such as 'u', 'v', 'w', 'sig',
%            'KH',...
%             
%    frame - An integer time frame.
%
% Outputs:
%    var - The requested field at the requested time frame
%
% Example:
%    Example 1: var = aes_getfield('u',10)
%    Example 2: var = aes_getfield('sig',1)
%
% Author: Daniel Bourgault, ISMER
% September 2015

% Extract the path string which may be needed to locate
% the parameter file. 
[pathstr,name,ext] = fileparts(field);

%% First find the grid dimension form the parameters.in file
% Open the parameter file and find an extract the grid size outputted
% by the model
fid_param  = fopen([pathstr,'/parameters.in']);
found_line = false;

% Check for string 'I1'. Once found, we are at the right place and
% need to read and evaluate this line and the following three for
% the matrix size delimited by (I2, I2, K1 K2)   
while found_line == false
    tline = strtrim(fgetl(fid_param));
    if length(tline) >= 2
      found_line = strcmp(tline(1:2),'I1');
    end
end

% Evaluate I1 = x,
% Replace the comma by a semi-colon to stop the verbose mode.
j_comma = find(tline == ',');
tline(j_comma) = ';';
eval(tline);
  
% Evaluate I2 = x, K1=, K2=
for j = 1:3
    tline = strtrim(fgetl(fid_param));
    j_comma = find(tline == ',');
    tline(j_comma) = ';';
    eval(tline);
end

% Determine the matrix dimension
dimx = I2 - I1 + 1;
dimz = K2 - K1 + 1;

% Next line in the file is the precision of the output
tline   = strtrim(fgetl(fid_param));
j_apos  = find(tline == '"');
j_comma = find(tline == ',');
tline(j_apos)  = '''';
tline(j_comma) = ';';
eval(tline)

fclose(fid_param);

%% Second read the data at the right location 
fid_data = fopen([field,'.out']);

if strcmp(OUT_PRECISION,'single') 
  nbit = 4; 
elseif strcmp(OUT_PRECISION,'double')
  nbit = 8; 
end

if strcmp(field,'h')  % The surface elevation is only a 1-D vector
    % Positionning within the file according to time frame.
    recl   = dimx;
    offset = nbit*recl*(frame-1);
    fseek(fid_data,offset,'bof');

    % Get the matrix in a column vector 
    var = fread(fid_data,[dimx 1],OUT_PRECISION);
    
else
    % Positionning within the file according to time frame.
    recl   = dimx*dimz;
    offset = nbit*recl*(frame-1);
    fseek(fid_data,offset,'bof');

    % Get the matrix in a column vector 
    var = fread(fid_data,[dimx dimz],OUT_PRECISION);
end

fclose(fid_data);
