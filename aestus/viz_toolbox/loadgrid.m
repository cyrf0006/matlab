% Load the ascii files defining the grid 

 load x.grid;
 load z.grid;
 load H.grid;
 load B.grid;
 load flag.grid;

 [imax dum]=size(x);
 [kmax dum]=size(z);

 line=0;
 for i=1:imax
   for k=1:kmax
     line=line+1;
     flag_u(i,k)=flag(line,3);
     flag_s(i,k)=flag(line,4);
     Bw(i,k)=B(line,3);
     Bu(i,k)=B(line,4);
     Bs(i,k)=B(line,5);
    end
  end
 Hu=H(:,2);
 Hw=H(:,1);
 H0=H(:,3);

 chi=x(:,1)';
 x=x(:,2)';
 zeta=z(:,1);
 z=z(:,2);
 z=z';
 zeta=zeta';

 clear dum line i k flag BB H
