%n1 = 720;
%n2 = 1440;

n1 = 1;
n2 = 318;

load grid;
U(1:imax,1:kmax) = 0;
V(1:imax,1:kmax) = 0;

for n = n1:n2
  u = getfield('u',n);
  v = getfield('v',n);
  U = U + u;
  V = V + v;
end
U = U/(n2-n1+1);

figure(1)
pcolor(x,z,U');
shading('interp');
hold;
viztopo(chi,zeta,Hw);
colorbar;

figure(2);
pcolor(x,z,V');
shading('interp');
hold;
viztopo(chi,zeta,Hw);
colorbar;

