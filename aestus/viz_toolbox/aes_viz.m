function aes_viz(field,frame);
%AES_VIZ - This is a function for quickly and conveniently inspect the 
%    solution of the Aestus model. 
%
% Syntax:  aes_viz(field,frame)
%
% Inputs:
%    field - A string of the required field which could be any of the .out
%            files produced by aestus such as 'u', 'v', 'w', 'sig',
%            'KH',...
%             
%    frame - An integer time frame.
%            Note the special case when the time frame is set to 0
%            actually means to plos the last frame available in the file.
%
% Outputs:
%    No output. A figure is produced
%
% Example:
%    Example 1: aes_viz('u',10)
%    Example 2: aes_viz('sig',1)
%    Example 3: aes_viz('KH',0)   This will plot the last frame.
%
% Other files required: aes_getfield.m
%                       aes_viztopo.m
%                       grid.mat
%                       aes_colormap.mat : An orange-blue custom colormap
%
% Author: Daniel Bourgault, ISMER
% September 2015

clf;

% Some fontsize
fs1 = 14;
fs2 = 16;

load grid.mat;

load time.out;
% This is a special case when the frame is set to 0
% it means the last frame available.
if frame == 0 
    frame = length(time(:,1));
end
timestr = datestr(time(frame,3),'yyyy/mm/dd HH:MM:SS.FFF');


% Get the variables 
var = aes_getfield(field,frame);
h   = aes_getfield('h',frame);
  
if strcmp(field,'sig')  | ... 
   strcmp(field,'S')    | ...
   strcmp(field,'T')    | ...
   strcmp(field(1),'C') | ...
   strcmp(field,'PI')   | ... 
   strcmp(field,'KV')   | ...
   strcmp(field,'KH')   | ...
   strcmp(field,'divU') | ... 
   strcmp(field(1),'s') 
     var  = aes_putNaN(var,flag_s);
    [X Z] = meshgrid(chi,z);
elseif strcmp(field,'u')  | ...
       strcmp(field,'v')  | ...
       strcmp(field,'AV') | ...
       strcmp(field,'AH')
    var   = aes_putNaN(var,flag_u);
    [X Z] = meshgrid(x,z);
elseif strcmp(field,'w')
    var   = aes_putNaN(var,flag_s);
    [X Z] = meshgrid(chi,zeta);
else
    display(['The variable ',field, ' is not listed explicitly in the aes_viz.m file. Make modification accordingly.']); 
end
  
if strcmp(field,'KH') | ...
   strcmp(field,'KV') | ...
   strcmp(field,'AH') | ... 
   strcmp(field,'AV')  
    var = log10(var);
end  
  
% The surface cells adjust to the water level.
Z(1,:)=-h';
  
% Load the custom colormap
load aes_colormap;
colormap(cmap)

pcolor(X,Z,var');
shading('interp');
hold;
plot(chi,-h);
colorbar;

set(gca,'fontname','Times','fontsize',fs1);
set(gca,'YDir','reverse');
xlabel('Distance (m)','fontname','Times','fontsize',fs2);
ylabel('Depth (m)','fontname','Times','fontsize',fs2);
set(gca,'TickDir','out','XminorTick','on','YMinorTick','on');
if strcmp(field,'KH') | ...
   strcmp(field,'KV') | ...
   strcmp(field,'AH') | ... 
   strcmp(field,'AV')  
    title(['Field: log(',field,'); Frame: ',num2str(frame),'; Time: ',timestr]);      
else
    title(['Field: ',field,'; Frame: ',num2str(frame),'; Time: ',timestr]);
end

aes_viztopo(chi,z,Hw);
