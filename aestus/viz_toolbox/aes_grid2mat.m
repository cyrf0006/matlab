function aes_grid2mat
% Load the ascii files that define the grid and rewrite it
% into a binary .mat file

 load x.grid;
 load z.grid;
 load H.grid;
 load B.grid
 load flag.grid;
 
 [imax dum] = size(x);
 [kmax dum] = size(z);

 Hu = H(:,2);
 Hw = H(:,1);
 H0 = H(:,3);

 chi  = x(:,1)';
 x    = x(:,2)';
 zeta = z(:,1)';
 z    = z(:,2)';

 flag_u = reshape(flag(:,3),kmax,imax);
 flag_s = reshape(flag(:,4),kmax,imax);
 flag_u = flag_u';
 flag_s = flag_s';
 
 Bw = reshape(B(:,3),kmax,imax);
 Bu = reshape(B(:,4),kmax,imax);
 Bs = reshape(B(:,5),kmax,imax);
 Bw = Bw';
 Bu = Bu';
 Bs = Bs';
 
 save grid.mat x chi z zeta flag_s flag_u Hu Hw H0 Bu Bw Bs imax kmax