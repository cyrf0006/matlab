function viz(field,frame);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% viz.m
%
% viz(field,frame)
%
% Function to visualize a field from the
% model 'aestus'
%
% field:          string that identifies the desired field
% frame:          The output frame number
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  clf;
  
  field4(1:4) = '    ';
  field4(1:length(field)) = field;
  
  load grid.mat;

  % For versions previous to 2008.1 of aestus
  %[year,month,day,hh,mm,ss]=readtime;
  %timestr = datestr(datenum(year(frame),month(frame),day(frame),hh(frame),mm(frame),ss(frame)),31);
  load time.out;
  timestr = datestr(time(frame,3),'yyyy/mm/dd HH:MM:SS.FFF');

  fs1 = 14;
  fs2 = 16;

  % Get the variables 
  var = getfield(field,frame);
  h   = getfield('h',frame);

  if field4 == 'sig ' | field4 == 'S   ' | field4 == 'T   ' | field4(1) == 'C' | field4 == 'PI  ' ... 
   | field4 == 'KV  ' | field4 == 'KH  ' | field4 == 'divU' | field4(1) == 's'
    var   = putNaN(var,flag_s);
    [X Z] = meshgrid(chi,z);
  elseif field4 == 'u   ' | field4 == 'v   ' ...
       | field4 == 'AV  ' | field4 == 'AH  '
    var   = putNaN(var,flag_u);
    [X Z] = meshgrid(x,z);
  elseif field4  == 'w   '
    var   = putNaN(var,flag_s);
    [X Z] = meshgrid(chi,zeta);
  else
    display(['The variable ',field, ' does is not listed explicitly in the viz.m file. Make modification accordingly.']); 
  end
  
  if field4 == 'KH  ' | field4 == 'KV  ' | field4 == 'AH  ' | field4 == 'AV  '  
    var = log10(var);
  end  
  
  
  % The surface cells adjust to the water level.
  Z(1,:)=-h';
  
  % The colormap
  %scheme = 'Blue';
  %scheme = 'BlueGray';
  %scheme = 'BrownBlue';
  %scheme = 'RedBlue';
  %map = lbmap(128,scheme);
  %colormap(flipud(map));

  if field4(1) == 's'
    load sediment_cmap;
    colormap(sediment_cmap);
  else
    colormap(flipud(jet));
  end
  
  pcolor(X,Z,var');
  shading('interp');
  hold;
  plot(chi,-h);
  colorbar;

  set(gca,'fontname','Times','fontsize',fs1);
  set(gca,'YDir','reverse');
  xlabel('Distance (m)','fontname','Times','fontsize',fs2);
  ylabel('Depth (m)','fontname','Times','fontsize',fs2);
  set(gca,'TickDir','out','XminorTick','on','YMinorTick','on');
  if field4 == 'KH  ' | field4 == 'KV  ' | field4 == 'AH  ' | field4(1) == 'AV  '  
    title(['Field: log(',field,'); Frame: ',num2str(frame),'; Time: ',timestr]);      
  else
    title(['Field: ',field,'; Frame: ',num2str(frame),'; Time: ',timestr]);
  end

  viztopo(chi,z,Hw);
  %plot(chi,H0,'b');  
  
  % Plot the grid points
  % Sometime useful for debugging
  %[X Z] = meshgrid(x,z);
  %[CHI ZETA] = meshgrid(chi,zeta);
  %plot(X,Z,'k*');
  %plot(CHI,Z,'k+');
  %plot(CHI,ZETA,'kx');
