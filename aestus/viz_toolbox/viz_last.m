clf;

load time.out
[m n] = size(time);

figure(1)
h(m);

figure(2)
viz('u',m);

figure(3)
viz('v',m);

figure(4)
viz('w',m);

figure(5)
viz('T',m);

figure(6)
viz('S',m);

figure(7)
viz('AH',m);

figure(8)
viz('KH',m);

figure(9)
viz('KV',m);
