function [year,month,day,hh,mm,ss] = readtime

fid=fopen('time.out');

datetime = fscanf(fid,'%4d %*1s %2d %*1s %2d %2d %*1s %2d %*1s %2d',[6 inf]);

fclose(fid);

datetime=datetime';

year=datetime(:,1);
month=datetime(:,2);
day=datetime(:,3);
hh=datetime(:,4);
mm=datetime(:,5);
ss=datetime(:,6);
