function h(time);  
  
  clf;

  % Load the grid coordinates
  load grid.mat;

  % get the variables 
  h=getfield('h',time);
  'data read'
 
  plot(x,h);
  xlabel('Distance (m)');
  ylabel('h (m)');
