function viz(field,grid_type,frame);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% viz.m
%
% viz(field,grid_type,time)
%
% Function to visualize a field from the
% model 'aestus'
%
% field:          string that identifies the desired field
% grid_type:      Determine whether the desired field is 
%                 define on the pressure grid (e.g. sig, PI, KH)
%                 or velocity grid (u,w).
%                 grid_type = 1 : Pressure grid
%                 grid_type = 2 : Velocity grid
% frame:          The output frame number
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  clf;

  load grid.mat;

  % For older version than 2008.1. For versions after 2008.1 use 'viz.m'
  [year,month,day,hh,mm,ss]=readtime;
  timestr = datestr(datenum(year(frame),month(frame),day(frame),hh(frame),mm(frame),ss(frame)),31);
  
  fs1=14;
  fs2=16;

  % Get the variables 
  var=getfield(field,frame);
  h=getfield('h',frame);
  
  if grid_type==1 
    var=putNaN(var,flag_s);
    [X Z]=meshgrid(chi,z);
  else
    var=putNaN(var,flag_u);
    [X Z]=meshgrid(x,z);
  end

  % The surface cells adjust to the water level.
  Z(1,:)=-h';
  
  pcolor(X,Z,var');
  shading('interp');
  hold;
  plot(chi,-h);
  colorbar;
  colormap(flipud(jet));

  set(gca,'fontname','Times','fontsize',fs1);
  set(gca,'YDir','reverse');
  xlabel('Distance (m)','fontname','Times','fontsize',fs2);
  ylabel('Depth (m)','fontname','Times','fontsize',fs2);
  set(gca,'TickDir','out','XminorTick','on','YMinorTick','on');
  title(['Field: ',field,' Frame: ',num2str(frame),' Time: ',timestr]);
  viztopo(x,z,Hu);
