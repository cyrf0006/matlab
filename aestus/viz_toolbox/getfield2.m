function var = getfield(field,frame);

  fid=fopen([field,'.out']);

  %nbit=1; 
  nbit=4; 

  % Get the grid dimension from the two first records
  % of the file.
  dimx=fread(fid,1,'int32');
  dimz=fread(fid,1,'int32');

  % Positionning within the file according to frame.
  recl=dimx*dimz;
  offset=nbit*recl*(frame);
  fseek(fid,offset,-1);

  % Get the matrix in a column vector 
  var=fread(fid,recl,'float32');

  % Reshape it in a 2D matrix.
  var=reshape(var,dimx,dimz);

  fclose(fid);
