%function viz(field,grid_type,frame);
function viz_diff(field,frame0,frame);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% viz.m
%
% viz(field,grid_type,time)
%
% Function to visualize a field from the
% model 'aestus'
%
% field:          string that identifies the desired field
% grid_type:      Determine whether the desired field is 
%                 define on the pressure grid (e.g. sig, PI, KH)
%                 or velocity grid (u,w).
%                 grid_type = 1 : Pressure grid
%                 grid_type = 2 : Velocity grid
% frame:          The output frame number
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  clf;
  
  field3(1:3) = '   ';
  field3(1:length(field)) = field;
  
  load grid.mat;

  % For versions previous to 2008.1 of aestus
  %[year,month,day,hh,mm,ss]=readtime;
  %timestr = datestr(datenum(year(frame),month(frame),day(frame),hh(frame),mm(frame),ss(frame)),31);
  load time.out;
  timestr = datestr(time(frame,3),'yyyy/mm/dd HH:MM:SS.FFF');

  fs1 = 14;
  fs2 = 16;

  % Get the variables 
  var0 = getfield(field,frame0);
  h0   = getfield('h',frame0);
  var = getfield(field,frame);
  h   = getfield('h',frame);
  
  var = var - var0;
  h   = h - h0;
  
  if field3 == 'sig' | field3 == 'S  ' | field3 == 'T  ' | field3(1) == 'C' | field3 == 'PI ' ... 
   | field3 == 'KV ' | field3 == 'KH '
    var   = putNaN(var,flag_s);
    [X Z] = meshgrid(chi,z);
  elseif field3 == 'u  ' | field3 == 'v  ' ...
       | field3 == 'AV ' | field3 == 'AH '
    var   = putNaN(var,flag_u);
    [X Z] = meshgrid(x,z);
  elseif field3  == 'w  '
    var   = putNaN(var,flag_s);
    [X Z] = meshgrid(chi,zeta);
  else
    display(['The variable ',field, 'does not figure in the viz.m file. Make modification accordingly.']); 
  end
  
  
  % The surface cells adjust to the water level.
  Z(1,:)=-h';
  
  % The colormap
  %scheme = 'Blue';
  %scheme = 'BlueGray';
  %scheme = 'BrownBlue';
  %scheme = 'RedBlue';
  %map = lbmap(128,scheme);
  %colormap(flipud(map));

  colormap(flipud(jet));

  pcolor(X,Z,var');
  shading('interp');
  hold;
  plot(chi,-h);
  colorbar;

  set(gca,'fontname','Times','fontsize',fs1);
  set(gca,'YDir','reverse');
  xlabel('Distance (m)','fontname','Times','fontsize',fs2);
  ylabel('Depth (m)','fontname','Times','fontsize',fs2);
  set(gca,'TickDir','out','XminorTick','on','YMinorTick','on');
  title(['Field: ',field,'; Frame: ',num2str(frame),'; Time: ',timestr]);
  viztopo(chi,z,Hw);
  plot(chi,H0,'b');  
  
  % Plot the grid points
  % Sometime useful for debugging
  %[X Z] = meshgrid(x,z);
  %[CHI ZETA] = meshgrid(chi,zeta);
  %plot(X,Z,'k*');
  %plot(CHI,Z,'k+');
  %plot(CHI,ZETA,'kx');
  