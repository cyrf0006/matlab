origin = [43.635 7.3307];
target = [43+25.00/60 7+52.00/60]; % DYFAMED
target = [43+11.49/60 8+18.59/60]; % Int2
target = [43.35 8]; % DYFAMED-Int2
target = [43+31/60 8+6/60]; % DYFAMED-Int2

% For CTD
[Y, I2] = min(abs(latVecCTD-target(1)) + abs(lonVecCTD-target(2)));
[Y, I1] = min(abs(latVecCTD(1:I2)-origin(1)) + abs(lonVecCTD(1:I2)-origin(2)));
theIndexCTD = I1:I2;
[distance, lon, lat] = m_lldist([lonVecCTD(I1) lonVecCTD(I2)], [latVecCTD([I1]) latVecCTD([I2])], 10000);
distVecCTD = nan(size(theIndexCTD));
for i = 1:length(theIndexCTD)
    [Y, I] = min(abs(latVecCTD(theIndexCTD(i))-lat) + abs(lonVecCTD(theIndexCTD(i))-lon));
    distVecCTD(i) = m_lldist([origin(2) lon(I)], [origin(1) lat(I)]);
end
theIndexUnique = theIndexCTD;
Inegative = find(diff(distVecCTD)<0);
distVecCTD(Inegative+1) = [];
theIndexUnique(Inegative+1) = [];
Inegative = find(diff(distVecCTD)<0);
distVecCTD(Inegative+1) = [];
theIndexUnique(Inegative+1) = [];
[distVec_unique, I] = unique(distVecCTD, 'last');
distVecCTD = interp1(theIndexUnique(I), distVec_unique, theIndexCTD);
xVecCTD = distVecCTD;

% For other sensors
[Y, I2] = min(abs(latVec-target(1)) + abs(lonVec-target(2)));
[Y, I1] = min(abs(latVec(1:I2)-origin(1)) + abs(lonVec(1:I2)-origin(2)));
theIndex = I1:I2;
[distance, lon, lat] = m_lldist([lonVec(I1) lonVec(I2)], [latVec([I1]) latVec([I2])], 10000);
distVec = nan(size(theIndex));
for i = 1:length(theIndex)
    [Y, I] = min(abs(latVec(theIndex(i))-lat) + abs(lonVec(theIndex(i))-lon));
    distVec(i) = m_lldist([origin(2) lon(I)], [origin(1) lat(I)]);
end
theIndexUnique = theIndex;
Inegative = find(diff(distVec)<0);
distVec(Inegative+1) = [];
theIndexUnique(Inegative+1) = [];
Inegative = find(diff(distVec)<0);
distVec(Inegative+1) = [];
theIndexUnique(Inegative+1) = [];
[distVec_unique, I] = unique(distVec, 'last');
Inegative = find(diff(distVec_unique)<0);
I(Inegative) = []; % remove pts where glider go backward
distVec = interp1(theIndex(I), distVec_unique, theIndex);
xVec = distVec;



%%%%%%%%%%%%%%%%%%%%%%%
%%% --- FIGURES ----- %
%%%%%%%%%%%%%%%%%%%%%%%


%% Individual figures
keyboard
glider_transect_projection_plotosca01
glider_transect_projection_plotnexos01

