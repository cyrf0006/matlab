xbot = [timeVec; timeVec(end); timeVec(1); timeVec(1)]; 
ybot = [botFilt; zf; zf; botFilt(1)];
FS1 = 14+2;
FS2 = 16+2;
FS3 = 18+2;
FS0 = 12+2;

%TRY = ( ((TRYc-DARK)./(TRYm-DARK)) - ((V1b-DARK)./(TRYm(Ib)-DARK)) )./TRY_calib;
DARK = 4096;
TRY = ( ((tryMat-DARK)./(trymMat-DARK)) )./.002;
PHE = ( ((pheMat-DARK)./(phemMat-DARK)) )./.40;



figure(1)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, CTMat)
shading interp
hold on
[C, h] = contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k');
clabel(C, h, 'fontSize', FS0);
caxis([13 20])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
c = colorbar;
map = flipud(brewermap(14, 'RdBu'));
colormap(map);
xlim([t0 tf])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
text(timeVec(1), -5, 'WP1', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt1, -5, 'WP2', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt2, -5, 'WP3', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt3, -5, 'WP4', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt4, -5, 'WP5', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(timeVec(end), -5, 'WP6', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
plot([timeVec(1) timeVec(1)], [0 250], '--k', 'linewidth', 2);
plot([tt1 tt1], [0 250], '--k', 'linewidth', 2)
plot([tt2 tt2], [0 250], '--k', 'linewidth', 2)
plot([tt3 tt3], [0 250], '--k', 'linewidth', 2)
plot([tt4 tt4], [0 250], '--k', 'linewidth', 2)
plot([timeVec(end) timeVec(end)], [0 250], '--k', 'linewidth', 2);
patch(xbot, ybot, [1 .9333 .6667]/1.5)
text(datenum(2014,11,28,20,0,0), 230,'\Theta (^{\circ}C)', 'fontSize', FS3, 'fontWeight', 'bold')
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_temp.png')
print('-depsc', 'M241_temp.eps')


figure(2)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, SAMat)
shading interp
hold on
[C, h] = contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k');
clabel(C, h, 'fontSize', FS0);
caxis([37.5 38.6])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
colormap(map)
xlim([t0 tf])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
text(timeVec(1), -5, 'WP1', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt1, -5, 'WP2', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt2, -5, 'WP3', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt3, -5, 'WP4', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(tt4, -5, 'WP5', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
text(timeVec(end), -5, 'WP6', 'fontSize', FS2, 'fontWeight', 'bold', 'horizontalAlignment', 'center')
% $$$ text(datenum(2014,11,27,12,0,0), -5, 'R', 'fontSize', FS2, 'fontWeight', 'bold')
% $$$ text(datenum(2014,11,26,12,0,0), -5, 'M', 'fontSize', FS2, 'fontWeight', 'bold')
% $$$ text(datenum(2014,11,23,12,0,0), -5, 'T', 'fontSize', FS2, 'fontWeight', 'bold')
% $$$ text(datenum(2014,11,19,14,0,0), -5, 'C', 'fontSize', FS2, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
plot([timeVec(1) timeVec(1)], [0 250], '--k', 'linewidth', 2);
plot([tt1 tt1], [0 250], '--k', 'linewidth', 2)
plot([tt2 tt2], [0 250], '--k', 'linewidth', 2)
plot([tt3 tt3], [0 250], '--k', 'linewidth', 2)
plot([tt4 tt4], [0 250], '--k', 'linewidth', 2)
plot([timeVec(end) timeVec(end)], [0 250],'--k', 'linewidth', 2);
patch(xbot, ybot, [1 .9333 .6667]/1.5)
text(datenum(2014,11,28,20,0,0), 230,'S_A (g Kg^{-1})', 'fontSize', FS3, 'fontWeight', 'bold')
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_temp.png')
print('-depsc', 'M241_sali.eps')

keyboard

figure(3)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, chlMat)
shading interp
hold on
contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k')
patch(xbot, ybot, [1 .9333 .6667]/1.5)
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
map = brewermap(14, 'Ylgn'); 
colormap(map)
xlim([t0 tf])
caxis([0 .8])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
text(datenum(2014,11,28,20,0,0), 230,'chl-a (\mug L^{-1})', 'fontSize', FS3, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_chla.png')
print('-depsc', 'M241_chla.eps')


figure(4)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of fi
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, cdMat)
shading interp
hold on
contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k')
patch(xbot, ybot, [1 .9333 .6667]/1.5)
caxis([-.8 -.2])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
colorbar
map = brewermap(16, 'YlOrBr');        
colormap(map)
xlim([t0 tf])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
text(datenum(2014,11,28,20,0,0), 230,'CDOM (\mug L^{-1})', 'fontSize', FS3, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_cdom.png')
print('-depsc', 'M241_cdom.eps')



figure(5)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, log10(bbMat))
shading interp
hold on
contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k')
patch(xbot, ybot, [1 .9333 .6667]/1.5)
%caxis([34 39])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
colormap(map)
xlim([t0 tf])
caxis([-4.5 -2.5])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
text(datenum(2014,11,28,20,0,0), 230,'Turbidity / BB 700nm', 'fontSize', FS3, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
set(gca, 'box', 'on')
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_temp.png')
print('-depsc', 'M241_bb700.eps')



figure(6)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, TRY)
shading interp
hold on
contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k')
patch(xbot, ybot, [1 .9333 .6667]/1.5)
caxis([5 15])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
map = brewermap(14, 'PuBuGn'); 
colormap(map)
xlim([t0 tf])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
text(datenum(2014,11,28,20,0,0), 230,'[Try-like] (\mug L^{-1})', 'fontSize', FS3, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
set(c, 'pos', cbar_pos)
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_tryp.png')
print('-depsc', 'M241_tryp.eps')

figure(7)
clf
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.08; % very right of figure
tops = 0.05; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
pcolor(timeVec, zVec, PHE*1000)
shading interp
hold on
contour(timeVec, zVec, sigMat, [27:.5:30], 'lineColor', 'k')
patch(xbot, ybot, [1 .9333 .6667]/1.5)
caxis([5 15])
set(gca, 'ydir', 'reverse')
set(gca, 'xdir', 'reverse')
datetick('x', 7)
colorbar
colormap(map)
xlim([t0 tf])
xlabel('Nov. 2014', 'fontSize', FS2, 'fontWeight', 'bold')
ylabel('Depth (m)', 'fontSize', FS2, 'fontWeight', 'bold')
text(datenum(2014,11,28,20,0,0), 230,'[Phe-like] (ng L^{-1})', 'fontSize', FS3, 'fontWeight', 'bold')
set(gca, 'fontSize', FS0, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbar_pos = get(c, 'pos');
cbar_pos(2) = cbar_pos(2)+.02;
cbar_pos(4) = cbar_pos(4)-.04;
cbar_pos(3) = cbar_pos(3)*.5;
cbar_pos(1) = cbar_pos(1)-.025;
set(c, 'pos', cbar_pos)
set(gcf, 'renderer', 'painters')
print('-dpng', '-r300', 'M241_phen.png')
print('-depsc', 'M241_phen.eps')

%for i in *.eps; do gm convert -density 300 $i $(basename $i .eps).png; done
