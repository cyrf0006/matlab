function egoGlider_contours(structFile)

% usage ex: 
%  glider_contours('mooseT00_33_struct.mat')


%% Load structure
disp(sprintf('load %s', structFile));
load(structFile)
w = whos;

found = 0;
i = 1;
while found == 0
    if strcmp(w(i).class, 'struct')
        found = 1;
        structName = w(i).name;
    else
        i = i+1;
    end
end
command = sprintf('s = %s ;',structName);
eval(command);

zVec = s.PP(:,50);
timeVec = s.DAYS(1,:);
TMat = s.TP;
SMat = s.SS;
sigMat = s.GG;
o2Mat = s.OXWP;
cdomMat = s.CDOM;
bb880Mat = s.B880;
bb532Mat = s.B532;

% clean
I = find(~isnan(timeVec));
xVec = 1:length(timeVec);
timeVec = interp1(xVec(I), timeVec(I) , xVec);

I = find(~isnan(timeVec));
timeVec = timeVec(I);
TMat = TMat(:,I);
SMat = SMat(:,I);
sigMat = sigMat(:,I);
o2Mat = o2Mat(:,I);
cdomMat = cdomMat(:,I);
bb880Mat = bb880Mat(:,I);
bb532Mat = bb532Mat(:,I);

I = find(~isnan(zVec));
zVec = zVec(I);
TMat = TMat(I,:);
SMat = SMat(I,:);
sigMat = sigMat(I,:);
o2Mat = o2Mat(I,:);
cdomMat = cdomMat(I,:);
bb880Mat = bb880Mat(I,:);
bb532Mat = bb532Mat(I,:);

t0 = timeVec(1);
tf = timeVec(end);



figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 25])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 6; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.16; % very right of figure
tops = 0.02; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

t1 = datenum(2015, 11, 4, 3, 10, 0);
t2 = datenum(2015, 11, 7, 17, 0, 0);
zMin = 0;
zMax = 400;

% S1
subplot(6,1,1)
imagesc(timeVec, zVec, TMat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([11 20])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'T (^{\circ}C)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'a', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S2
subplot(6,1,2)
imagesc(timeVec, zVec, SMat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([38.2 39])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'S_A (g Kg^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'b', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S3
subplot(6,1,3)
imagesc(timeVec, zVec, o2Mat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([140 225])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'[O_2] (?)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'c', 'color', [1 1 1], 'fontsize', 14, 'fontweight', 'bold')
%plot([t1 t2 t2 t1 t1], [300 300 3 3 300], '--m', 'lineWidth', 2)

% S4
subplot(6,1,4)
imagesc(timeVec, zVec, cdomMat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([-1 0])
ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'cdom (rel. u.)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'd', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')
%plot([t1 t2 t2 t1 t1], [300 300 3 3 300], '--m', 'lineWidth', 2)

% S5
subplot(6,1,5)
imagesc(timeVec, zVec, bb532Mat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([-2e-4 4e-4])
%caxis([6e-5 20e-5])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'BB_{532} (m^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'e', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')
%plot([t1 t2 t2 t1 t1], [300 300 3 3 300], '--m', 'lineWidth', 2)


% S6
subplot(6,1,6)
imagesc(timeVec, zVec, bb880Mat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
%caxis([15 20])
%caxis([3.5 6.5])
caxis([-1e-4 3e-4])
%ylabel('Depth (m)', 'fontWeight', 'bold')
%set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'BB_{880} (m^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'f', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% $$$ % S7
% $$$ subplot(6,1,7)
% $$$ imagesc(timeVec, zVec, pheMat)
% $$$ datetick('x',7)
% $$$ xlim([t0 tf])
% $$$ hold on
% $$$ contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
% $$$ %caxis([.01 .017])
% $$$ %caxis([.005 .011])
% $$$ caxis([.3 .6])
% $$$ %ylabel('Depth (m)', 'fontWeight', 'bold')
% $$$ xlabel('Oct./Nov. 2015', 'fontWeight', 'bold')
% $$$ cb = colorbar;
% $$$ ti = ylabel(cb,'phe-like (ug L^{-1})', 'FontSize', 10, 'fontweight', 'bold');
% $$$ set(gca, 'fontWeight', 'bold')
% $$$ adjust_space
% $$$ pause(.1)
% $$$ cbPos = get(cb, 'pos');
% $$$ figPos = get(gca, 'pos');
% $$$ cbPos(1) = figPos(1)+figPos(3)+.01;
% $$$ cbPos(2) = cbPos(2)+.01;
% $$$ cbPos(4) = cbPos(4)-2*.01;
% $$$ cbPos(3) = cbPos(3)*.6;
% $$$ set(cb, 'pos', cbPos)
% $$$ text(tf-1, 800, 'g', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

%set(gcf, 'renderer', 'painters')
%print('-depsc2', outName)  
print('-dpng', '-r300', 'EGO_glider_prelim.png')  


