clear all
close all

%% Load data + info needed further (should remain at the beginning)
structFile = ['/home/cyrf0006/research/MIO/seaExplorerData/M78-' ...
              'OSCAHR/data_processing/osca01_struct.mat'];

disp(sprintf('load %s', structFile));
load(structFile);
w = whos;
structName = w.name;
command = sprintf('s = %s ;',structName);
eval(command);  

%% More info on deployment (manually edited)
zMax = 300;
t0 = datenum(2015, 10, 28, 13, 45, 0);
tf = datenum(2015, 11, 10, 6, 45, 0);

% MUST INCLUDE CALIBRATIONS IN STRUCT FILE <-------- !!!!!!!!!
TRY_calib = 0.0016; % L.μg-1
PHE_calib = 0.3877; % L.μg-1
NAP_calib = 0.0088; % L.μg-1
DARK = 4096;

lat = nanmean(s.log.Lat); % <------- CHECK THIS!!!!!!!
lon = nanmean(s.log.Lon);
lat = 43;
lon = 8;

% interp z to time (remove NaNs)
z = s.data.NAV_DEPTH;
n = s.mtimeDat;       
I = find(~isnan(z));
z = interp1(n(I), z(I), n);

%% "dirty" cast detection
W = gradient(z)./gradient(n*86400); % m/s
dt = diff(abs(n(1:2)))*86400; %sec
fs = 1/dt; % Hz
freq_low = 0.01; %Hz
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
I= find(isnan(W));
W(I) = 0;
Wfilt = filtfilt(b, a, W);

I = find(abs(Wfilt)<.01);
z(I) = NaN; % flag as NaN when not moving
I = find(z>zMax);
z(I) = NaN; % Ignore deeper casts


%% build the grid
x = t0:.5/1440:tf;
y = 0:1:max(z);
[X,Z] = meshgrid(x,y);    


%% -------------------- Data ---------------------- %%

%% SEABIRD (.25 Hz)
disp(' -> Seabird data')
T = s.data.SBD_TEMPERATURE;
C = s.data.SBD_CONDUCTIVITY;
P = s.data.SBD_PRESSURE;
I = find(~isnan(T) & P<zMax);
I(1) = []; % remove 1st point
T = T(I);
C = C(I);
P = P(I);
timeSBD = n(I);

% despike C
[Cdspk, Ispikes] = sx_despike(C, 3, .01, .25, 5);

% Thermal lag
% TO-DO!

% GSW stuff
SP = gsw_SP_from_C(Cdspk*10,T,P);
[SA, in_ocean] = gsw_SA_from_SP(SP,P,lon,lat);
CT = gsw_CT_from_t(SA,T,P);
sig0 = gsw_sigma0(SA,CT);

% Grid the data
% ** Between P and z, check to be sure. **
[XI,ZI,sigMat] = griddata(timeSBD*10,P,sig0,X*10,Z);
[XI,ZI,TMat] = griddata(timeSBD*10,P,CT,X*10,Z);
[XI,ZI,SMat] = griddata(timeSBD*10,P,SA,X*10,Z);

%% WL-TRIPLET
disp(' -> Wetlab triplet')
CHL = s.data.TRI_CHL_SCALED;
CDOM = s.data.TRI_CDOM_SCALED;
CHLc = s.data.TRI_CHL_COUNT;
volt = s.log.Voltage;

% interp P
Pchla = s.data.SBD_PRESSURE;
I = find(~isnan(Pchla));
Pchla = interp1(n(I), Pchla(I), n');

I = find(~isnan(CHL) & Pchla<zMax);
I(1) = []; % remove 1st point
CHL = CHL(I);
CDOM = CDOM(I);
CHLc = CHLc(I);
Pchla = Pchla(I);
timeCHL = n(I);
 
% Correction of shift that occured 4-7 Nov. 2015 (VERY BRUTAL!!!!!)
Ishift = [5.2e5 : 7.85e5]';
p = polyfit(Ishift, CHL(Ishift), 1);
CHL(Ishift) = CHL(Ishift) - Ishift*p(1) +.3;
%CHL(Ishift) = CHL(Ishift) - detrend(CHLfilt(Ishift))+.12;

p = polyfit(Ishift, CDOM(Ishift), 1);
%CDOM(Ishift) = detrend(CDOM(Ishift))+2.2;
CDOM(Ishift) = CDOM(Ishift) - Ishift*p(1) +4.8;

% Despike dangerous because not even time spacincing !!!!!!!!!!!!!!!!!!!!!!!
[CHLdspk, Ispikes] = sx_despike(CHL, 5, .01, .25, 5);
[CDOMdspk, Ispikes] = sx_despike(CDOM, 5, .01, .25, 5);
[XI,ZI,chlaMat] = griddata(timeCHL*10,Pchla,CHL,X*10,Z);
[XI,ZI,cdomMat] = griddata(timeCHL*10,Pchla,CDOMdspk,X*10,Z);


%% MiniFLuo
disp(' -> Minifluo-UV')
% SN <=8
TRYc = s.data.MFL_V1; % counts
PHEc = s.data.MFL_V2; % counts
PHEm = s.data.MFL_V3; % counts_monitor
TRYm = s.data.MFL_V4; % counts_monitor

% $$$ % other SN
% $$$ TRYc = s.data.MLF_V1; % counts
% $$$ PHEc = s.data.MLF_V2; % counts
% $$$ TRYm = s.data.MLF_V3; % counts_monitor
% $$$ PHEm = s.data.MLF_V4; % counts_monitor

Pmfl = s.data.SBD_PRESSURE;
I = find(~isnan(Pmfl));
Pmfl = interp1(n(I), Pmfl(I), n');

I = find(~isnan(TRYc) & Pmfl<zMax);
TRYc = TRYc(I);
TRYm = TRYm(I);
PHEc = PHEc(I);
PHEm = PHEm(I);
Pmfl = Pmfl(I);
timeMFL = n(I);


[V1b, Ib] = min(TRYc);
%TRY = ( ((TRYc-DARK)./(TRYm-DARK)) - ((V1b-DARK)./(TRYm(Ib)-DARK)) )./TRY_calib;
TRY = ( ((TRYc-DARK)./(TRYm-DARK)) )./TRY_calib;
PHE = ( ((PHEc-DARK)./(PHEm-DARK)) )./PHE_calib;

[TRYdspk, Ispikes] = sx_despike(TRY, 5, .01, .25, 5);
[PHEdspk, Ispikes] = sx_despike(PHE, 5, .01, .25, 5);

[XI,ZI,tryMat] = griddata(timeMFL*10,Pmfl,TRYdspk,X*10,Z);
[XI,ZI,pheMat] = griddata(timeMFL*10,Pmfl,PHEdspk,X*10,Z);

% ---------------------------------------------------- %




%%  ------- Plots ------ %%
disp('Plot data!')
oscahr_contours_plot
