figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 25])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 7; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.16; % very right of figure
tops = 0.02; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(7,1,1)
imagesc(x,y, TMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([11 20])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'T (^{\circ}C)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'a', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S2
subplot(7,1,2)
imagesc(x,y, SMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([38.2 39])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'S_A (g Kg^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'b', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S3
subplot(7,1,3)
imagesc(x,y, chlaMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([0 0.9])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'chl-a (rel. u.)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'c', 'color', [1 1 1], 'fontsize', 14, 'fontweight', 'bold')

% S4
subplot(7,1,4)
imagesc(x,y, cdomMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([0 2.5])
ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'cdom (rel. u.)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'd', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S5
subplot(7,1,5)
imagesc(x,y, bbMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([6e-5 20e-5])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'BB_{700} (m^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'e', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')


% S6
subplot(7,1,6)
imagesc(x,y, tryMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
caxis([15 20])
%caxis([4 9])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'tri-like (ug L^{-1})^*', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'f', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S7
subplot(7,1,7)
imagesc(x,y, pheMat)
datetick('x',7)
xlim([t0 tf])
hold on
contour(x, y, sigMat, [27:.5:29.5], 'k')
%caxis([.01 .017])
caxis([.005 .011])
%ylabel('Depth (m)', 'fontWeight', 'bold')
xlabel('Oct./Nov. 2015', 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'phe-like (ug L^{-1})^*', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(datenum(2015, 11, 9, 12, 0, 0), 250, 'g', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

%set(gcf, 'renderer', 'painters')
%print('-depsc2', outName)  
print('-dpng', '-r300', 'OSCAHR_glider_prelim.png')  
