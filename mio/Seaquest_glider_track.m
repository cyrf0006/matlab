
% function sx_surveyMap(structFile, [lims])
%  
%  where 'lims' an optional argument and whould correspond to:
%  [lat_min lat_max lon_min lon_max]
%
%  usage ex: sx_surveyMap('osca01_struct.mat', [42 45 6 10])
% 

%% stations
T6 = [42.75033   5.53151];
T7 = [42.87799   5.46849];
T8 = [43.00978   5.40548];
T9 = [43.23973   5.29241];
solemio = [43.2383 5.2883];
julio = [43.135 5.255];
julio2 = [43.12 5.345];

track = [solemio;julio2;T8;T7;T6;];
latVec = track(:,1);
lonVec = track(:,2);

dc = mean(diff(track));
TX1 = T6 + dc;
TX2 = TX1 + dc;
TX3 = TX2 + dc;
TX4 = TX3 + dc;
TX5 = TX4 + dc;

%% Deal with varargin (map limits)
lonLims = [min(lonVec)-.5 max(lonVec)+1];
latLims = [min(latVec)-.7 max(latVec)+.5];        

%% bathymetry
path = ['~/data/matlab_bathym/MedRhone.mat']; % 30-sec.
load(path)
I=find(lat<latLims(2) & lat>latLims(1));
J=find(lon<lonLims(2) & lon>lonLims(1));
latitude=lat(I);
longitude=lon(J);
bathy=z(I,J);
        


%% plot map
close all
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.0 ; % horiz. space between subplots
dy = 0.0; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.05; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
FS = 10;
%V = 100:1:300;

figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 12 15])
m_proj('mercator','long',lonLims,'lat',latLims);
hold on
load gebco64
colormap(gebco);
V=[0:100:1000];
m_pcolor(longitude,latitude,abs(bathy)); shading flat;
[HH, HH] = m_contour(longitude,latitude,abs(bathy), [0:500:4000], 'color', 'k');
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold')
ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold')

% add track
m_line([solemio(2) TX5(2)], [solemio(1), TX5(1)], 'lineStyle', '--', 'color', 'k')
m_plot(solemio(2),solemio(1), '.', 'color', 'k', 'markerSize', 10);
m_plot(julio2(2),julio2(1), '.', 'color', 'k', 'markerSize', 10);
m_plot(julio(2),julio(1), '.', 'color', 'k', 'markerSize', 10);
m_plot(T6(2),T6(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(T7(2),T7(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(T8(2),T8(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(T9(2),T9(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(TX1(2),TX1(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(TX2(2),TX2(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(TX3(2),TX3(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(TX4(2),TX4(1), '.', 'color', 'm', 'markerSize', 10);
m_plot(TX5(2),TX5(1), '.', 'color', 'm', 'markerSize', 10);


%m_text(solemio(2),solemio(1), 'Sole/T9', 'color', 'k');
m_text(julio(2),julio(1), 'JULIO', 'verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(julio2(2),julio2(1), 'J2', 'verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(T6(2),T6(1), 'T6','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(T7(2),T7(1), 'T7','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(T8(2),T8(1), 'T8','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(T9(2),T9(1), 'Somlit/T9','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(TX1(2),TX1(1), 'TX1','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(TX2(2),TX2(1), 'TX2','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(TX3(2),TX3(1), 'TX3','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(TX4(2),TX4(1), 'TX4','verticalAlignment', 'top', 'horizontalAlignment', 'right');
m_text(TX5(2),TX5(1), 'TX5','verticalAlignment', 'top', 'horizontalAlignment', 'right');

m_grid('box','fancy')
adjust_space

print('-dpng', '-r300',  'Seaquest_glider_track.png')

m_lldist([T9(2) TX5(2)], [T9(1) TX5(1)])



track2 = ...
[solemio(2),solemio(1);
julio2(2),julio2(1);
T6(2),T6(1);
T7(2),T7(1);
T8(2),T8(1);
T9(2),T9(1);
TX1(2),TX1(1);
TX2(2),TX2(1);
TX3(2),TX3(1);
TX4(2),TX4(1);
TX5(2),TX5(1)];
