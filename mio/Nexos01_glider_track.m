
% function sx_surveyMap(structFile, [lims])
%  
%  where 'lims' an optional argument and whould correspond to:
%  [lat_min lat_max lon_min lon_max]
%
%  usage ex: sx_surveyMap('osca01_struct.mat', [42 45 6 10])
% 

%% WPs
% $$$ WP = ...
% $$$     [43+33.19/60 7+35.06/60;...
% $$$      43+25.00/60 7+52.00/60;...
% $$$      43+11.49/60 8+18.59/60;...
% $$$      42+57.31/60 8+46.53/60;
% $$$      43.317050 8.822521;
% $$$      43.699534 8.862020;
% $$$      44.189756 8.937793;
% $$$      44.184534 8.442202;
% $$$      43.603760 8.456637
% $$$      43.774741 7.773650];

% $$$ WP = ...
% $$$     [43+33.19/60 7+35.06/60;...
% $$$      43+25.00/60 7+52.00/60;...
% $$$      43+11.49/60 8+18.59/60;...
% $$$      42+57.31/60 8+46.53/60;
% $$$      43.524129 8.706810;...
% $$$      43.895384 8.622660;...
% $$$      44.05 8.60;...
% $$$      44.05 8.35;...
% $$$      43+25.00/60 7+52.00/60];


WP = ...
    [43+33.19/60 7+35.06/60;...
     43+25.00/60 7+52.00/60;...
     43+11.49/60 8+18.59/60;...
     42+57.31/60 8+46.53/60;
     43.524129 8.706810;...
     43.895384 8.622660;...
     44.05 8.60;...
     44.10 8.45
     43.90 8.33;...
     43+25.00/60 7+52.00/60];

WP_names = {' Int1',' DYFA',' Int2',' Edge',' L1',' L2',' L3',' L4',' L5',''};

oilPatchLon = [8.33 8.75 8.75 8.33 8.33];
oilPatchLat = [43.90 43.90 44.17 44.17 43.90];


lonVec = load('gliderLon.txt');
latVec = load('gliderLat.txt');


%% Deal with varargin (map limits)
lonLims = [min(lonVec)-.5 max(lonVec)+1];
latLims = [min(latVec)-.7 max(latVec)+1];        

%% bathymetry
path = ['~/data/matlab_bathym/MedWest.mat']; % 30-sec.
lims = [37 44.5 5 13];

fid = fopen(path);
if fid == -1 % doesnt exist yet!
    disp('30-sec. region doesn''t exist yet!')
    disp(' Extracting from GEBCO, may take some time...')
    [lat lon z] = getGebco('~/data/GEBCO/GEBCO_08.nc', 'z', lims);
    disp('   -> done!')
    save(path, 'lat', 'lon', 'z');
else
    load(path)
end
% $$$ path = ['~/data/matlab_bathym/MedRhone.mat']; % 30-sec.
load(path)
I=find(lat<latLims(2) & lat>latLims(1));
J=find(lon<lonLims(2) & lon>lonLims(1));
latitude=lat(I);
longitude=lon(J);
bathy=z(I,J);
        


%% plot map
close all
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.0 ; % horiz. space between subplots
dy = 0.0; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.05; % very right of figure
tops = 0.05; % top of figure
bots = 0.1; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
FS = 10;
%V = 100:1:300;

figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 12 15])
m_proj('mercator','long',lonLims,'lat',latLims);
hold on
load gebco64
colormap(gebco);
V=[0:100:1000];
m_pcolor(longitude,latitude,-bathy); shading flat;
[cc, HH] = m_contour(longitude,latitude,-bathy, [0:500:3000], 'color', 'k');
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)
clabel(cc, HH)   
caxis([0 3000])
xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold')
ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold')

%% add track
for i = 1:length(lonVec)
    m_plot(lonVec(i),latVec(i), '.', 'color', 'r', 'markerSize', 10);
end

%% add WPs
m_plot(WP(:,2),WP(:,1), '--m', 'lineWidth', 2)
for i = 1:size(WP,1)
    m_plot(WP(i,2),WP(i,1), 'p', 'color', 'm', 'markerSize', 10);
    m_text(WP(i,2),WP(i,1), WP_names(i), 'color', 'm', ...
           'verticalAlignment', 'bottom', 'fontSize', 10, 'fontweight', 'bold');
end

% $$$ m_plot(8.20,44.05, '*', 'color', 'r', 'markerSize', 10);
% $$$ m_plot([8.35 8.20],[44.05 44.05], '--', 'color', 'r');
% $$$ m_text(8.20, 44.1, '12km', 'color', 'r')
m_plot(oilPatchLon, oilPatchLat, '--', 'color', 'g', 'lineWidth', 2);

%m_plot( 8.33,44.17, '*', 'color', 'r', 'markerSize', 10);
%m_plot([8.45 8.33], [44.10 44.17], '--', 'color', 'r');
%m_text(8.40, 44.17, '13km', 'color', 'r')



% $$$ m_line([solemio(2) TX5(2)], [solemio(1), TX5(1)], 'lineStyle', '--', 'color', 'k')
% $$$ m_plot(solemio(2),solemio(1), '.', 'color', 'k', 'markerSize', 10);
% $$$ m_plot(julio2(2),julio2(1), '.', 'color', 'k', 'markerSize', 10);
% $$$ m_plot(julio(2),julio(1), '.', 'color', 'k', 'markerSize', 10);
% $$$ m_plot(T6(2),T6(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(T7(2),T7(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(T8(2),T8(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(T9(2),T9(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(TX1(2),TX1(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(TX2(2),TX2(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(TX3(2),TX3(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(TX4(2),TX4(1), '.', 'color', 'm', 'markerSize', 10);
% $$$ m_plot(TX5(2),TX5(1), '.', 'color', 'm', 'markerSize', 10);



%m_text(solemio(2),solemio(1), 'Sole/T9', 'color', 'k');
% $$$ m_text(julio(2),julio(1), 'JULIO', 'verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(julio2(2),julio2(1), 'J2', 'verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(T6(2),T6(1), 'T6','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(T7(2),T7(1), 'T7','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(T8(2),T8(1), 'T8','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(T9(2),T9(1), 'Somlit/T9','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(TX1(2),TX1(1), 'TX1','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(TX2(2),TX2(1), 'TX2','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(TX3(2),TX3(1), 'TX3','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(TX4(2),TX4(1), 'TX4','verticalAlignment', 'top', 'horizontalAlignment', 'right');
% $$$ m_text(TX5(2),TX5(1), 'TX5','verticalAlignment', 'top', 'horizontalAlignment', 'right');

m_grid('box','fancy')
adjust_space

print('-dpng', '-r300',  'nexos01_glider_track.png')

