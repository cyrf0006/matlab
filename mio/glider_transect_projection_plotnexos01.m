xtra_offset = 0.03;
cbar_offset = 0.02;
FONTSIZE = 10;
vredux = .8;
hredux = .6;

textx = 5;
texty = 275;
texty3 = 20;

FS1=12; % label
XLIM = [0 distance];
v1 = [28:.1:30];
v2 = [28:.2:30];


figure(1) % Temp
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(CT(:,theIndexCTD),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('T (^{\circ}C)', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
adjust_space

% S2
subplot(2,4,[2 4])
pcolor(xVecCTD,zVec, CT(:,theIndexCTD)); shading interp
hold on
[C,h] = contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5]);
clabel(C,h,v2, 'color', [.5 .5 .5])
map = flipud(brewermap(14, 'RdBu'));
colormap(map);
caxis([11.5 20])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')

cb = colorbar;
ti = ylabel(cb,'T (^{\circ}C)', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_T.png')  


%% --------------------------------------------------------------------- %

figure(2) % Sal
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(SA(:,theIndexCTD),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('S_A (g Kg^{-1})', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
adjust_space

% S2
subplot(2,4,[2 4])
pcolor(xVecCTD,zVec, SA(:,theIndexCTD)); shading interp
hold on
[C,h] = contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5]);
clabel(C,h,v2, 'color', [.5 .5 .5])
map = flipud(brewermap(14, 'RdBu'));
colormap(map);
caxis([38.25 38.75])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'S_A (g Kg^{-1})', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_S.png')  


%% --------------------------------------------------------------------- %

figure(3) % CHL
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(CHL(:,theIndex),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('[Chl-a] (\mug L^{-1})', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
adjust_space

% S2
subplot(2,4,[2 4])
pcolor(xVec, zVec, CHL(:,theIndex)); shading flat
hold on
contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5])
map = brewermap(14, 'PuBuGn'); 
colormap(map);
caxis([0 .8])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'[Chl-a] (\mug L^{-1})', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_CHL.png')  


%% --------------------------------------------------------------------- %

figure(4) % TRY
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(TRY(:,theIndex),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('[Try-like] (\mug L^{-1})', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
set(gca, 'xlim', [0 2.5])
ylim([0 zMax])
adjust_space

% Add a second axis system
ax1 = get(gca);
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos, 'XAxisLocation','top', 'YAxisLocation','right', 'Color','none');
%line(nanmean(CHL(:,Ibox),2), zVec,'Parent',ax2, 'Color', [.5 .5 .5], 'lineStyle', '--', 'lineWidth', 2)
line(nanmean(CHL(:,theIndex),2), zVec,'Parent',ax2, 'Color', [.5 .5 .5], 'lineStyle', '--', 'lineWidth', 2)
set(ax2, 'xticklabel', []);
set(ax2, 'yticklabel', []);
set(ax2, 'ydir', 'reverse');
set(ax2, 'ylim', [0 zMax])
set(ax2, 'xlim', [0 .8])

% S2
subplot(2,4,[2 4])
pcolor(xVec, zVec, TRY(:,theIndex)); shading interp
hold on
contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5])
map = brewermap(14, 'PuBuGn'); 
colormap(map);
caxis([0 2.5])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'[Tryptophan-like] (\mug L^{-1})', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_TRY.png')  


%% --------------------------------------------------------------------- %

figure(5) % PHE
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(CDOM(:,theIndex),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('[CDOM] (\mug L^{-1})', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
xlim([0.2 1.5])
ylim([0 zMax])
adjust_space


% S2
subplot(2,4,[2 4])
pcolor(xVec,zVec, CDOM(:,theIndex)); shading interp
hold on
contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5])
map = brewermap(16, 'YlOrBr');        
colormap(map)
caxis([1 1.8])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'[CDOM] (\mug L^{-1})', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_CDOM.png')  


%% --------------------------------------------------------------------- %

figure(6) % PHE
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(PHE(:,theIndex),2)*1000, zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('[Phe-like] (ng L^{-1})', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
xlim([0 7])
ylim([0 zMax])
adjust_space


% S2
subplot(2,4,[2 4])
pcolor(xVec, zVec, PHE(:,theIndex)*1000); shading interp
hold on
contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5])
map = brewermap(14, 'PuBuGn'); 
colormap(map);
caxis([0 7])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'[Phenanthrene-like] (ng L^{-1})', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_PHE.png')  


%% --------------------------------------------------------------------- %

figure(7) % Turbidity
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 16 6])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 4; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.1; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.1; % very right of figure
tops = 0.02; % top of figure
bots = 0.2; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% S1
subplot(1,4,1)
plot(nanmean(BB(:,theIndex),2), zVec, 'k', 'lineWidth', 2)
ylabel('Depth (m)', 'FontSize', FS1, 'fontWeight', 'bold')
xlabel('Turbidity (Backscat.)', 'FontSize', FS1, 'fontweight', 'bold');
set(gca, 'ygrid', 'on');
set(gca, 'xscale', 'log');
set(gca, 'ydir', 'reverse');
%xlim([0 7])
ylim([0 zMax])
adjust_space


% S2
subplot(2,4,[2 4])
pcolor(xVec, zVec, log10(BB(:,theIndex))); shading interp
hold on
contour(xVecCTD, zVec, sig0(:,theIndexCTD), v1, 'color', [.5 .5 .5])
map = brewermap(16, 'YlOrBr');        
colormap(map)
caxis([-4 -3.5])
set(gca, 'yticklabel', []);
set(gca, 'ygrid', 'on');
set(gca, 'ydir', 'reverse');
ylim([0 zMax])
xlim(XLIM)
xlabel('Along-transect distace (km)', 'FontSize', 10, 'fontWeight', 'bold')
cb = colorbar;
ti = ylabel(cb,'log_{10}(Backscatter)', 'FontSize', 10, 'fontweight', 'bold');

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
adjust_space
pos3 = get(gca, 'pos');

pos = pos1;
pos(3) = pos(3)*3+xtra_offset;
set(gca, 'pos', pos)
pause(1)

cpos = get(cb, 'pos');
cpos(1) = cpos(1) - cbar_offset;
cpos(2) = cpos(2)+(cpos(4)-cpos(4)*vredux)/2;
cpos(4) = cpos(4)*vredux;
cpos(3) = cpos(3)*hredux;
set(cb, 'pos', cpos)
print('-dpng', '-r300', 'M299_BB.png')  
