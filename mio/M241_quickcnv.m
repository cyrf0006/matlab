% A quick script to read cnv data from M241. Done for Madeleine's
% poster at Ocean Sciences. 
% Feb. 2016
clear all
close all

%% Orig. files
%cnvList = './M241_cnv.list';
cnvList = './M241_down.list';
latFile = './CNV_files/LATITUDE';
lonFile = './CNV_files/LONGITUDE';
timeFile = './CNV_files/TIME';

%% lon, lat & time vectors

latVec = load(latFile);
lonVec = load(lonFile);

fid = fopen(timeFile);
C = textscan(fid, '%s', 'delimiter', '\n');
fclose(fid);
timeVec = datenum(char(C{1}), 'mmm dd yyyy HH:MM:SS');
zVec = 0:700; % must be larger than max depth

%% test
timeVec = timeVec(1:2:end);
lonVec = lonVec(1:2:end);
latVec = latVec(1:2:end); 

t0 = datenum(2014, 11, 20, 0, 0, 0);
tf = datenum(2014, 11, 29);
z0 = 1;
zf = 250;
Itime = find(timeVec>=t0 & timeVec <= tf);
Iz = find(zVec>=z0 & zVec<=zf);


%% Loop on cnv files
fid = fopen(cnvList);
C = textscan(fid, '%s', 'delimiter', '\n');
cnvFiles = char(C{1});
fclose(fid);

% $$$ # name 0 = prM: Pressure [db]
% $$$ # name 1 = time:time_julian
% $$$ # name 2 = t:temperature [deg C]
% $$$ # name 3 = s:salinity [PSU]
% $$$ # name 4 = p:potential density []
% $$$ # name 5 = sv:sound velocity [m/s]
% $$$ # name 6 = chl: Chl [ug/L]
% $$$ # name 7 = cd: Cdom [ug/L]
% $$$ # name 8 = bb: BB700 []
% $$$ # name 9 = try: Try [counts]
% $$$ # name 10 = phe: Phe [counts]
% $$$ # name 11 = tmp: TMP1 [deg C]
% $$$ # name 12 = tmp2: TMP2 [deg C]
% $$$ # name 13 = mon1: MON_TRY [counts]
% $$$ # name 14 = mon2: MON_PHE [counts]

dataCube = nan(length(zVec), 15, length(timeVec));
for i = 1:size(cnvFiles, 1)-1
    fileName = cnvFiles(i,:);
    I = find(fileName == ' ');
    fileName(I) = [];
    
    
    %    data = dlmread(fileName, ' ', 18, 0);
    
    fid = fopen(fileName);
    string_format = repmat('%f ', 1, 15);
    data_values = textscan(fid, string_format, 'headerlines', 18,  'delimiter', '\n');
    data = cell2mat(data_values);

    if ~isempty(data)
        zMin = min(data(:,1));
        zMax = max(data(:,1));
        I  = find(zVec>=zMin & zVec<=zMax);
        dataCube(I,:,i) = data;
    end
    
end

dataCube = dataCube(Iz,:,Itime);
timeVec = timeVec(Itime);
zVec = zVec(Iz);
lonVec = lonVec(Itime);
latVec = latVec(Itime);

PMat = squeeze(dataCube(:,1,:));
TMat = squeeze(dataCube(:,3,:));
SMat = squeeze(dataCube(:,4,:));
RMat = squeeze(dataCube(:,5,:));
chlMat = squeeze(dataCube(:,7,:));
cdMat = squeeze(dataCube(:,8,:));
bbMat = squeeze(dataCube(:,9,:));
tryMat = squeeze(dataCube(:,10,:));
pheMat = squeeze(dataCube(:,11,:));
trymMat = squeeze(dataCube(:,14,:));
phemMat = squeeze(dataCube(:,15,:));


%% compute aprox of bottom topography
botVec = nan(size(timeVec));
for i = 1:length(timeVec)
    I = find(~isnan(TMat(:,i)));
    if ~isempty(I)
        botVec(i) = max(zVec(I));
    end    
end

I = find(~isnan(botVec));
botVec = interp1(timeVec(I), botVec(I), timeVec);
botFilt = gaussfiltfilt(botVec, 5, 10);


%% Horizontal interpolation
for i = 1:size(TMat,1)
    vec = TMat(i, :);
    I = find(~isnan(vec));
    TMat(i,:) = interp1(timeVec(I), vec(I), timeVec);
    PMat(i,:) = interp1(timeVec(I), PMat(i,I), timeVec);
    SMat(i,:) = interp1(timeVec(I), SMat(i,I), timeVec);
    RMat(i,:) = interp1(timeVec(I), RMat(i,I), timeVec);
    chlMat(i,:) = interp1(timeVec(I), chlMat(i,I), timeVec);
    cdMat(i,:) = interp1(timeVec(I), cdMat(i,I), timeVec);
    bbMat(i,:) = interp1(timeVec(I), bbMat(i,I), timeVec);
    tryMat(i,:) = interp1(timeVec(I), tryMat(i,I), timeVec);
    trymMat(i,:) = interp1(timeVec(I), trymMat(i,I), timeVec);
    pheMat(i,:) = interp1(timeVec(I), pheMat(i,I), timeVec);
    phemMat(i,:) = interp1(timeVec(I), phemMat(i,I), timeVec);
end

%% Potential density
[SAMat, in_ocean] = gsw_SA_from_SP(SMat,PMat,nanmean(lonVec),nanmean(latVec));
CTMat = gsw_CT_from_t(SAMat,TMat,PMat);
sigMat = gsw_sigma0(SAMat,CTMat);



% $$$ figure(1)
% $$$ clf
% $$$ contourf(timeVec, zVec, SMat, [32:.1:40], 'lineStyle', 'none')
% $$$ hold on
% $$$ contour(timeVec, zVec, RMat, [20:.2:30], 'lineColor', 'k')
% $$$ caxis([32 40])
% $$$ set(gca, 'ydir', 'reverse')
% $$$ set(gca, 'xdir', 'reverse')
% $$$ colorbar
M241_quickcnv_map
M241_quickcnv_plot