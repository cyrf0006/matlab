lonLims = [2 8];
latLims = [42 44];



%% plot map
close all
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.0 ; % horiz. space between subplots
dy = 0.0; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.02; % very right of figure
tops = 0.01; % top of figure
bots = 0.03; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
FS = 12+2;
FS1 = 14+2;
FS2 = 16+2;
FS3 = 18+2;

path = ['~/data/matlab_bathym/MedRhone.mat']; % 30-sec.
lims = [latLims lonLims];

fid = fopen(path);
if fid == -1 % doesnt exist yet!
    disp('30-sec. region doesn''t exist yet!')
    disp(' Extracting from GEBCO, may take some time...')
    [lat lon z] = getGebco('~/data/GEBCO/GEBCO_08.nc', 'z', lims);
    disp('   -> done!')
    save(path, 'lat', 'lon', 'z');
else
    load(path)
end

I=find(lat<latLims(2) & lat>latLims(1));
J=find(lon<lonLims(2) & lon>lonLims(1));
latitude=lat(I);
longitude=lon(J);
bathy=z(I,J);



figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 30 15])
m_proj('mercator','long',lonLims,'lat',latLims);
hold on
load gebco64
colormap(gebco);
m_pcolor(longitude,latitude,abs(bathy)); shading flat;
[HH, HH] = m_contour(longitude,latitude,-bathy, [0:200:1000 2000], 'color', 'k');
m_gshhs_f('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlabel('Longitude', 'FontSize', FS2, 'fontweight', 'bold')
ylabel('Latitude', 'FontSize', FS2, 'fontweight', 'bold')
set(gca, 'FontSize', FS1, 'fontweight', 'bold')

% add track
h1 = m_plot(lonVec, latVec, '-', 'color', 'm', 'lineWidth', 2);
% add time
% $$$ for i = [1 round(length(timeVec)/2) length(timeVec)] % plot one out of X
% $$$     h2 = m_text(lonVec(i), latVec(i), datestr(timeVec(i), 31), 'fontWeight', 'bold');
% $$$     h3 = m_plot(lonVec(i), latVec(i), '.k', 'markerSize', 8);
% $$$     h1 = [h1 h2 h3];
% $$$ end

%% Few WPs on the transect
I = 1;
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP1', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'bottom', 'horizontalAlignment', 'left')

tt1 = datenum(2014,11,21,15,0,0);
[Y, I] = min(abs(timeVec-tt1));
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP2', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')


tt2 = datenum(2014,11,23,15,0,0);
[Y, I] = min(abs(timeVec-tt2));
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP3', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')

tt3 = datenum(2014,11,26,0,0,0);
[Y, I] = min(abs(timeVec-tt3));
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP4', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')

tt4 = datenum(2014,11,27,12,0,0);
[Y, I] = min(abs(timeVec-tt4));
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP5', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')

I = length(timeVec);
m_plot(lonVec(I), latVec(I), 'or', 'markerSize', 8, 'markerFaceColor', 'r');
m_text(lonVec(I), latVec(I), 'WP6', 'color', 'r', 'FontSize', FS1, 'fontweight', 'bold', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')


m_grid('box','fancy')
adjust_space
cbar_offset = 0;%0.04; 
offset2 =0;% 0.02; % offset between heigth of colorbar 
c = colorbar('location','north');
cb_pos = get(c, 'position');
cb_back = cb_pos;
cb_pos(4) = .02;
cb_pos(1) = .12;%cb_pos(1)+.1*cb_pos(3);
cb_pos(3) = .25;
cb_pos(2) = .8;%cb_pos(2)+cbar_offset;
set(c, 'pos', cb_pos);
set(c, 'fontsize', FS2, 'fontweight', 'bold')

ti = ylabel(c,'Depth (m)', 'FontSize', FS, 'fontweight', 'bold');
ti_pos = get(ti, 'position');
set(ti, 'Rotation',0.0);     
clim = get(gca, 'clim');
set(ti, 'position', [1000  3 ti_pos(3)]); 
set(gca, 'fontSize', FS)



% $$$ 
% $$$ set(gcf, 'renderer', 'painters'); % vectorial figure
print('-dpng', '-r300',  'M241_map.png')

