clear all
close all

%% Info on deployment (manually edited)
structFile = ['/home/cyrf0006/research/MIO/seaExplorerData/M78-' ...
              'OSCAHR/data_processing/osca01_struct.mat'];

zMax = 300;
t0 = datenum(2015, 10, 28, 13, 45, 0);
tf = datenum(2015, 11, 10, 6, 45, 0);


%% Load data + info needed further
load(structFile);
w = whos;
structName = w.name;
command = sprintf('s = %s ;',structName);
eval(command);    
lat = nanmean(s.log.Lat); % <------- CHECK THIS!!!!!!!
lon = nanmean(s.log.Lon);
lat = 43;
lon = 8;