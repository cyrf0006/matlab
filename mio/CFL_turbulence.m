clear all
close all
% $$$ 
% $$$ path1 = ['/media/cyrf0006/Seagate Backup Plus Drive/cyrf0006_2014-03-26/PhD/CamilH/bootstrap'];
% $$$ path2 = ['/media/cyrf0006/Seagate Backup Plus Drive/cyrf0006_2014-03-26/PhD/CamilH/station1'];
% $$$ 
% $$$ 
% $$$ load(fullfile(path2, 'tout_epsbinH.mat'));

GAMMA = .2;

% depth range for extracting the average
zmin = 10; % top of the fisrt bin
zbin = 1;
zmax = 300;
nboot = 500;    
P_bin = [zmin+zbin/2:zbin:zmax-zbin/2]';


% Files' list
fid = fopen('eps_21Nov.list');
fid = fopen('eps_all.list');
fid = fopen('eps_all_2.list');
C = textscan(fid, '%s', 'delimiter', '\n');
epsFiles = char(C{1});
noProfiles = size(epsFiles,1); %number of eps_files 

% raw matrix to fill
epsVec = [];
zVec = [];
N2Vec = [];
KVec = [];

N2_count = 0;
N2_min = -6;

% raw matrix to fill
mat_eps_bin = nan(length(P_bin), noProfiles);
mat_K_bin = mat_eps_bin;
mat_N2_bin = mat_eps_bin;
timeVec = nan(1,noProfiles);
for i = 1:noProfiles

% $$$     if i == 4
% $$$         keyboard
% $$$     end
% $$$     
    fname = epsFiles(i, :);
    I = find(fname==' ');   
    fname(I) = [];
    fid = fopen(fname);
    if fid ~= -1
        load(fname)        
    else
        continue
    end
    
    maxp(i) = max(p_eps1(end), p_eps2(end));
    timeVec(i) = mtime_eps(1);
    %%%%%%%%%%%%%%%%%%%%%%
    % - EPSILON, N2, K - %
    %%%%%%%%%%%%%%%%%%%%%%    
    
    % ---- average of 2 epsilon profiles ---- %
    % second turbulence sensor not present!!
    p_k = p_eps1;
    p_N = p_eps1;
    EPS = eps1';
    
    % if only nans
    if sum(~isnan(EPS))==0;
        continue
    end
    
    % Homestyle despike
    [Y, No] = Dspike(EPS, 5, 8);
    % uses home despike
    EPS = Y;
    
    % ---- mean N2 ---- %
    N2=nan(length(p_k),1);
    if length(p_N)==length(p_k);
        N2(:) = N.^2;
    else
        N2(ind:length(p_N))=N.^2;
    end
    
    % ---- compute K_rho ---- %
    K_rho = GAMMA.*EPS./(N2);

   
    % ---- Remove unrealistic diffusivity ---- %
    I = find(log10(N2)<N2_min);
    if ~isempty(I)==1
        K_rho_ignored(N2_count+1:N2_count+length(I))=K_rho(I)';
        K_rho(I)=NaN;
        N2_count = N2_count+length(I);
    end    
    
    % ------ remove last bins ------------ %
    I = find(~isnan(EPS==1));
        
    % keyboard
    no_remove = 10;
    if length(I)<no_remove
        continue
    elseif no_remove ~= 0
        EPS(I(end-no_remove+1:end))=NaN;
        K_rho(I(end-no_remove+1:end))=NaN;
        N2(I(end-no_remove+1:end))=NaN;
    end
    % ----------------------------------- %
    
    % ---------- bin profile ------------ %
    for j = 1:length(P_bin)
        I = find(p_k >= P_bin(j)-zbin/2 & p_k <= P_bin(j)+zbin/2);
        if ~isempty(I)
            mat_eps_bin(j, i) = nanmean(EPS(I));
            mat_K_bin(j, i) = nanmean(K_rho(I));
            mat_N2_bin(j, i) = nanmean(N2(I));
        end
    end
    % ----------------------------------- %
    
    % --------- Store vector data ------- %
    epsVec = [epsVec; EPS];
    KVec = [KVec; K_rho];
    N2Vec = [N2Vec; N2];
    zVec = [zVec; p_eps1'];
    % ----------------------------------- %
end 


%% Some cleaning

I  = find(~isnan(KVec));
epsVec = epsVec(I);
KVec = KVec(I);
zVec = zVec(I);
N2Vec = N2Vec(I);
save CFLstats.mat epsVec KVec N2Vec zVec 

figure(1)
clf
hist(log10(epsVec), 100)
hold on
plot([-9.5 -9.5], [0 6000], '--r')
I = find(epsVec>=3e-10);
epsVec = epsVec(I);
KVec = KVec(I);
zVec = zVec(I);
N2Vec = N2Vec(I);

disp('noise removed!')
save CFLstats_noiseRemoved.mat epsVec KVec N2Vec zVec 



%% Loop on each depth bin + Bootstrap
disp('bootstrap...')
zBin = 2;
zReg = [zBin/2:zBin:max(zVec)]';

eps_ave = nan(size(zReg));
eps_2p5 = nan(size(zReg));
eps_97p5 = nan(size(zReg));

nboot = 500;

for i = 1:length(zReg)

    I = find(zVec>=zReg(i)-zBin/2 & zVec<=zReg(i)+zBin/2);
    myEps = epsVec(I);
    N = length(myEps);
    
    % create random sampling
    for b = 1:nboot
        r = rand(N,1);
        r = ceil(r*N/1);        
        eps_boot_b(b) = nanmean(myEps(r));
    end

    % mean of random sampling
    eps_ave(i) = nanmean(eps_boot_b);

    % Compute 95% confidence interval
    eps_sort = sort(eps_boot_b);
    CI_2p5 = round(2.5/100*nboot);
    CI_97p5 = round(97.5/100*nboot);
    eps_2p5(i) = eps_sort(CI_2p5);
    eps_97p5(i) = eps_sort(CI_97p5);
end


figure(3)
clf
semilogx(eps_ave, zReg, 'k', 'linewidth', 0.25);

% shade area
hold on
x2 = eps_2p5;
x1 = eps_97p5;
I = find(~isnan(x1)==1 & ~isnan(x1)==1);
patch([x1(I); flipud(x2(I)); x1(I(1))], [zReg(I); flipud(zReg(I)); zReg(I(1))], [.8 .8 .8], 'edgecolor', 'none');
semilogx(eps_ave, zReg, 'k', 'linewidth', 0.25);
hold off
set(gca, 'ydir', 'reverse')
axis([1e-9 5e-6 0 105])
set(gca, 'xtick', [1e-9 1e-8 1e-7 1e-6 1e-5 1e-4])
ylabel('P (dbar)', 'FontSize', 9)
xlabel('\epsilon (W kg^{-1})', 'FontSize', 9)
set(gca, 'FontSize', 9)
set(gca, 'xminortick', 'off')
set(gca, 'yminortick', 'on')
set(gca, 'xgrid', 'on')
set(gca, 'xminorgrid', 'off')
set(gca, 'tickdir', 'out')





figure(2)
clf
imagesc(log10(mat_eps_bin))

figure(4)
clf
pcolor(timeVec, P_bin, log10(mat_eps_bin))
shading flat
set(gca, 'ydir', 'reverse')



figure(5)
clf
imagesc(timeVec(82:136), P_bin, log10(mat_eps_bin(:,82:136)))
shading flat
set(gca, 'ydir', 'reverse')
