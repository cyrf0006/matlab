%function glider_contours(dplFile)

dplFile = 'roma02_contours.dpl';

% usage ex:
% glider_contours('roma02_contours.dpl')

%% Read deploy. file
fid = fopen(dplFile);
if fid == -1
    disp('No deployment file was found, please check this! [Quit]')
    return
else
    tline = fgetl(fid);
    while ischar(tline)
        eval(tline);
        tline = fgetl(fid);
    end
    fclose(fid);
end

%% Load structure
disp(sprintf('load %s', structFile));
load(structFile)
w = whos;

found = 0;
i = 1;
while found == 0
    if strcmp(w(i).class, 'struct')
        found = 1;
        structName = w(i).name;
    else
        i = i+1;
    end
end
command = sprintf('s = %s ;',structName);
eval(command);

% lat-lon for GSW toolbox
latDeg = floor(s.log.Lat/100);
lonDeg = floor(s.log.Lon/100);
I = find(lonDeg>=0 & lonDeg <=360);
lon = nanmean(lonDeg(I));
I = find(latDeg>=-90 & lonDeg <=90);
lat = nanmean(latDeg(I));

% interp z to time (remove NaNs)
z = s.data.NAV_DEPTH;
n = s.mtimeDat;       
I = find(~isnan(z));
z = interp1(n(I), z(I), n);

%% "dirty" cast detection
W = gradient(z)./gradient(n*86400); % m/s
dt = diff(abs(n(1:2)))*86400; %sec
fs = 1/dt; % Hz
freq_low = 0.01; %Hz
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
I= find(isnan(W));
W(I) = 0;
Wfilt = filtfilt(b, a, W);

I = find(abs(Wfilt)<.01);
z(I) = NaN; % flag as NaN when not moving
I = find(z>zMax);
z(I) = NaN; % Ignore deeper casts


%% build the grid
x = t0:.5/1440:tf;
y = 0:1:max(z);
[X,Z] = meshgrid(x,y);    


%% -------------------- Data ---------------------- %%

%% SEABIRD (.25 Hz)
disp(' -> Seabird data')
T = s.data.SBD_TEMPERATURE;
C = s.data.SBD_CONDUCTIVITY;
P = s.data.SBD_PRESSURE;
I = find(~isnan(T) & P<zMax & P>0);
I(1) = []; % remove 1st point
T = T(I);
C = C(I);
P = P(I);
timeSBD = n(I);

% despike C
[Cdspk, Ispikes] = sx_despike(C, 3, .01, .25, 5);

% Thermal lag
% TO-DO!

% GSW stuff
SP = gsw_SP_from_C(Cdspk*10,T,P);
[SA, in_ocean] = gsw_SA_from_SP(SP,P,lon,lat);
CT = gsw_CT_from_t(SA,T,P);
sig0 = gsw_sigma0(SA,CT);

% Grid the data
% ** Between P and z, check to be sure. **
[XI,ZI,sigMat] = griddata(timeSBD*10,P,sig0,X*10,Z);
[XI,ZI,TMat] = griddata(timeSBD*10,P,CT,X*10,Z);
[XI,ZI,SMat] = griddata(timeSBD*10,P,SA,X*10,Z);

%% WL-TRIPLET
disp(' -> Wetlab triplet')
CHL = s.data.TRI_CHL_SCALED;
CDOM = s.data.TRI_CDOM_SCALED;
CHLc = s.data.TRI_CHL_COUNT;
BB = s.data.TRI_BB_700_SCALED;
volt = s.log.Voltage;

% interp P
Pchla = s.data.SBD_PRESSURE;
I = find(~isnan(Pchla));
Pchla = interp1(n(I), Pchla(I), n');

I = find(~isnan(CHL) & Pchla<zMax);
I(1) = []; % remove 1st point
CHL = CHL(I);
CDOM = CDOM(I);
BB = BB(I);
CHLc = CHLc(I);
Pchla = Pchla(I);
timeCHL = n(I);

% Despike dangerous because dt may be be even !!!!!!!!!!!!!!!!!!!!!!!
[CHLdspk, Ispikes] = sx_despike(CHL, 5, .01, .25, 5);
[CDOMdspk, Ispikes] = sx_despike(CDOM, 5, .01, .25, 5);
[BBdspk, Ispikes] = sx_despike(BB, 5, .01, .25, 5);
[XI,ZI,chlaMat] = griddata(timeCHL*10,Pchla,CHL,X*10,Z);
[XI,ZI,cdomMat] = griddata(timeCHL*10,Pchla,CDOMdspk,X*10,Z);
[XI,ZI,bbMat] = griddata(timeCHL*10,Pchla,BBdspk,X*10,Z);


%% MiniFLuo
disp(' -> Minifluo-UV')
% $$$ % SN <=8
% $$$ TRYc = s.data.MFL_V1; % counts
% $$$ PHEc = s.data.MFL_V2; % counts
% $$$ PHEm = s.data.MFL_V3; % counts_monitor
% $$$ TRYm = s.data.MFL_V4; % counts_monitor

% other SN
TRYc = s.data.MLF_V1; % counts
PHEc = s.data.MLF_V2; % counts
TRYm = s.data.MLF_V3; % counts_monitor
PHEm = s.data.MLF_V4; % counts_monitor

% $$$ TRYc = s.data.UV1_V1; % counts
% $$$ PHEc = s.data.UV1_V2; % counts
% $$$ TRYm = s.data.UV1_V3; % counts_monitor
% $$$ PHEm = s.data.UV1_V4; % counts_monitor

Pmfl = s.data.SBD_PRESSURE;
I = find(~isnan(Pmfl));
Pmfl = interp1(n(I), Pmfl(I), n');

I = find(~isnan(TRYc) & Pmfl<zMax);
TRYc = TRYc(I);
TRYm = TRYm(I);
PHEc = PHEc(I);
PHEm = PHEm(I);
Pmfl = Pmfl(I);
timeMFL = n(I);


[V1b, Ib] = min(TRYc);
%TRY = ( ((TRYc-DARK)./(TRYm-DARK)) - ((V1b-DARK)./(TRYm(Ib)-DARK)) )./TRY_calib;
TRY = ( ((TRYc-DARK)./(TRYm-DARK)) )./TRY_calib;
PHE = ( ((PHEc-DARK)./(PHEm-DARK)) )./PHE_calib;

[TRYdspk, Ispikes] = sx_despike(TRY, 5, .01, .25, 5);
[PHEdspk, Ispikes] = sx_despike(PHE, 5, .01, .25, 5);

[XI,ZI,tryMat] = griddata(timeMFL*10,Pmfl,TRYdspk,X*10,Z);
[XI,ZI,pheMat] = griddata(timeMFL*10,Pmfl,PHEdspk,X*10,Z);

% ---------------------------------------------------- %




%%  ------- Plots ------ %%
disp('Plot data!')
glider_contours_plot
