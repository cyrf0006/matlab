function egoGlider_TScontours2(structFile)

% Script for basic treatment of ego .mat structure (fresher
% subsurface layer observed on Nice-Dyfamed transects).
%
% usage ex: 
%  glider_contours('mooseT00_33_struct.mat')

zMin = 0;
zMax = 200;

%% Load structure
disp(sprintf('load %s', structFile));
load(structFile)
w = whos;

found = 0;
i = 1;
while found == 0
    if strcmp(w(i).class, 'struct')
        found = 1;
        structName = w(i).name;
    else
        i = i+1;
    end
end
command = sprintf('s = %s ;',structName);
eval(command);


% lat-lon for GSW toolbox
lon = 8;
lat = 44;


dataName = [structFile '_TS.mat'];
fid = fopen(dataName);
if fid ~= -1
    disp('load existing TS.mat')
    load(dataName)
else
    disp(['Will create ' dataName '.May take some time...'])

    %% interp z to time (remove NaNs)
    z = s.zzz;
    n = s.days;
    I = find(isnan(n));
    n(I) = [];
    z(I) = [];
    I = find(~isnan(z));
    z = interp1(n(I), z(I), n);


    %% "dirty" cast detection

    W = gradient(z)./gradient(n*86400); % m/s
    dt = diff(abs(n(10:11)))*86400; %sec
    fs = 1/dt; % Hz
    freq_low = 0.01; %Hz
    Wn_low = freq_low/(fs/2);
    [b,a] = butter(4, Wn_low);
    I= find(isnan(W));
    W(I) = 0;
    Wfilt = filtfilt(b, a, W);

    I = find(abs(Wfilt)<.01);
    z(I) = NaN; % flag as NaN when not moving
    I = find(z>zMax);
    z(I) = NaN; % Ignore deeper casts


    %% build the grid
    x = min(n):.5/1440:max(n);
    y = 0:1:max(z);
    [X,Z] = meshgrid(x,y);  

    %% SEABIRD (.25 Hz, ~9pts/m)
    disp(' -> Seabird data')
    T = s.tp;
    C = s.Rt;
    P = s.pp;
    I = find(~isnan(T) & P<zMax & P>0);
    T = T(I);
    C = C(I);
    P = P(I);
    timeSBD = n(I);

    % despike C
    [Cdspk, Ispikes] = sx_despike(C, 3, .01, .25, 5);

    % Thermal lag
    % TO-DO!

    % GSW stuff
    SP = gsw_SP_from_C(Cdspk*10,T,P);
    [SA, in_ocean] = gsw_SA_from_SP(SP,P,lon,lat);
    CT = gsw_CT_from_t(SA,T,P);
    sig0 = gsw_sigma0(SA,CT);

    % Grid the data
    % ** Between P and z, check to be sure. **
    [XI,ZI,sigMat] = griddata(timeSBD*10,P,sig0,X*10,Z);
    [XI,ZI,TMat] = griddata(timeSBD*10,P,CT,X*10,Z);
    [XI,ZI,SMat] = griddata(timeSBD*10,P,SA,X*10,Z);
    % rename
    timeVec = x;
    zVec = y;
    save(dataName, 'timeVec', 'zVec', 'sigMat', 'TMat', 'SMat');
end


% clean
t0 = timeVec(1);
tf = timeVec(end);

figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 25 25])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 2; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.16; % very right of figure
tops = 0.02; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

%t1 = datenum(2015, 11, 4, 3, 10, 0);
%t2 = datenum(2015, 11, 7, 17, 0, 0);
t1 = min(timeVec);
t2 = max(timeVec);
%zMin = 0;
%zMax = 200;

% S1
subplot(2,1,1)
imagesc(timeVec, zVec, TMat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([12 20])
%ylabel('Depth (m)', 'fontWeight', 'bold')
set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'T (^{\circ}C)', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'a', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')

% S2
subplot(2,1,2)
imagesc(timeVec, zVec, SMat)
datetick('x',7)
xlim([t0 tf])
ylim([zMin zMax])
hold on
contour(timeVec, zVec, sigMat, [27:.5:30.5], 'k')
caxis([37.7 39])
ylabel('Depth (m)', 'fontWeight', 'bold')
%set(gca, 'xticklabel', []);
cb = colorbar;
ti = ylabel(cb,'S_A (g Kg^{-1})', 'FontSize', 10, 'fontweight', 'bold');
set(gca, 'fontWeight', 'bold')
adjust_space
pause(.1)
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.6;
set(cb, 'pos', cbPos)
text(tf-1, 800, 'b', 'color', [1 1 1]*0, 'fontsize', 14, 'fontweight', 'bold')
xlabel(datestr(timeVec(1),28), 'fontWeight', 'bold')


figureName = [structFile '_TS.png'];
print('-dpng', '-r300', figureName)  


