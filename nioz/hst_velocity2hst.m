function [U, E, N, W] = velocity2hst(adcpFile, zVec, timeVec)

% function velocity2hst(adcpFile, zVec, timeVec)
%
% usage ex: 
%  velocity2hst('/media/Seagate1TB/NIOZ/thermistordata/ROC12/roc12.mat', zVec, timeVec)   
%  velocity2hst(adcpFile, zVec, timeVec) (called in hst_batchelor2turbulence.m)
    
%% Load and get the relevant variables
load(adcpFile);

N = [SerNmmpersec/1000]'; % in m/s now
E = [SerEmmpersec/1000]';
W = [SerVmmpersec/1000]';
uAdcp = sqrt(N.^2 + E.^2);

% build depth matrix (time variable)
zAdcp = AnDepthmm/1000; % instrument depth
zMatrix = nan(size(N));

% remove values at wrong depth (while it was lowering?)
I = abs(zAdcp - nanmean(zAdcp))>10; % flag if depth is 10m from mean
uAdcp(:,I) = NaN;

for i = 1:length(zAdcp)    
    zMatrix(:,i) = [SerBins*RDIBinSize+zAdcp(i)+RDIBin1Mid]';
end

% time vector
timeAdcp = [datenum(SerYear+2000, SerMon, SerDay, SerHour, SerMin, SerSec)]';
clear Ser* An*


%% Average/interp to thermistor depth
Uz = nan(length(zVec), length(timeAdcp));
for i = 1:length(timeAdcp)
    Uz(:,i) = interp1(zMatrix(:,i), uAdcp(:,i), abs(zVec));
    Ez(:,i) = interp1(zMatrix(:,i), E(:,i), abs(zVec));
    Nz(:,i) = interp1(zMatrix(:,i), N(:,i), abs(zVec));
    Wz(:,i) = interp1(zMatrix(:,i), W(:,i), abs(zVec));
end

%% Average/interp to thermistor resolution
if diff(timeAdcp(1:2)) > diff(timeVec(1:2)) % adcp velocity must be interpolated
    U = interp1(timeAdcp, Uz', timeVec);
    E = interp1(timeAdcp, Ez', timeVec);
    N = interp1(timeAdcp, Nz', timeVec);
    W = interp1(timeAdcp, Wz', timeVec);
    U = U';
    E = E';
    N = N';
    W = W';
else % adcp vel must be averaged
    U = nan(size(Uz,1), length(timeVec))
    E = nan(size(Ez,1), length(timeVec))
    N = nan(size(Nz,1), length(timeVec))
    W = nan(size(Wz,1), length(timeVec))    
    dt = diff(timeVec(1:2));
    disp('NEVER BEEN CHECK! [K]')
    keyboard
    for i = 1:length(timeVec)
        I = find(timeAdcp>=timeVec-dt/2 & timeAdcp<timeVec+dt/2);
        U(:,i) = nanmean(Uz(:,I), 2);
        E(:,i) = nanmean(Ez(:,I), 2);
        N(:,i) = nanmean(Nz(:,I), 2);
        W(:,i) = nanmean(Wz(:,I), 2);

    end
end
