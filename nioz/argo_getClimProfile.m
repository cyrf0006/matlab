function [P,T,S] = argo_getClimProfile(ncFile, lat, lon)

% function argo_getClim(ncFile, latLims, lonLims)
%
% usage ex: [P,T,S] = argo_getClimProfile('RG_ArgoClim_33pfit_2015_mean.nc', 55, 348);    

    
latitude = nc_varget(ncFile, 'LATITUDE');
longitude = nc_varget(ncFile, 'LONGITUDE');
P =  nc_varget(ncFile, 'PRESSURE');

T = nc_varget(ncFile, 'ARGO_TEMPERATURE_MEAN');  
S = nc_varget(ncFile, 'ARGO_SALINITY_MEAN');  

[Y, I] = min(abs(longitude-348));
[Y, J] = min(abs(latitude-55));

keyboard

T = squeeze(T(:,J,I));
S = squeeze(S(:,J,I));


