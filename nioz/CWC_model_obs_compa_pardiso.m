clear all
close all


figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 20 10])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.09; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
load ~/git/matlab/colormaps/oxygen_discrete.mat
colormap(oxygen_discrete)
i=2;

%cd /home/cyrf0006/research/Aestus/runF8/output
%cd /media/Seagate2TB/Aestus_outputs/output_runD01/
cd /media/Seagate2TB/Aestus_outputs/runF09_grid_5/output


%% some info on the grid
load time.out;
load grid.mat

% snapshot info 
%XLIM = [-1.5 4]-.5;
XLIM = [-2 4];
ZLIM = [300 1000];
% Get data from this frame
timeVec = time(:,3) + datenum(2000,1,1);

cycle = 4;
% $$$ t1 = datenum(2000, 1, 1, 3, 0, 0)+cycle;
% $$$ t2 = datenum(2000, 1, 1, 6, 0, 0)+cycle;
% $$$ t3 = datenum(2000, 1, 1, 12, 0, 0)+cycle;
% $$$ t4 = datenum(2000, 1, 1, 17, 0, 0)+cycle;
% $$$ t5 = datenum(2000, 1, 1, 22, 0, 0)+cycle;
% $$$ t6 = datenum(2000, 1, 2, 3, 0, 0)+cycle;

t1 = datenum(2000, 1, 1, 3, 0, 0)+cycle;
t2 = datenum(2000, 1, 1, 9, 0, 0)+cycle;   % <---
t3 = datenum(2000, 1, 1, 15, 0, 0)+cycle;  % <--- 
t4 = datenum(2000, 1, 1, 12, 0, 0)+cycle;
t5 = datenum(2000, 1, 1, 21, 0, 0)+cycle;  % <---
t6 = datenum(2000, 1, 2, 18, 0, 0)+cycle;


[Y, frameNos(1)] = min(abs(timeVec-t1));
[Y, frameNos(2)] = min(abs(timeVec-t2));
[Y, frameNos(3)] = min(abs(timeVec-t3));
[Y, frameNos(4)] = min(abs(timeVec-t4));
[Y, frameNos(5)] = min(abs(timeVec-t5));
[Y, frameNos(6)] = min(abs(timeVec-t6));

quiverDecX = 8;
quiverDecZ = 20;


% get bathym
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
xCWC = [CWC_bathy(:,1)+200]/1000; %km
zCWC = abs(CWC_bathy(:,2));
ydepthVec = [zCWC];
xdepthVec = [xCWC];
xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];

Vsig = 31:.02:32.5;
Vo2 = 200:1:240;
x = (x)/1000;
chi = chi/1000;

sig = aes_getfield('sig',frameNos(i));
sig = aes_putNaN(sig,flag_s);
sig = sig';
c1 = aes_getfield('C01',frameNos(i));
c1 = aes_putNaN(c1,flag_s);
c1 = c1';    
c2 = aes_getfield('C02',frameNos(i));
c2 = aes_putNaN(c2,flag_s);
c2 = c2';   
u = aes_getfield('u',frameNos(i));
u = aes_putNaN(u,flag_u);
u = u';   
w = aes_getfield('w',frameNos(i));
w = aes_putNaN(w,flag_s);
w = w'; 

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
c1 = c1(J,II);
c2 = c2(J,II);
sig = sig(J,II);
w = w(J,I);
u = u(J,I);

% S1
subplot(121)
contourf(chiVec, zVec, c1, Vo2, 'linestyle', 'none')
%shading flat
set(gca, 'ydir', 'reverse')
hold on
contour(chiVec, zVec, sig, Vsig, 'color', 'k')
patch(xpatch, ypatch, [1 1 1]*.6)   

Z = zVec(1:quiverDecZ:end);
X = xVec(1:quiverDecX:end);
U = u(1:quiverDecZ:end, 1:quiverDecX:end);
W = -w(1:quiverDecZ:end, 1:quiverDecX:end);


[kmax imax] = size(U);
vecColor= 'm';
unit = false;

        
%% 2D vector field
scale = .5;
for i = 1:imax
    for k = 1:kmax-1
        arrow7(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
    end
end 

arrow7(-.55,850,.2,0,scale,vecColor,unit);
text(.65,845,'0.20 ms^{-1}', 'color', 'm', 'fontWeight', 'bold', 'verticalAlignment', 'middle')
arrow7(-.55,900,.35,0,scale,vecColor,unit);
text(.65,895,'0.35 ms^{-1}', 'color', 'm', 'fontWeight', 'bold', 'verticalAlignment', 'middle')
arrow7(-.55,950,.50,0,scale,vecColor,unit);
text(.65,945,'0.50 ms^{-1}', 'color', 'm', 'fontWeight', 'bold', 'verticalAlignment', 'middle')

caxis([205 235])
xlim(XLIM)
ylim(ZLIM)

[Y, I] = min(abs([zCWC-919]));
plot(xCWC(I), zCWC(I), 'rp', 'markerSize', 14, 'markerFaceColor', 'r')
 
caxis([205 235])
xlim(XLIM)
ylim(ZLIM)
adjust_space

xlabel('x (km)', 'fontWeight', 'bold')
ylabel('z (m)', 'fontWeight', 'bold')
text(-1.75, 975, 'a', 'fontWeight', 'bold', 'fontSize', 14)        
set(gca, 'ytick', [300:100:1000])



%%% --------------- Other simulation ---------- %%



%cd /home/cyrf0006/research/Aestus/runF9/output
%cd /media/Seagate2TB/Aestus_outputs/output_runD02/
cd /media/Seagate2TB/Aestus_outputs/runF08_grid_5/output

%% some info on the grid
load time.out;
load grid.mat
i=2;

XLIM = [-1.5 4];
XLIM = [-2 4];
timeVec = time(:,3) + datenum(2000,1,1);

% get bathym
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC_2.dat');
xCWC = [CWC_bathy(:,1)+200]/1000; %km
zCWC = abs(CWC_bathy(:,2));
ydepthVec = [zCWC];
xdepthVec = [xCWC];
xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];
 

x = (x)/1000;
chi = chi/1000;

sig = aes_getfield('sig',frameNos(i));
sig = aes_putNaN(sig,flag_s);
sig = sig';
c1 = aes_getfield('C01',frameNos(i));
c1 = aes_putNaN(c1,flag_s);
c1 = c1';    
c2 = aes_getfield('C02',frameNos(i));
c2 = aes_putNaN(c2,flag_s);
c2 = c2';   
u = aes_getfield('u',frameNos(i));
u = aes_putNaN(u,flag_u);
u = u';   
w = aes_getfield('w',frameNos(i));
w = aes_putNaN(w,flag_s);
w = w'; 

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
c1 = c1(J,II);
c2 = c2(J,II);
sig = sig(J,II);
w = w(J,I);
u = u(J,I);


%% SUBPLOT 2
subplot(122)

contourf(chiVec, zVec, c1, Vo2, 'linestyle', 'none')
%shading flat
set(gca, 'ydir', 'reverse')
hold on
contour(chiVec, zVec, sig, Vsig, 'color', 'k')
patch(xpatch, ypatch, [1 1 1]*.6)   

Z = zVec(1:quiverDecZ:end);
X = xVec(1:quiverDecX:end);
U = u(1:quiverDecZ:end, 1:quiverDecX:end);
W = -w(1:quiverDecZ:end, 1:quiverDecX:end);


[kmax imax] = size(U);
vecColor= 'm';
unit = false;


%% 2D vector field
for i = 1:imax
    for k = 1:kmax-1
        arrow7(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
    end
end 

caxis([205 235])
xlim(XLIM)
ylim(ZLIM)

set(gca, 'yticklabel', [])

cb = colorbar;
adjust_space
cbPos = get(cb, 'pos');
figPos = get(gca, 'pos');
cbPos(1) = figPos(1)+figPos(3)+.01;
cbPos(2) = cbPos(2)+.01;
cbPos(4) = cbPos(4)-2*.01;
cbPos(3) = cbPos(3)*.7;
set(cb, 'pos', cbPos)
ti = ylabel(cb,'[O_2] (\mumol/kg)', 'FontSize', 10, 'fontweight', 'bold');
tiPos = get(ti, 'pos');
set(ti, 'rotation', 90)
%        tiPos = [-1 -4.2 1];
set(ti, 'pos', tiPos)

xlabel('x (km)', 'fontWeight', 'bold')

text(-1.75, 975, 'b', 'fontWeight', 'bold', 'fontSize', 14)        
set(gca, 'ytick', [300:100:1000])


cd /home/cyrf0006/research/NIOZ/RockallBank
set(gcf, 'renderer', 'painters')
print('-depsc2', './ctd_transectO2_model_compa.eps')  
