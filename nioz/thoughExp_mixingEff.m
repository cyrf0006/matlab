clear all

g = 9.81;
H = 100;

rhoMin = 31.8;
rhoMax = 32.15;
rhoMean = mean([rhoMin, rhoMax]);
rho0 = rhoMean;
drho = .02; % rho anomaly (z profile)

z = 1:H;
x1  = [rhoMin rhoMean+drho rhoMean-drho rhoMax]; 
z1 = [1 30 70 H];
rho1 = interp1(z1, x1, z);

[rho_sort1, I] = sort(rho1);
d = z - z(I);
Lt1 = rms(d);

A = rand(H,1)/3;
A = A - nanmean(A)/2;
A(1:30) = 0;
A(70:end) = 0;
C = (rhoMax-rhoMin)/H*z+rhoMin;
rho2 = A'+C;

[rho_sort2, I] = sort(rho2);
d = z - z(I);
Lt2 = rms(d);

clf
plot(rho1, z)
set(gca, 'ydir', 'reverse')
hold on
plot(rho2, z, 'r')



% rho1
N2 = g./rho0.*gradient(rho_sort1)./gradient(z);
N = nanmean(sqrt(N2));
EP1 = sum(rho1.*(max(z)-z)); 
EP2 = sum(rho_sort1.*(max(z)-z));
Jb1 = g./H/rho0.*(EP1-EP2).*N;
eps1 = .64*Lt1.^2.*N.^3; 


% rho2
N2 = g./rho0.*gradient(rho_sort2)./gradient(z);
N = nanmean(sqrt(N2));
EP1 = sum(rho2.*(max(z)-z)); 
EP2 = sum(rho_sort2.*(max(z)-z));
Jb2 = g./H/rho0.*(EP1-EP2).*N;
eps2 = .64*Lt2.^2.*N.^3; 


[Lt1 Lt2;
    eps1 eps2;
    Jb1 Jb2;
    Jb1/eps1 Jb2/eps2]