close all
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.25; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

FS = 12;
% Get global bathym    
path = ['~/data/matlab_bathym/rockall.mat']; % 30-sec.
load(path)

% Lower res.
decimFactor = 40;
lat1m = lat(1:decimFactor:end);
lon1m = lon(1:decimFactor:end);
z1m = z(1:decimFactor:end, 1:decimFactor:end);

% Large map
lon_min = min(lonVec2);
lon_max = max(lonVec2);
lat_min = min(latVec2);
lat_max = max(latVec2);

I=find(lat1m<lat_max & lat1m>lat_min);
J=find(lon1m<lon_max & lon1m>lon_min);
latitude=lat1m(I);
longitude=lon1m(J);
bathy=z1m(I,J);


figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 20 20])
m_proj('mercator','long',[min(longitude) max(longitude)],'lat',[min(latitude) max(latitude)]);
m_contourf(lonVec2, latVec2, O2Map, 15, 'color', [.5 .5 .5]); 
hold on
[HH, HH] = m_contour(longitude,latitude,bathy, [0:-1000:-4000], 'color', [.5 .5 .5]*0);
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlab = xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold');
ylab = ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold');
m_line(-15.7975, 55.4824,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',10);
%caxis([230 280])
caxis([200 300])
m_grid('box','fancy')
c= colorbar;
adjust_space

cb_pos = get(c, 'position');
cb_back = cb_pos;
cb_pos(2) = cb_pos(2)+.05; 
cb_pos(3) = cb_pos(3)*.75;
cb_pos(4) = cb_pos(4)-.1;  
set(c, 'pos', cb_pos);
set(c, 'fontsize', FS, 'fontweight', 'bold')

ti = ylabel(c,'[O_2] (\mumol kg^{-1})', 'FontSize', FS, 'fontweight', 'bold');
ti_pos = get(ti, 'position');
ti_pos(1) = 7.9;
set(ti, 'position', ti_pos)

ylab_pos = get(ylab, 'pos');
ylab_pos(1) = -0.22295;
set(ylab, 'position', ylab_pos)

set(gcf, 'renderer', 'painters'); % vectorial figure
print('-depsc', 'MyOcean_O2climLarge.eps')


%% -------------  Figure 2 - Zoom -------------------- %%

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.05; % very left of figure
rigs = 0.3; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

% Zoom map
lon_min = min(lonVec2);
lon_max = -8;
lat_min = 53;
lat_max = 60;

I=find(lat1m<lat_max & lat1m>lat_min);
J=find(lon1m<lon_max & lon1m>lon_min);
latitude=lat1m(I);
longitude=lon1m(J);
bathy=z1m(I,J);


figure(2);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 20 20])
m_proj('mercator','long',[min(longitude) max(longitude)],'lat',[min(latitude) max(latitude)]);
m_contourf(lonVec2, latVec2, O2Map, 30, 'color', [.5 .5 .5]); 
hold on
[HH, HH] = m_contour(longitude,latitude,bathy, [0:-1000:-4000], 'color', [.5 .5 .5]*0);
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlab = xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold');
ylab = ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold');
m_line(-15.7975, 55.4824,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',10);
caxis([230 270])
m_grid('box','fancy')
c= colorbar;
adjust_space

cb_pos = get(c, 'position');
cb_back = cb_pos;
cb_pos(2) = cb_pos(2)+.05; 
cb_pos(3) = cb_pos(3)*.75;
cb_pos(4) = cb_pos(4)-.1;  
set(c, 'pos', cb_pos);
set(c, 'fontsize', FS, 'fontweight', 'bold')

ti = ylabel(c,'[O_2] (\mumol kg^{-1})', 'FontSize', FS, 'fontweight', 'bold');
ti_pos = get(ti, 'position');
ti_pos(1) = 7.9;
set(ti, 'position', ti_pos)

ylab_pos = get(ylab, 'pos');
%ylab_pos(1) = -0.22295;
set(ylab, 'position', ylab_pos)

set(gcf, 'renderer', 'painters'); % vectorial figure
print('-depsc', 'MyOcean_O2climZoom.eps')
