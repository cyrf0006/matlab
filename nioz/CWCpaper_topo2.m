% to be run anywhere, inpired from Rockall_grid_plot.m

clear all
close all

%% parameters
XLIM = [-60 60]; %km
YLIM = [0 1500]; %m


%%%% ---- Haas Mound ---- %%%%%
MY_PATH = pwd;
cd /media/Seagate2TB/Aestus_outputs/output_runF08/

%% grid preparation
load grid
zeta0 = zeta;
xVec = x/1000; %km
zVec = z;
depthVec = H0;

realBathy  = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
realz1 = abs(realBathy(:,2));
realx1 = realBathy(:,1)/1000+.2;


%% initial fields
deepCTD = load('/home/cyrf0006/research/NIOZ/RockallBank/deepCTD/deepCast.mat');
sig = getfield('sig',1);
sig = putNaN(sig,flag_s);
% $$$ sig = sig';
O2 = getfield('C01',1);
O2 = putNaN(O2,flag_s);
% $$$ O2 = O2'; 
[Y, I] = max(depthVec);
sigVec = sig(I,:);
o2Vec = O2(I,:);
I = find(sigVec<0);
sigVec(I) = [];
o2Vec(I) = [];
zVec(I) = [];

[SA, in_ocean] = gsw_SA_from_SP(deepCTD.SP,deepCTD.P,-15.8044,55.4324);
CT = gsw_CT_from_t(SA,deepCTD.T90,deepCTD.P);
sig1 = gsw_sigma1(SA,CT);

% ----> From deep CTD
deepCTD = load('/home/cyrf0006/research/NIOZ/RockallBank/deepCTD/deepCast.mat');
O2Deep = [deepCTD.O2(3); deepCTD.O2(3:end); 250.1; 250.1];
zVecDeep = [0; deepCTD.P(3:end); 2000; 3000];

[SA, in_ocean] = gsw_SA_from_SP(deepCTD.SP,deepCTD.P,-15.8044,55.4324);
CT = gsw_CT_from_t(SA,deepCTD.T90,deepCTD.P);
sig1 = gsw_sigma1(SA,CT);
sig1Deep = [31; sig1(3:end); 32.5];
zVecDeep_sig = [0; deepCTD.P(3:end); 2600];

% home-made gaussian filter <---- dangerous because smoothing an un-even Vector
Psmooth = 0:min(diff(zeta0)):max(zeta0)+min(diff(zeta0));
O2smooth = interp1(zVecDeep, O2Deep, Psmooth); % interp to zeta0
sigsmooth = interp1(zVecDeep_sig, sig1Deep, Psmooth); % interp to zeta0                                        
O2filt = gaussfiltfilt(O2smooth, 14, 75);
sigfilt = gaussfiltfilt(sigsmooth, 14,75);
% $$$ O2Itp2 = O2filt;
% $$$ sigItp2 = sigfilt;
O2Itp2 = interp1(Psmooth, O2filt, zeta0);
sigItp2 = interp1(Psmooth, sigfilt, zeta0);

% ----> From 24h transect
cwcCTD = load('/home/cyrf0006/research/NIOZ/RockallBank/CTD_rockall2012.mat');
I = find(zeta0>=1250);
O2CWC = [cwcCTD.o2Vec(3); cwcCTD.o2Vec(3:end); O2Itp2(I)'; O2Itp2(end)];
sig1CWC = [cwcCTD.sig1Vec(3); cwcCTD.sig1Vec(3:end); sigItp2(I)'; sigItp2(end)];
zVecCWC = [0; cwcCTD.zVec(3:end)'; zeta0(I)'; max(Psmooth)];
% $$$ O2smooth = interp1(zVecCWC, O2CWC, zeta0);
% $$$ sigsmooth = interp1(zVecCWC, sig1CWC, zeta0);
O2smooth = interp1(zVecCWC, O2CWC, Psmooth);
sigsmooth = interp1(zVecCWC, sig1CWC, Psmooth);

O2filt = gaussfiltfilt(O2smooth, 10, 75);
sigfilt = gaussfiltfilt(sigsmooth, 10, 75);

% $$$ O2Itp3 = O2filt;
% $$$ sigItp3 = sigfilt;
O2Itp3 = interp1(Psmooth, O2filt, zeta0);
sigItp3 = interp1(Psmooth, sigfilt, zeta0);

%%%% ---- Other Mound ---- %%%%%
realBathy  = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC_2.dat');
realz2 = abs(realBathy(:,2));
realx2 = realBathy(:,1)/1000+.2;


command = sprintf('cd %s', MY_PATH);
eval(command);
    
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 6; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.15; % very left of figure
rigs = 0.05; % very right of figure
tops = 0.13; % top of figure
bots = 0.15; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

figure(1)
clf
set(gcf, 'PaperUnits', 'Centimeters', 'PaperPosition', [0 0 20 12])

% S1
s1 = subplot(1,7,[1 2]);

%plot(xVec, depthVec, 'k', 'linewidth', 2);
plot(realx1, realz1, 'color', [.5 .5 .5]*0, 'lineStyle', '-','lineWidth', 2)
hold on
%plot(realx, realz, 'color', [.5 .5 .5], 'lineStyle', '--','lineWidth', 2)
xlim([XLIM])
set(gca, 'ydir', 'reverse')

plot([5 20], [860.27 860.27], 'k', 'linewidth', .25)
plot([5 20], [555.87 555.87], 'k', 'linewidth', .25)
plot([13.5 13.5], [555.87 860.27], 'k', 'linewidth', .25)
text(25,730, '305 m', 'fontSize', 10)

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
pos3 = pos2;
pos3(1) = pos1(1);
pos3(3) = pos2(1)+pos2(3)-pos1(1);
set(gca, 'pos', pos3)
set(gca, 'xgrid', 'on', 'ygrid', 'on')

%% Here this is a composite between GEBCO and multibeam ([-7, 12.4]km)
%   (see cleanRockallBathym_CWC.m)
%load ~/research/NIOZ/RockallBank/bathymetry/bathy_multi_CWC.mat
%plot(xVec, abs(zVecSmooth), 'color', [.5 .5 .5], 'lineStyle', '--','lineWidth', 2)


ylabel('z (m)', 'fontWeight', 'bold')
xlabel('x (km)', 'fontWeight', 'bold')
text(-55, 2400, 'a', 'fontWeight', 'bold', 'fontSize', 14)        


% S2
s2 = subplot(1,7, 4);

%plot(xVec, depthVec, 'k', 'linewidth', 2);
plot(realx2, realz2, 'color', [.5 .5 .5]*0, 'lineStyle', '-','lineWidth', 2)
hold on 
%plot(realx, realz, 'color', [.5 .5 .5], 'lineStyle', '--','lineWidth', 2)
xlim([XLIM])
set(gca, 'ydir', 'reverse')

plot([5 20], [862.1735 862.1735], 'k', 'linewidth', .25)
plot([5 20], [788.435 788.435], 'k', 'linewidth', .25)
plot([13.5 13.5], [788.435 862.1735], 'k', 'linewidth', .25)
text(25,830, '75 m', 'fontSize', 10)

adjust_space
pos1 = get(gca, 'pos');
adjust_space
pos2 = get(gca, 'pos');
pos3 = pos2;
pos3(1) = pos1(1);
pos3(3) = pos2(1)+pos2(3)-pos1(1);
adjust_space
pos4 = get(gca, 'pos');
adjust_space
pos5 = get(gca, 'pos');
set(gca, 'pos', pos3)
set(gca, 'xgrid', 'on', 'ygrid', 'on')

%% Here this is a composite between GEBCO and multibeam ([-7, 12.4]km)
%   (see cleanRockallBathym_CWC.m)
% $$$ load ~/research/NIOZ/RockallBank/bathymetry/bathy_multi_CWC2.mat
% $$$ plot(xVec, abs(zVecSmooth), 'color', [.5 .5 .5], 'lineStyle', '--','lineWidth', 2)


xlabel('x (km)', 'fontWeight', 'bold')
text(-55, 2400, 'b', 'fontWeight', 'bold', 'fontSize', 14)        
set(gca, 'yticklabel', [])

% S3
s3 = subplot(176);
load('/home/cyrf0006/research/myOcean/O2_NO3_clim.mat')

xlabels{1} = '[O_2] (\mumol kg^{-1})';
xlabels{2} = '\sigma_1 (kg m^{-3})';
ylabels{1} = '';
ylabels{2} = '';
%[ax,h1,h2] = plotxx(deepCTD.O2(3:end),deepCTD.P(3:end),sig1,deepCTD.P,xlabels,ylabels);
[ax,h1,h2] = plotxx(cwcCTD.o2Vec(3:end),cwcCTD.zVec(3:end),cwcCTD.sig1Vec,cwcCTD.zVec,xlabels,ylabels);

set(h1, 'lineWidth', 2)
set(h2, 'lineWidth', 2)

set(ax(1), 'pos', pos4)
set(ax(2), 'pos', pos4)
set(ax(1), 'ydir', 'reverse')
set(ax(2), 'ydir', 'reverse')
set(ax(1), 'yticklabel', [])
set(ax(2), 'yticklabel', [])
set(ax(1), 'xgrid', 'on', 'ygrid', 'on')
%set(ax(2), 'xgrid', 'on', 'ygrid', 'on')
set(ax(1), 'xlim', [190 260])
%set(ax(2), 'xlim', [30.9 32.55])
set(ax(2), 'xlim', [30.75 32.5])
set(ax(2), 'xtick', [31 32])
set(ax(1), 'ylim', [0 2500])
set(ax(2), 'ylim', [0 2500])
set(get(ax(1),'xlabel'),'fontWeight','bold')
set(get(ax(2),'xlabel'),'fontWeight','bold')

hold(ax(1),'on')
plot(ax(1), o2Vec, zVec, '--', 'lineWidth', 2, 'color', [.5 .5 .5])
%plot(ax(1), cwcCTD.o2Vec, cwcCTD.zVec, '--k', 'lineWidth', 2, 'color', [.5 .5 .5])
hold(ax(1),'off')


hold(ax(2),'on')
plot(ax(2), sigVec, zVec, '--r', 'lineWidth', 2,  'color',[1 .5 .5])
hold(ax(2),'off')
text(30.9, 2400, 'c', 'fontWeight', 'bold', 'fontSize', 14)        


% S4
s4 = subplot(177);
xlabels{1} = '[O_2] (\mumol kg^{-1})';
xlabels{2} = '[NO_3] (\mumol kg^{-1})';
ylabels{1} = '';
ylabels{2} = '';
[ax,h1,h2] = plotxx(O2prof,zVec1,NO3prof,zVec1,xlabels,ylabels);

set(h1, 'lineWidth', 2, 'linestyle', '--')
set(h2, 'lineWidth', 2, 'linestyle', '--')

set(ax(1), 'pos', pos5)
set(ax(2), 'pos', pos5)
set(ax(1), 'ydir', 'reverse')
set(ax(2), 'ydir', 'reverse')
set(ax(1), 'yticklabel', [])
set(ax(2), 'yticklabel', [])
set(ax(1), 'xgrid', 'on', 'ygrid', 'on')

set(ax(1), 'xlim', [190 250])
set(ax(2), 'xlim', [2 13])
set(ax(1), 'xtick', [200 250])
set(ax(2), 'xtick', [5 10])
set(ax(1), 'ylim', [0 2500])
set(ax(2), 'ylim', [0 2500])
set(get(ax(1),'xlabel'),'fontWeight','bold')
set(get(ax(2),'xlabel'),'fontWeight','bold')


text(30.9, 2400, 'd', 'fontWeight', 'bold', 'fontSize', 14)        


print('-depsc', 'AestusCWC_setup2.eps')
