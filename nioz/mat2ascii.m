% run in /home/cyrf0006/research/students/BourgaultP
clear all
close all



%% Termistors
load Temp_fullHD.mat;

timeVec = X + datenum(1950,1,1);

dlmwrite('Temp.dat', Txy, 'delimiter',' ','precision',6);
dlmwrite('TempDepth.dat', YF, 'delimiter',' ','precision',6);
dlmwrite('TempTime.dat', timeVec, 'delimiter',' ','precision',16);


%% ADCP
load uvw_MN120.mat
timeVec = Xtot + datenum(2011,1,1);
zVec = Ytot;

dlmwrite('velU.dat', Utot, 'delimiter',' ','precision',6);
dlmwrite('velV.dat', Vtot, 'delimiter',' ','precision',6);
dlmwrite('velW.dat', Wtot, 'delimiter',' ','precision',6);
dlmwrite('velTime.dat', timeVec, 'delimiter',' ','precision',16);
dlmwrite('velDepth.dat', zVec, 'delimiter',' ','precision',6);
