%function rockall_24htransect(ctdList, timeFile, latfile)
    
% function hst_TSrel(ctdList, timeFile)
%
% Made specificly to read and process CTD casts from ROC12
% cruise. Shell preccessing were done to clean file names and to
% build the timeFile. I also removed few lines in one CTD file
% (64PE360_S101C01fil_ctm_drv_bav_buo.asc)
clear
    
%% Few info on files and parameter
ctdList = '/home/cyrf0006/research/NIOZ/RockallBank/ctdFiles24h.list';
timeFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/timeFile24h.txt';
latFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/lat24h.txt';
lonFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/lon24h.txt';
depthFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/depth24h.txt';
g = 9.81;
griddataItp = false;

XLIM = [-1.65 2.66647];
XLIM = [-1.45 2.8665];
ZLIM = [300 959];
yMin = 0;

%% pre-process ctd info
ctdLat  = load(latFile);
ctdLon  = load(lonFile);
latVec = ctdLat(:,1) + ctdLat(:,2)/60;
lonVec = [ctdLon(:,1) + ctdLon(:,2)/60]*-1;
zVecCTD = load(depthFile);
zMax = max(zVecCTD);
zbin = 10;
zVec = zbin/2:zbin:zMax; % regular data storage


%% load CTD files
fid = fopen(ctdList);
C = textscan(fid, '%s', 'delimiter', '\n');
ctd = char(C{1});
noFiles = size(ctd,1);

% get time vector
fid = fopen(timeFile);
C = textscan(fid, '%s', 'delimiter', '\n');
ctdTime = char(C{1});
ctdTime = datenum(ctdTime);


N2Mat = nan(length(zVec), noFiles);
TMat = nan(length(zVec), noFiles);
O2Mat = nan(length(zVec), noFiles);
FlMat = nan(length(zVec), noFiles);
TrMat = nan(length(zVec), noFiles);
Sig1Mat = nan(length(zVec), noFiles);

for i = 1:noFiles

    fname = ctd(i, :);
    I = find(fname==' ');   
    fname(I) = [];
    
    data = load(fname);
   
    Z = data(:, 14);
    T = data(:, 2);
    C = data(:, 3);
    O2 = data(:, 6);
    Fl = data(:, 5);
    Tr = data(:, 4);    
    SP = data(:, 16);    

    %    SP = gsw_SP_from_C(C,T,Z);
    SA = gsw_SA_from_SP(SP, Z,-13.8, 57.5);
    CT = gsw_CT_from_t(SA, T, Z);
    sig1 = gsw_sigma1(SA,CT);
    rho = gsw_rho(SA,CT,Z);

    % Isolate downcast
    dz = gradient(Z);
    I = find(dz>0.5*mean(abs(dz)));    
% $$$     [zm, I] = max(Z);
% $$$     I = 1:I;
    
    rhoSort = sort(rho(I));
    N2 = g./nanmean(rho(I)).*gradient(rhoSort)./gradient(Z(I));
    
    Z = Z(I);
    CT = CT(I);
    O2 = O2(I);
    Fl = Fl(I);
    Tr = Tr(I)-nanmean(Tr(I));
    sig1 = sig1(I);
    
    % bin profiles to regular zVec
    for j = 1:length(zVec)
        I = find(Z>=zVec(j)-zbin/2 & Z<zVec(j)+zbin/2);
        TMat(j,i) = nanmean(CT(I));
        N2Mat(j,i) = nanmean(N2(I));
        O2Mat(j,i) = nanmean(O2(I));
        FlMat(j,i) = nanmean(Fl(I));
        TrMat(j,i) = nanmean(Tr(I));
        Sig1Mat(j,i) = nanmean(sig1(I));
    end
        
end

%% Here by-pass T for sigma_1
TMat = Sig1Mat;



%% Plot each 7 stations transects
% Now we assume that each series of 7 casts is taken at exact
% position.

xlon = lonVec(1:7);
xlat = latVec(1:7);
xVec = nan(size(xlon));
for i = 1:length(xlon)
    xVec(i) = m_lldist([xlon(1) xlon(i)], [xlat(1) xlat(i)]);
end
depthVec = zVecCTD(1:7);
%xVec = xVec - (2.15-.5); % adjust to model grid
xVec = xVec - (2.15-.7); % adjust to model grid


% Here adjust the grid
xMooring = m_lldist([xlon(1), -15.7975], [xlat(1), 55.4824]);
yMooring = 919;

% get bathym
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
xCWC = [CWC_bathy(:,1)+200]/1000; %km
%xCWC = [CWC_bathy(:,1)]/1000; %km
zCWC = abs(CWC_bathy(:,2));


ydepthVec = [zCWC];%;zCWC(end); zCWC(1); zCWC(1)];
xdepthVec = [xCWC];%; xCWC(end); xCWC(1); zCWC(1)];
% $$$ ydepthVec = [depthVec(1:6); yMooring; depthVec(7)];
% $$$ xdepthVec = [xVec(1:6); xMooring; xVec(7)];


rockall_24htransect_plotO2
disp('Other plots? ["return" for all]')
keyboard

rockall_24htransect_plotN
rockall_24htransect_plotT
rockall_24htransect_plotO2
rockall_24htransect_plotFl
rockall_24htransect_plotTr



%% Xtras O2 plot
figure(3)
clf
subplot(121)
plot(O2Mat(:,17), zVec, 'k', 'linewidth', 2)
hold on
plot(O2Mat(:,18), zVec, 'r', 'linewidth', 2)
plot(O2Mat(:,19), zVec, 'm', 'linewidth', 2)
legend('-400m','0m','+400m')
set(gca, 'ydir', 'reverse')
set(gca, 'xgrid','on')
set(gca, 'ygrid','on')
ylabel('Depth (m)', 'fontWeight', 'bold')
xlabel('[O_2] (\mu mol kg^{-1})')

subplot(122)
plot(TMat(:,17), zVec, 'k', 'linewidth', 2)
hold on
plot(TMat(:,18), zVec, 'r', 'linewidth', 2)
plot(TMat(:,19), zVec, 'm', 'linewidth', 2)
set(gca, 'ydir', 'reverse')
set(gca, 'yticklabel',[])
set(gca, 'xgrid','on')
set(gca, 'ygrid','on')
xlabel('T (^{\circ}C)')


%% get tidal amplitude
tide = load('~/research/CimaStuff/tide/tide_Oct_2012.mat');

mooringLat = 55.4824;
mooringLon = -15.7975;

lonVec = tide.lon_range;
latVec = tide.lat_range;
timeVec = tide.time;
origin = datenum(1950, 1, 1);
timeVec = origin + timeVec;

[Y, I] = min(abs(mooringLon-lonVec));
[Y, J] = min(abs(mooringLat-latVec));

etaVec = squeeze(tide.slev(J,I,:));
uVec = squeeze(tide.uu(J,I,:));
vVec = squeeze(tide.vv(J,I,:));
[U,V] = rotate_vecd(uVec,vVec,-30);

I = find(timeVec>=datenum(2012, 10, 16, 16, 27, 0) & timeVec<=datenum(2012, 10, 17, 20, 50, 0));
%I = find(timeVec>=datenum(2012, 10, 7, 16, 27, 0) & timeVec<=datenum(2012, 10, 17, 20, 50, 0));
etaVec = etaVec(I);
timeVec = timeVec(I);
U = U(I);
V = V(I);