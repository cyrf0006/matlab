clear all
close all
load O2_atlantic


lonLims = [-18 -12];
latLims = [54 56];

% Resize latlon
I = find(latVec>=latLims(1) & latVec<=latLims(2) & lonVec>=lonLims(1) & lonVec<=lonLims(2));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);

% cleaning (cleaned anyway after, but may speed up the process)
I = find(nanmean(o2Mat,1)>0);
disp(sprintf('%d profile(s) out of %d past test 1', length(I), length(latVec)));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);

I = find(nanmean(o2Mat,1)>150); % another "home-made" test (to remove few bad data)
disp(sprintf('%d profile(s) out of %d past test 2', length(I), length(latVec)));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);


% $$$ % Resize timing
% $$$ mm = str2num(datestr(timeVec,5));
% $$$ %I = find(mm>=5 & mm <=10);
% $$$ I = find(mm<4 | mm >9);
% $$$ %I = find(mm>=1);




%% average profile to force the model
sig_ave = nanmean(sigMat,2);
O2_ave = nanmean(o2Mat,2);
save o2_meanProfile_rockallthough.mat O2_ave sig_ave zVec