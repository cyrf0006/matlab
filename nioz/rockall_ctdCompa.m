clear all
close all
    
%% Few info on files and parameter
ctdList = '/home/cyrf0006/research/NIOZ/RockallBank/ctdFiles24h.list';
timeFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/timeFile24h.txt';
latFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/lat24h.txt';
lonFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/lon24h.txt';
depthFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD360/fred_processed/depth24h.txt';
g = 9.81;
griddataItp = false;
yMin = 00;


%% pre-process ctd info
ctdLat  = load(latFile);
ctdLon  = load(lonFile);
latVec = ctdLat(:,1) + ctdLat(:,2)/60;
lonVec = [ctdLon(:,1) + ctdLon(:,2)/60]*-1;
zVecCTD = load(depthFile);
zMax = max(zVecCTD);
zbin = 10;
zVec = zbin/2:zbin:zMax; % regular data storage


%% load CTD files
fid = fopen(ctdList);
C = textscan(fid, '%s', 'delimiter', '\n');
ctd = char(C{1});
noFiles = size(ctd,1);

% get time vector
fid = fopen(timeFile);
C = textscan(fid, '%s', 'delimiter', '\n');
ctdTime = char(C{1});
ctdTime = datenum(ctdTime);


N2Mat = nan(length(zVec), noFiles);
TMat = nan(length(zVec), noFiles);
O2Mat = nan(length(zVec), noFiles);
FlMat = nan(length(zVec), noFiles);
TrMat = nan(length(zVec), noFiles);
sig1Mat = nan(length(zVec), noFiles);

for i = 1:noFiles

    fname = ctd(i, :);
    I = find(fname==' ');   
    fname(I) = [];
    
    data = load(fname);
   
    Z = data(:, 1);
    T = data(:, 2);
    C = data(:, 3);
    O2 = data(:, 6);
    Fl = data(:, 5);
    Tr = data(:, 4);
    SP = data(:, 16);

    %    SP = gsw_SP_from_C(C,T,Z);
    SA = gsw_SA_from_SP(SP, Z,-13.8, 57.5);
    CT = gsw_CT_from_t(SA, T, Z);
    sig1 = gsw_sigma1(SA,CT);
    rho = gsw_rho(SA,CT,Z);

    % Isolate downcast
    dz = gradient(Z);
    I = find(dz>0.5*mean(abs(dz)));    
% $$$     [zm, I] = max(Z);
% $$$     I = 1:I;
    
    rhoSort = sort(rho(I));
    N2 = g./nanmean(rho(I)).*gradient(rhoSort)./gradient(Z(I));
    
    Z = Z(I);
    CT = CT(I);
    O2 = O2(I);
    Fl = Fl(I);
    Tr = Tr(I)-nanmean(Tr(I));
    sig1 = sig1(I);
    
    % bin profiles to regular zVec
    for j = 1:length(zVec)
        I = find(Z>=zVec(j)-zbin/2 & Z<zVec(j)+zbin/2);
        TMat(j,i) = nanmean(CT(I));
        N2Mat(j,i) = nanmean(N2(I));
        O2Mat(j,i) = nanmean(O2(I));
        FlMat(j,i) = nanmean(Fl(I));
        TrMat(j,i) = nanmean(Tr(I));
        sig1Mat(j,i) = nanmean(sig1(I));
    end
        
end

figure(1)
clf
plot(nanmean(O2Mat,2),zVec, 'linewidth', 2)
hold on
load deepCTD/deepCast.mat
plot(O2, P, 'r', 'linewidth', 2)
set(gca, 'ydir', 'reverse')


o2Vec = nanmean(O2Mat,2);
sig1Vec = nanmean(sig1Mat,2);


save CTD_rockall2012.mat o2Vec sig1Vec zVec


figure(2)
clf
plot(O2Mat(:), TMat(:), '.k')
xlabel('O2')
ylabel('T')