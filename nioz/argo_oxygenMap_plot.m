clear all
close all
load O2_atlantic


lonLims = [-27 -12];
latLims = [45 60];
% $$$ lonLims = [-20 -12];
% $$$ latLims = [50 57];
lonLims = [-18 -15];
latLims = [54 56];

% $$$ latLims = [45 60];
% $$$ lonLims = [-24 -10];
% $$$ 
% $$$ latLims = [45 70]; %full map
% $$$ lonLims = [-30 0];  
% $$$ 
% $$$ latLims = [53 60];
% $$$ lonLims = [-20 -8];

% Resize latlon
I = find(latVec>=latLims(1) & latVec<=latLims(2) & lonVec>=lonLims(1) & lonVec<=lonLims(2));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);

% cleaning (cleaned anyway after, but may speed up the process)
I = find(nanmean(o2Mat,1)>0);
disp(sprintf('%d profile(s) out of %d past test 1', length(I), length(latVec)));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);

I = find(nanmean(o2Mat,1)>150); % another "home-made" test (to remove few bad data)
disp(sprintf('%d profile(s) out of %d past test 2', length(I), length(latVec)));
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);
o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);


% Resize timing
mm = str2num(datestr(timeVec,5));
%I = find(mm>=5 & mm <=10);
I = find(mm<4 | mm >9);
%I = find(mm>=1);

o2Mat = o2Mat(:,I);
sigMat = sigMat(:,I);
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);

% Find the good depth
sigRef = 32;
o2Vec = nan(1,length(timeVec));
for i = 1:size(sigMat,2)
    [Y, I] = min(abs(sigMat(:,i)-sigRef));
    if Y < 0.5
        o2Vec(i) = o2Mat(I,i);
    end
end

% $$$ zRef = 550;
% $$$ 
% $$$ [Y, I] = min(abs(zVec-zRef));
% $$$ o2Vec = o2Mat(I,:);

% cleaning
I = find(~isnan(o2Vec) & o2Vec>0);
o2Vec = o2Vec(I);
latVec = latVec(I);
lonVec = lonVec(I);
timeVec = timeVec(I);

% on the new grid
latMin = latLims(1);
latMax = latLims(2);
lonMin = lonLims(1);
lonMax = lonLims(2);
x = lonMin:.01:lonMax;
y = latMin:.01:latMax;

[X,Y] = meshgrid(x,y);
[XI,YI,Z] = griddata(lonVec, latVec, o2Vec,X,Y);

%b=gaussfir(0.001, 5, 3);  %Gaussian filter designer ()
b=gaussfir(10);  %Gaussian filter designer ()
Z2=filter2(b,Z);  %Application of the filter



%% Bathymetry
% Get global bathym    
path = ['~/data/matlab_bathym/rockall.mat']; % 30-sec.
load(path)

% Lower res.
decimFactor = 40;
lat1m = lat(1:decimFactor:end);
lon1m = lon(1:decimFactor:end);
z1m = z(1:decimFactor:end, 1:decimFactor:end);

% Large map
latLims = [53 60];
lonLims = [-20 -8];

lon_min = lonLims(1);
lon_max = lonLims(2);
lat_min = latLims(1);
lat_max = latLims(2);

I=find(lat1m<lat_max & lat1m>lat_min);
J=find(lon1m<lon_max & lon1m>lon_min);
latitude=lat1m(I);
longitude=lon1m(J);
bathy=z1m(I,J);


%% plot map
close all
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.09; % very left of figure
rigs = 0.25; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
FS = 10;
V = 100:1:300;

figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 20 20])
m_proj('mercator','long',[min(longitude) max(longitude)],'lat',[min(latitude) max(latitude)]);
m_contourf(X,Y,Z, V, 'color', [.5 .5 .5]); 
hold on
[HH, HH] = m_contour(longitude,latitude,bathy, [0:-1000:-4000], 'color', [.5 .5 .5]*0);
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlab = xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold');
ylab = ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold');
m_line(-15.7975, 55.4824,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',10);

% argo casts
mm = str2num(datestr(timeVec,5));
I = find(mm>=5 & mm <=10);
J = find(mm<5 | mm >10);
m_plot(lonVec(I), latVec(I), 'mo')
m_plot(lonVec(J), latVec(J), 'o', 'color', [0 .5 .5])

%caxis([230 280])
%caxis([150 250])
caxis([185 195])
m_grid('box','fancy')
c= colorbar;
adjust_space

cb_pos = get(c, 'position');
cb_back = cb_pos;
cb_pos(2) = cb_pos(2)+.05; 
cb_pos(3) = cb_pos(3)*.75;
cb_pos(4) = cb_pos(4)-.1;  
set(c, 'pos', cb_pos);
set(c, 'fontsize', FS, 'fontweight', 'bold')

ti = ylabel(c,'[O_2] (\mumol kg^{-1})', 'FontSize', FS, 'fontweight', 'bold');
ti_pos = get(ti, 'position');
ti_pos(1) = 7.9;
set(ti, 'position', ti_pos)

ylab_pos = get(ylab, 'pos');
ylab_pos(1) = -0.22295;
set(ylab, 'position', ylab_pos)

set(gcf, 'renderer', 'painters'); % vectorial figure
print('-depsc', 'argo_O2map_toberename.eps')





% $$$ 
% $$$ 
% $$$ 
% $$$ 
% $$$ 
% $$$ figure(1)
% $$$ clf
% $$$ %set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 p])
% $$$ m_proj('mercator','long',[lonMin lonMax],'lat',[latMin latMax]);
% $$$ m_contourf(X,Y,Z,100, 'linestyle', 'none');
% $$$ hold on
% $$$ V=[0:-500:-3000];
% $$$ [HH, HH] = m_contour(longitude,latitude,bathy, V, 'color', 'k');
% $$$ m_gshhs_l('patch',[1 .9333 .6667]); %coastlines (Beige)                               
% $$$ xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold')
% $$$ ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold')
% $$$ caxis([100 350])
% $$$ colorbar
% $$$ m_grid('box','fancy')
% $$$ 
% $$$ 
% $$$ 
mm = str2num(datestr(timeVec,5));
I = find(mm>=5 & mm <=10);
J = find(mm<5 | mm >10);
m_plot(lonVec(I), latVec(I), 'mo')
m_plot(lonVec(J), latVec(J), 'o', 'color', [0 .5 .5])
% $$$ 
% $$$ m_line(-15.7975, 55.4824,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',14);
% $$$ %caxis([185 195])




% $$$ %% average profile to force the model
% $$$ sig_ave = nanmean(sigMat,2);
% $$$ O2_ave = nanmean(o2Mat,2);
% $$$ save o2_meanProfile_rockallthough.mat O2_ave sig_ave zVec