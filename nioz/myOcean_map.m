%function myOcean_map(ncFile, latLims, lonLims, Z, timeLims)

% function argo_getClim(ncFile, latLims, lonLims)
%
% usage ex:>> myOcean_O2('MetO-NWS-BIO-dm-Agg_1433237606185.nc', [45 70], [-20 0], 750,[datenum(2012,4,1) datenum(2012,10,1)])
clear all
close all

sigRef = 32;

ncFileBio = 'MetO-NWS-REAN-BIO-monthly_1433249807076.nc';
ncFilePhys = 'MetO-NWS-REAN-PHYS-monthly_1433249266432.nc';

disp('Loading data...')
latVec1 = nc_varget(ncFileBio, 'lat');
lonVec1 = nc_varget(ncFileBio, 'lon');
zVec1 =  nc_varget(ncFileBio, 'depth');
timeVec1 = nc_varget(ncFileBio, 'time');
timeVec1 = timeVec1/86400+datenum(1985,1,1);
O2box = nc_varget(ncFileBio, 'O2o');
NO3box = nc_varget(ncFileBio, 'N3n');

latVec2 = nc_varget(ncFilePhys, 'lat');
lonVec2 = nc_varget(ncFilePhys, 'lon');
zVec2 =  nc_varget(ncFilePhys, 'depth');
timeVec2 = nc_varget(ncFilePhys, 'time');
timeVec2 = timeVec2/86400+datenum(1985,1,1);
Tbox = nc_varget(ncFilePhys, 'votemper');
Sbox = nc_varget(ncFilePhys, 'vosaline');
disp('  -> done!')


% Restrict time
timeLims = [datenum(2011,4,1) datenum(2011,8,31)];
timeLims = [datenum(2008,1,1) datenum(2011,12,31)];

I = find(timeVec2>=timeLims(1) & timeVec2<=timeLims(2));
%I = find(timeVec<timeLims(1));
Tclim = squeeze(nanmean(Tbox(I,:,:,:)));
Sclim = squeeze(nanmean(Sbox(I,:,:,:)));
O2clim = squeeze(nanmean(O2box(I,:,:,:)));
NO3clim = squeeze(nanmean(NO3box(I,:,:,:)));

keyboard

% From T,S to sigma_1000
disp('Finding oxygen level for desired isopycnal,')
disp('may take some time...')
O2Map = nan(length(latVec2), length(lonVec2));
for in = 1:length(lonVec2)
    for im = 1:length(latVec2)        
        SA = gsw_SA_from_SP(Sclim(:,im,in), zVec2,-13.8, 57.5);
        T = Tclim(:,im,in);
        CT = gsw_CT_from_t(SA, T, zVec2);
        sig1 = gsw_sigma1(SA,CT);
        [Y,I] = min(abs(sig1-sigRef));
        O2Map(im, in) = O2clim(I,im, in);
    end
end
disp('  -> done!')

save tmp.mat


%% Plot map
myOcean_map_plot





%%Extra stuff

[Y, I] = min(abs(lonVec1-(-12)));
[Y, J] = min(abs(latVec1-55));

NO3prof = squeeze(NO3clim(:,J,I));
O2prof = squeeze(O2clim(:,J,I));

figure(3)
clf
patch( [190 250 250 190 190], [800 800 1200 1200 800], [.7 .7 .7])
hold on
[ax, h1, h2] = plotxx(O2prof, zVec1, NO3prof, zVec1)
set(ax(1), 'ydir', 'reverse')
set(ax(2), 'ydir', 'reverse')
set(ax(2), 'yticklabel', [])
set(ax(2), 'yticklabel', [])
set(h1, 'linewidth', 3)
set(h2, 'linewidth', 3)
set(ax, 'fontSize', 14, 'fontWeight', 'bold' )

ylabel(ax(1), 'Depth (m)', 'fontSize', 16, 'fontWeight', 'bold')
xlabel(ax(1), 'O_2 (\mumol kg^{-1})', 'fontSize', 16, 'fontWeight', 'bold')
xlabel(ax(2), 'NO_3 (\mumol kg^{-1})', 'fontSize', 16, 'fontWeight', 'bold')

save O2_NO3_clim.mat O2prof NO3prof zVec1