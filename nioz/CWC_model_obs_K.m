clear all
close all


%% some info on the grid
load time.out;
load grid.mat

% snapshot info 
% $$$ XLIM = [-4e4 2e4];
% $$$ ZLIM = [0 1600];
%XLIM = [-2.783e3 1.717e3];
XLIM = [-1.65 2.66647];
XLIM = [-3 4];
%ZLIM = [300 959];
ZLIM = [300 1000];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [450 550];
% $$$ XLIM = [-2.85e4 -2.6e4];
% $$$ ZLIM = [510 545];
% Get data from this frame
timeVec = time(:,3);

cycle = 0;
t1 = datenum(2000, 1, 1, 3, 0, 0)+cycle;
t2 = datenum(2000, 1, 1, 7, 0, 0)+cycle;
t3 = datenum(2000, 1, 1, 12, 0, 0)+cycle;
t4 = datenum(2000, 1, 1, 17, 0, 0)+cycle;
t5 = datenum(2000, 1, 1, 22, 0, 0)+cycle;
t6 = datenum(2000, 1, 2, 3, 0, 0)+cycle;

% $$$ t1 = datenum(2000, 1, 7, 3, 0, 0);
% $$$ t2 = datenum(2000, 1, 7, 7, 0, 0);
% $$$ t3 = datenum(2000, 1, 7, 12, 0, 0);
% $$$ t4 = datenum(2000, 1, 7, 17, 0, 0);
% $$$ t5 = datenum(2000, 1, 7, 22, 0, 0);
% $$$ t6 = datenum(2000, 1, 8, 3, 0, 0);
[Y, frameNos(1)] = min(abs(timeVec-t1));
[Y, frameNos(2)] = min(abs(timeVec-t2));
[Y, frameNos(3)] = min(abs(timeVec-t3));
[Y, frameNos(4)] = min(abs(timeVec-t4));
[Y, frameNos(5)] = min(abs(timeVec-t5));
[Y, frameNos(6)] = min(abs(timeVec-t6));


% get bathym
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC.dat');
xCWC = [CWC_bathy(:,1)+200]/1000; %km
zCWC = abs(CWC_bathy(:,2));
ydepthVec = [zCWC];
xdepthVec = [xCWC];
xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];
 

figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
colormap(jet)

letterID = ['a', 'b', 'c', 'd', 'e', 'f'];
Vsig = 31:.02:32;
Vo2 = -5:.1:5;
x = (x)/1000;
chi = chi/1000;
for i = 1:6


sig = getfield('sig',frameNos(i));
sig = putNaN(sig,flag_s);
sig = sig';
% $$$ c1 = getfield('C01',frameNos(i));
% $$$ c1 = putNaN(c1,flag_s);
% $$$ c1 = c1';    
% $$$ c2 = getfield('C02',frameNos(i));
% $$$ c2 = putNaN(c2,flag_s);
% $$$ c2 = c2';   
u = getfield('u',frameNos(i));
u = putNaN(u,flag_u);
u = u';   
w = getfield('w',frameNos(i));
w = putNaN(w,flag_s);
w = w'; 
K = getfield('KH',frameNos(i));
K = putNaN(K,flag_s);
K = K'; 

% reduce the grid
I = find(x>=XLIM(1) & x<=XLIM(2));
J = find(z>=ZLIM(1) & z<=ZLIM(2));
II = find(chi>=XLIM(1) & chi<=XLIM(2));
JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
xVec = x(I);
zVec = z(J);
chiVec = chi(II);
zetaVec = zeta(JJ);
% $$$ c1 = c1(J,II);
% $$$ c2 = c2(J,II);
sig = sig(J,II);
w = w(J,I);
u = u(J,I);
K = K(J,II);

quiverDecX = 1;
quiverDecZ = 10;
subplot(3,2,i)

contourf(chiVec, zVec, log10(K), Vo2, 'linestyle', 'none')
%shading flat
set(gca, 'ydir', 'reverse')
hold on
contour(chiVec, zVec, sig, Vsig, 'color', 'k')
patch(xpatch, ypatch, [1 1 1]*.6)   

Z = zVec(1:quiverDecZ:end);
X = xVec(1:quiverDecX:end);
U = u(1:quiverDecZ:end, 1:quiverDecX:end);
W = -w(1:quiverDecZ:end, 1:quiverDecX:end);


[kmax imax] = size(U);
vecColor= 'm';
unit = false;
scale = .25;

[Y,I] = min(abs(X-1.25));

% $$$ for i = 1:imax-1
for k = 1:kmax-1
    arrow7(X(I),Z(k),U(k,I),W(k,I)*0,scale,vecColor,unit);
end

if i == 6
    arrow7(1.25,920,.25,0,scale,vecColor,unit);
    text(1.1,850,'0.25 ms^{-1}', 'color', 'm', 'fontWeight', 'bold')
end
%max(max(abs(u)))

% $$$ end
%colorbar
%caxis([-1 .5])
%title(datestr(frameTime, 0))
xlim(XLIM)
ylim(ZLIM)
%xlabel('x (km)')
%ylabel('z (m)')

% $$$ keyboard
% $$$ XTICKLABEL = get(gca, 'xTickLabel');
% $$$ XTICKLABEL = str2num(XTICKLABEL)./1000;
% $$$ set(gca, 'xTickLabel', XTICKLABEL);

% $$$ outFile = sprintf('frame%0.4d', i);
% $$$ print(gcf, '-dpng', '-r150', outFile)
if mod(i,2) == 0
    set(gca, 'yticklabel', [])
end

if i < 5
        set(gca, 'xticklabel', [])
end
if i == 6
    cb = colorbar;
    adjust_space
    cbPos = get(cb, 'pos');
    figPos = get(gca, 'pos');
    cbPos(1) = figPos(1)+figPos(3)+.01;
    cbPos(2) = cbPos(2)+.01;
    cbPos(4) = cbPos(4)-2*.01;
    cbPos(3) = cbPos(3)*.7;
    set(cb, 'pos', cbPos)
    ti = ylabel(cb,'[O_2] (umol/Kg)', 'FontSize', 10, 'fontweight', 'bold');
    tiPos = get(ti, 'pos');
    set(ti, 'rotation', 90)
    %        tiPos = [-1 -4.2 1];
    set(ti, 'pos', tiPos)
else
    adjust_space
end

if i == 5 | i == 6
    xlabel('x (km)', 'fontWeight', 'bold')
end

if i == 3
    ylabel('Depth (m)', 'fontWeight', 'bold')
end

% $$$ if i == 1
% $$$     text(0.4, 750, 'Hass mound', 'fontWeight', 'bold')        
% $$$ end
text(-1.2, 900, letterID(i), 'fontWeight', 'bold', 'fontSize', 14)        
set(gca, 'ytick', [300:100:900])

end

set(gcf, 'renderer', 'painters')
print('-depsc2', './ctd_transectO2_model_K.eps')  



% $$$ 
% $$$ 
% $$$ figure(2)
% $$$ clf
% $$$ c2 = getfield('C02',1);
% $$$ c2 = putNaN(c2,flag_s);
% $$$ c2 = c2';    
% $$$ II = find(chi>=XLIM(1) & chi<=XLIM(2));
% $$$ o2_mean1 = nanmean(c2(:,II),2);
% $$$ c2 = getfield('C02',length(timeVec)-2);
% $$$ c2 = putNaN(c2,flag_s);
% $$$ c2 = c2';    
% $$$ II = find(chi>=XLIM(1) & chi<=XLIM(2));
% $$$ o2_mean2 = nanmean(c2(:,II),2);
% $$$ 
% $$$ plot(o2_mean1, z, 'k')
% $$$ hold on
% $$$ plot(o2_mean2, z, '--r')
% $$$ ylim([0 1000])