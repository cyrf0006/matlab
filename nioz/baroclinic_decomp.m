function baroclinic_decomp(timeVec, zVec, U, V, cutOff)


% cutOff: filter time in minutes

    
Uiw = nan(size(U));
Viw = nan(size(V));
Ubc = nan(size(U));
Vbc = nan(size(V));

% remove barotropic
Ubt = nanmean(U,1);
Vbt = nanmean(V,1);
for i = 1:size(U,1)
    Ubc(i,:) = U(i,:) - Ubt;
    Vbc(i,:) = V(i,:) - Vbt;
end

% Apply the Filter ...
%  ... on velocities
dt = (timeVec(2)-timeVec(1));
fs = 1/(dt*86400); %high pass
freq = 1/(cutOff*60); %Hz
Wn = freq/(fs/2);
[b,a] = butter(6, Wn, 'high');
%[b,a] = cheby1(6, 0.5,  Wn, 'high');
for i = 1:size(Ubc, 1)
    
    % remove NaNs
    vec = Ubc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    if length(vec)<2
        continue
    end
    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Uiw(i, I) = filtfilt(b, a, vec(I));
    end
    
    vec = Vbc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Viw(i, I) = filtfilt(b, a, vec(I));
    end
end

%% subinertial
Usub = U - Uiw;
Vsub = V - Viw;


%% plot
disp('plotting...')
% -------- Plotting ------------ %
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 4; % no. subplot row
dx = 0.01 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 16 15])   


subplot(4, 1, 1)
contourf(timeVec, zVec, Usub, 100, 'linestyle', 'none')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'reverse')
colorbar
caxis([-.15 .15])
adjust_space

subplot(4, 1, 2)
contourf(timeVec, zVec, Vsub, 100, 'linestyle', 'none')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'reverse')
colorbar
caxis([-.15 .15])
adjust_space

subplot(4, 1, 3)
contourf(timeVec, zVec, Uiw, 100, 'linestyle', 'none')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'reverse')
colorbar
caxis([-.1 .1])
caxis([-.04 .04])
adjust_space


subplot(4, 1, 4)
contourf(timeVec, zVec, Viw, 100, 'linestyle', 'none')
datetick('x', 15)
set(gca, 'ydir', 'reverse')
xlim([timeVec(1) timeVec(end)])
colorbar
caxis([-.1 .1])
caxis([-.04 .04])
adjust_space



% Apply the Filter ...
%  ... on velocities
dt = (timeVec(2)-timeVec(1));
fs = 1/(dt*86400); %high pass
freq = 1/(cutOff*60); %Hz
Wn = freq/(fs/2);
[b,a] = butter(6, Wn, 'high');
[b,a] = cheby1(6, 0.5,  Wn, 'high');
for i = 1:size(Ubc, 1)
    
    % remove NaNs
    vec = Ubc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    if length(vec)<2
        continue
    end
    
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Uiw(i, I) = filtfilt(b, a, vec(I));
    end
    
    vec = Vbc(i,:);
    I = find(isnan(vec)==1);
    xitp = 1:length(vec);
    x = xitp;
    vec(I) = [];
    x(I) = [];
    vec = interp1(x, vec, xitp);  
    I = find(~isnan(vec)==1);
    if length(I)>6*3
        Viw(i, I) = filtfilt(b, a, vec(I));
    end
end

%% subinertial
Usub = U - Uiw;
Vsub = V - Viw;






%% Extra high freq.
I = find(timeVec>=datenum(2010,3,6,0,0,0) & timeVec<=datenum(2010,3,6,6,0,0));
timeVec = timeVec(I);
Uiw = Uiw(:,I);
Viw = Viw(:,I);

% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 2; % no. subplot row
dx = 0.01 ; % horiz. space between subplots
dy = 0.03; % vert. space between subplots
lefs = 0.08; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %

figure(2)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 16 15])   


subplot(2, 1, 1)
contourf(timeVec, zVec, Uiw, 100, 'linestyle', 'none')
set(gca, 'xticklabel', [])
set(gca, 'ydir', 'reverse')
ylabel('Depth (m)')
c= colorbar;
ti = ylabel(c,'Uiw', 'FontSize', 10);
caxis([-.05 .05])
adjust_space

subplot(2, 1, 2)
contourf(timeVec, zVec, Viw, 100, 'linestyle', 'none')
set(gca, 'ydir', 'reverse')
c = colorbar;
ti = ylabel(c,'Viw', 'FontSize', 10);
caxis([-.05 .05])
datetick('x', 15)
ylabel('Depth (m)')
xlabel('6 March 2010')
xlim([timeVec(1) timeVec(end)])
adjust_space


keyboard

load ../Titp_T1C_p2std_1minAve.mat
myT = Tbin(1:10:end,:);
myZ = zVec(1:10:end,:);
myZ = myZ+max(zVec);
I = find(timeVec>=datenum(2010,3,6,0,0,0) & timeVec<=datenum(2010,3,6,6,0,0));
myt = timeVec(I);
myT = myT(:,I);
hold on
for i = 1:length(myZ)
    plot(myt, (myT(i,:)*100+myZ(i)), 'k')
end
