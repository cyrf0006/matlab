function myOcean_O2(ncFile, latLims, lonLims, Z, timeLims)

% function argo_getClim(ncFile, latLims, lonLims)
%
% usage ex:>> myOcean_O2('MetO-NWS-BIO-dm-Agg_1433237606185.nc', [45 70], [-20 0], 750,[datenum(2012,4,1) datenum(2012,10,1)])
 

    
latVec = nc_varget(ncFile, 'lat');
lonVec = nc_varget(ncFile, 'lon');
zVec =  nc_varget(ncFile, 'depth');
timeVec = nc_varget(ncFile, 'time');
timeVec = timeVec/86400+datenum(2011,4,7);
O2box = nc_varget(ncFile, 'O2o');


[Y,I] = min(abs(zVec-Z));
field = squeeze(O2box(:,I,:,:));


I = find(lonVec>=lonLims(1) & lonVec<=lonLims(2));
J = find(latVec>=latLims(1) & latVec<=latLims(2));
K = find(timeVec>=timeLims(1) & timeVec<=timeLims(2));
K = find(timeVec<timeLims(1));
field = squeeze(nanmean(field(K,J,I)));


figure(1)
clf
imagesc(lonVec, latVec, field)
set(gca, 'ydir', 'normal')
caxis([200 250])
hold on
plot(-15.7975, 55.4824,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',14);

keyboard


