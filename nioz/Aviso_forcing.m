clear all
close all


%% plotting info
%figure dimension
paperwidth = 20;%cm
paperheight = 25;%cm
FS = 15;

% param for colorbar
cbar_width = 0.02;
cbar_offset = 0.01; % colorbar offset from figure
offset2 = 0.1; 

% time of the tide
t0 = datenum(2012,10,17,03,0,0);

%% Get global bathym    
path = ['~/data/matlab_bathym/rockall.mat']; % 30-sec.
lims = [40 70 -35 10];
fid = fopen(path);
if fid == -1 % doesnt exist yet!
    disp('30-sec. region doesn''t exist yet!')
    disp(' Extracting from GEBCO, may take some time...')
    [lat lon z] = getGebco('~/data/GEBCO/GEBCO_08.nc', 'z', lims);
    disp('   -> done!')
    save(path, 'lat', 'lon', 'z');
else
    load(path)
end
% Lower res.
%decimFactor = 4;
decimFactor = 40;
lat1m = lat(1:decimFactor:end);
lon1m = lon(1:decimFactor:end);
z1m = z(1:decimFactor:end, 1:decimFactor:end);

lon_min=-20;
lon_max=-11;
lat_min=54;
lat_max=59;

I=find(lat<lat_max & lat>lat_min);
J=find(lon<lon_max & lon>lon_min);
latitude=lat(I);
longitude=lon(J);
bathy=z(I,J);

%% AVISO tides
tide = load('~/research/CimaStuff/tide/tide_Oct_2012.mat');
lonVecTide = tide.lon_range;
latVecTide = tide.lat_range;
timeVecTide = tide.time;
%etaTide = tide.slev;
origin = datenum(1950, 1, 1);
timeVecTide = origin + timeVecTide;

[Y, I] = min(abs(timeVecTide-(t0-.5)));
time1 = timeVecTide(I);
etaMat = squeeze(tide.slev(:,:,I));
anom = nan(size(etaMat));

for i = 9:size(etaMat,1)-9
    for j = 9:size(etaMat,2)-9
        anom(i,j) = etaMat(i,j) - nanmean(nanmean(etaMat(i-8:i+8, j-8:j+8)));
    end
end



%% ------- Plot main figure ------ %%
%h = figure('visible', 'off');
figure(1);
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 paperwidth paperheight])

m_proj('mercator','long',[min(longitude) max(longitude)],'lat',[min(latitude) max(latitude)]);

V=[0:100:4000];
m_contourf(lonVecTide,latVecTide,anom, 50, 'lineStyle', 'none');
hold on
[HH, HH] = m_contour(longitude,latitude,abs(bathy), [0:500:2500], 'color', 'k');
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               
xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold')
set(gca, 'fontsize', FS)
caxis([-1 1])    
m_grid('box','fancy')
c = colorbar('location','southoutside');

% Rectangle around study region (zoom++ )
lon_min=-15.85;
lon_max=-15.6;
lat_min=55.45;
lat_max=55.55;
[lomi, lami] = m_ll2xy(lon_min,lat_min);
[loma, lama] = m_ll2xy(lon_max,lat_max);
rectangle('Position', [lomi lami loma-lomi lama-lami],'linewidth', 1, 'edgecolor', 'r') 


%% deta/dy deta/dx
% draw lines
lonLimsy = [-15 -13];
latLimsy = [55.8 57.5];
m_line(lonLimsy, latLimsy, 'color', 'm', 'linestyle', '--')
lonLimsx = [-16.5 -15];
latLimsx = [56 55.25];
m_line(lonLimsx, latLimsx, 'color', 'm', 'linestyle', '--')


[range, lony, laty] = m_lldist(lonLimsy, latLimsy, 500);
range = range*1000;
drangey = range./length(lony);
range = 0:drangey:range;
rangey = range(1:length(lony));

[range, lonx, latx] = m_lldist(lonLimsx, latLimsx, 500);
range = range*1000;
drangex = range./length(lonx);
range = 0:drangex:range;
rangex = range(1:length(lonx));

[X, Y] = meshgrid(lonVecTide, latVecTide);
X = X(:);
Y = Y(:);

index_y = nan(size(rangey));
for i = 1:length(rangey)
    [val, I] = min(abs(lony(i)-X) + abs(laty(i)-Y));
    index_y(i) = I;
end   
index_x = nan(size(rangex));
for i = 1:length(rangex)
    [val, I] = min(abs(lonx(i)-X) + abs(latx(i)-Y));
    index_x(i) = I;
end    


detady = nan(1,length(timeVecTide));
detadx = nan(1,length(timeVecTide));
for iFrame = 1:200
    disp(sprintf('Frame %d / %d', iFrame, length(timeVecTide)))
    etaMat = squeeze(tide.slev(:,:,iFrame))/100;
    anom = nan(size(etaMat));
   
    for i = 9:size(etaMat,1)-9
        for j = 9:size(etaMat,2)-9
            anom(i,j) = etaMat(i,j) - nanmean(nanmean(etaMat(i-8:i+8, j-8:j+8)));
        end
    end
    %keyboard
% $$$     index = nan(size(rangey));
% $$$     for i = 1:length(rangey)
% $$$         [val, I] = min(abs(lony(i)-X) + abs(laty(i)-Y));
% $$$         index(i) = I;
% $$$     end    
    %    detady(iFrame) = max(gradient(anom(index),1,1)./drangey);
    %    detady(iFrame) = max(abs(gradient(anom(index_y)./drangey)));    
    
    [Ymax, Imax] = nanmax(anom(index_y));
    [Ymin, Imin] = nanmin(anom(index_y));
    detady(iFrame) = (Ymax-Ymin)/abs(rangey(Imin) - rangey(Imax));

    [Ymax, Imax] = nanmax(anom(index_x));
    [Ymin, Imin] = nanmin(anom(index_x));
    detadx(iFrame) = (Ymax-Ymin)/abs(rangex(Imin) - rangex(Imax));
    
% $$$     index = nan(size(rangex));
% $$$     for i = 1:length(rangex)
% $$$         [val, I] = min(abs(lonx(i)-X) + abs(latx(i)-Y));
% $$$         index(i) = I;
% $$$     end    
    %    detadx(iFrame) = max(gradient(anom(index),1,1)./drangex);    
    %    detadx(iFrame) = max(abs(gradient(anom(index_x)./drangex)));    

end

figure(2)
clf
plot(timeVecTide(1:200), detady(1:200))
datetick
ylabel('d\eta / dy,dx', 'fontSize', 12, 'fontWeight', 'bold')
hold on
plot(timeVecTide(1:200), detadx(1:200),'r')
legend('deta/dy', 'deta/dz')
xlabel('date', 'fontSize', 12, 'fontWeight', 'bold')
set(gca, 'fontSize', 12, 'fontWeight', 'bold')