function argo_oxygenMap(fileList, latLims, lonLims)

% function argo_getClim(ncFile, latLims, lonLims)
%
% usage ex:>> argo_oxygenMap('oxyArgo.list', [45 70], [-30 0])
% usage ex:>> argo_oxygenMap('oxyArgo.list', [45 70], [-30 0])

% Open list
fid = fopen(fileList);
C = textscan(fid, '%s', 'delimiter', '\n');
argoFiles = char(C{1});
noFiles = size(argoFiles, 1);

% raw matrix to fill
dz = 10; %m
zVec = [dz/2:dz:2100]';
sigMat = nan(length(zVec), noFiles);
o2Mat = nan(length(zVec), noFiles);
latVec = nan(1,noFiles);
lonVec = nan(1,noFiles);
timeVec = nan(1,noFiles);


%% loop on profiles
for i = 1:noFiles

    if i >= 193 & i<=200
        continue
    end
    
    ncFile = argoFiles(i, :);
    I = find(ncFile==' ');   
    ncFile(I) = [];

    disp(sprintf('process %s (file %d / %d)',ncFile,  i, noFiles))

    % Check what is molar_doxy_adjusted, etc.   < ---------------
    try out = nc_varget(ncFile, 'molar_doxy'); end

    if exist('out')
        O2 = out;
    else
        try out = nc_varget(ncFile, 'doxy'); end
        
        if exist('out')
            O2 = out; 
        else
            disp('Oxygen not found')
            continue
        end
    end
    clear out
    

    T = nc_varget(ncFile, 'temp');
    SP = nc_varget(ncFile, 'psal');
    P = nc_varget(ncFile, 'pres'); 
    if min(size(P)) ~= 1
        P = P(2,:)';
        T = T(2,:)';
        SP = SP(2,:)';
        O2 = O2(2,:)';
    end

    latitude = nc_varget(ncFile, 'latitude');
    longitude = nc_varget(ncFile, 'longitude');
    julDay =  nc_varget(ncFile, 'juld');
    origin = datenum(1950, 1, 1);
    mtime = origin + julDay;
    
    if max(size(latitude)) ~= 1
        latitude = latitude(1);
        longitude = longitude(1);
        mtime = mtime(1);
    end
    
    % make sure unique P
    [P, I] = unique(P);
    SP = SP(I);
    T = T(I);
    O2 = O2(I);
    
    % gsw
    SA = gsw_SA_from_SP(SP, P,longitude, latitude);
    CT = gsw_CT_from_t(SA, T, P);
    sig1 = gsw_sigma1(SA,CT);
    rho = gsw_rho(SA,CT,P);

    % interpolation to regular zVec    
    I = find(~isnan(sig1));
    if length(I) < 2
        sigItp = nan(size(zVec));
    else
        sigItp = interp1(P(I), sig1(I), zVec);
    end

    I = find(~isnan(O2));
    if length(I) < 2
        o2Itp = nan(size(zVec));
    else
        o2Itp = interp1(P(I), O2(I), zVec);
    end
    
    sigMat(:,i) = sigItp;
    o2Mat(:,i) = o2Itp;
    latVec(i) = latitude;
    lonVec(i) = longitude;
    timeVec(i) = mtime;
    
end

sigRef = 32;
o2Vec = nan(1,length(timeVec));
for i = 1:size(sigMat,2)
    [Y, I] = min(abs(sigMat(:,i)-sigRef));
    if Y < 0.5
        o2Vec(i) = o2Mat(I,i);
    end
end
    
save O2_atlantic

keyboard


argo_oxygenMap_plot