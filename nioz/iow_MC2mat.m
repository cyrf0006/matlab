function iow_cats2mat(configFile)

% usage ex:
% iow_cats2mat('T1A.config')
    
%% Open and read config file
fid = fopen(configFile);
tline = fgetl(fid);
while ischar(tline)
  eval(tline);
  tline = fgetl(fid);
end
fclose(fid);    

%% load list file
fid = fopen(listMC);
C = textscan(fid, '%s', 'delimiter', '\n');
MCfiles = char(C{1});
noFiles = size(MCfiles, 1); 

if noFiles ~= length(habMC)
    disp('Number of files does not match depth vector size (quit)')
    return
end

%% Loop on microCats files
TMat = [];
SMat = [];
sigMat = [];
for i = 1:noFiles
    
    fname = MCfiles(i, :);
    I = find(fname==' '); %remove white space  
    fname(I) = [];
    disp(fname);
    
    data = dlmread(fname,'\t',1,0); 
    TVec = data(:,1);
    SVec = data(:,3);
    timeSensor = datenum(data(:,5),data(:,6),data(:,7),data(:,8),data(:,9),data(:,10));
    
    for itime = 1:length(timeVec)
        I = find(timeSensor>=timeVec(itime)-dt/2 & timeSensor<=timeVec(itime)+dt/2);
        Titp(itime) = nanmean(TVec(I));
        Sitp(itime) = nanmean(SVec(I));
    end
    
    SA = gsw_SA_from_SP(Sitp,0,lon,lat);
    CT = gsw_CT_from_pt(SA,Titp);
    sigma0 = gsw_sigma0(SA,CT);

    TMat = [TMat; CT];
    SMat = [SMat; SA];
    sigMat = [sigMat; sigma0];

end

%% Quick plot of the result    
figure(1)
clf
pcolor(timeVec, habMC, sigMat); shading flat
datetick
ylabel('mab')


habVec = habMC;
zVec = totalDepth - habVec;
save(outFile, 'TMat', 'SMat', 'sigMat', 'timeVec', 'zVec', 'habVec')