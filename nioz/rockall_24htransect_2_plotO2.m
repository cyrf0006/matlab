%% Plot N2
V = 5:.1:20;
figure(2)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
load ~/git/matlab/colormaps/oxygen_discrete.mat
colormap(oxygen_discrete)

letterID = ['a', 'b', 'c', 'd', 'e', 'f'];
Vsig = 31:.02:32;
Vo2 = 200:1:240;
for i = 1:5
    s = subplot(3,2,i);
    
    I = [1:6]+(i-1)*6;
    
    contourf(xVec, zVec, O2Mat(:,I), Vo2, 'linestyle', 'none')
    ylim(ZLIM)
    hold on
    contour(xVec, zVec, TMat(:,I), Vsig, 'color', 'k');  
    
    if mod(i,2) == 0 
        set(gca, 'yticklabel', [])
    end
    
    for ii = 1:length(xVec)
        plot([xVec(ii) xVec(ii)], ZLIM, '--k')
    end
    
    xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
    ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];
    
    patch(xpatch, ypatch, [1 1 1]*.6)      
    hold off
    
    % put mooring position
    line(xMooring, yMooring,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',10)
    
    set(gca, 'ydir', 'reverse')
    set(gca, 'tickdir', 'out')
% $$$     caxis([200 240])
    caxis([205 235])
    %    caxis([205 250])

    drawnow
    if i < 5
        set(gca, 'xticklabel', [])
    end

    if i == 6
        cb = colorbar;
        adjust_space
        cbPos = get(cb, 'pos');
        figPos = get(gca, 'pos');
        cbPos(1) = figPos(1)+figPos(3)+.01;
        cbPos(2) = cbPos(2)+.01;
        cbPos(4) = cbPos(4)-2*.01;
        cbPos(3) = cbPos(3)*.7;
        set(cb, 'pos', cbPos)
        ti = ylabel(cb,'[O_2] (umol/Kg)', 'FontSize', 10, 'fontweight', 'bold');
        tiPos = get(ti, 'pos');
        set(ti, 'rotation', 90)
        set(ti, 'pos', tiPos)
    else
        adjust_space
    end
    
    if i == 5 | i == 6
        xlabel('x (km)', 'fontWeight', 'bold')
    end
    
    if i == 3
        ylabel('Depth (m)', 'fontWeight', 'bold')
    end
    
% $$$     if i == 1
% $$$         text(0.4, 750, 'Hass mound', 'fontWeight', 'bold')        
% $$$     end
    text(-1.2, 900, letterID(i), 'fontWeight', 'bold', 'fontSize', 14)        
    %xlim(XLIM)
    ylim(YLIM)
   
end
 

set(gcf, 'renderer', 'painters')
print('-depsc2', './ctd_transectO2_2.eps')  

