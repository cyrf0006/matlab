function [P,T,S] = argo_getClim(ncFile, lat, lon)

% function argo_getClim(ncFile, latLims, lonLims)
%
% usage ex: argo_getClim('RG_ArgoClim_33pfit_2015_mean.nc', 55, 348)    

    
latitude = nc_varget(ncFile, 'LATITUDE');
longitude = nc_varget(ncFile, 'LONGITUDE');
P =  nc_varget(ncFile, 'PRESSURE');

% $$$ I = find(longitude>=lonLims(1) & longitude<=lonLims(2));
% $$$ J = find(latitude>=latLims(1) & latitude<=latLims(2));
% $$$ T = nc_varget(ncFile, 'ARGO_TEMPERATURE_MEAN', [I(1) J(1) 0], [length(I), length(J) length(P)]);
% $$$ S = 

T = nc_varget(ncFile, 'ARGO_TEMPERATURE_MEAN');  
S = nc_varget(ncFile, 'ARGO_SALINITY_MEAN');  

[Y, I] = min(abs(longitude-348));
[Y, J] = min(abs(latitude-55));

T = squeeze(T(:,J,I));
S = squeeze(S(:,J,I));
[SA, in_ocean] = gsw_SA_from_SP(S,P,-15,55);
CT = gsw_CT_from_t(SA,T,P);
sigma1 = gsw_sigma1(SA,CT);
       
