clear all
close all

XLIM = [-1.45 2.8665]-.5;
XLIM = [-1.5 1.5];
ZLIM = [300 1000];
%%%%%%%%%%  ---- Model results ---- %%%%%%%%

load time.out;
load grid.mat
%timeVec = time(:,3);
timeVec = time(:,3);

cycle = 3;
% $$$ t1 = datenum(2000, 1, 1, 3, 0, 0)+cycle;
% $$$ t2 = datenum(2000, 1, 1, 7, 0, 0)+cycle;
% $$$ t3 = datenum(2000, 1, 1, 12, 0, 0)+cycle;
% $$$ t4 = datenum(2000, 1, 1, 17, 0, 0)+cycle;
% $$$ t5 = datenum(2000, 1, 1, 22, 0, 0)+cycle;
% $$$ t6 = datenum(2000, 1, 2, 3, 0, 0)+cycle;

t1 = datenum(2000, 1, 1, 3, 0, 0)+cycle;
t2 = datenum(2000, 1, 1, 6, 0, 0)+cycle;   % <---
t3 = datenum(2000, 1, 1, 12, 0, 0)+cycle;  % <--- 
t4 = datenum(2000, 1, 1, 17, 0, 0)+cycle;
t5 = datenum(2000, 1, 1, 18, 0, 0)+cycle;  % <---
t6 = datenum(2000, 1, 2, 3, 0, 0)+cycle;

[Y, frameNos(1)] = min(abs(timeVec-t1));
[Y, frameNos(2)] = min(abs(timeVec-t2));
[Y, frameNos(3)] = min(abs(timeVec-t3));
[Y, frameNos(4)] = min(abs(timeVec-t4));
[Y, frameNos(5)] = min(abs(timeVec-t5));
[Y, frameNos(6)] = min(abs(timeVec-t6));

% get bathym
CWC_bathy = load('~/research/NIOZ/RockallBank/bathymetry/SmoothRockalBathymWithCWC_CWC_2.dat');
xCWC = [CWC_bathy(:,1)+200]/1000; %km
zCWC = abs(CWC_bathy(:,2));
ydepthVec = [zCWC];
xdepthVec = [xCWC];
xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];
 

%%%%%% ---- CTD obs ---- %%%%%%%
ctdList = '/home/cyrf0006/research/NIOZ/RockallBank/CTD2013/CTD2013_transect.list';
timeFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD2013/timeCTD2013_transect.txt';
latFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD2013/latCTD2013_transect.txt';
lonFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD2013/lonCTD2013_transect.txt';
% $$$ depthFile = '/home/cyrf0006/research/NIOZ/RockallBank/CTD2013/
g = 9.81;
griddataItp = false;
yMin = 0;

%% pre-process ctd info
ctdLat  = load(latFile);
ctdLon  = load(lonFile);
latVec = ctdLat(:,1) + ctdLat(:,2)/60;
lonVec = [ctdLon(:,1) + ctdLon(:,2)/60]*-1;
%zVecCTD = load(depthFile);
%Max = max(zVecCTD);
zMax = ZLIM(2);
zbin = 10;
zVec = zbin/2:zbin:zMax; % regular data storage


%% load CTD files
fid = fopen(ctdList);
C = textscan(fid, '%s', 'delimiter', '\n');
ctd = char(C{1});
noFiles = size(ctd,1);

% get time vector
fid = fopen(timeFile);
C = textscan(fid, '%s', 'delimiter', '\n');
ctdTime = char(C{1});
ctdTime = datenum(ctdTime);

O2Mat = nan(length(zVec), noFiles);
Sig1Mat = nan(length(zVec), noFiles);

for i = 1:noFiles

    fname = ctd(i, :);
    I = find(fname==' ');   
    fname(I) = [];
    
    data = load(fname);
   
    Z = data(:, 1); %(see note book 23 june)
    T = data(:, 3);
    C = data(:, 1);
    O2 = data(:, 5);
    SP = data(:, 12);    

    %    SP = gsw_SP_from_C(C,T,Z);
    SA = gsw_SA_from_SP(SP, Z,-13.8, 57.5);
    CT = gsw_CT_from_t(SA, T, Z);
    sig1 = gsw_sigma1(SA,CT);
    rho = gsw_rho(SA,CT,Z);

    % Isolate downcast
    dz = gradient(Z);
    I = find(dz>0.5*mean(abs(dz)));    
    rhoSort = sort(rho(I));
    N2 = g./nanmean(rho(I)).*gradient(rhoSort)./gradient(Z(I));
    
    Z = Z(I);
    O2 = O2(I);
    sig1 = sig1(I);
    
    % bin profiles to regular zVec
    for j = 1:length(zVec)
        I = find(Z>=zVec(j)-zbin/2 & Z<zVec(j)+zbin/2);
        O2Mat(j,i) = nanmean(O2(I));
        Sig1Mat(j,i) = nanmean(sig1(I));
    end        
end

%% Here by-pass T for sigma_1
TMat = Sig1Mat;

%% Plot each 7 stations transects
% Now we assume that each series of 7 casts is taken at exact
% position.

xlon = lonVec(1:6);
xlat = latVec(1:6);
xVec = nan(size(xlon));
for i = 1:length(xlon)
    xVec(i) = m_lldist([xlon(1) xlon(i)], [xlat(1) xlat(i)]);
end
xVec = xVec - (1.25); % adjust to model grid

% Here adjust the grid
xMooring = m_lldist([xlon(1), -15.7975], [xlat(1), 55.4824]);
yMooring = 919;


%%%%%% ---- plotting ---- %%%%
figure(1)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
load ~/git/matlab/colormaps/oxygen_discrete.mat
colormap(oxygen_discrete)
% $$$ load ~/git/matlab/colormaps/oxygen.mat
% $$$ colormap(oxygen)

letterID = ['a', 'b', 'c', 'd', 'e', 'f'];
Vsig = 31:.02:32.5;
Vo2 = 200:1:240;
x = (x)/1000;
chi = chi/1000;

iCTD = [2 0 4 0 5 0];
iMod = [0 2 0 3 0 5];

for iFrame = 1:6
    s = subplot(3,2,iFrame);

    if mod(iFrame,2) ~=0 % CTD results
        i = iCTD(iFrame);
        I = [1:6]+(i-1)*6;
        
        contourf(xVec, zVec, O2Mat(:,I), Vo2, 'linestyle', 'none')
        ylim(ZLIM)
        hold on
        contour(xVec, zVec, TMat(:,I), Vsig, 'color', 'k');  
        
        for ii = 1:length(xVec)
            plot([xVec(ii) xVec(ii)], ZLIM, '--k')
        end
        
        xpatch = [xdepthVec; xdepthVec(1); xdepthVec(1)];
        ypatch = [ydepthVec; ydepthVec(end); ydepthVec(1)];        
        patch(xpatch, ypatch, [1 1 1]*.6)      
        hold off        
        
        set(gca, 'ydir', 'reverse')
        set(gca, 'tickdir', 'out')
       
    else % model results
        i = iMod(iFrame);
        sig = getfield('sig',frameNos(i));
        sig = putNaN(sig,flag_s);
        sig = sig';
        c1 = getfield('C01',frameNos(i));
        c1 = putNaN(c1,flag_s);
        c1 = c1';    
        u = getfield('u',frameNos(i));
        u = putNaN(u,flag_u);
        u = u';   
        w = getfield('w',frameNos(i));
        w = putNaN(w,flag_s);
        w = w'; 
        
        % reduce the grid
        I = find(x>=XLIM(1) & x<=XLIM(2));
        J = find(z>=ZLIM(1) & z<=ZLIM(2));
        II = find(chi>=XLIM(1) & chi<=XLIM(2));
        JJ = find(zeta>=ZLIM(1) & zeta<=ZLIM(2));
        xVecMod = x(I);
        zVecMod = z(J);
        chiVec = chi(II);
        zetaVec = zeta(JJ);
        c1 = c1(J,II);
        sig = sig(J,II);
        w = w(J,I);
        u = u(J,I);
        
        quiverDecX = 1; %1D
        quiverDecZ = 10;
        quiverDecX = 6; %2D
        quiverDecZ = 20;

        contourf(chiVec, zVecMod, c1, Vo2, 'linestyle', 'none')
        %shading flat
        set(gca, 'ydir', 'reverse')
        hold on
        contour(chiVec, zVecMod, sig, Vsig, 'color', 'k')
        patch(xpatch, ypatch, [1 1 1]*.6)   
        
        Z = zVecMod(1:quiverDecZ:end);
        X = xVecMod(1:quiverDecX:end);
        U = u(1:quiverDecZ:end, 1:quiverDecX:end);
        W = -w(1:quiverDecZ:end, 1:quiverDecX:end);
        
        [kmax imax] = size(U);
        vecColor= 'm';
        unit = false;
        
        
% $$$         %% Only horiz vector above sill:
% $$$         scale = .22;
% $$$         [Y,I] = min(abs(X-1.25));        
% $$$         for k = 1:kmax-1
% $$$             arrow7(X(I),Z(k),U(k,I),W(k,I)*0,scale,vecColor,unit);
% $$$         end
        
        %% 2D vector field
        scale = .7;
        %scale = .5;
        for i = 1:imax
            for k = 1:kmax-1
                arrow7(X(i),Z(k),U(k,i),W(k,i),scale,vecColor,unit);
            end
        end 
  
        set(gca, 'yticklabel', [])
    end

    caxis([205 235])
    xlim(XLIM)
    ylim(ZLIM)
    
    if iFrame < 5
        set(gca, 'xticklabel', [])
    end
    if iFrame == 6
        cb = colorbar;
        adjust_space
        cbPos = get(cb, 'pos');
        figPos = get(gca, 'pos');
        cbPos(1) = figPos(1)+figPos(3)+.01;
        cbPos(2) = cbPos(2)+.01;
        cbPos(4) = cbPos(4)-2*.01;
        cbPos(3) = cbPos(3)*.7;
        set(cb, 'pos', cbPos)
        ti = ylabel(cb,'[O_2] (\mumol/kg)', 'FontSize', 10, 'fontweight', 'bold');
        tiPos = get(ti, 'pos');
        set(ti, 'rotation', 90)
        %        tiPos = [-1 -4.2 1];
        set(ti, 'pos', tiPos)
    else
        adjust_space
    end
    
    if iFrame == 5 | iFrame == 6
        xlabel('x (km)', 'fontWeight', 'bold')
    end
    
    if iFrame == 3
        ylabel('z (m)', 'fontWeight', 'bold')
    end
    
    text(-1.25, 950, [letterID(iFrame)], 'fontWeight', 'bold', 'fontSize', 14)        
    set(gca, 'ytick', [300:100:1000])    
    if iFrame == 6
        arrow7(-.75,900,.10,0,scale,vecColor,unit);
        text(-0.3,895,'0.10 ms^{-1}', 'color', 'm', 'fontWeight', 'bold', 'verticalAlignment', 'middle')
        arrow7(-.75,950,.25,0,scale,vecColor,unit);
        text(-0.3,945,'0.25 ms^{-1}', 'color', 'm', 'fontWeight', 'bold', 'verticalAlignment', 'middle')
    end
    if iFrame == 1
        text(mean(XLIM), 250, 'Observations', 'fontWeight', 'bold', 'fontSize', 14, 'horizontalAlignment', 'center')
    end
    if iFrame == 2
        text(mean(XLIM), 250, 'Model', 'fontWeight', 'bold', 'fontSize', 14, 'horizontalAlignment', 'center')
    end   
    if iFrame == 2
        text(-1.45, 375, '\phi_4-\pi/2', 'fontWeight', 'bold', 'fontSize', 14,'BackgroundColor',[1 1 1])
    end      
    if iFrame == 4
        text(-1.45, 375, '\phi_4', 'fontWeight', 'bold', 'fontSize', 14,'BackgroundColor',[1 1 1])
    end    
    if iFrame == 6
        text(-1.45, 375, '\phi_4+\pi/2', 'fontWeight', 'bold', 'fontSize', 14,'BackgroundColor',[1 1 1])
    end 
    
      %% xtra axes - phase
    dphi = pi/20;
    xx = -pi:dphi:pi;
    yy = sin(xx);
    transectDur = [3.33 3.33 3.33];
    transectDurPts = round(transectDur/24*2*pi/dphi); if iFrame == 1
        a2 = axes('position',[0.2525 0.709 0.08 0.04]) ; % inset
        plot(xx,yy, 'k')
        hold on
        [Y, I] = min(abs(xx+pi/2));
        plot(xx(I-3:I+3), yy(I-3:I+3), 'k', 'lineWidth', 2)
        plot(xx(I), yy(I), '.r', 'markerSize', 10)
        set(a2, 'xtick', [-pi/2 0 pi/2])
        set(a2, 'ytick', [0])
        set(a2, 'xticklabel', [])
        set(a2, 'tickDir', 'out')
        set(a2, 'ygrid', 'on')
        set(a2, 'xgrid', 'on')
        text(-pi/2, 1, 'u', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(-pi/2, -1.2, '-\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(0, -1.2, '\phi_4', 'verticalAlignment', 'top', 'horizontalAlignment', 'center')
        text(pi/2, -1.2, '\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')
        ylim([-1.1 1.1])
        xlim([-pi pi])        
    end

   if iFrame == 3
        a2 = axes('position',[0.2525 0.402 0.08 0.04]) ; % inset
        plot(xx,yy, 'k')
        hold on
        [Y, I] = min(abs(xx-0));
        plot(xx(I-3:I+3), yy(I-3:I+3), 'k', 'lineWidth', 2)
        plot(xx(I), yy(I), '.r', 'markerSize', 10)
        set(a2, 'xtick', [-pi/2 0 pi/2])
        set(a2, 'ytick', [0])
        set(a2, 'xticklabel', [])
        set(a2, 'ygrid', 'on')
        set(a2, 'xgrid', 'on')
        set(a2, 'tickDir', 'out')
        text(-pi/2, 1, 'u', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(-pi/2, -1.2, '-\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(0, -1.2, '\phi_4', 'verticalAlignment', 'top', 'horizontalAlignment', 'center')
        text(pi/2, -1.2, '\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')
        ylim([-1.1 1.1])
        xlim([-pi pi])
    end    
    
    if iFrame == 5
        a2 = axes('position',[0.2525 0.095 0.08 0.04]) ; % inset
        plot(xx,yy, 'k')
        hold on
        [Y, I] = min(abs(xx-pi/2));
        plot(xx(I-2:I+3), yy(I-2:I+3), 'k', 'lineWidth', 2)
        plot(xx(I), yy(I), '.r', 'markerSize', 10)
        set(a2, 'xtick', [-pi/2 0 pi/2])
        set(a2, 'ytick', 0)
        set(a2, 'xticklabel', [])
        set(a2, 'ygrid', 'on')
        set(a2, 'xgrid', 'on')
        set(a2, 'tickDir', 'out')
        text(-pi/2, 1, 'u', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(-pi/2, -1.2, '-\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'right')
        text(0, -1.2, '\phi_4', 'verticalAlignment', 'top', 'horizontalAlignment', 'center')
        text(pi/2, -1.2, '\pi/2', 'verticalAlignment', 'top', 'horizontalAlignment', 'left')
        ylim([-1.1 1.1])
        xlim([-pi pi])    
    end
    
    
end

outName = sprintf('transectCompa2_cycle%0.2d.eps', cycle);
set(gcf, 'renderer', 'painters')
print('-depsc2', outName)  
