clear all
close all
load('Rockall_2minAve.mat')


[timeVec, KE, KE1, KE2] = hst_KEtimeserie('Rockall_2minAve.mat', 'temp_rho_relFile_3rd.mat', 'roc12.mat');



figure(1)
clf
plot(timeVec, KE, 'k', 'linewidth', 2)
hold on
% $$$ plot(timeVec, KE1, 'r')
% $$$ plot(timeVec, KE2, 'm')
datetick('x', 7)
YLIM = get(gca, 'ylim');
xlim([min(timeVec) max(timeVec)])


%% identify high tide
% 1) with velocity
t0 = datenum(2012, 10, 7, 19, 31, 0);
time2 = time2maxV('roc12.mat',timeVec); 
HT = [];
for i = 2:length(timeVec)-1
    if abs(time2(i))<=abs(time2(i-1)) & abs(time2(i))<abs(time2(i+1));
        HT = [HT timeVec(i)];
    end
end


% 2) with temperature
TbotVec = nanmean(Tbin(end-5:end,:));
dt = diff(abs(timeVec(1:2))); %day
fs = 1/dt; % cpd
freq_low = 6; %cpd
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
Tfilt = filtfilt(b, a, TbotVec);
dT = gradient(Tfilt,1,1);
I = find(dT>-9.6398e-3);
dT(I) = 0;
HT2 = [];
for i = 2:length(timeVec)-1
    if dT(i)<dT(i-1) & dT(i)<dT(i+1)
        HT2 = [HT2 timeVec(i)];
    end
end


% 3) manually (t0 + 2xM2 period)
for i = 1:length(HT)
    plot([HT(i) HT(i)], YLIM, '--k')
    plot([HT2(i) HT2(i)], YLIM, '--m')
    plot([t0 t0]+(i-1)*24.84/24, YLIM, '--g')
    if i==1
        legend('KE','vel timing', 'temp timing', '2xM_2 timing', 'location', 'northWest')
        %legend('KE', 'KE_{top}', 'KE_{bot}', 'vel timing', 'temp timing', '2xM_2 timing', 'location', 'northWest')
    end
end


%% Spectra
figure(2)
clf
fs = 1/dt;
freq_low = 0.1;
Wn_low = freq_low/(fs/2);
[b,a] = butter(4, Wn_low);
nx = max(size(TbotVec)); % Hanning
na = 1;
w = hanning(floor(nx/na));

[ps, f] = pwelch(TbotVec, w, 0, [], fs); 
loglog(f, ps, 'k')
hold on
%plot([1/(23.934/24) 1/(23.934/24)],[1e-10 1e5], '--k')
%plot([1/(25.819/24) 1/(25.819/24)],[1e-10 1e5], '--m')
plot([1/(24.84/24) 1/(24.84/24)],[1e-10 1e5], '--r')
plot([1/(12.42/24) 1/(12.42/24)],[1e-10 1e5], '--r')




keyboard