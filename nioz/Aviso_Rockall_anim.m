clear all
close all

tide = load('~/research/CimaStuff/tide/tide_Oct_2012.mat');


mooringLat = 55.4824;
mooringLon = -15.7975;

lonVec = tide.lon_range;
latVec = tide.lat_range;
timeVec = tide.time;
origin = datenum(1950, 1, 1);
timeVec = origin + timeVec;

% $$$ n = datenum(2012,0,0) - datenum(0062,0,0);
% $$$ timeVec = timeVec+n;


[Y, I] = min(abs(mooringLon-lonVec));
[Y, J] = min(abs(mooringLat-latVec));
Imooring = I;
Jmooring = J;

etaVec = squeeze(tide.slev(J,I,:));
uVec = squeeze(tide.uu(J,I,:));
vVec = squeeze(tide.vv(J,I,:));


[U,V] = rotate_vecd(uVec,vVec,-30);

I = find(timeVec>=datenum(2012, 10, 1) & timeVec<=datenum(2012, 10, 30));

figure(2)
clf
% $$$ plot(timeVec(I), uVec(I))
% $$$ hold on
% $$$ plot(timeVec(I), vVec(I), 'r')
% $$$ plot(timeVec(I), U(I), 'm', 'linewidth', 2)

plot(timeVec(I), V(I), 'k', 'linewidth', 2)
hold on
t0 = datenum(2012, 10, 7, 19, 31, 0);
% $$$ for i = 1:20
% $$$     plot([t0 t0]+(i-1)*24.84/24, YLIM, '--k')
% $$$     plot([t0 t0]+(i-1)*24/24, YLIM, '--r')
% $$$ end
xlim([timeVec(I(2)) timeVec(I(end))])
datetick('x', 7)
xlim([timeVec(I(2)) timeVec(I(end))])
set(gca, 'xgrid', 'on')


% plot max vel
time2 = time2maxV('~/research/NIOZ/RockallBank/roc12.mat',timeVec); 
for i = 2:length(time2)-1
    if abs(time2(i)) < abs(time2(i-1)) & abs(time2(i)) < abs(time2(i+1))
        plot(timeVec(i), V(i), 'mp', 'markerFaceColor', 'm', 'markerSize', 10)
        %        plot([timeVec(i) timeVec(i)], [-10 15], '--m', 'linewidth', 2)
    end
end



%% Spectra
figure(3)
clf
dt = diff(abs(timeVec(1:2))); %day
fs = 1/dt;
freq_low = 0.1;
Wn_low = freq_low/(fs/2);
%[b,a] = butter(4, Wn_low);
nx = max(size(V(I))); % Hanning
na = 1;
w = hanning(floor(nx/na));

[ps, f] = pwelch(V(I), w, 0, [], fs); 
loglog(f, ps)
hold on
plot([1/(23.934/24) 1/(23.934/24)],[1e-10 1e5], '--k')
plot([1/(25.819/24) 1/(25.819/24)],[1e-10 1e5], '--k')
%plot([1/(24.84/24) 1/(24.84/24)],[1e-10 1e5], '--r')
plot([1/(12.42/24) 1/(12.42/24)],[1e-10 1e5], '--k')
plot([1/(12.42/2/24) 1/(12.42/2/24)],[1e-10 1e5], '--k')
plot([1/(12.42*2/3/24) 1/(12.42*2/3/24)],[1e-10 1e5], '--k')
plot([1/(12.42/3/24) 1/(12.42/3/24)],[1e-10 1e5], '--k')
plot([1/(12.42/4/24) 1/(12.42/4/24)],[1e-10 1e5], '--k')



%% bathymetry
%% Get global bathym    
path = ['~/data/matlab_bathym/rockall_barotide.mat']; % 30-sec.
lims = [min(latVec) max(latVec) min(lonVec) max(lonVec)];

fid = fopen(path);
if fid == -1 % doesnt exist yet!
    disp('30-sec. region doesn''t exist yet!')
    disp(' Extracting from GEBCO, may take some time...')
    [lat lon z] = getGebco('~/data/GEBCO/GEBCO_08.nc', 'z', lims);
    disp('   -> done!')
    save(path, 'lat', 'lon', 'z');
else
    load(path)
end
latitude=lat;
longitude=lon;
bathy=z;

%% plot
FS = 10;
for i = 1:length(timeVec)
    figure(1)
    clf
    %    set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 10 10])
    m_proj('mercator','long',[min(lonVec) max(lonVec)],'lat',[min(latVec) max(latVec)]);
    m_grid('box','fancy')

    
    %% velocity calculation
    etaMat = squeeze(tide.slev(:,:,i));
    vMat = squeeze(tide.vv(:,:,i));
    uMat = squeeze(tide.uu(:,:,i));
    anom = nan(size(etaMat));
    uanom = nan(size(etaMat));
    vanom = nan(size(etaMat));

    for ipix = 9:size(etaMat,1)-9
        for jpix = 9:size(etaMat,2)-9
            anom(ipix,jpix) = etaMat(ipix,jpix) - nanmean(nanmean(etaMat(ipix-8:ipix+8, jpix-8:jpix+8)));
            uanom(ipix,jpix) = uMat(ipix,jpix) - nanmean(nanmean(uMat(ipix-8:ipix+8, jpix-8:jpix+8)));    
            vanom(ipix,jpix) = vMat(ipix,jpix) - nanmean(nanmean(vMat(ipix-8:ipix+8, jpix-8:jpix+8)));
        end
    end
    
    anom = anom - nanmean(anom(:));
    
    load BR_white % <---- for velocities
    colormap(BR_white);
    
    m_contourf(lonVec,latVec,anom, 50, 'lineStyle', 'none');
    hold on
    [HH, HH] = m_contour(longitude,latitude,abs(bathy), [0:500:2500], 'color', 'k');
    
    %% Current arrows
    quiverDec = 8;
    Z = latVec(1:quiverDec:end);
    X = lonVec(1:quiverDec:end);
    U = uMat(1:quiverDec:end, 1:quiverDec:end);
    V = vMat(1:quiverDec:end, 1:quiverDec:end);
    [XX, ZZ] = meshgrid(X, Z);
    
    [kmax imax] = size(U);
    scale = .7;
    
    %    m_arrow(lonVec(Imooring),latVec(Jmooring),uMat(Jmooring,Imooring),vMat(Jmooring,Imooring),scale);
% $$$ 
% $$$     scale = (U.^2 + V.^2) ./ max(U(:).^3 + V(:).^2)*10;
% $$$    
% $$$     for ii = 1:length(X)
% $$$         for jj = 1:length(Z)
% $$$             m_arrow(X(ii),Z(jj),U(jj,ii),V(jj,ii),scale(jj,ii));
% $$$         end
% $$$     end

    m_quiver(XX,ZZ,U,V, 'color', 'k')
    
    %% Custom figure
    m_gshhs_l('patch',[1 .9333 .6667]); %coastlines (Beige)                               
    [HH, HH] = m_contour(longitude,latitude,abs(bathy), [0:500:4000], 'color', 'k');
    xlabel('Longitude', 'FontSize', FS, 'fontweight', 'bold')
    ylabel('Latitude', 'FontSize', FS, 'fontweight', 'bold')
    caxis([-3 3])
    c = colorbar;
    ti = ylabel(c,'\eta'' (cm)', 'FontSize', FS, 'fontweight', 'bold','color',[1 1 1]*0);
    m_grid('box','fancy')
    title(sprintf(datestr(timeVec(i), 0)));
    outfile = sprintf('window%04d.png',i);
    disp(outfile)
    print('-dpng', '-r200', outfile)
 
end


