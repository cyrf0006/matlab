%% Plot N2
figure(2)
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 17 20])
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 2; % no. subplot column
nrow = 3; % no. subplot row
dx = 0.04; % horiz. space between subplots
dy = 0.02; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.12; % very right of figure
tops = 0.03; % top of figure
bots = 0.07; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %
for i = 1:6
    s = subplot(3,2,i);
    
    I = [1:7]+(i-1)*6;


    if griddataItp
        [XI, ZI] = meshgrid(xVec, zVec);
        A = TrMat(:,I);
        F = TriScatteredInterp(XI(:), ZI(:), A(:));
        myMat = F(XI, ZI);
    else
        myMat = TrMat(:,I);
    end
    
    if mod(i,2) ~= 0
        pcolor(xVec, zVec, TrMat(:,I)); shading interp
        ylim([yMin zMax])
        hold on
        contour(xVec, zVec, TMat(:,I), 30, 'color', 'k');        
    else
        pcolor(xVec, zVec, fliplr(TrMat(:,I))); shading interp
        ylim([yMin zMax])
        hold on
        contour(xVec, zVec, fliplr(TMat(:,I)), 30, 'color', 'k');
        set(gca, 'yticklabel', [])
        % set(gca, 'xdir', 'reverse')
    end
    
    xpatch = [xdepthVec; xdepthVec(end); xdepthVec(1); xdepthVec(1)];
    ypatch = [ydepthVec; zMax; zMax; ydepthVec(1)];
    patch(xpatch, ypatch, [1 1 1]*.6)      
    hold off
    
    % put mooring position
    line(xMooring, yMooring,'marker','p','color',[0 0 0], 'markerfacecolor', 'r','markersize',10)
    
    set(gca, 'ydir', 'reverse')
    set(gca, 'tickdir', 'out')
    caxis([0 .4])
    
    drawnow
    if i < 5
        set(gca, 'xticklabel', [])
    end

    if i == 6
        cb = colorbar;
        adjust_space
        cbPos = get(cb, 'pos');
        figPos = get(gca, 'pos');
        cbPos(1) = figPos(1)+figPos(3)+.02;
        cbPos(2) = cbPos(2)+.01;
        cbPos(4) = cbPos(4)-2*.02;
        cbPos(3) = cbPos(3)*.9;
        set(cb, 'pos', cbPos)
        ti = ylabel(cb,'Tr(%)', 'FontSize', 10, 'fontweight', 'bold');
        tiPos = get(ti, 'pos');
        set(ti, 'rotation', 90)
        %        tiPos = [-1 -4.2 1];
        set(ti, 'pos', tiPos)
    else
        adjust_space
    end
    
    if i == 5
        xlabel('x (km)')
    end
    
    if i == 3
        ylabel('Depth (m)')
    end
    
    if i == 1
        text(.25, 900, 'Logachev mound', 'fontWeight', 'bold')        
    end
    
end
 

set(gcf, 'renderer', 'painters')
print('-depsc2', './ctd_transectTr.eps')  

