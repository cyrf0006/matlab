function fr_windTimeSerie(windFile)
    
    

data = load(windFile);

noCol = 7;
noRow = length(data)/noCol;

if mod(noRow, 1) ~= 0
    disp('problem with file size!? [K]')
    keyboard
end


data = reshape(data, noCol, noRow);
data = data'; % now data should be [y d U V vel dir_tri dir_geo];


timeVec = datenum(1985,1,1):datenum(2014,12,31);
% remove Bissextile years
A = datestr(timeVec,6);
I = [];
for icount = 1:length(timeVec)
    if strcmp(A(icount,:), '02/29');
        I = [I icount];
    end
end
timeVec(I) = [];


U = data(:,3);
V = data(:,4);
UU = data(:,5);
AA = data(:,6);


Ufilt = runmean(U, 30); % 30-day moving ave
Vfilt = runmean(V, 30); % 30-day moving ave
UUfilt = runmean(UU, 30); % 30-day moving ave
AAfilt = runmean(AA, 30);


figure(1)
clf
subplot(311)
plot(timeVec, Ufilt, 'g', 'linewidth',2)
hold on
plot(timeVec, Vfilt, 'r', 'linewidth',2)
ylabel('vel (m/s)')
datetick

subplot(312)
plot(timeVec, UU)
hold on
plot(timeVec, UUfilt, 'r', 'linewidth',2)
ylabel('vel (m/s)')
datetick

subplot(313)
plot(timeVec, AA)
hold on
plot(timeVec, AAfilt, 'r', 'linewidth',2)
ylabel('ang (deg)')
datetick





