function fr_frontSurfaceTimeserie(probList, LatLon, region)

% function fr_mean_prob(sstList, LatLon, years, months, destination, varargin)
%
% Usage ex:
%   fr_frontSurfaceTimeserie('prob_atlantic_04.list', '~/data/front_data/AtlanticLatLon.mat', 'JacquesCartier')
%   fr_frontSurfaceTimeserie('prob_atlantic_monthly.list', '~/data/front_data/AtlanticLatLon.mat', 'JacquesCartier')
%
% !ls /media/Seagate1TB/Front_data/SST_atlantic/*.mat > SST_atlantic
%
% Outfile name will be with varargin extension, in local folder. complete this..
% Shoul be run in ~IML/Fronts/matlab_workspace/probability/ and
% then mv into OUTPUT


% Deal with "region" input
if strcmp(region, 'BelleIsle')==1 
    corners = [-60 -54 50.5 52];
elseif strcmp(region, 'JacquesCartier')==1 
    corners = [-65 -62 49.5 50.5];
elseif strcmp(region, 'SteAnne')==1 
    corners = [-67.5 -66.5 49 49.5];
elseif strcmp(region, 'GaspeCurrent')==1 
    corners = [-66.5 -63.3 48.9 49.5]; 
elseif strcmp(region, 'BaieChaleur')==1 
    corners = [-67 -64.5 47.5 48.5]; 
else
    disp('Region unkwnown, try to parametrize it')
    return
end  


fid = fopen(probList);
C = textscan(fid, '%s', 'delimiter', '\n');
probFiles = char(C{1});
nFiles = size(probFiles, 1); %number of files 

% Latitude longitude
LatLon = load(LatLon);
lat = LatLon.lat;
lon = LatLon.lon;
lonVec = lon(1,:);
latVec = lat(:,1);
Ilon = find(lonVec>=corners(1) & lonVec<=corners(2));
Ilat = find(latVec>=corners(3) & latVec<=corners(4));
latVec = latVec(Ilat);
lonVec = lonVec(Ilon);

surfVec = nan(nFiles, 1);

% Buid the cube
disp('Build the cube')
for i = 1:size(probFiles, 1) 
    disp(sprintf('File %d of %d', i, nFiles))
    data=load(probFiles(i,:));  
    mat = data.probability(Ilat, Ilon);
    surfVec(i) = nansum(mat(:));
end
disp('  done!')

timeVec = [];
for yyyy = 1986:2010
    for mm = 1:12
        timeVec = [timeVec datenum(yyyy, mm, 15)];
    end
end

I = find(str2num(datestr(timeVec, 5))>4 & str2num(datestr(timeVec, 5))<11);
plot(timeVec(I), surfVec(I), '.-k')
datetick
ylabel('Arbitrary Scale (sum of prob.)')

keyboard

% compute slope
disp('Compute slope')
slopeMat = nan(size(cube,1), size(cube,2));
years = [1:size(cube,3)]';
for i = 1:size(cube,1)
    for j = 1:size(cube,2)
        probVec = squeeze(cube(i,j,:));
        I = find(~isnan(probVec));
        if sum(probVec(I))~=0
            p = polyfit(years(I), probVec(I), 1);
            slopeMat(i,j) = p(1);
        end
    end
end
disp('  done!')


save(outFile, 'latVec', 'lonVec', 'slopeMat')


