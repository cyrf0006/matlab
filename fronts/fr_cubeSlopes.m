function fr_cubeSlopes(probList, LatLon, corners, outFile)

% function fr_mean_prob(sstList, LatLon, years, months, destination, varargin)
%
% Usage ex:
%   fr_cubeSlopes('prob_atlantic_08.list', '~/data/front_data/AtlanticLatLon.mat', [-70 -54 45 52], 'slopeMat08.mat')
% !ls /media/Seagate1TB/Front_data/SST_atlantic/*.mat > SST_atlantic
%
% Outfile name will be with varargin extension, in local folder. complete this..
% Shoul be run in ~IML/Fronts/matlab_workspace/probability/ and
% then mv into OUTPUT

fid = fopen(probList);
C = textscan(fid, '%s', 'delimiter', '\n');
probFiles = char(C{1});
nFiles = size(probFiles, 1); %number of files 

% Latitude longitude
LatLon = load(LatLon);
lat = LatLon.lat;
lon = LatLon.lon;
lonVec = lon(1,:);
latVec = lat(:,1);
Ilon = find(lonVec>=corners(1) & lonVec<=corners(2));
Ilat = find(latVec>=corners(3) & latVec<=corners(4));
latVec = latVec(Ilat);
lonVec = lonVec(Ilon);

cube = nan(length(Ilat), length(Ilon), nFiles);

% Buid the cube
disp('Build the cube')
for i = 1:size(probFiles, 1) 
    disp(sprintf('File %d of %d', i, nFiles))
    data=load(probFiles(i,:));   
    cube(:,:,i) = data.probability(Ilat, Ilon);
end
disp('  done!')


% compute slope
disp('Compute slope')
slopeMat = nan(size(cube,1), size(cube,2));
years = [1:size(cube,3)]';
for i = 1:size(cube,1)
    for j = 1:size(cube,2)
        probVec = squeeze(cube(i,j,:));
        I = find(~isnan(probVec));
        if sum(probVec(I))~=0
            p = polyfit(years(I), probVec(I), 1);
            slopeMat(i,j) = p(1);
        end
    end
end
disp('  done!')


save(outFile, 'latVec', 'lonVec', 'slopeMat')


