function fr_eof(probList, LatLon, corners, outFile)

% function fr_mean_prob(sstList, LatLon, years, months, destination, varargin)
%
% Usage ex:
%   fr_eof('monthly_frontalProbs.list', '~/data/front_data/AtlanticLatLon.mat', [-70 -54 45 52], 'eof.mat')
% !ls /media/Seagate1TB/Front_data/SST_atlantic/*.mat > SST_atlantic
%
% Outfile name will be with varargin extension, in local folder. complete this..
% Shoul be run in ~IML/Fronts/matlab_workspace/probability/ and
% then mv into OUTPUT

fid = fopen(probList);
C = textscan(fid, '%s', 'delimiter', '\n');
probFiles = char(C{1});
nFiles = size(probFiles, 1); %number of files 

% Latitude longitude
LatLon = load(LatLon);
lat = LatLon.lat;
lon = LatLon.lon;
lonVec = lon(1,:);
latVec = lat(:,1);
Ilon = find(lonVec>=corners(1) & lonVec<=corners(2));
Ilat = find(latVec>=corners(3) & latVec<=corners(4));
latVec = latVec(Ilat);
lonVec = lonVec(Ilon);

cube = nan(nFiles, length(Ilat), length(Ilon));

% Buid the cube
disp('Build the cube')
for i = 1:size(probFiles, 1) 
    disp(sprintf('File %d of %d', i, nFiles))
    data=load(probFiles(i,:));   
    cube(i, :,:) = data.probability(Ilat, Ilon);
end
disp('  done!')

keyboard


disp('Compute EFOs...')

N = 5;
G = map2mat(ones(size(cube,2),size(cube,3)),cube);
for method = 1 : 4
  [E,pc,expvar] = caleof(G,N,method);  
  eof = mat2map(ones(size(cube,2),size(cube,3)),E);
  

figure;iw=1;jw=N+1;
%set(gcf,'MenuBar','none');
%posi = [576 0 710 205];
%set(gcf,'position',[posi(1) (method-1)*250+50 posi(3) posi(4)]);

for i=1:iw*jw
  if i<= iw*jw-1
    
  C = squeeze(eof(i,:,:));
  subplot(iw,jw,i);
  cont = 12;
  [cs,h] = contourf(X,Y,C,cont);
  clabel(cs,h); 
  title(strcat('EOF:',num2str(i),'/',num2str(expvar(i)),'%'));
  axis square;
  %caxis([cont(1) cont(end)]);
  
  else
  subplot(iw,jw,iw*jw);
  plot(pc');
  grid on
  xlabel('time')
  title('PC')
  legend(num2str([1:N]'),2);
  box on
  

  end %if
  
end %for i
suptitle(strcat('METHOD:',num2str(method)));

end %for method
% $$$ 
% $$$ 
% $$$ [eofs,pc,expvar] = caleof(cube,5,1);
disp('  done!')



keyboard

save(outFile, 'latVec', 'lonVec', 'slopeMat')


