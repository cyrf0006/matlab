function fr_slope_map(slopeData, region, varargin)

%usage ex:    
% fr_slope_map('slopeMat08.mat', 'GSL', [45 52 -70 -55])

% !!!! IMPORTANT: if you want to use saved display value for a certain
% region, you must only provide 1 varargin argument (otherwise
% display info will be overwritten) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
% *********************** Adjust_space.m ************************ %
% Fields required by the function adjust_space.m. Please fill every
% of the following and call "adjust_space" in the script whenever
% you want. Do not touch four last fields
ncol = 1; % no. subplot column
nrow = 1; % no. subplot row
dx = 0.03 ; % horiz. space between subplots
dy = 0.04; % vert. space between subplots
lefs = 0.1; % very left of figure
rigs = 0.05; % very right of figure
tops = 0.05; % top of figure
bots = 0.05; % bottom of figure
figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
count_col = 1;
count_row = 1;
% *************************************************************** %


%     Preamble, check input and deal with varargin    
path = ['~/data/matlab_bathym/' region '.mat'];

fid = fopen(path);
if fid == -1 % doesnt exist yet!
    disp('Region doesn''t exist yet!')
    if isempty(varargin) == 1 
        disp([' -> [ERROR!] No coordinates provided. Please check help menu'])
        return
    else
        if length(varargin{1}) == 4
            disp(' -> Extract bathym from Gebco 30'''', may take some time...')
            lims = varargin{1};       
            [lat lon z] = getGebco('~/data/GEBCO/GEBCO_08.nc', 'z', lims);
            disp('   done!')
            save(path, 'lat', 'lon', 'z');
        else
            disp(' -> [ERROR!] Wrong input, please check help menu')
            return
        end
    end
else
    load(path)
end



% Check for figure dimensions, colorbar position, etc.
if size(varargin,2) == 2 
    if strcmp(varargin{2}, 'North') == 1
        paperwidth = 23;%cm
        paperheight = 18;%cm
        cbar_width = 0.02;
        cbar_offset = 0.1; % colorbar offset from figure
        offset2 = 0.1; % offset between heigth of colorbar 
        tipos1 = 0.15;
        lefs = 0.1; % very left of figure
        rigs = 0.05; % very right of figure
        tops = 0.05; % top of figure
        bots = 0.05; % bottom of figure
        figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
        figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
        cbar_loc = 'NorthOutside';
        north = 1;
    else        
        % (East)
        paperwidth = 15;%cm
        paperheight = 25;%cm
        cbar_width = 0.02;
        cbar_offset = 0.12; % colorbar offset from figure
        offset2 = 0.1; % offset between heigth of colorbar 
        tipos1 = 0.15;
        cbar_loc = 'EastOutside';
        lefs = 0.05; % very left of figure
        rigs = 0.12; % very right of figure
        tops = 0.05; % top of figure
        bots = 0.1; % bottom of figure
        figw = (1-(lefs+rigs+(ncol-1)*dx))/ncol;
        figh = (1-(tops+bots+(nrow-1)*dy))/nrow;
        north = 0;
    end    
else
    % If second varargin not provided, we assume these parameters
    % exists
    path_display = ['~/data/matlab_bathym/' region '_display.mat'];
    load(path_display)
end

path_display = ['~/data/matlab_bathym/' region '_display.mat'];
load(path_display)


%load('~/data/matlab_bathym/GebcoColormapFred.mat')
load BR_symetric.mat
load BR_white.mat


% data reduction
slope = load(slopeData);
frontlat = slope.latVec;
frontlon = slope.lonVec;
slopeMat = slope.slopeMat;
clear slope


h = figure('Visible', 'off');
clf
set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 paperwidth paperheight])
%set(gcf, 'renderer', 'opengl')
load /home/cyrf0006/research/fronts/matlab_workspace/GS-SB_position.mat
m_proj('mercator','long',[min(lon) max(lon)],'lat',[min(lat) max(lat)]);
m_grid('box','fancy')
hold on
%  WATCH OUT! make sure m_gshhs_h is  'high res' for final figure
m_pcolor(frontlon,frontlat,slopeMat); shading flat;
colormap(BR_white)
m_gshhs_h('patch',[1 .9333 .6667]); %coastlines (Beige)                               

plot_border('~/research/fronts/borders/NorthAmerica.dat',[min(lon) max(lon)], [min(lat) max(lat)]);



% ---------- Xtra features on different plots ------------- %
%      (see also external function fr_putplaces.m)
if strcmp(region, 'HudsonSt') == 1
    load ~/research/fronts/matlab_workspace/taggart_line.txt;
    taggart_line(end,:) = [];
    m_plot([taggart_line(:,1)], [taggart_line(:,2)], 'r', 'linewidth', 2);
    [HH, HH] = m_contour(lon,lat,z, [-50 -200 -1000], 'color', 'k');
    fr_putPlaces('HUDS')
end

if strcmp(region, 'SouthLabrador') == 1
    [HH, HH] = m_contour(lon,lat,z, [-50 -200 -1000], 'color', 'k');
    fr_putPlaces('LABS')
end

if  strcmp(region, 'NSshelf') == 1
    [HH, HH] = m_contour(lon,lat,z, [-50 -100 -200 -1000], 'color', 'k');
    fr_putPlaces('SCOS')
end

if strcmp(region, 'NFDLinner') == 1 | strcmp(region, 'NFDLshelf') == 1
    [HH, HH] = m_contour(lon,lat,z, [-100 -200 -1000], 'color', 'k');
    fr_putPlaces('NFDL')
end

if strcmp(region, 'Baffin') == 1
    [H3, H3] = m_contour(lon,lat,z, [-500 -1000], 'color', 'k');
    load ~/research/fronts/matlab_workspace/lobb2003.mat
    fr_putPlaces('BAFF')
    m_line(-69.3457, 77.4830, 'marker','.','MarkerFaceColor', ...
           'k','markersize',14,'color',[.6 0 0]);
end

if strcmp(region, 'GSL') == 1
    [HH, HH] = m_contour(lon,lat,z, [-50 -300], 'color', 'k');
    fr_putPlaces('GULF2')
    m_line(-69.716606, 48.156604,'marker','.','MarkerFaceColor', ...
           'k','markersize',14,'color',[1 1 1]);
    m_line(-67.386367, 49.35, 'marker','.','MarkerFaceColor', ...
           'k','markersize',14,'color',[1 1 1]);
end

if strcmp(region, 'Pacific') == 1
    [H3, H3] = m_contour(lon,lat,z, [-1000 -1000], 'color', 'k');
    fr_putPlaces('PACI')
end

if strcmp(region, 'Atlantic') == 1
    [H3, H3] = m_contour(lon,lat,z, [-1000 -1000], 'color', 'k');
end

if strcmp(region, 'Hudson') == 1 
    [H3, H3] = m_contour(lon,lat,z, [-100 -100], 'color', 'k');
    HudsonRiver = load('~/research/fronts/rivers/rivers_clim_lat_lon');
    fr_putPlaces('HUDB')
end

if   strcmp(region, 'Belcher') == 1
    [H3, H3] = m_contour(lon,lat,z, [-25 -50 -100], 'color', 'k');
end

if   strcmp(region, 'HudsonWest') == 1
    [H3, H3] = m_contour(lon,lat,z, [-25 -50 -100], 'color', 'k');
    fr_putPlaces('WHUD')
end

if   strcmp(region, 'Foxe') == 1
    [H3, H3] = m_contour(lon,lat,z, [-25 -50 -100], 'color', 'k');
    fr_putPlaces('FOXE')
end

if   strcmp(region, 'Vancouver') == 1
    [H3, H3] = m_contour(lon,lat,z, [-100 -1000], 'color', 'k');
    fr_putPlaces('VANC')
end

if strcmp(region, 'BeringSea') == 1
    [H3, H3] = m_contour(lon,lat,z, [-50 -100 -1000], 'color', 'k');
    m_line([-172 -167.5], [54 55], 'color', 'k','linewidth', 2);
    m_line([-173 -166], [56 56.5], 'color', 'k','linewidth', 2);
    m_line([-175 -167], [57.5 58.5], 'color', 'k','linewidth', 2);
    m_text(-172, 54, 'Shelf Break ', 'horizontalAlignment', 'right', 'fontWeight', 'bold')
    m_text(-173, 56, 'Middle ', 'horizontalAlignment', 'right', 'fontWeight', 'bold')
    m_text(-175, 57.5, 'Inner ', 'horizontalAlignment', 'right', 'fontWeight', 'bold')
end

if strcmp(region, 'GulfAlaska') == 1 
    [H3, H3] = m_contour(lon,lat,z, [-100 -1000], 'color', 'k');
    fr_putPlaces('GALA')
end

if  strcmp(region, 'WestCoast') == 1
    [H3, H3] = m_contour(lon,lat,z, [-100 -1000], 'color', 'k');
end
% --------------------------------------------------------- %
hold off

xlabel('Longitude', 'FontSize', 10)
ylabel('Latitude', 'FontSize', 10)
set(gca, 'fontsize', 10)

adjust_space

caxis([-.4 .4])
%caxis([.5 1.5])

m_grid('box','fancy')

if north
    c = colorbar(cbar_loc);
    Pos = get(gca, 'position');
    set(c, 'FontSize', 10, 'position', [Pos(1)+offset2 Pos(2)+Pos(4)+cbar_offset Pos(3)-2*offset2 cbar_width]);
    ti = ylabel(c,'s(% y^{-1})', 'FontSize', 10);
    ti_pos = get(ti, 'position');
    set(ti, 'Rotation',0.0);     
    clim = get(gca, 'clim');
    set(ti, 'position', [0  8 ti_pos(3)]); 

else
    c = colorbar(cbar_loc);
    Pos = get(gca, 'position');
    set(c, 'FontSize', 10, 'position', [Pos(1)+Pos(3)+cbar_offset Pos(2)+offset2 cbar_width Pos(4)-2*offset2]);
    ti = ylabel(c,'f(%)', 'FontSize', 10);
    ti_pos = get(ti, 'position');
    set(ti, 'position', [ti_pos(1)-tipos1 ti_pos(2) ti_pos(3)]); 
end
%  WATCH OUT! Here -r100 is quite low resolution, just to speed up
%  treatment. Adjust before submission!
set(gcf, 'renderer', 'painters')
outfile1 = ['slope_' region '.png'];
print(h, '-dpng', '-r300',  outfile1)
% $$$ outfile2 = ['freq_' region '.eps'];
% $$$ print(h, '-depsc2',  outfile2)



% Save information on display
path_display = ['~/data/matlab_bathym/' region '_display.mat'];

save(path_display, 'paperwidth', 'paperheight', 'cbar_width', ...
     'cbar_offset', 'offset2', 'tipos1', 'cbar_loc', 'north', 'lefs', ...
     'rigs', 'tops', 'bots', 'figw', 'figh');


