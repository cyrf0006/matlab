function sx_checkFlight(filename)

% usage ex:
% sx_checkFlight('sea003.299.gli.sub.80')

[meta, data] = sx2mat(filename);

% plot angle
figure
plot(data(:,1)-data(1,1), data(:,5))
ylabel('pitch')
xlabel('Time (seconds in profiles)')
title([filename])

% plot velocity
figure
plot(data(2:end,1)-data(1,1), diff(data(:,7))./diff(data(:,1)))
ylabel('vert. vel.')
xlabel('Time (seconds in profiles)')
title([filename])


% plot Depth
figure
plot(data(:,1)-data(1,1), data(:,7))
ylabel('Depth')
xlabel('Time (seconds in profiles)')
title([filename])

% plot Ballast
figure
plot(data(:,1)-data(1,1), data(:,13))
hold on
plot(data(:,1)-data(1,1), data(:,14), 'r')
legend('Cmd', 'Pos')
ylabel('Ballast')
xlabel('Time (seconds in profiles)')
title([filename])

% plot NavState
figure
plot(data(:,1)-data(1,1), data(:,2))
ylabel('NavState')
xlabel('Time (seconds in profiles)')
title([filename])

% plot NavState
figure
plot(data(:,1)-data(1,1), data(:,3))
ylabel('Alarm')
xlabel('Time (seconds in profiles)')
title([filename])